package jvm.parallel.logging

import org.apache.logging.log4j.LogManager

trait Logger {
  private[this] def getName = getClass.getSimpleName
  val logger = LogManager.getLogger(getName)
}
