package jvm.parallel.parsers.java.helpers

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.expression.types.coupled.ChildOfAny
import jvm.parallel.ast.parsers.expression.types.references.ClassType
import jvm.parallel.ast.parsers.statement.Block
import jvm.parallel.ast.parsers.statement.declarator.{InitializedVariableDeclarator, VariableDeclarator}
import jvm.parallel.ast.parsers.statement.instructions.LocalVariableDeclaration
import jvm.parallel.ast.parsers.statement.instructions.interruptable.{CatchClause, TryWithResources}
import jvm.parallel.ast.parsers.statement.modifers.Modifier
import jvm.parallel.ast.parsers.statement.instructions.switch.{EmptySwitchCases, SwitchRule, SwitchStatement}

protected[parsers] trait JavaStatementHelper extends JavaDeclaratorHelper {
  def composeTryStatement: (Seq[LocalVariableDeclaration], Block, Option[Seq[CatchClause]], Option[Block]) => TryWithResources =
    (rs, tb, cs, fb) =>
      TryWithResources(rs, tb, cs.getOrElse(Vector()), fb)

  def composeOptionalSwitchRule: (Seq[Expression]) => Option[EmptySwitchCases] =
    (es) =>
      if (es.size == 0) None
      else Some(EmptySwitchCases(es))

  def composeSwitchStatement: (Expression, Seq[SwitchRule], Option[SwitchRule]) => SwitchStatement =
    (e, cs1, c2) =>
      if (c2.isDefined) SwitchStatement(e, cs1 :+ c2.get)
      else SwitchStatement(e, cs1)

  def composeCatchTypeBound: (Seq[ClassType]) => Type with Product with Serializable =
    (seq) =>
      if (seq.size == 1) seq.head
      else ChildOfAny(seq)

  def composeVariableDeclaration: (Seq[Annotation], Seq[Modifier], Type, Name) => LocalVariableDeclaration =
    (as, mods, t, n) =>
    LocalVariableDeclaration(as, mods, t, Vector(VariableDeclarator(n)))
  
  def composeInitializedVariableDeclaration: (Seq[Annotation], Seq[Modifier], Type, Name, Expression) => LocalVariableDeclaration =
     (as, mods, t, n, e) =>
      LocalVariableDeclaration(as, mods, t, Vector(InitializedVariableDeclarator(n, e)))

}
