package jvm.parallel.parsers.java.helpers

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.instructions.discardable.dimension.Dimension
import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.expression.types.references.ArrayType
import jvm.parallel.ast.parsers.statement.modifers.Modifier
import jvm.parallel.ast.parsers.statement.parameter.{FormalParameter, Parameter, VariableArityParameter}

protected[parsers] trait JavaParameterHelper extends JavaModifierHelper{
  def composeVarArgs: (Seq[Annotation], Seq[Modifier], Type, Name, Seq[Dimension]) => VariableArityParameter = {
    (as, mods, t, i, ds) =>
      if (ds.size == 0) VariableArityParameter(as, mods, t, i)
      else VariableArityParameter(as, mods, ArrayType(t, ds), i)
  }

  def composeFormalParameter: (Seq[Annotation], Seq[Modifier], Type, Name, Seq[Dimension]) => FormalParameter =
    (as, mods, t, i, ds) =>
      if (ds.size == 0) FormalParameter(as, mods, t, i)
      else FormalParameter(as, mods, ArrayType(t, ds), i)

  def composeParameters: (Parameter, Seq[Parameter], Option[Parameter]) => Seq[Parameter] =
    (p, ps, v) =>
      if (v.isDefined) p +: (ps :+ v.get)
      else p +: ps
}
