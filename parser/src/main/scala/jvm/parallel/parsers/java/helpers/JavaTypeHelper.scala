package jvm.parallel.parsers.java.helpers

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.instructions.discardable.dimension.Dimension
import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.expression.types.coupled.ChildOfAll
import jvm.parallel.ast.parsers.expression.types.references.{ArrayType, ClassType}
import jvm.parallel.ast.parsers.expression.types.templates.TemplateArgument
import jvm.parallel.ast.parsers.statement.modifers.Modifier
import shapeless.HNil

protected[parsers] trait JavaTypeHelper {
  protected[this] implicit def str2Name(str:String): Name = new Name(str)
  protected[this] implicit def strs2Name(strs:Seq[String]): Name = new Name(strs )

  def composeTypeBound: (Type, Seq[ClassType]) => Type = (t, is) => {
    if (is.size == 0) t else ChildOfAll(t +: is)
  }

  def nestClassTypes: (ClassType, Seq[Annotation], Name, Seq[TemplateArgument]) => ClassType =
    (p, as, id, args) => {
      ClassType(as, Some(p), id, args)
    }

  def composeArrayType: (Type, Seq[Dimension]) => Type = (t, ds) => {
    if (ds.size == 0) t else ArrayType(t, ds)
  }

  def toRule: ((Seq[Annotation], Seq[Modifier])) => shapeless.::[Seq[Annotation], shapeless.::[Seq[Modifier], HNil]] =
    t => t._1 :: t._2 :: HNil

}
