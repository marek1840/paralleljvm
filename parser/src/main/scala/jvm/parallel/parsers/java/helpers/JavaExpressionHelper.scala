package jvm.parallel.parsers.java.helpers

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.instructions.discardable.DiscardableExpression
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.{Extraction, MethodInvocation, QualifiedMethodInvocation}
import jvm.parallel.ast.parsers.statement.instructions.discardable.dimension.{AbstractDimension, Dimension, InitializedDimension}
import jvm.parallel.ast.parsers.statement.instructions.discardable.instantiation.{NestedAnonymousObjectInstantiation, NestedObjectInstantiation, ObjectInstantiation}
import jvm.parallel.ast.parsers.statement.instructions.discardable.reference.Select
import jvm.parallel.ast.parsers.expression.operators.BinaryOperator
import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.expression.types.templates.TemplateArgument
import jvm.parallel.ast.parsers.expression.{BinaryOperations, Expression}
import jvm.parallel.ast.parsers.statement.declaration.members.MemberDeclaration

protected[parsers] trait JavaExpressionHelper extends JavaTypeHelper {
  def composeSelect: (String, Seq[String]) => Select = (i, is) => Select(i +: is)

  def composeNestedInstantiation: (Expression, Seq[TemplateArgument], Type, Seq[Expression], Option[Seq[MemberDeclaration]]) => ObjectInstantiation =
    (e, ts, t, args, b) =>
      if (b.isDefined) NestedAnonymousObjectInstantiation(e, ts, t, args, b.get)
      else NestedObjectInstantiation(e, ts, t, args)

  def composeBinaryOperationsArguments:
  ((Vector[BinaryOperator], Vector[Expression]), BinaryOperator, Expression) => (Seq[BinaryOperator], Seq[Expression]) =
    (acc, op, expr) => (acc._1 :+ op, acc._2 :+ expr)

  def composeBinaryOperations: (Seq[BinaryOperator], Seq[Expression]) => Expression =
    (ops, exprs) =>
      if (exprs.size == 1) exprs.head
      else BinaryOperations(ops, exprs)

  def composeInvocation: (Name, Seq[Expression]) => DiscardableExpression =
    (n, as) =>
      if (n.name.size == 1) MethodInvocation(Vector(), n, as)
      else QualifiedMethodInvocation(Select(n.name.dropRight(1)), Vector(), n.name.last, as)

  def composeExtraction: (Name, Expression) => Extraction = (ss, e) => Extraction(Select(ss), e)

  def concatDimensions: (Seq[InitializedDimension], Seq[AbstractDimension]) => Seq[Dimension] = (d1, d2) => d1 ++: d2
}
