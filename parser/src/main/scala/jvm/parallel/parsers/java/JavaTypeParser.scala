package jvm.parallel.parsers.java

import jvm.parallel.ast.parsers.statement.instructions.discardable.dimension.{AbstractDimension, Dimension}
import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.expression.types.primitives._
import jvm.parallel.ast.parsers.expression.types.references.{ArrayType, ClassType, ReferenceType}
import jvm.parallel.ast.parsers.expression.types.templates._
import jvm.parallel.parsers.java.helpers.JavaTypeHelper
import org.parboiled2.{ParserInput, Rule1}

protected[parsers] abstract class JavaTypeParser(override val input: ParserInput) extends JavaLiteralParser(input) with JavaTypeHelper {
  protected def annotations: Rule1[Seq[Annotation]]

  def `type`: Rule1[Type] = rule {
    (primitiveType | classType) ~ optionalDims ~> composeArrayType
  }

  protected def primitiveType: Rule1[PrimitiveType] = rule {
    annotations ~ {
      `byte` ~> BytePrimitive |
        `short` ~> ShortPrimitive |
        `float` ~> FloatPrimitive |
        `int` ~> IntegerPrimitive |
        `long` ~> LongPrimitive |
        `char` ~> CharPrimitive |
        `double` ~> DoublePrimitive |
        `boolean` ~> BooleanPrimitive |
        `void` ~> VoidPrimitive
    }
  }

  protected def referenceType: Rule1[ReferenceType] = rule {
    arrayType | classType
  }

  protected def classType: Rule1[ClassType] = rule {
    annotations ~ push(None) ~ name ~ optionalTypeArguments ~> ClassType ~ nestedClasstype.*
  }

  protected def nestedClasstype = rule{
    dot ~ annotations ~ name ~ optionalTypeArguments ~> nestClassTypes
  }

  protected def arrayType: Rule1[ArrayType] = rule {
    (primitiveType | classType) ~ dims ~> ArrayType
  }

  protected def dims: Rule1[Seq[Dimension]] = rule {
    (annotations ~ `[` ~ `]` ~> AbstractDimension).+
  }

  protected def optionalDims: Rule1[Seq[Dimension]] = rule {
    (annotations ~ `[` ~ `]` ~> AbstractDimension).*
  }

  protected def typeParameter: Rule1[TemplateParameter] = rule {
    annotations ~ name ~ {
      typeBound ~> BoundedParameterTemplate |
        MATCH ~> ParameterTemplate
    }
  }

  private def typeBound: Rule1[Type] = rule {
    `extends` ~ ((classType ~ additionalBound.*) ~> composeTypeBound)
  }


  protected def additionalBound: Rule1[ClassType] = rule {
    `&` ~ classType
  }

  protected def optionalTypeArguments: Rule1[Seq[TemplateArgument]] = rule {
    `<` ~ typeArgument.*(comma) ~ `>` | push(Vector())
  }

  protected def typeArguments: Rule1[Seq[TemplateArgument]] = rule {
    `<` ~ typeArgument.*(comma) ~ `>`
  }

  private def typeArgument: Rule1[TemplateArgument] = rule {
    referenceType ~> ArgumentTemplate |
      annotations ~ {
        `?` ~ `extends` ~ referenceType ~> AnySubClassTemplate |
          `?` ~ `super` ~ referenceType ~> AnyBaseClassTemplate |
          `?` ~> AnyTemplate
      }
  }
}