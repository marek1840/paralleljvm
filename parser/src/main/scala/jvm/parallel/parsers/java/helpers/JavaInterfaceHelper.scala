package jvm.parallel.parsers.java.helpers

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.statement.instructions.discardable.dimension.Dimension
import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.expression.types.references.ArrayType
import jvm.parallel.ast.parsers.statement.declaration.members.{AnnotationDefaultElementDeclaration, AnnotationElementDeclaration}
import jvm.parallel.ast.parsers.statement.modifers.Modifier

protected[parsers] trait JavaInterfaceHelper extends JavaClassHelper{
  def composeAnnotationElementDeclaration: (Seq[Annotation], Seq[Modifier], Type, Name, Seq[Dimension]) => AnnotationElementDeclaration =
    (as, ms, t, n, dims) =>
      if (dims.size == 0) AnnotationElementDeclaration(as, ms, t, n)
      else AnnotationElementDeclaration(as, ms, ArrayType(t, dims), n)


  def composeAnnotationDefaultElementDeclaration: (Seq[Annotation], Seq[Modifier], Type, Name, Seq[Dimension], Expression) => AnnotationDefaultElementDeclaration =
    (as, ms, t, n, dims, e) =>
      if (dims.size == 0) AnnotationDefaultElementDeclaration(as, ms, t, n, e)
      else AnnotationDefaultElementDeclaration(as, ms, ArrayType(t, dims), n, e)

}
