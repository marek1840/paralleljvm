package jvm.parallel.parsers.java.helpers

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.statement.instructions.discardable.dimension.Dimension
import jvm.parallel.ast.parsers.statement.declarator._

protected[parsers] trait JavaDeclaratorHelper extends JavaParameterHelper{
  def composeDeclarator: (Name, Seq[Dimension], Option[Expression]) => Declarator  = {
    (n, ds, i) =>
      if (ds.size == 0 && i.isDefined) InitializedVariableDeclarator(n, i.get)
      else if (i.isDefined) InitializedArrayDeclarator(n, ds, i.get)
      else if (ds.size == 0) VariableDeclarator(n)
      else ArrayDeclarator(n, ds)
  }
}
