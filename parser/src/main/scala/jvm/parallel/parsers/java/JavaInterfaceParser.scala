package jvm.parallel.parsers.java

import jvm.parallel.ast.parsers.expression.types.annotations._
import jvm.parallel.ast.parsers.expression.types.references.ClassType
import jvm.parallel.ast.parsers.expression.{ArrayInitializer, Expression}
import jvm.parallel.ast.parsers.statement.declaration.members.{FieldDeclaration, MemberDeclaration, MethodDeclaration}
import jvm.parallel.ast.parsers.statement.declaration.types.{AnnotationDeclaration, InterfaceDeclaration, TypeDeclaration}
import jvm.parallel.parsers.java.helpers.JavaInterfaceHelper
import org.parboiled2.{ParserInput, Rule1}

protected[parsers] abstract class JavaInterfaceParser(override val input: ParserInput) extends JavaClassParser(input) with JavaInterfaceHelper{
  def interfaceDeclaration: Rule1[TypeDeclaration] = rule {
    interfaceModifiers ~ `interface` ~ name ~ optionalTypeParameters ~
      extendedInterfaces ~ `{` ~ interfaceBody ~ `}` ~> InterfaceDeclaration
  }

  def annotationDeclaration: Rule1[TypeDeclaration] = rule {
    interfaceModifiers ~ `@` ~ `interface` ~ name ~ `{` ~
      annotationMemberDeclaration.* ~ `}` ~> AnnotationDeclaration
  }

  private def extendedInterfaces: Rule1[Seq[ClassType]] = rule {
    `extends` ~ classType.+(comma) | push(Vector())
  }
  private def interfaceBody: Rule1[Seq[MemberDeclaration]] = rule { interfaceMemberDeclaration.* }

  def interfaceMemberDeclaration: Rule1[MemberDeclaration] = rule {
    constantDeclaration | interfaceMethodDeclaration | typeDeclaration
  }

  private def constantDeclaration: Rule1[MemberDeclaration] = rule {
    constantModifiers ~ `type` ~ variableDeclarators ~ semicolon ~> FieldDeclaration
  }

  private def interfaceMethodDeclaration: Rule1[MemberDeclaration] = rule {
    interfaceMethodModifiers ~ methodHeader ~ methodBody ~> MethodDeclaration
  }

  protected def annotations: Rule1[Seq[Annotation]] = rule { annotation.* }

  def annotation: Rule1[Annotation] = rule {
    `@` ~ qualifiedName ~ {
      namedAnnotationParameters ~> NormalAnnotation |
        `(` ~ elementValue ~ `)` ~> SingleElementAnnotation |
        MATCH ~> MarkerAnnotation
    }
  }

  private def namedAnnotationParameters :Rule1[Seq[NamedAnnotationParameter]]= rule{
    `(` ~ (name ~ `=` ~ elementValue ~> NamedAnnotationParameter).*(`comma`) ~ `)`
  }

  private def elementValue: Rule1[Expression] = rule {
    elementValueArrayInitializer | annotation | conditionalExpression
  }

  private def elementValueArrayInitializer: Rule1[Expression] = rule {
    `{` ~ `}` ~ push(ArrayInitializer(Vector())) |
      `{` ~ elementValue.+(`comma`) ~ comma.? ~ `}` ~> ArrayInitializer
  }

  def annotationMemberDeclaration: Rule1[MemberDeclaration] = rule {
    annotationElementDeclaration | constantDeclaration | typeDeclaration
  }
  private def annotationElementDeclaration: Rule1[MemberDeclaration] = rule {
    annotationElementModifiers ~ `type` ~ name ~ `(` ~ `)` ~ optionalDims ~ {
      `default` ~ elementValue ~> composeAnnotationDefaultElementDeclaration |
        MATCH ~> composeAnnotationElementDeclaration
    } ~ semicolon
  }
}
