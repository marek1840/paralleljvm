package jvm.parallel.parsers.java

import jvm.parallel.ast.parsers.expression.types.references.ClassType
import jvm.parallel.ast.parsers.expression.types.templates.TemplateParameter
import jvm.parallel.ast.parsers.statement.instructions.constructor.{AlternateConstructorInvocation, IndirectParentConstructorInvocation, IntermediateConstructorInvocation, ParentConstructorInvocation}
import jvm.parallel.ast.parsers.statement.declaration.initializers.{InstanceInitializerDeclaration, StaticInitializerDeclaration}
import jvm.parallel.ast.parsers.statement.declaration.members.{ConstructorDeclaration, FieldDeclaration, MemberDeclaration, MethodDeclaration}
import jvm.parallel.ast.parsers.statement.declaration.types.{ClassDeclaration, EmptyDeclaration, EnumDeclaration, TypeDeclaration}
import jvm.parallel.ast.parsers.statement.{Block, Statement}
import jvm.parallel.ast.parsers.statement.instructions.LocalVariableDeclaration
import jvm.parallel.parsers.java.helpers.JavaClassHelper
import org.parboiled2.{ParserInput, Rule1}

protected[parsers] abstract class JavaClassParser(override val input: ParserInput) extends JavaStatementParser(input) with JavaClassHelper {
  def typeDeclaration: Rule1[TypeDeclaration]

  def classDeclaration: Rule1[TypeDeclaration] = rule {
    classModifiers ~ `class` ~ name ~ optionalTypeParameters ~
      superClass ~ superInterfaces ~ classBody ~> ClassDeclaration
  }

  def enumDeclaration: Rule1[TypeDeclaration] = rule {
    classModifiers ~ `enum` ~ name ~ superInterfaces ~ enumBody ~> EnumDeclaration
  }

  protected def optionalTypeParameters: Rule1[Seq[TemplateParameter]] = rule {
    `<` ~ typeParameter.+(comma) ~ `>` | push(Vector())
  }

  private def superClass: Rule1[Option[ClassType]] = rule {
    optional(`extends` ~ classType)
  }

  private def superInterfaces: Rule1[Seq[ClassType]] = rule {
    `implements` ~ classType.*(comma) | push(Vector())
  }

  protected def classBody: Rule1[Seq[MemberDeclaration]] = rule {
    `{` ~ classMembers ~ `}`
  }

  private def classMembers: Rule1[Seq[MemberDeclaration]] = rule {
    (classMemberDeclaration | instanceInitializer | staticInitializer | constructorDeclaration).*
  }

  def classMemberDeclaration: Rule1[MemberDeclaration] = rule {
    fieldDeclaration | methodDeclaration | typeDeclaration
  }

  protected def emptyDeclaration: Rule1[TypeDeclaration] = rule {
    semicolon ~ push(EmptyDeclaration)
  }

  private def fieldDeclaration: Rule1[FieldDeclaration] = rule {
    fieldModifiers ~ `type` ~ variableDeclarators ~ semicolon ~> FieldDeclaration
  }

  protected def variableDeclaration: Rule1[LocalVariableDeclaration] = rule {
    variableModifiers ~ `type` ~ variableDeclarators ~> LocalVariableDeclaration
  }


  def methodDeclaration: Rule1[MethodDeclaration] = rule {
    methodModifiers ~ methodHeader ~ methodBody ~> MethodDeclaration
  }

  protected def methodBody: Rule1[Statement] = rule {
    block | emptyStatement
  }

  protected def methodHeader = rule {
    optionalTypeParameters ~ `type` ~ methodDeclarator ~ thrown
  }

  private def thrown: Rule1[Seq[ClassType]] = rule {
    `throws` ~ classType.+(comma) | push(Vector())
  }


  private def instanceInitializer: Rule1[MemberDeclaration] = rule {
    block ~> InstanceInitializerDeclaration
  }

  private def staticInitializer: Rule1[MemberDeclaration] = rule {
    `static` ~ block ~> StaticInitializerDeclaration
  }

  private def constructorDeclaration: Rule1[ConstructorDeclaration] = rule {
    constuctorModifiers ~ optionalTypeParameters ~ constructorDeclarator ~ thrown ~ constructorBody ~> ConstructorDeclaration
  }

  private def constructorBody: Rule1[Block] = rule {
    `{` ~ explicitConstructorInvocation.? ~ blockStatement.* ~ `}` ~> composeBlock
  }

  private def explicitConstructorInvocation: Rule1[Statement] = rule {
    {
      optionalTypeArguments ~ {
        `this` ~ arguments ~> AlternateConstructorInvocation |
          `super` ~ arguments ~> ParentConstructorInvocation
      } |
        referenceNoMethodAccess ~ dot ~ optionalTypeArguments ~ `super` ~ arguments ~> IndirectParentConstructorInvocation |
        primaryExpression ~ dot ~ optionalTypeArguments ~ `super` ~ arguments ~> IntermediateConstructorInvocation
    } ~ semicolon
  }


  def enumBody: Rule1[Seq[MemberDeclaration]] = rule {
    `{` ~ enumConstant.*(comma) ~ comma.? ~ {
      (semicolon ~ classMembers) |
        MATCH ~ push(Vector())
    } ~ `}` ~> concatMembers
  }

  private def enumConstant: Rule1[MemberDeclaration] = rule {
    annotations ~ name ~ arguments.? ~ classBody.? ~> composeEnumDeclaration
  }
}
