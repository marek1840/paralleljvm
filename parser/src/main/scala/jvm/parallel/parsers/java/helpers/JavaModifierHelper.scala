package jvm.parallel.parsers.java.helpers

import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.statement.modifers.Modifier

protected[parsers] trait JavaModifierHelper extends JavaExpressionHelper{
  def appendAnnotation: ((Seq[Annotation], Seq[Modifier]), Annotation) => (Seq[Annotation], Seq[Modifier]) =
    (t, a) => (t._1 :+ a, t._2)

  def appendModifier: ((Seq[Annotation], Seq[Modifier]), Modifier) => (Seq[Annotation], Seq[Modifier]) =
    (t, a) => (t._1, t._2  :+ a)
}
