package jvm.parallel.parsers.java

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression._
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary._
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.assignment.{Assignment, AugmentedBinding, Binding}
import jvm.parallel.ast.parsers.statement.instructions.discardable.dimension.{AbstractDimension, Dimension, InitializedDimension}
import jvm.parallel.ast.parsers.statement.instructions.discardable.instantiation._
import jvm.parallel.ast.parsers.statement.instructions.discardable.literals.{ClassLiteral, Literal}
import jvm.parallel.ast.parsers.statement.instructions.discardable.reference.{ParentReference, Select, ThisReference}
import jvm.parallel.ast.parsers.statement.instructions.discardable.unary.{PostDecrementation, PostIncrementation, PreDecrementation, PreIncrementation}
import jvm.parallel.ast.parsers.statement.instructions.discardable.{ArrayConstructorReference, ConstructorReference, DiscardableExpression}
import jvm.parallel.ast.parsers.expression.operators.{BinaryOperator, UnaryOperator}
import jvm.parallel.ast.parsers.expression.types.references.ClassType
import jvm.parallel.ast.parsers.expression.types.templates.TemplateArgument
import jvm.parallel.ast.parsers.statement.declaration.members.MemberDeclaration
import jvm.parallel.ast.parsers.statement.instructions.flow.ImplicitReturn
import jvm.parallel.ast.parsers.statement.parameter.{InferredParameter, Parameter}
import jvm.parallel.ast.parsers.statement.{Block, Statement}
import jvm.parallel.parsers.java.helpers.JavaExpressionHelper
import org.parboiled2.{ParserInput, Rule1}

protected[parsers] abstract class JavaExpressionParser(override val input: ParserInput) extends JavaTypeParser(input) with JavaExpressionHelper {
  protected def classBody: Rule1[Seq[MemberDeclaration]]
  protected def formalParameters: Rule1[Seq[Parameter]]
  protected def block: Rule1[Block]

  def expression: Rule1[Expression] = rule {
    lambda | assignment | conditionalExpression
  }

  private def lambda: Rule1[Lambda] = rule {
    lambdaParameters ~ `->` ~ lambdaBody ~> Lambda
  }

  private def lambdaBody: Rule1[Statement] = rule {
    expression ~> ImplicitReturn | block
  }

  private def lambdaParameters: Rule1[Seq[Parameter]] = rule {
    name ~> ((e: Name) => Vector(InferredParameter(e))) |
      `(` ~ (name ~> InferredParameter).+(comma) ~ `)` |
      `(` ~ optionalFormalParameters ~ `)`
  }

  private def optionalFormalParameters: Rule1[Seq[Parameter]] = rule {
    formalParameters | push(Vector())
  }

  protected def referenceNoMethodAccess: Rule1[Select] = rule {
    identifier ~ (dot ~ identifier ~ !`(`).* ~> composeSelect
  }

  protected def primaryExpression: Rule1[Expression] = rule {
    expressionPrimaryBase ~ primaryRest.*
  }

  protected def primaryStatement: Rule1[Statement] = rule {
    statementPrimaryBase ~ primaryRest.+ |
      statementPrimaryBase
  }

  protected def primaryRest = rule {
    dot ~ {
      optionalTypeArguments ~ name ~ arguments ~> QualifiedMethodInvocation |
        name ~> FieldAccess |
        `new` ~ optionalTypeArguments ~ classTypeWithDaiamond ~ arguments ~ classBody.? ~> composeNestedInstantiation
    } | {
      `[` ~ expression ~ `]` ~> Extraction |
        `::` ~ optionalTypeArguments ~ name ~> MethodReference
    }
  }

  private def expressionPrimaryBase: Rule1[Expression] = rule {
    `(` ~ expression ~ `)` | primaryBase
  }

  private def statementPrimaryBase: Rule1[DiscardableExpression] = rule {
    `(` ~ primaryBase ~ `)` | primaryBase
  }

  private def primaryBase: Rule1[DiscardableExpression] = rule {
    `this` ~ push(ThisReference) |
      literal |
      classLiteral |
      `super` ~ push(ParentReference) ~ {
        dot ~ {
          optionalTypeArguments ~ name ~ arguments ~> QualifiedMethodInvocation |
            name ~> FieldAccess
        } |
          `::` ~ optionalTypeArguments ~ name ~> MethodReference
      } |
      `new` ~ (classCreator | arrayCreator) |
      qualifiedName ~ {
        `[` ~ expression ~ `]` ~> composeExtraction |
          arguments ~> composeInvocation |
          dot ~ {
            `this` ~> QualifiedThisReference |
              `super` ~> QualifiedParentReference ~ {
                dot ~ {
                  optionalTypeArguments ~ name ~ arguments ~> QualifiedMethodInvocation |
                    name ~> FieldAccess
                } |
                  `::` ~ optionalTypeArguments ~ name ~> MethodReference
              }
          } |
          run { (s: Name) => Select(s) } ~ dot ~ {
            `new` ~ optionalTypeArguments ~ classTypeWithDaiamond ~ arguments ~ classBody.? ~> composeNestedInstantiation |
              typeArguments ~ name ~ arguments ~> QualifiedMethodInvocation
          } |
          `::` ~ optionalTypeArguments ~ name ~> MethodReference
      } |
      referenceType ~ `::` ~ optionalTypeArguments ~ name ~> MethodReference |
      classType ~ `::` ~ optionalTypeArguments ~ `new` ~> ConstructorReference |
      arrayType ~ `::` ~ `new` ~> ArrayConstructorReference
  }

  protected def classLiteral: Rule1[Literal] = rule {
    `type` ~ dot ~ `class` ~> ClassLiteral
  }

  protected def arguments: Rule1[Seq[Expression]] = rule {
    `(` ~ expression.*(comma) ~ `)`
  }

  private def classCreator: Rule1[ObjectInstantiation] = rule {
    optionalTypeArguments ~ classTypeWithDaiamond ~ arguments ~ {
      classBody ~> AnonymousObjectInstantiation |
        MATCH ~> SimpleObjectInstantiation
    }
  }

  private def classTypeWithDaiamond: Rule1[ClassType] = rule {
    annotations ~ push(None) ~ name ~ typeArgumentsOrDiamond ~> ClassType ~
      (dot ~ annotations ~ name ~ typeArgumentsOrDiamond ~> nestClassTypes).*
  }

  private def typeArgumentsOrDiamond: Rule1[Seq[TemplateArgument]] = rule {
    optionalTypeArguments | (`<` ~ `>` ~ !dot ~ push(Vector()))
  }

  private def arrayCreator: Rule1[ArrayInstantiation] = rule {
    `type` ~ dimExprs ~> EmptyArrayInstantiation |
      arrayType ~ arrayInitializer ~> InitializedArrayInstantiation
  }

  private def dimExprs: Rule1[Seq[Dimension]] = rule {
    (annotations ~ `[` ~ expression ~ `]` ~> InitializedDimension).+ ~
      (annotations ~ `[` ~ `]` ~> AbstractDimension).* ~> concatDimensions
  }

  private def assignmentExpression: Rule1[Expression] = rule {
    conditionalExpression | assignment
  }

  protected def assignment: Rule1[Assignment] = rule {
    (primaryExpression | reference) ~ {
      `=` ~ expression ~> Binding | assignmentOps ~ expression ~> AugmentedBinding
    }
  }

  protected def conditionalExpression: Rule1[Expression] = rule {
    or ~ (`?` ~ expression ~ colon ~ conditionalExpression ~> TernaryConditional).?
  }

  private def or: Rule1[Expression] = rule {
    binaryOperation(orOp, and)
  }

  private val and = () => rule {
    binaryOperation(andOp, binOr)
  }
  private val binOr = () => rule {
    binaryOperation(binOrOp, xor)
  }
  private val xor = () => rule {
    binaryOperation(xorOp, binAnd)
  }
  private val binAnd = () => rule {
    binaryOperation(binAndOp, equality)
  }
  private val equality = () => rule {
    binaryOperation(eqOps, () => relational)
  }

  private def relational: Rule1[Expression] = rule {
    shift() ~> ((expr: Expression) => (Vector[BinaryOperator](), Vector(expr))) ~ {
      zeroOrMore {
        (relOps() ~ shift() | `instanceof` ~ push(BinaryOperator.instanceof) ~ referenceType) ~> (
          (acc: (Vector[BinaryOperator], Vector[Expression]), op: BinaryOperator, expr: Expression) =>
            (acc._1 :+ op, acc._2 :+ expr))
      } ~> ((acc: (Vector[BinaryOperator], Vector[Expression])) =>
        if (acc._2.size == 1) acc._2(0) else BinaryOperations(acc._1, acc._2))
    }
  }

  private val shift = () => rule {
    binaryOperation(shiftOps, additive)
  }
  private val additive = () => rule {
    binaryOperation(addOps, multiplicative)
  }
  private val multiplicative = () => rule {
    binaryOperation(mulOps, () => unary)
  }

  protected def unary: Rule1[Expression] = rule {
    `++` ~ unary ~> PreIncrementation | `--` ~ unary ~> PreDecrementation |
      unaryOps.+ ~ unary ~> (UnaryOperations(_: Seq[UnaryOperator], _: Expression)) |
      cast | postfix
  }

  private def postfix: Rule1[Expression] = rule {
    (primaryExpression | reference) ~ {
      `++` ~> PostIncrementation | `--` ~> PostDecrementation
    }.?
  }

  protected def reference: Rule1[Expression] = rule {
    qualifiedName ~> ((name: Name) => Select(name))
  }

  private def cast: Rule1[Cast] = rule {
    {
      `(` ~ primitiveType ~ `)` ~ unary |
        `(` ~ (referenceType ~ additionalBound.*) ~> composeTypeBound ~ `)` ~ (lambda | postfix)
    } ~> Cast
  }

  protected def arrayInitializer: Rule1[ArrayInitializer] = rule {
    `{` ~ (expression | arrayInitializer).*(comma) ~ comma.? ~ `}` ~> ArrayInitializer
  }

  private def binaryOperation(operator: () => Rule1[BinaryOperator], next: () => Rule1[Expression]): Rule1[Expression] = rule {
    next() ~> ((expr: Expression) => (Vector[BinaryOperator](), Vector(expr))) ~ {
      zeroOrMore {
        operator() ~ next() ~> ((acc: (Vector[BinaryOperator], Vector[Expression]), op: BinaryOperator, expr: Expression) =>
          (acc._1 :+ op, acc._2 :+ expr))
      }
    } ~> ((acc: (Vector[BinaryOperator], Vector[Expression])) =>
      if (acc._2.size == 1) acc._2(0) else BinaryOperations(acc._1, acc._2))
  }

  private def assignmentOps: Rule1[BinaryOperator] = rule {
    `*=` ~ push(BinaryOperator.*) | `/=` ~ push(BinaryOperator./) |
      `%=` ~ push(BinaryOperator.mod) | `+=` ~ push(BinaryOperator.+) |
      `-= ` ~ push(BinaryOperator.-) | `<<=` ~ push(BinaryOperator.<<) |
      `>>= ` ~ push(BinaryOperator.>>) | `>>>=` ~ push(BinaryOperator.>>>) |
      `&=` ~ push(BinaryOperator.&) | `^=` ~ push(BinaryOperator.^) |
      `|=` ~ push(BinaryOperator.|)
  }

  private def unaryOps: Rule1[UnaryOperator] = rule {
    plus ~ push(UnaryOperator.+) | minus ~ push(UnaryOperator.-) |
      tilde ~ push(UnaryOperator.~) | exclamation ~ push(UnaryOperator.not)
  }

  private val relOps = () => rule {
    `<=` ~ push(BinaryOperator.<=) | `<` ~ push(BinaryOperator.<) |
      `>=` ~ push(BinaryOperator.>=) | `>` ~ push(BinaryOperator.>)
  }

  private val mulOps = () => rule {
    `*` ~ push(BinaryOperator.*) | `/` ~ push(BinaryOperator./) | `%` ~ push(BinaryOperator.mod)
  }
  private val addOps = () => rule {
    `plus` ~ push(BinaryOperator.+) | `minus` ~ push(BinaryOperator.-)
  }
  private val shiftOps = () => rule {
    `>>>` ~ push(BinaryOperator.>>>) | `>>` ~ push(BinaryOperator.>>) | `<<` ~ push(BinaryOperator.<<)
  }
  private val eqOps = () => rule {
    `==` ~ push(BinaryOperator.==) | `!=` ~ push(BinaryOperator.!=)
  }
  private val binAndOp = () => rule {
    `&` ~ push(BinaryOperator.&)
  }
  private val xorOp = () => rule {
    `^` ~ push(BinaryOperator.^)
  }
  private val binOrOp = () => rule {
    `|` ~ push(BinaryOperator.|)
  }
  private val andOp = () => rule {
    `&&` ~ push(BinaryOperator.and)
  }
  private val orOp = () => rule {
    `||` ~ push(BinaryOperator.or)
  }
}