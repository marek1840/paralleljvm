package jvm.parallel.parsers.java

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.instructions.discardable.dimension.Dimension
import jvm.parallel.ast.parsers.statement.parameter.{FormalParameter, InstanceReceiverParameter, NestedReceiverParameter, Parameter}
import jvm.parallel.parsers.java.helpers.JavaParameterHelper
import org.parboiled2._

protected[parsers] abstract class JavaParameterParser(override val input: ParserInput) extends JavaModifiersParser(input) with JavaParameterHelper {
  protected def variableDeclaratorId: Rule2[Name, Seq[Dimension]]

  override protected def formalParameters: Rule1[Seq[Parameter]] = rule {
    {
      receiverParameter | formalParameter
    } ~ (comma ~ formalParameter).* ~ (comma ~ varArgParameter).? ~> composeParameters |
      varArgParameter ~> ((p: Parameter) => Vector(p)) |
      MATCH ~ push(Vector())
  }

  private def formalParameter: Rule1[FormalParameter] = rule {
    variableModifiers ~ `type` ~ variableDeclaratorId ~> composeFormalParameter
  }

  private def receiverParameter: Rule1[Parameter] = rule {
    annotations ~ `type` ~ {
      name ~ dot ~> NestedReceiverParameter |
        MATCH ~> InstanceReceiverParameter
    } ~ `this`
  }

  private def varArgParameter: Rule1[Parameter] = rule {
    variableModifiers ~ `type` ~ `...` ~ variableDeclaratorId ~> composeVarArgs
  }
}
