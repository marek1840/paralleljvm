package jvm.parallel.parsers.java

import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.statement.modifers._
import jvm.parallel.parsers.java.helpers.JavaModifierHelper
import org.parboiled2._
import shapeless.HNil

protected[parsers] abstract class JavaModifiersParser(override val input: ParserInput) extends JavaExpressionParser(input) with JavaModifierHelper{
  def annotation: Rule1[Annotation]

  protected def classModifiers: Rule2[Seq[Annotation], Seq[Modifier]] = rule {
    zeroOrMore(annotation | classModifier) ~> { (seq: Seq[Any]) =>
      var as = Vector[Annotation]()
      var ms = Vector[Modifier]()

      for (i <- seq) {
        i match {
          case annotation: Annotation => as = as :+ annotation
          case modifier: Modifier => ms = ms :+ modifier
          case _ =>
        }
      }
      as :: ms :: HNil
    }
  }

  protected def constuctorModifiers: Rule2[Seq[Annotation], Seq[Modifier]] = rule {
    push((Vector(), Vector())) ~ zeroOrMore {
      annotation ~> appendAnnotation |
        constructorModifier ~> appendModifier
    } ~> toRule
  }

  protected def methodModifiers: Rule2[Seq[Annotation], Seq[Modifier]] = rule {
    push((Vector(), Vector())) ~ zeroOrMore {
      annotation ~> appendAnnotation |
        methodModifier ~> appendModifier
    } ~> toRule
  }

  protected def fieldModifiers: Rule2[Seq[Annotation], Seq[Modifier]] = rule {
    push((Vector(), Vector())) ~ zeroOrMore {
      annotation ~> appendAnnotation |
        fieldModifier ~> appendModifier
    } ~> toRule
  }

  protected def variableModifiers: Rule2[Seq[Annotation], Seq[Modifier]] = rule {
    annotations ~ variableModifier.* ~ annotations ~> {
      (as1: Seq[Annotation], mods: Seq[Modifier], as2: Seq[Annotation]) =>
        (as1 ++ as2) :: mods :: HNil
    }
  }

  protected def interfaceModifiers: Rule2[Seq[Annotation], Seq[Modifier]] = rule {
    push((Vector(), Vector())) ~ zeroOrMore {
      annotation ~> appendAnnotation |
        interfaceModifier ~> appendModifier
    } ~> toRule
  }


  protected def constantModifiers: Rule2[Seq[Annotation], Seq[Modifier]] = rule {
    push((Vector(), Vector())) ~ zeroOrMore {
      annotation ~> appendAnnotation |
        constantModifier ~> appendModifier
    } ~> toRule
  }


  protected def interfaceMethodModifiers: Rule2[Seq[Annotation], Seq[Modifier]] = rule {
    push((Vector(), Vector())) ~ zeroOrMore {
      annotation ~> appendAnnotation |
        interfaceMethodModifier ~> appendModifier
    } ~> toRule
  }


  protected def annotationElementModifiers: Rule2[Seq[Annotation], Seq[Modifier]] = rule {
    push((Vector(), Vector())) ~ zeroOrMore {
      annotation ~> appendAnnotation |
        annotationElementModifier ~> appendModifier
    } ~> toRule
  }

  protected def variableModifier: Rule1[Modifier] = rule{
    `final` ~ push(Final)
  }
  protected def parameterModifier: Rule1[Modifier] = rule{
    `final` ~ push(Final)
  }

  protected def classModifier: Rule1[Modifier] = rule {
    `public` ~ push(Public) | `protected` ~ push(Protected) |
      `private` ~ push(Private) | `abstract` ~ push(Abstract) |
      `static` ~ push(Static) | `final` ~ push(Final) |
      `strictfp` ~ push(Strictfp)
  }

  protected def constructorModifier: Rule1[Modifier] = rule {
    `public` ~ push(Public) |
      `protected` ~ push(Protected) |
      `private` ~ push(Private)
  }


  protected def methodModifier: Rule1[Modifier] = rule {
    `public` ~ push(Public) | `protected` ~ push(Protected) |
      `private` ~ push(Private) | `abstract` ~ push(Abstract) |
      `static` ~ push(Static) | `final` ~ push(Final) |
      `synchronized` ~ push(Synchronized) | `native` ~ push(Native) |
      `strictfp` ~ push(Strictfp)
  }

  protected def fieldModifier: Rule1[Modifier] = rule {
    `public` ~ push(Public) | `protected` ~ push(Protected) |
      `private` ~ push(Private) | `static` ~ push(Static) |
      `final` ~ push(Final) | `transient` ~ push(Transient) |
      `volatile` ~ push(Volatile)
  }

  protected def interfaceMethodModifier: Rule1[Modifier] = rule {
    `public` ~ push(Public) | `static` ~ push(Static) |
      `abstract` ~ push(Abstract) | `default` ~ push(Default) |
      `strictfp` ~ push(Strictfp)
  }

  protected def annotationElementModifier: Rule1[Modifier] = rule {
    `public` ~ push(Public) | `abstract` ~ push(Abstract)
  }

  protected def constantModifier: Rule1[Modifier] = rule {
    `public` ~ push(Public) | `static` ~ push(Static) | `final` ~ push(Final)
  }

  protected def interfaceModifier: Rule1[Modifier] = rule {
    `public` ~ push(Public) | `protected` ~ push(Protected) |
      `private` ~ push(Private) | `abstract` ~ push(Abstract) |
      `static` ~ push(Static) | `strictfp` ~ push(Strictfp)
  }

}
