package jvm.parallel.parsers.java

import jvm.parallel.ast.name.Name
import jvm.parallel.parsers.commons.AbstractParser
import org.parboiled2.{ParserInput, Rule1}


protected[parsers] abstract class JavaIdentifierParser(override val input:ParserInput) extends AbstractParser  {
  def name :Rule1[Name]= rule {
    identifier ~> (new Name(_ : String))
  }
  
  protected[this] def identifier : Rule1[String] = rule{
    !keyword ~ capture(identifierStart ~ identifierChar.*) ~ whitespace
  }

  def qualifiedName: Rule1[Name] = rule { identifier.+(dot) ~> (new Name(_ : Seq[String]))}

  protected def keyword = rule {
    ("abstract" | "continue" | "for" | "new" | "switch" | "assert" | "default" | "if" | "package" | "synchronized" |
      "boolean" | "double" | "do" | "goto" | "private" | "this" | "break" | "implements" | "protected" | "throws" |
      "throw" | "byte" | "else" | "imports" | "public" | "case" | "enum" | "instanceof" | "return" | "transient" |
      "catch" | "extends" | "interface" | "int" | "short" | "try" | "char" | "finally" | "final" | "static" | "void" |
      "class" | "long" | "strictfp" | "volatile" | "const" | "float" | "native" | "super" | "while" | "long") ~ !identifierChar ~ whitespace
  }
}
