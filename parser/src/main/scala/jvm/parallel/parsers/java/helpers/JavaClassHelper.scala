package jvm.parallel.parsers.java.helpers

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.statement.declaration.members.{AnonymousEnumConstantDeclaration, EnumConstantDeclaration, MemberDeclaration}
import jvm.parallel.ast.parsers.statement.{Block, Statement}

protected[parsers] trait JavaClassHelper extends JavaStatementHelper{
  def composeBlock: (Option[Statement], Seq[Statement]) => Block =
    (s, ss) =>
      if (s.isDefined) Block(s.get +: ss)
      else Block(ss)

  def composeEnumDeclaration: (Seq[Annotation], Name, Option[Seq[Expression]], Option[Seq[MemberDeclaration]]) => MemberDeclaration with Product with Serializable =
      (as, n, args, b) =>
      if (b.isDefined) AnonymousEnumConstantDeclaration(as, n, args.getOrElse(Vector()), b.get)
      else EnumConstantDeclaration(as, n, args.getOrElse(Vector()))


  def concatMembers: (Seq[MemberDeclaration], Seq[MemberDeclaration]) => Seq[MemberDeclaration] =
    (cs, ms) => cs ++: ms
}
