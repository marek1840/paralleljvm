package jvm.parallel.parsers.java

import jvm.parallel.ast.parsers.ASTNode
import jvm.parallel.ast.parsers.statement.Statement
import jvm.parallel.ast.parsers.statement.declaration.CompilationUnitDeclaration
import jvm.parallel.ast.parsers.statement.declaration.members.MethodDeclaration
import jvm.parallel.logging.Logger
import jvm.parallel.parsers.commons.AbstractParser
import org.parboiled2.{ParseError, ParserInput}

import scala.util.{Failure, Success, Try}


object JavaParser extends Logger {
  private def parser(input: String): JavaCompilationUnitParser = new JavaCompilationUnitParser(ParserInput(input))

  private[this] def parse[A <: ASTNode](input: String)(f: JavaCompilationUnitParser => Try[A]): A = {
//    logger.debug("Parsing: " + input)
    implicit val p: JavaCompilationUnitParser = parser(input)
    val result = f(p)
    get(result)
  }

  final def parseCompilationUnit[A <: ASTNode](input: String): CompilationUnitDeclaration =
    parse(input)(_.compilationUnit.run())

  final def parseStatement[A <: ASTNode](input: String): Statement = parse(input)(_.blockStatement.run())

  final def parseMethod[A <: ASTNode](input: String): MethodDeclaration = parse(input)(_.methodDeclaration.run())

  protected def get[A](a: Try[A])(implicit parser: AbstractParser): A = a match {
    case Success(res) => res
    case Failure(error: ParseError) => throw new Exception(parser.formatError(error))
    case r => r.get
  }

}
