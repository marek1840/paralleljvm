package jvm.parallel.parsers.java

import jvm.parallel.ast.parsers.statement.declaration.CompilationUnitDeclaration
import jvm.parallel.ast.parsers.statement.declaration.imports._
import jvm.parallel.ast.parsers.statement.declaration.packages.{NamedPackageDeclaration, PackageDeclaration, UnnamedPackageDeclaration}
import jvm.parallel.ast.parsers.statement.declaration.types.TypeDeclaration
import org.parboiled2.{ParserInput, Rule1}

protected[parsers] class JavaCompilationUnitParser(override val input:ParserInput) extends JavaInterfaceParser(input) {
  def compilationUnit: Rule1[CompilationUnitDeclaration] = rule {
    whitespace ~ packageDeclaration ~ importDeclarations ~ typeDeclarations ~ EOI ~> CompilationUnitDeclaration
  }

  private def typeDeclarations: Rule1[Seq[TypeDeclaration]] = rule {
    typeDeclaration.*
  }

  def typeDeclaration: Rule1[TypeDeclaration] = rule {
    classDeclaration |
      enumDeclaration |
      interfaceDeclaration |
      annotationDeclaration |
      emptyDeclaration
  }

  private def packageDeclaration: Rule1[PackageDeclaration] = rule {
    annotations ~ `package` ~ qualifiedName ~ semicolon ~> NamedPackageDeclaration |
      push(UnnamedPackageDeclaration)
  }

  def importDeclarations: Rule1[Seq[ImportDeclaration]] = rule {
    zeroOrMore {
      `import` ~ {
        `static` ~ {
          qualifiedName ~ {
            dot ~ `*` ~> StaticLazyImportDeclaration |
              MATCH ~> StaticImportDeclaration
          }
        } |
          qualifiedName ~ {
            dot ~ `*` ~> LazyImportDeclaration |
              MATCH ~> SingleImportDeclaration
          }
      } ~ semicolon
    }
  }
}
