package jvm.parallel.parsers.java

import jvm.parallel.ast.parsers.statement.instructions.discardable.literals._
import org.parboiled2.{ParserInput, Rule1}

protected[parsers] abstract class JavaLiteralParser(override val input:ParserInput) extends JavaIdentifierParser(input) {
  def literal: Rule1[Literal] = rule {
    { floatLiteral ~> FloatLiteral|
      integerLiteral ~> IntegerLiteral|
      characterLiteral ~> CharLiteral|
      stringLiteral ~> StringLiteral|
      `null` ~ push(NullLiteral) |
      booleanLiteral ~> BooleanLiteral } ~ whitespace
  }

  protected def integerLiteral: Rule1[String] = rule {
    capture((hexNumber | binaryNumber | octalNumber | decimalNumber) ~ optionalIntegerSuffix)
  }

  protected def floatLiteral: Rule1[String] = rule { hexFloat | decimalFloat }

  protected def booleanLiteral: Rule1[String] = rule { capture(`true` | `false`) }

  protected def characterLiteral: Rule1[String] = rule {
    capture("'" ~ (escapeSequence | unicodeEscape | noneOf("'\\" + 10.toChar + 13.toChar)) ~ "'")
  }

  protected def stringLiteral: Rule1[String] = rule {
    capture('"' ~ (escapeSequence | noneOf("\\\"\n\r")).* ~ '"')
  }


  private def unicodeEscape = rule {
    "\\" ~ oneOrMore("u") ~ hexDigit ~ hexDigit ~ hexDigit ~ hexDigit
  }

  private def optionalIntegerSuffix = rule(("l" | "L").?)
  private def optionalFloatSuffix = rule(("f" | "F" | "d" | "D").?)
  private def exponent = rule(("e" | "E") ~ signedInteger)
  private def signedInteger = rule { ("+" | "-").? ~ decimalDigit.* }

  private def decimalFloat: Rule1[String] = rule {
    capture {
      decimalNumber ~ "." ~ decimalDigit.* ~ exponent.? ~ optionalFloatSuffix |
        decimalNumber ~ exponent ~ optionalFloatSuffix |
        decimalNumber ~ exponent.? ~ ("f" | "F" | "d" | "D") |
        "." ~ decimalDigit.+ ~ exponent.? ~ optionalFloatSuffix
    }
  }

  private def hexFloat: Rule1[String] = rule {
    capture(("0x" | "0X") ~ {
      (hexDigit.* ~ "." ~ hexDigit.+) | (hexDigit.+ ~ ".".?)
    } ~ ("p" | "P") ~ signedInteger ~ optionalFloatSuffix)
  }
}
