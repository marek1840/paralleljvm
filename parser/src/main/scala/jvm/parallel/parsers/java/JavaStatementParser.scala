package jvm.parallel.parsers.java

import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.statement.instructions._
import jvm.parallel.ast.parsers.statement.instructions.discardable.unary.{PostDecrementation, PostIncrementation, PreDecrementation, PreIncrementation}
import jvm.parallel.ast.parsers.statement._
import jvm.parallel.ast.parsers.statement.instructions.conditional.{ConditionalStatement, If, IfThenElse}
import jvm.parallel.ast.parsers.statement.instructions.flow._
import jvm.parallel.ast.parsers.statement.instructions.interruptable._
import jvm.parallel.ast.parsers.statement.instructions.loop._
import jvm.parallel.ast.parsers.statement.instructions.switch.{DefaultSwitch, SwitchCases}
import jvm.parallel.parsers.java.helpers.JavaStatementHelper
import org.parboiled2.{ParserInput, Rule1}

protected[parsers] abstract class JavaStatementParser(override val input: ParserInput) extends JavaDeclaratorParser(input) with
JavaStatementHelper {
  protected def variableDeclaration: Rule1[LocalVariableDeclaration]
  protected def classDeclaration: Rule1[Statement]

  override protected def block: Rule1[Block] = rule {
    `{` ~ blockStatement.* ~ `}` ~> Block
  }

  protected def blockStatements: Rule1[Seq[Statement]] = rule {
    blockStatement.+
  }

  def blockStatement: Rule1[Statement] = rule {
    variableDeclaration ~ semicolon | classDeclaration | statement
  }

  def statement: Rule1[Statement] = rule {
    whileLoop | forLoop | labeledStatement | conditionalStatement | statementWithoutTrailingSubstatement
  }

  private def statementWithoutTrailingSubstatement: Rule1[Statement] = rule {
    block | emptyStatement | discardableExpressionStatement ~ semicolon |
      assertStatement | breakStatement | continueStatement | returnStatement |
      throwStatement | doWhileLoop | tryStatement | switchStatement |
      synchronizedBlock
  }

  protected def emptyStatement: Rule1[Statement] = rule {
    semicolon ~ push(EmptyStatement)
  }

  private def labeledStatement: Rule1[LabeledStatement] = rule {
    name ~ colon ~ statement ~> LabeledStatement
  }

  private def discardableExpressionStatement: Rule1[Statement] = rule {
    assignment |
      reference ~ (`++` ~> PostIncrementation | `--` ~> PostDecrementation) |
      (primaryExpression | primaryStatement) ~ (`++` ~> PostIncrementation | `--` ~> PostDecrementation) |
      primaryStatement |
      `++` ~ unary ~> PreIncrementation | `--` ~ unary ~> PreDecrementation
  }

  private def assertStatement: Rule1[Assertion] = rule {
    `assert` ~ expression ~ (colon ~ expression).? ~> Assertion
  }

  private def breakStatement: Rule1[Statement] = rule {
    (`break` ~ name ~> TargetedBreak | `break` ~ push(Break)) ~ semicolon
  }

  private def continueStatement: Rule1[Statement] = rule {
    (`continue` ~ name ~> TargetedContinue | `continue` ~ push(Continue)) ~ semicolon
  }

  private def returnStatement: Rule1[Statement] = rule {
    (`return` ~ expression ~> Return | `return` ~ push(EmptyReturn)) ~ semicolon
  }

  private def throwStatement: Rule1[FlowStatement] = rule {
    `throw` ~ expression ~ semicolon ~> Throw
  }

  private def conditionalStatement: Rule1[ConditionalStatement] = rule {
    `if` ~ `(` ~ expression ~ `)` ~ statement ~ {
      `else` ~ statement ~> IfThenElse |
        MATCH ~> If
    }
  }

  private def tryStatement: Rule1[TryStatement] = rule {
    `try` ~ {
      resources ~ block ~ catches.? ~ finallyBlock.? ~> composeTryStatement |
        block ~ {
          finallyBlock ~> TryFinally |
            catches ~ {
              finallyBlock ~> TryCatchFinally |
                MATCH ~> TryCatch
            }
        }
    }
  }

  private def switchStatement = rule {
    `switch` ~ `(` ~ expression ~ `)` ~ `{` ~ {
      (switchLabel.+ ~ blockStatement.+ ~> SwitchCases).* ~
        switchLabel.* ~> composeOptionalSwitchRule
    } ~ `}` ~> composeSwitchStatement
  }

  private def switchLabel: Rule1[Expression] = rule {
    (`case` ~ (expression | reference) | `default` ~ push(DefaultSwitch)) ~ colon
  }

  private def synchronizedBlock: Rule1[SynchronizedBlock] = rule {
    `synchronized` ~ (`(` ~ expression ~ `)`).? ~ block ~> SynchronizedBlock
  }

  private def catches: Rule1[Seq[CatchClause]] = rule {
    (      `catch` ~ `(` ~ catchFormalParameter ~ `)` ~ block ~> CatchClause      ).*
  }

  private def finallyBlock: Rule1[Block] = rule {
    `finally` ~ block
  }

  private def catchFormalParameter: Rule1[LocalVariableDeclaration] = rule {
    variableModifiers ~ (classType.*(`|`) ~> composeCatchTypeBound) ~ name ~> composeVariableDeclaration
  }

  private def resources: Rule1[Seq[LocalVariableDeclaration]] = rule {
    `(` ~ resource.+(semicolon) ~ semicolon.? ~ `)`
  }

  private def resource: Rule1[LocalVariableDeclaration] = rule {
    variableModifiers ~ classType ~ name ~ `=` ~ expression ~> composeInitializedVariableDeclaration
  }

  private def doWhileLoop: Rule1[Loop] = rule {
    `do` ~ statement ~ `while` ~ `(` ~ expression ~ `)` ~ semicolon ~> DoWhile
  }

  private def whileLoop: Rule1[While] = rule {
    `while` ~ `(` ~ expression ~ `)` ~ statement ~> While
  }

  private def forLoop: Rule1[Loop] = rule {
    `for` ~ `(` ~ variableDeclaration ~ colon ~ expression ~ `)` ~ statement ~> Iteration |
      `for` ~ `(` ~ forInit ~ semicolon ~ expression.? ~ semicolon ~ forUpdate ~ `)` ~ statement ~> For
  }

  private def forInit: Rule1[Seq[Statement]] = rule {
    discardableExpressionStatement.+(comma) |
      variableDeclaration ~> ((d: LocalVariableDeclaration) => Vector(d)) |
      push(Vector())
  }

  private def forUpdate: Rule1[Seq[Statement]] = rule {
    discardableExpressionStatement.*(comma)
  }
}