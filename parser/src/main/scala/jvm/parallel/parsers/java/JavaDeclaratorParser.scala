package jvm.parallel.parsers.java

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.instructions.discardable.dimension.Dimension
import jvm.parallel.ast.parsers.statement.declarator._
import jvm.parallel.parsers.java.helpers.JavaDeclaratorHelper
import org.parboiled2._

protected[parsers] abstract class JavaDeclaratorParser(override val input: ParserInput) extends JavaParameterParser(input) with JavaDeclaratorHelper {
  protected def constructorDeclarator: Rule1[ConstructorDeclarator] = rule {
    name ~ `(` ~ formalParameters ~ `)` ~> ConstructorDeclarator
  }

  protected def methodDeclarator: Rule1[FunctionDeclarator] = rule {
    name ~ `(` ~ formalParameters ~ `)` ~ {
      dims ~> ArrayMethodDeclarator |
        MATCH ~> MethodDeclarator
    }
  }

  override protected def variableDeclaratorId: Rule2[Name, Seq[Dimension]] = rule {
    name ~ optionalDims
  }

  protected def variableDeclarators: Rule1[Seq[Declarator]] = rule {
    (variableDeclaratorId ~ optional(`=` ~ (expression | arrayInitializer)) ~> composeDeclarator).*(comma)
  }
}
