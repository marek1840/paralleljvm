package jvm.parallel.parsers.java

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.Block
import jvm.parallel.ast.parsers.statement.instructions.EmptyStatement
import jvm.parallel.ast.parsers.statement.instructions.discardable.dimension.AbstractDimension
import jvm.parallel.ast.parsers.statement.instructions.discardable.literals.IntegerLiteral
import jvm.parallel.ast.parsers.expression.types.annotations.MarkerAnnotation
import jvm.parallel.ast.parsers.expression.types.primitives.IntegerPrimitive
import jvm.parallel.ast.parsers.expression.types.references.{ArrayType, ClassType}
import jvm.parallel.ast.parsers.expression.types.templates.ParameterTemplate
import jvm.parallel.ast.parsers.statement.declaration.members._
import jvm.parallel.ast.parsers.statement.declaration.types.{AnnotationDeclaration, InterfaceDeclaration, TypeDeclaration}
import jvm.parallel.ast.parsers.statement.declarator.{InitializedVariableDeclarator, MethodDeclarator, VariableDeclarator}
import jvm.parallel.ast.parsers.statement.modifers._
import jvm.parallel.parsers.ParserTest

class InterfaceDeclarationTest extends ParserTest {
  def parse(string: String): TypeDeclaration = {
    implicit val parser = new Parser(string)
    get(parser.typeDeclaration.run())
  }

  def Interface(body: MemberDeclaration*) = InterfaceDeclaration(Vector, Vector, new Name("A"), Vector, Vector, body.toVector)
  def Annotation(body: MemberDeclaration*) = AnnotationDeclaration(Vector, Vector, new Name("A"), body.toVector)

  "Interface parser should parse" - {
    "interfaces" - {
      "interface A extends A1, A2" in {
        parse("interface A extends A1, A2{}") shouldBe InterfaceDeclaration(
          Vector,
          Vector,
          new Name("A"),
          Vector,
          Vector(ClassType(Vector, None,new Name( "A1"), Vector), ClassType(Vector, None, new Name("A2"), Vector)),
          Vector)
      }
      "@A interface A{}" in {
        parse("@A interface A{}") shouldBe InterfaceDeclaration(
          Vector(MarkerAnnotation(new Name("A"))),
          Vector,
          new Name("A"),
          Vector,
          Vector,
          Vector)
      }
      "public interface A{}" in {
        parse("public interface A{}") shouldBe InterfaceDeclaration(
          Vector,
          Vector(Public),
          new Name("A"),
          Vector,
          Vector,
          Vector)
      }
      "protected interface A{}" in {
        parse("protected interface A{}") shouldBe InterfaceDeclaration(
          Vector,
          Vector(Protected),
          new Name("A"),
          Vector,
          Vector,
          Vector)
      }
      "private  interface A{}" in {
        parse("private  interface A{}") shouldBe InterfaceDeclaration(
          Vector,
          Vector(Private),
          new Name("A"),
          Vector,
          Vector,
          Vector)
      }
      "abstract interface A{}" in {
        parse("abstract interface A{}") shouldBe InterfaceDeclaration(
          Vector,
          Vector(Abstract),
          new Name("A"),
          Vector,
          Vector,
          Vector)
      }
      "static interface A{}" in {
        parse("static interface A{}") shouldBe InterfaceDeclaration(
          Vector,
          Vector(Static),
          new Name("A"),
          Vector,
          Vector,
          Vector)
      }
      "strictfp interface A{}" in {
        parse("strictfp interface A{}") shouldBe InterfaceDeclaration(
          Vector,
          Vector(Strictfp),
          new Name("A"),
          Vector,
          Vector,
          Vector)
      }
      "interface A extends B{}" in {
        parse("interface A extends B{}") shouldBe InterfaceDeclaration(
          Vector,
          Vector,
          new Name("A"),
          Vector,
          Vector(ClassType(Vector, None, new Name("B"), Vector)),
          Vector)
      }
      "interface A<T> {}" in {
        parse("interface A<T> {}") shouldBe InterfaceDeclaration(
          Vector,
          Vector,
          new Name("A"),
          Vector(ParameterTemplate(Vector, new Name("T"))),
          Vector,
          Vector)
      }
      "interface A{ public static final int a; }" in {
        parse("interface A{ public static final int a; }") shouldBe Interface(FieldDeclaration(
          Vector,
          Vector(Public, Static, Final),
          IntegerPrimitive(Vector),
          Vector(VariableDeclarator(new Name("a")))))
      }
      "interface A{ @A public static final int a = 5; }" in {
        parse("interface A{ @A public static final int a = 5; }") shouldBe Interface(FieldDeclaration(
          Vector(MarkerAnnotation(new Name("A"))),
          Vector(Public, Static, Final),
          IntegerPrimitive(Vector),
          Vector(InitializedVariableDeclarator(new Name("a"), IntegerLiteral("5")))))
      }
      "interface A{ int f(); }" in {
        parse("interface A{ int f(); }") shouldBe Interface(MethodDeclaration(
          Vector,
          Vector,
          Vector,
          IntegerPrimitive(Vector),
          MethodDeclarator(new Name("f"), Vector),
          Vector,
          EmptyStatement))
      }
      "interface A{ @A int f(); }" in {
        parse("interface A{ @A int f(); }") shouldBe Interface(MethodDeclaration(
          Vector(MarkerAnnotation(new Name("A"))),
          Vector,
          Vector,
          IntegerPrimitive(Vector),
          MethodDeclarator(new Name("f"), Vector),
          Vector,
          EmptyStatement))
      }
      "interface A{ public int f(); }" in {
        parse("interface A{ public int f(); }") shouldBe Interface(MethodDeclaration(
          Vector,
          Vector(Public),
          Vector,
          IntegerPrimitive(Vector),
          MethodDeclarator(new Name("f"), Vector),
          Vector,
          EmptyStatement))
      }
      "interface A{ abstract int f(); }" in {
        parse("interface A{ abstract int f(); }") shouldBe Interface(MethodDeclaration(
          Vector,
          Vector(Abstract),
          Vector,
          IntegerPrimitive(Vector),
          MethodDeclarator(new Name("f"), Vector),
          Vector,
          EmptyStatement))
      }
      "interface A{ default int f(); }" in {
        parse("interface A{ default int f(); }") shouldBe Interface(MethodDeclaration(
          Vector,
          Vector(Default),
          Vector,
          IntegerPrimitive(Vector),
          MethodDeclarator(new Name("f"), Vector),
          Vector,
          EmptyStatement))
      }
      "interface A{ static int f(); }" in {
        parse("interface A{ static int f(); }") shouldBe Interface(MethodDeclaration(
          Vector,
          Vector(Static),
          Vector,
          IntegerPrimitive(Vector),
          MethodDeclarator(new Name("f"), Vector),
          Vector,
          EmptyStatement))
      }
      "interface A{ strictfp int f(); }" in {
        parse("interface A{ strictfp int f(); }") shouldBe Interface(MethodDeclaration(
          Vector,
          Vector(Strictfp),
          Vector,
          IntegerPrimitive(Vector),
          MethodDeclarator(new Name("f"), Vector),
          Vector,
          EmptyStatement))
      }
      "interface A{ default int f(){;} }" in {
        parse("interface A{ default int f(){;} }") shouldBe Interface(MethodDeclaration(
          Vector,
          Vector(Default),
          Vector,
          IntegerPrimitive(Vector),
          MethodDeclarator(new Name("f"), Vector),
          Vector,
          Block(Vector(EmptyStatement))))
      }
    }
    "annotations" - {
      "@A @interface A{}" in {
        parse("@A @interface A{}") shouldBe AnnotationDeclaration(
          Vector(MarkerAnnotation(new Name("A"))),
          Vector,
          new Name("A"),
          Vector)
      }
      "public @interface A{}" in {
        parse("public @interface A{}") shouldBe AnnotationDeclaration(
          Vector,
          Vector(Public),
          new Name("A"),
          Vector)
      }
      "protected @interface A{}" in {
        parse("protected @interface A{}") shouldBe AnnotationDeclaration(
          Vector,
          Vector(Protected),
          new Name("A"),
          Vector)
      }
      "private @interface A{}" in {
        parse("private @interface A{}") shouldBe AnnotationDeclaration(
          Vector,
          Vector(Private),
          new Name("A"),
          Vector)
      }
      "abstract @interface A{}" in {
        parse("abstract @interface A{}") shouldBe AnnotationDeclaration(
          Vector,
          Vector(Abstract),
          new Name("A"),
          Vector)
      }
      "static @interface A{}" in {
        parse("static @interface A{}") shouldBe AnnotationDeclaration(
          Vector,
          Vector(Static),
          new Name("A"),
          Vector)
      }
      "strictfp @interface A{}" in {
        parse("strictfp @interface A{}") shouldBe AnnotationDeclaration(
          Vector,
          Vector(Strictfp),
          new Name("A"),
          Vector)
      }
      "@interface A{ int a();}" in {
        parse("@interface A{ int a();}") shouldBe Annotation(AnnotationElementDeclaration(
          Vector,
          Vector,
          IntegerPrimitive(Vector),
          new Name("a")))
      }
      "@interface A{ @A int a();}" in {
        parse("@interface A{ @A int a();}") shouldBe Annotation(AnnotationElementDeclaration(
          Vector(MarkerAnnotation(new Name("A"))),
          Vector,
          IntegerPrimitive(Vector),
          new Name("a")))
      }
      "@interface A{ public int a();}" in {
        parse("@interface A{ public int a();}") shouldBe Annotation(AnnotationElementDeclaration(
          Vector,
          Vector(Public),
          IntegerPrimitive(Vector),
          new Name("a")))
      }
      "@interface A{ abstract int a();}" in {
        parse("@interface A{ abstract int a();}") shouldBe Annotation(AnnotationElementDeclaration(
          Vector,
          Vector(Abstract),
          IntegerPrimitive(Vector),
          new Name("a")))
      }
      "@interface A{ int a()[];}" in {
        parse("@interface A{ int a()[];}") shouldBe Annotation(AnnotationElementDeclaration(
          Vector,
          Vector,
          ArrayType(IntegerPrimitive(Vector), Vector(AbstractDimension(Vector))),
          new Name("a")))
      }
      "@interface A{ int a() default 3;}" in {
        parse("@interface A{ int a() default 3;}") shouldBe Annotation(AnnotationDefaultElementDeclaration(
          Vector,
          Vector,
          IntegerPrimitive(Vector),
          new Name("a"),
          IntegerLiteral("3")))
      }
    }
  }

}