package jvm.parallel.parsers.java

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression._
import jvm.parallel.ast.parsers.statement.Block
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.assignment.{AugmentedBinding, Binding}
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.{Extraction, QualifiedMethodInvocation}
import jvm.parallel.ast.parsers.statement.instructions.discardable.literals.{IntegerLiteral, NullLiteral}
import jvm.parallel.ast.parsers.statement.instructions.discardable.reference.Select
import jvm.parallel.ast.parsers.statement.instructions.discardable.unary.{PostDecrementation, PostIncrementation, PreDecrementation, PreIncrementation}
import jvm.parallel.ast.parsers.expression.operators.{BinaryOperator, UnaryOperator}
import jvm.parallel.ast.parsers.expression.types.coupled.ChildOfAll
import jvm.parallel.ast.parsers.expression.types.primitives.IntegerPrimitive
import jvm.parallel.ast.parsers.expression.types.references.ClassType
import jvm.parallel.ast.parsers.statement.instructions.flow.{ImplicitReturn, Return}
import jvm.parallel.ast.parsers.statement.modifers.Final
import jvm.parallel.ast.parsers.statement.parameter.{FormalParameter, InferredParameter}
import jvm.parallel.parsers.ParserTest

class ExpressionTest extends ParserTest {
  def parse(string: String): Expression = {
    implicit val parser = new Parser(string)
    get(parser.expression.run())
  }

  "Expression parser should parse" - {
    "reference" - {
      "a" in {parse("a") shouldBe Select(new Name("a"))}
      "trueHash" in {parse("trueHash") shouldBe Select(new Name("trueHash"))}
    }

    "lambda expressions" - {
      "() -> {}" in { parse("() -> {}") shouldBe Lambda(Vector, Block(Vector)) }
      "() -> 42" in { parse("() -> 42") shouldBe Lambda(Vector, ImplicitReturn(IntegerLiteral("42"))) }
      "() -> null" in { parse("() -> null") shouldBe Lambda(Vector, ImplicitReturn(NullLiteral)) }
      "() -> {return 42; }" in { parse("() -> {return 42; }") shouldBe Lambda(Vector, Block(Vector(Return(IntegerLiteral("42"))))) }
      "() -> {System.gc();}" in { parse("() -> {System.gc();}") shouldBe Lambda(Vector, Block(Vector
        (QualifiedMethodInvocation(Select(new Name("System")), Vector, new Name("gc"), Vector)))) }

      "(final int x) -> x+1" in {
        parse("(final int x) -> x+1") shouldBe Lambda(
          Vector(FormalParameter(Vector, Final, IntegerPrimitive(Vector), new Name("x"))),
          ImplicitReturn(BinaryOperations(BinaryOperator.+, Vector(Select(new Name("x")), IntegerLiteral("1")))))
      }
      "(int x) -> { return x+1; }" in {
        parse("(int x) -> { return x+1; }") shouldBe Lambda(
          Vector(FormalParameter(Vector, Vector, IntegerPrimitive(Vector), new Name("x"))),
          Block(Vector(Return(BinaryOperations(BinaryOperator.+, Vector(Select(new Name("x")), IntegerLiteral("1")))))))
      }
      "(x) -> x+1" in {
        parse("(x) -> x+1") shouldBe Lambda(
          Vector(InferredParameter(new Name("x"))),
          ImplicitReturn(BinaryOperations(BinaryOperator.+, Vector(Select(new Name("x")), IntegerLiteral("1")))))
      }
      "x -> x+1" in {
        parse("x -> x+1") shouldBe Lambda(
          Vector(InferredParameter(new Name("x"))),
          ImplicitReturn(BinaryOperations(BinaryOperator.+, Vector(Select(new Name("x")), IntegerLiteral("1")))))
      }

      "(String s) -> s.length()" in {
        parse("(String s) -> s.length()") shouldBe Lambda(
          Vector(FormalParameter(Vector, Vector, ClassType(Vector, None, new Name("String"), Vector), new Name("s"))),
          ImplicitReturn(QualifiedMethodInvocation(Select(new Name("s")), Vector, new Name("length"), Vector)))
      }
      "(Thread t) -> { t.start(); }" in {
        parse("(Thread t) -> { t.start(); }") shouldBe Lambda(
          Vector(FormalParameter(Vector, Vector, ClassType(Vector, None, new Name("Thread"), Vector), new Name("t"))),
          Block(QualifiedMethodInvocation(Select(new Name("t")), Vector, new Name("start"), Vector)))
      }
      "s -> s.length()" in {
        parse("s -> s.length()") shouldBe Lambda(
          Vector(InferredParameter(new Name("s"))),
          ImplicitReturn(QualifiedMethodInvocation(Select(new Name("s")), Vector, new Name("length"), Vector)))
      }
      "t -> { t.start(); }" in {
        parse("t -> { t.start(); }") shouldBe Lambda(
          Vector(InferredParameter(new Name("t"))),
          Block(QualifiedMethodInvocation(Select(new Name("t")), Vector, new Name("start"), Vector)))
      }

      "(int x, int y) -> x+y" in {
        parse("(int x, int y) -> x+y") shouldBe Lambda(
          Vector(FormalParameter(Vector, Vector, IntegerPrimitive(Vector), new Name("x")), FormalParameter(Vector, Vector,
            IntegerPrimitive(Vector), new Name("y"))),
          ImplicitReturn(BinaryOperations(BinaryOperator.+, Vector(Select(new Name("x")), Select(new Name("y"))))))
      }
      "(x, y) -> x+y" in {
        parse("(x, y) -> x+y") shouldBe Lambda(
          Vector(InferredParameter(new Name("x")), InferredParameter(new Name("y"))),
          ImplicitReturn(BinaryOperations(BinaryOperator.+, Vector(Select(new Name("x")), Select(new Name("y"))))))
      }
      "(x, int y) -> x+y " in { an[Exception] should be thrownBy parse("(x, int y) -> x+y ") }
      "(x, final y) -> x+y" in { an[Exception] should be thrownBy parse("(x, final y) -> x+y") }
    }

    "assignments" - {
      "expr.field = expr" in { parse("expr.field = expr") shouldBe Binding(Select(new Name(Vector("expr", "field"))),Select(new Name("expr")))
      }
      "array[1] = expr" in { parse("array[1] = expr") shouldBe Binding(Extraction(Select(new Name("array")), IntegerLiteral("1")), Select(new Name("expr"))) }

      "expr= expr" in { parse("expr= expr") shouldBe Binding(Select(new Name("expr")), Select(new Name("expr"))) }
      "expr =expr" in { parse("expr =expr") shouldBe Binding(Select(new Name("expr")), Select(new Name("expr"))) }

      "expr = expr" in { parse("expr = expr") shouldBe Binding(Select(new Name("expr")), Select(new Name("expr"))) }
      "expr *= expr" in { parse("expr *= expr") shouldBe AugmentedBinding(Select(new Name("expr")), BinaryOperator.*, Select(new Name("expr"))) }
      "expr /= expr" in { parse("expr /= expr") shouldBe AugmentedBinding(Select(new Name("expr")), BinaryOperator./, Select(new Name("expr"))) }
      "expr %= expr" in { parse("expr %= expr") shouldBe AugmentedBinding(Select(new Name("expr")), BinaryOperator.mod, Select(new Name("expr"))) }
      "expr += expr" in { parse("expr += expr") shouldBe AugmentedBinding(Select(new Name("expr")), BinaryOperator.+, Select(new Name("expr"))) }
      "expr -= expr" in { parse("expr -= expr") shouldBe AugmentedBinding(Select(new Name("expr")), BinaryOperator.-, Select(new Name("expr"))) }
      "expr <<= expr" in { parse("expr <<= expr") shouldBe AugmentedBinding(Select(new Name("expr")), BinaryOperator.<<, Select(new Name("expr"))) }
      "expr >>= expr" in { parse("expr >>= expr") shouldBe AugmentedBinding(Select(new Name("expr")), BinaryOperator.>>, Select(new Name("expr"))) }
      "expr >>>= expr" in { parse("expr >>>= expr") shouldBe AugmentedBinding(Select(new Name("expr")), BinaryOperator.>>>, Select(new Name("expr"))) }
      "expr &= expr" in { parse("expr &= expr") shouldBe AugmentedBinding(Select(new Name("expr")), BinaryOperator.&, Select(new Name("expr"))) }
      "expr ^= expr" in { parse("expr ^= expr") shouldBe AugmentedBinding(Select(new Name("expr")), BinaryOperator.^, Select(new Name("expr"))) }
      "expr |= expr" in { parse("expr |= expr") shouldBe AugmentedBinding(Select(new Name("expr")), BinaryOperator.|, Select(new Name("expr"))) }
    }
    "ternany" - {
      "x || y ? x : y" in {
        parse("x || y ? x : y") shouldBe TernaryConditional(
          BinaryOperations(BinaryOperator.or, Select(new Name("x")) +: Select(new Name("y"))),
          Select(new Name("x")),
          Select(new Name("y")))
      }
      "x || y ? x || y ? x : y : y" in {
        parse("x || y ? x || y ? x : y : y") shouldBe TernaryConditional(
          BinaryOperations(BinaryOperator.or, Select(new Name("x")) +: Select(new Name("y"))),
          TernaryConditional(
            BinaryOperations(BinaryOperator.or, Select(new Name("x")) +: Select(new Name("y"))),
            Select(new Name("x")),
            Select(new Name("y"))),
          Select(new Name("y")))
      }
      "x || y ? x : x || y ? x : y" in {
        parse("x || y ? x : x || y ? x : y") shouldBe TernaryConditional(
          BinaryOperations(BinaryOperator.or, Select(new Name("x")) +: Select(new Name("y"))),
          Select(new Name("x")),
          TernaryConditional(
            BinaryOperations(BinaryOperator.or, Select(new Name("x")) +: Select(new Name("y"))),
            Select(new Name("x")),
            Select(new Name("y"))))
      }
    }

    "binary operations" - {
      "x || y" in { parse("x || y") shouldBe BinaryOperations(BinaryOperator.or, Select(new Name("x")) +: Select(new Name("y"))) }
      "x && y" in { parse("x && y") shouldBe BinaryOperations(BinaryOperator.and, Select(new Name("x")) +: Select(new Name("y"))) }
      "x | y" in { parse("x | y") shouldBe BinaryOperations(BinaryOperator.|, Select(new Name("x")) +: Select(new Name("y"))) }
      "x ^ y" in { parse("x ^ y") shouldBe BinaryOperations(BinaryOperator.^, Select(new Name("x")) +: Select(new Name("y"))) }
      "x & y" in { parse("x & y") shouldBe BinaryOperations(BinaryOperator.&, Select(new Name("x")) +: Select(new Name("y"))) }
      "x == y" in { parse("x == y") shouldBe BinaryOperations(BinaryOperator.==, Select(new Name("x")) +: Select(new Name("y"))) }
      "x != y" in { parse("x != y") shouldBe BinaryOperations(BinaryOperator.!=, Select(new Name("x")) +: Select(new Name("y"))) }
      "x < y" in { parse("x < y") shouldBe BinaryOperations(BinaryOperator.<, Select(new Name("x")) +: Select(new Name("y"))) }
      "x <= y" in { parse("x <= y") shouldBe BinaryOperations(BinaryOperator.<=, Select(new Name("x")) +: Select(new Name("y"))) }
      "x > y" in { parse("x > y") shouldBe BinaryOperations(BinaryOperator.>, Select(new Name("x")) +: Select(new Name("y"))) }
      "x >= y" in { parse("x >= y") shouldBe BinaryOperations(BinaryOperator.>=, Select(new Name("x")) +: Select(new Name("y"))) }
      "x instanceof y" in { parse("x instanceof y") shouldBe BinaryOperations(BinaryOperator.instanceof, Select(new Name("x")) +: ClassType(Vector(), None, new Name("y"), Vector())) }
      "x << y" in { parse("x << y") shouldBe BinaryOperations(BinaryOperator.<<, Select(new Name("x")) +: Select(new Name("y"))) }
      "x >> y" in { parse("x >> y") shouldBe BinaryOperations(BinaryOperator.>>, Select(new Name("x")) +: Select(new Name("y"))) }
      "x >>> y" in { parse("x >>> y") shouldBe BinaryOperations(BinaryOperator.>>>, Select(new Name("x")) +: Select(new Name("y"))) }
      "x + y" in { parse("x + y") shouldBe BinaryOperations(BinaryOperator.+, Select(new Name("x")) +: Select(new Name("y"))) }
      "x - y" in { parse("x - y") shouldBe BinaryOperations(BinaryOperator.-, Select(new Name("x")) +: Select(new Name("y"))) }
      "x * y" in { parse("x * y") shouldBe BinaryOperations(BinaryOperator.*, Select(new Name("x")) +: Select(new Name("y"))) }
      "x / y" in { parse("x / y") shouldBe BinaryOperations(BinaryOperator./, Select(new Name("x")) +: Select(new Name("y"))) }
      "x % y" in { parse("x % y") shouldBe BinaryOperations(BinaryOperator.mod, Select(new Name("x")) +: Select(new Name("y"))) }

      "with correct priorities" in {
        parse("x % y / y * y - y + y >>> y >> y << y instanceof Class >= y > y <= y < y != y == y & y ^ y | y && y || y") shouldBe BinaryOperations(Vector(BinaryOperator.or),
          Vector(BinaryOperations(Vector(BinaryOperator.and), Vector(BinaryOperations(Vector(BinaryOperator.|), Vector(BinaryOperations(Vector(BinaryOperator.^), Vector(BinaryOperations(Vector(BinaryOperator.&), Vector(BinaryOperations(Vector(BinaryOperator.!=, BinaryOperator.==), Vector(BinaryOperations(Vector(BinaryOperator.instanceof, BinaryOperator.>=, BinaryOperator.>, BinaryOperator.<=, BinaryOperator.<), Vector(BinaryOperations(Vector(BinaryOperator.>>>, BinaryOperator.>>, BinaryOperator.<<), Vector(BinaryOperations(Vector(BinaryOperator.-, BinaryOperator.+), Vector(BinaryOperations(Vector(BinaryOperator.mod, BinaryOperator./, BinaryOperator.*), Vector(Select(new Name("x")), Select(new Name("y")), Select(new Name("y")), Select(new Name("y")))),
            Select(new Name("y")), Select(new Name("y")))),
            Select(new Name("y")), Select(new Name("y")), Select(new Name("y")))),
            ClassType(Vector, None, new Name("Class"), Vector), Select(new Name("y")), Select(new Name("y")), Select(new Name("y")), Select(new Name("y")))),
            Select(new Name("y")), Select(new Name("y")))),
            Select(new Name("y")))),
            Select(new Name("y")))),
            Select(new Name("y")))),
            Select(new Name("y")))),
            Select(new Name("y"))))
      }

    }
    "unary operations" - {
      "++i" in { parse("++i") shouldBe PreIncrementation(Select(new Name("i"))) }
      "--i" in { parse("--i") shouldBe PreDecrementation(Select(new Name("i"))) }
      "i++" in { parse("i++") shouldBe PostIncrementation(Select(new Name("i"))) }
      "i--" in { parse("i--") shouldBe PostDecrementation(Select(new Name("i"))) }
      "+ i" in { parse("+ i") shouldBe UnaryOperations(UnaryOperator.+, Select(new Name("i"))) }
      "-i" in { parse("-i") shouldBe UnaryOperations(UnaryOperator.-, Select(new Name("i"))) }
      "~i" in { parse("~i") shouldBe UnaryOperations(UnaryOperator.~, Select(new Name("i"))) }
      "!i" in { parse("!i") shouldBe UnaryOperations(UnaryOperator.not, Select(new Name("i"))) }
    }

    "casts" - {
      "(int) ++i" in {
        parse("(int) ++i") shouldBe Cast(IntegerPrimitive(Vector), PreIncrementation(Select(new Name("i"))))
      }
      "(Class) x" in {
        parse("(Class) x") shouldBe Cast(ClassType(Vector, None, new Name("Class"), Vector), Select(new
            Name("x")))
      }
      "(Class ) () -> 3" in {
        parse("(Class ) () -> 3") shouldBe Cast(ClassType(Vector, None, new Name("Class"),
          Vector), Lambda(Vector, ImplicitReturn(IntegerLiteral("3"))))
      }
      "(Class & I1 & I2) () -> 3" in {
        parse("(Class & I1 & I2) () -> 3") shouldBe Cast(ChildOfAll(Seq(ClassType
          (Vector, None, new Name("Class"), Vector), ClassType(Vector, None, new Name("I1"), Vector), ClassType(Vector, None,
          new Name("I2"), Vector))), Lambda(Vector, ImplicitReturn(IntegerLiteral("3"))))
      }
    }

  }
}