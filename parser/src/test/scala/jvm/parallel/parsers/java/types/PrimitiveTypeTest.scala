package jvm.parallel.parsers.java.types

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.expression.types.annotations.MarkerAnnotation
import jvm.parallel.ast.parsers.expression.types.primitives._
import jvm.parallel.parsers.ParserTest

class PrimitiveTypeTest extends ParserTest {
  def parse(string: String): Type = {
    implicit val parser = new Parser(string)
    get(parser.`type`.run())
  }

  "Primitive type parser should parse" - {
    "types" - {
      "byte" in { parse("byte") shouldBe BytePrimitive(Nil) }
      "short" in { parse("short") shouldBe ShortPrimitive(Nil) }
      "int" in { parse("int") shouldBe IntegerPrimitive(Nil) }
      "long" in { parse("long") shouldBe LongPrimitive(Nil) }
      "char" in { parse("char") shouldBe CharPrimitive(Nil) }
      "float" in { parse("float") shouldBe FloatPrimitive(Nil) }
      "double" in { parse("double") shouldBe DoublePrimitive(Nil) }
      "boolean" in { parse("boolean") shouldBe BooleanPrimitive(Nil) }
      "void" in { parse("void") shouldBe VoidPrimitive(Nil) }

      "with annotations" - {
        "@A byte" in { parse("@A byte") shouldBe BytePrimitive(MarkerAnnotation(new Name("A"))) }
        "@A short" in { parse("@A short") shouldBe ShortPrimitive(MarkerAnnotation(new Name("A"))) }
        "@A int" in { parse("@A int") shouldBe IntegerPrimitive(MarkerAnnotation(new Name("A"))) }
        "@A long" in { parse("@A long") shouldBe LongPrimitive(MarkerAnnotation(new Name("A"))) }
        "@A char" in { parse("@A char") shouldBe CharPrimitive(MarkerAnnotation(new Name("A"))) }
        "@A float" in { parse("@A float") shouldBe FloatPrimitive(MarkerAnnotation(new Name("A"))) }
        "@A double" in { parse("@A double") shouldBe DoublePrimitive(MarkerAnnotation(new Name("A"))) }
        "@A boolean" in { parse("@A boolean") shouldBe BooleanPrimitive(MarkerAnnotation(new Name("A"))) }
        "@A void" in { parse("@A void") shouldBe VoidPrimitive(MarkerAnnotation(new Name("A"))) }
      }
    }

  }
}