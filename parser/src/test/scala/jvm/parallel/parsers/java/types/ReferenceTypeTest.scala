package jvm.parallel.parsers.java.types

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.instructions.discardable.dimension.AbstractDimension
import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.expression.types.annotations.MarkerAnnotation
import jvm.parallel.ast.parsers.expression.types.primitives.IntegerPrimitive
import jvm.parallel.ast.parsers.expression.types.references.{ArrayType, ClassType}
import jvm.parallel.ast.parsers.expression.types.templates.{AnyBaseClassTemplate, AnySubClassTemplate, AnyTemplate, ArgumentTemplate}
import jvm.parallel.parsers.ParserTest

class ReferenceTypeTest extends ParserTest {
  def parse(string: String): Type = {
    implicit val parser = new Parser(string)
    get(parser.`type`.run())
  }

  "Reference parser should parse" - {

    "class or interface type" - {
      "Class<T.U>" in { parse("Class<T.U>") shouldBe ClassType(Vector, None, new Name("Class"), ArgumentTemplate(ClassType(Vector, 
        ClassType(Vector, None, new Name("T"), Vector), new Name("U"), Vector))) }
      "Class<?>" in { parse("Class<?>") shouldBe ClassType(Vector, None, new Name("Class"), AnyTemplate(Vector)) }
      "Class<? extends T>" in { parse("Class<? extends T>") shouldBe ClassType(Vector, None, new Name("Class"), AnySubClassTemplate(Vector, ClassType(Vector, None, new Name("T"), Vector))) }
      "Class<? super T>" in { parse("Class<? super T>") shouldBe ClassType(Vector, None, new Name("Class"), AnyBaseClassTemplate(Vector, ClassType(Vector, None, new Name("T"), Vector))) }
    	"Class<T>" in { parse("Class<T>") shouldBe ClassType(Vector, None, new Name("Class"), ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector))) }
      "Class.Inner<T,U>" in { parse("Class.Inner<T,U>") shouldBe ClassType(Vector, ClassType(Vector, None, new Name("Class"), Vector), new Name("Inner"), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector)), ArgumentTemplate(ClassType(Vector, None, new Name("U"), Vector)))) }
      "Class<T,U>" in { parse("Class<T,U>") shouldBe ClassType(Vector, None, new Name("Class"), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector)), ArgumentTemplate(ClassType(Vector, None, new Name("U"), Vector)))) }
      "Class<T,U>.Inner" in { parse("Class<T,U>.Inner") shouldBe ClassType(Vector, ClassType(Vector, None, new Name("Class"), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector)), ArgumentTemplate(ClassType(Vector, None, new Name("U"), Vector)))), new Name("Inner"), Vector) }
      "Class<T,U>.Inner<T,U>" in { parse("Class<T,U>.Inner<T,U>") shouldBe ClassType(Vector, ClassType(Vector, None, new Name("Class"), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector)), ArgumentTemplate(ClassType(Vector, None, new Name("U"), Vector)))), new Name("Inner"), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector)), ArgumentTemplate(ClassType(Vector, None, new Name("U"), Vector)))) }
    }

    "with annotation" - {
      "@A Class" in { parse("@A Class") shouldBe ClassType(MarkerAnnotation(new Name("A")), None, new Name("Class"), Vector) }
      "@A Class.Inner" in { parse("@A Class.Inner") shouldBe ClassType(Vector, ClassType(MarkerAnnotation(new Name("A")), None, new Name("Class"), Vector), new Name("Inner"), Vector) }
      "@A Class. @A Inner" in { parse("@A Class. @A Inner") shouldBe ClassType(MarkerAnnotation(new Name("A")), ClassType(MarkerAnnotation(new Name("A")), None, new Name("Class"), Vector), new Name("Inner"), Vector) }
      "@A Class.@A Inner<T,U>" in { parse("@A Class.@A Inner<T,U>") shouldBe ClassType(MarkerAnnotation(new Name("A")), ClassType(MarkerAnnotation(new Name("A")), None, new Name("Class"), Vector), new Name("Inner"), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector)), ArgumentTemplate(ClassType(Vector, None, new Name("U"), Vector)))) }
      "@A Class<T>" in { parse("@A Class<T>") shouldBe ClassType(MarkerAnnotation(new Name("A")), None, new Name("Class"), ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector))) }
      "@A Class<T,U>" in { parse("@A Class<T,U>") shouldBe ClassType(MarkerAnnotation(new Name("A")), None, new Name("Class"), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector)), ArgumentTemplate(ClassType(Vector, None, new Name("U"), Vector)))) }
      "Class<T,U>.@A Inner" in { parse("Class<T,U>.@A Inner") shouldBe ClassType(MarkerAnnotation(new Name("A")), ClassType(Vector, None, new Name("Class"), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector)), ArgumentTemplate(ClassType(Vector, None, new Name("U"), Vector)))), new Name("Inner"), Vector) }
      "@A Class<T,U>.@A Inner<T,U>" in { parse("@A Class<T,U>.@A Inner<T,U>") shouldBe ClassType(MarkerAnnotation(new Name("A")), ClassType(MarkerAnnotation(new Name("A")), None, new Name("Class"), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector)), ArgumentTemplate(ClassType(Vector, None, new Name("U"), Vector)))), new Name("Inner"), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector)), ArgumentTemplate(ClassType(Vector, None, new Name("U"), Vector)))) }
    }
    "array type" - {
      "int []" in { parse("int []") shouldBe ArrayType(IntegerPrimitive(Vector), Vector(AbstractDimension(Vector))) }
      "Class[]" in { parse("Class[]") shouldBe ArrayType(ClassType(Vector, None, new Name("Class"), Vector), Vector(AbstractDimension(Vector))) }
      "Class@A[]" in { parse("Class@A[]") shouldBe ArrayType(ClassType(Vector, None, new Name("Class"), Vector), Vector(AbstractDimension(Vector(MarkerAnnotation(new Name("A")))))) }
      "Class @A [] @A @A[]" in { parse("Class @A [] @A @A[]") shouldBe ArrayType(ClassType(Vector, None, new Name("Class"), Vector), Vector(AbstractDimension(Vector(MarkerAnnotation(new Name("A")))), AbstractDimension(Vector(MarkerAnnotation(new Name("A")), MarkerAnnotation(new Name("A")))))) }
    }
  }
}