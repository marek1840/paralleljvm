package jvm.parallel.parsers.java

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.statement.instructions.discardable.dimension.AbstractDimension
import jvm.parallel.ast.parsers.statement.instructions.discardable.literals.ClassLiteral
import jvm.parallel.ast.parsers.expression.types.primitives.{IntegerPrimitive, VoidPrimitive}
import jvm.parallel.ast.parsers.expression.types.references.{ArrayType, ClassType}
import jvm.parallel.parsers.ParserTest
import org.parboiled2.ParserInput.apply

class ClassLiteralTest extends ParserTest {
  def parse(string: String): Expression = {
    implicit val parser = new Parser(string)
    get(parser.expression.run())
  }

  "Class literal parser should" - {
    "parse" - {
      "int.class" in { parse("int.class") shouldBe ClassLiteral(IntegerPrimitive(Vector)) }
      "name.class" in (parse("name.class") shouldBe ClassLiteral(ClassType(Vector, None, new Name("name"), Vector)))
      "name . class" in (parse("name . class") shouldBe ClassLiteral(ClassType(Vector, None, new Name("name"), Vector)))
      "void.class" in (parse("void.class") shouldBe ClassLiteral(VoidPrimitive(Vector)))
      "name1.name2.class" in (parse("name1.name2.class") shouldBe ClassLiteral(ClassType(Vector, Some(ClassType(Vector, None, new Name("name1"), Vector)), new Name("name2"), Vector)))
      "name[].class" in (parse("name[].class") shouldBe ClassLiteral(ArrayType(ClassType(Vector, None, new Name("name"), Vector), Vector(AbstractDimension(Vector)))))
      "name[][].class" in (parse("name[][].class") shouldBe ClassLiteral(ArrayType(ClassType(Vector, None, new Name("name"), Vector), Vector(AbstractDimension(Vector), AbstractDimension(Vector)))))
      "name1.name2[][].class" in (parse("name1.name2[][].class") shouldBe ClassLiteral(ArrayType(ClassType(Vector, Some(ClassType(Vector, None, new Name("name1"), Vector)), new Name("name2"), Vector), Vector(AbstractDimension(Vector), AbstractDimension(Vector)))))
      "name1 . name2[][] . class" in (parse("name1 . name2[][] . class") shouldBe ClassLiteral(ArrayType(ClassType(Vector, Some(ClassType(Vector, None, new Name("name1"), Vector)), new Name("name2"), Vector), Vector(AbstractDimension(Vector), AbstractDimension(Vector)))))
    }
    "not parse" - {
      "class.class" in (an[Exception] should be thrownBy parse("class.class"))
    }
  }
}