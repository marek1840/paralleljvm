package jvm.parallel.parsers.java

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.BinaryOperations
import jvm.parallel.ast.parsers.statement.instructions._
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.assignment.Binding
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.{MethodInvocation, QualifiedMethodInvocation}
import jvm.parallel.ast.parsers.statement.instructions.discardable.dimension.AbstractDimension
import jvm.parallel.ast.parsers.statement.instructions.discardable.instantiation.SimpleObjectInstantiation
import jvm.parallel.ast.parsers.statement.instructions.discardable.literals.IntegerLiteral
import jvm.parallel.ast.parsers.statement.instructions.discardable.reference.{Select, ThisReference}
import jvm.parallel.ast.parsers.statement.instructions.discardable.unary.{PostDecrementation, PostIncrementation, PreDecrementation, PreIncrementation}
import jvm.parallel.ast.parsers.expression.operators.BinaryOperator
import jvm.parallel.ast.parsers.expression.types.annotations.MarkerAnnotation
import jvm.parallel.ast.parsers.expression.types.primitives.IntegerPrimitive
import jvm.parallel.ast.parsers.expression.types.references.{ArrayType, ClassType}
import jvm.parallel.ast.parsers.statement._
import jvm.parallel.ast.parsers.statement.instructions.conditional.{If, IfThenElse}
import jvm.parallel.ast.parsers.statement.declarator.{InitializedArrayDeclarator, InitializedVariableDeclarator, VariableDeclarator}
import jvm.parallel.ast.parsers.statement.instructions.flow._
import jvm.parallel.ast.parsers.statement.instructions.interruptable._
import jvm.parallel.ast.parsers.statement.instructions.loop.{DoWhile, For, Iteration, While}
import jvm.parallel.ast.parsers.statement.modifers.Final
import jvm.parallel.ast.parsers.statement.instructions.switch.{DefaultSwitch, EmptySwitchCases, SwitchCases, SwitchStatement}
import jvm.parallel.parsers.ParserTest

class StatementsTest extends ParserTest {
  def parse(string: String): Statement = {
    implicit val parser = new Parser(string)
    get(parser.blockStatement.run())
  }

  "Statement parser should parse" - {
    "local variable declaration" - {
      "int a;" in {
        parse("int a;") shouldBe LocalVariableDeclaration(Vector, Vector, IntegerPrimitive(Vector), Vector(VariableDeclarator(new Name("a"))))
      }
      "@A  int a;" in {
        parse("@A  int a;") shouldBe LocalVariableDeclaration(Vector(MarkerAnnotation(new Name("A"))), Vector, IntegerPrimitive(Vector), Vector(VariableDeclarator(new Name("a"))))
      }
      "final int a;" in {
        parse("final int a;") shouldBe LocalVariableDeclaration(Vector, Final, IntegerPrimitive(Vector), Vector(VariableDeclarator(new Name("a"))))
      }
      "@A final int a;" in {
        parse("@A final int a;") shouldBe LocalVariableDeclaration(Vector(MarkerAnnotation(new Name("A"))), Final, IntegerPrimitive(Vector), Vector(VariableDeclarator(new Name("a"))))
      }
      "@A final @A int a;" in {
        parse("@A final @A int a;") shouldBe LocalVariableDeclaration(Vector(MarkerAnnotation(new Name("A")), MarkerAnnotation(new Name("A"))), Final, IntegerPrimitive(Vector), Vector(VariableDeclarator(new Name("a"))))
      }
      "int a, y[] = x;" in {
        parse("int a, y[] = x;") shouldBe LocalVariableDeclaration(Vector, Vector,
          IntegerPrimitive(Vector), Vector(VariableDeclarator(new Name("a")), InitializedArrayDeclarator(new Name("y"), Vector(AbstractDimension(Vector)), Select(new Name("x")))))
      }
      "Class a, b = new Class(), c;" in {
        parse("Class a, b = new Class(), c;") shouldBe
          LocalVariableDeclaration(Vector(), Vector, ClassType(Vector, None, new Name("Class"), Vector), Vector(VariableDeclarator(new Name("a")),
          InitializedVariableDeclarator(new Name("b"), SimpleObjectInstantiation(Vector, ClassType(Vector, None,
            new Name( "Class"), Vector), Vector)), VariableDeclarator(new Name("c"))))
      }
    }
    "discardable expression statements" - {
      "a = b;" in { parse("a = b;") shouldBe Binding(Select(new Name("a")), Select(new Name("b"))) }
      "a++;" in { parse("a++;") shouldBe PostIncrementation(Select(new Name("a"))) }
      "a--;" in { parse("a--;") shouldBe PostDecrementation(Select(new Name("a"))) }
      "--a;" in { parse("--a;") shouldBe PreDecrementation(Select(new Name("a"))) }
      "++a;" in { parse("++a;") shouldBe PreIncrementation(Select(new Name("a"))) }
      "a().f();" in { parse("a().f();") shouldBe QualifiedMethodInvocation(MethodInvocation(Vector, new Name("a"), Vector), Vector, new Name("f"), Vector) }
      "new Class();" in { parse("new Class();") shouldBe SimpleObjectInstantiation(Vector, ClassType(Vector, None,
        new Name("Class"), Vector),
        Vector) }
    }
    "assertions" - {
      "assert x;" in { parse("assert x;") shouldBe Assertion(Select(new Name("x")), None) }
      "assert x: y;" in { parse("assert x: y;") shouldBe Assertion(Select(new Name("x")), Some(Select(new Name("y")))) }
    }
    "flow statements" - {
      "break;" in { parse("break;") shouldBe Break }
      "break a;" in { parse("break a;") shouldBe TargetedBreak(new Name("a")) }
      "continue;" in { parse("continue;") shouldBe Continue }
      "continue a;" in { parse("continue a;") shouldBe TargetedContinue(new Name("a")) }
      "return;" in { parse("return;") shouldBe EmptyReturn }
      "return a;" in { parse("return a;") shouldBe Return(Select(new Name("a"))) }
      "throw a;" in { parse("throw a;") shouldBe Throw(Select(new Name("a"))) }
    }
    "blocks" - {
      "{int a;}" in { parse("{int a;}") shouldBe Block(Vector(LocalVariableDeclaration(Vector, Vector, IntegerPrimitive(Vector), Vector(VariableDeclarator(new Name("a"))))))}
      "{int a = 3;}" in {
        parse("{int a = 3;}") shouldBe Block(Vector(LocalVariableDeclaration(Vector, Vector,
          IntegerPrimitive(Vector),
          Vector(InitializedVariableDeclarator(new Name("a"), IntegerLiteral("3"))))))
      }
    }
    "synchronized blocks" - {
      "synchronized {int x;}" in { parse("synchronized {int x;}") shouldBe SynchronizedBlock(None, Block(LocalVariableDeclaration(Vector, Vector, IntegerPrimitive(Vector), Vector(VariableDeclarator(new Name("x")))))) }
      "synchronized(this) {int x;}" in { parse("synchronized(this) {int x;}") shouldBe SynchronizedBlock(Some(ThisReference), Block(LocalVariableDeclaration(Vector, Vector, IntegerPrimitive(Vector), Vector(VariableDeclarator(new Name("x")))))) }
    }
    "try statements" - {
      val tb = Block(Vector(LocalVariableDeclaration(Vector, Vector, IntegerPrimitive(Vector), Vector(VariableDeclarator
        (new Name("x"))))))
      val fb = Block(Vector())
      val cs = Vector(CatchClause(LocalVariableDeclaration(Vector, Vector, ClassType(Vector, None, new Name("Exception"),
        Vector), VariableDeclarator(new Name("e"))), Block(Vector)))

      "try {int x;} catch(Exception e){}" in { parse("try {int x;} catch(Exception e){}") shouldBe TryCatch(tb, cs) }
      "try {int x;} finally{}" in { parse("try {int x;} finally{}") shouldBe TryFinally(tb, fb) }
      "try {int x;} catch(Exception e){} finally{}" in { parse("try {int x;} catch(Exception e){} finally{}") shouldBe TryCatchFinally(tb, cs, fb) }
      "try(C c = new C()){int x;} catch(Exception e){} finally{}" in { parse("try(C c = new C()){int x;} catch(Exception e){} finally{}") shouldBe TryWithResources(Vector(LocalVariableDeclaration(Vector, Vector, ClassType(Vector, None, new Name("C"), Vector), Vector(InitializedVariableDeclarator(new Name("c"), SimpleObjectInstantiation(Vector, ClassType(Vector, None, new Name("C"), Vector), Vector))))), tb, cs, Some(fb)) }
    }
    "switch statements" - {
      val s =
        """switch(a){
        |     case 0:
        |     case 1: ;
        |     default: break;
        |     case Enum.Value: ;
        |     case 3:
        |     case 4:
        |}""".stripMargin

      val s2 =
        """switch (data){
        |     default:
        |        packet = new Packet(datagram);
        |        break;
        |}""".stripMargin

      "switch(a){}" in { parse("switch(a){}") shouldBe SwitchStatement(Select(new Name("a")), Vector) }
      s in {
        parse(s) shouldBe SwitchStatement(Select(new Name("a")), Vector(
          SwitchCases(Vector(IntegerLiteral("0"), IntegerLiteral("1")), EmptyStatement),
          SwitchCases(Vector(DefaultSwitch), Break),
          SwitchCases(Vector(Select(new Name(Vector("Enum", "Value")))), EmptyStatement),
          EmptySwitchCases(Vector(IntegerLiteral("3"), IntegerLiteral("4")))))
      }
      s2 in {
        parse(s2) shouldBe SwitchStatement(Select(new Name("data")), Vector(
          SwitchCases(Vector(DefaultSwitch), Vector(
            Binding(Select(new Name("packet")), SimpleObjectInstantiation(Vector(), ClassType(Vector(), None, new
                Name("Packet"), Vector()), Vector(Select(new Name("datagram"))))),
            Break))))
      }
    }
    "labeled statements" - {
      "label: {}" in { parse("label: {}") shouldBe LabeledStatement(new Name("label"), Block(Vector)) }
    }
    "conditional statements" - {
      "if(a) {}" in { parse("if(a) {}") shouldBe If(Select(new Name("a")), Block(Vector)) }
      "if(a) {} else {}" in { parse("if(a) {} else {}") shouldBe IfThenElse(Select(new Name("a")), Block(Vector), Block(Vector)) }
      "if(a) if(b) {} else {}" in { parse("if(a) if(b) {} else {}") shouldBe If(Select(new Name("a")), IfThenElse(Select(new Name("b")), Block(Vector), Block(Vector))) }
      "if(a) if(b) {} else {} else{}" in { parse("if(a) if(b) {} else {} else{}") shouldBe IfThenElse(Select(new Name("a")), IfThenElse(Select(new Name("b")), Block(Vector), Block(Vector)), Block(Vector)) }
    }
    "loops" - {
      "for(;;);" in {
        parse("for(;;);") shouldBe For(Vector, None, Vector, EmptyStatement)
      }
      "for (;a instanceof Object[];);" in {
        parse("for (;a instanceof Object[];);") shouldBe For(Vector, Some
          (BinaryOperations(BinaryOperator.instanceof, Vector(Select(new Name("a")), ArrayType(ClassType(Vector, None,
            new Name("Object"), Vector), Seq(AbstractDimension(Vector)))))), Vector, EmptyStatement)
      }
      "for(f(); i < 10; ++i)" in {
        parse("for(f(); i < 10; ++i){}") shouldBe For(Vector(MethodInvocation(Vector, new Name("f"), Vector)), BinaryOperations(BinaryOperator.<, Vector(Select(new Name("i")), IntegerLiteral("10"))), Vector(PreIncrementation(Select(new Name("i")))), Block(Vector))
      }
      "for(int i = 0, j = i; i < 10; ++i, j++){}" in {
        parse("for(int i = 0, j=i; i < 10; ++i, j++){}") shouldBe For(Vector(LocalVariableDeclaration(Vector, Vector,
          IntegerPrimitive(Vector), Vector(InitializedVariableDeclarator(new Name("i"), IntegerLiteral("0")),
            InitializedVariableDeclarator(new Name("j"), Select(new Name("i")))))), BinaryOperations(BinaryOperator.<, Vector(Select(new Name("i")), IntegerLiteral("10"))), Vector(PreIncrementation(Select(new Name("i"))), PostIncrementation(Select(new Name("j")))), Block(Vector))
      }
      "for(X x :xs){}" in {
        parse("for(X x :xs){}") shouldBe Iteration(LocalVariableDeclaration(Vector, Vector, ClassType(Vector, None,
          new Name("X"), Vector), Vector(VariableDeclarator(new Name("x")))), Select(new Name("xs")), Block(Vector))
      }
      "do ++a; while (b);" in {
        parse("do ++a; while (b);") shouldBe DoWhile(PreIncrementation(Select(new Name("a"))), Select(new Name("b")))
      }
      "while (b) ++a;" in {
        parse("while (b) ++a;") shouldBe While(Select(new Name("b")), PreIncrementation(Select(new Name("a"))))
      }
    }

  }
}