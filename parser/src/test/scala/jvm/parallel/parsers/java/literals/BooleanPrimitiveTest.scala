package jvm.parallel.parsers.java.literals

import jvm.parallel.ast.parsers.statement.instructions.discardable.literals.{BooleanLiteral, Literal}
import jvm.parallel.parsers.ParserTest

class BooleanPrimitiveTest extends ParserTest {
  def parse(string: String): Literal = {
    implicit val parser = new Parser(string)
    get(parser.literal.run())
  }

  "Boolean parser should parse" - {
    "true" in (parse("true") shouldBe BooleanLiteral("true"))
    "false" in (parse("false") shouldBe BooleanLiteral("false"))
  }
}