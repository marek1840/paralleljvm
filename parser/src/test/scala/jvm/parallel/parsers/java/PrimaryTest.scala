package jvm.parallel.parsers.java

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary._
import jvm.parallel.ast.parsers.statement.instructions.discardable.dimension.{AbstractDimension, InitializedDimension}
import jvm.parallel.ast.parsers.statement.instructions.discardable.instantiation._
import jvm.parallel.ast.parsers.statement.instructions.discardable.literals.{ClassLiteral, IntegerLiteral, StringLiteral}
import jvm.parallel.ast.parsers.statement.instructions.discardable.reference.{ParentReference, Select, ThisReference}
import jvm.parallel.ast.parsers.statement.instructions.discardable.{ArrayConstructorReference, ConstructorReference}
import jvm.parallel.ast.parsers.expression.types.annotations.MarkerAnnotation
import jvm.parallel.ast.parsers.expression.types.primitives.IntegerPrimitive
import jvm.parallel.ast.parsers.expression.types.references.{ArrayType, ClassType}
import jvm.parallel.ast.parsers.expression.types.templates.ArgumentTemplate
import jvm.parallel.ast.parsers.expression.{ArrayInitializer, Expression}
import jvm.parallel.ast.parsers.statement.declaration.types.EmptyDeclaration
import jvm.parallel.parsers.ParserTest

class PrimaryTest extends ParserTest {
  def parse(string: String): Expression = {
    implicit val parser = new Parser(string)
    get(parser.expression.run())
  }

  "Primary parser should parse" - {
    "this" in (parse("this") shouldBe ThisReference)
    "(this)" in (parse("(this)") shouldBe ThisReference)
    "((a))" in { parse("((a))") shouldBe Select(new Name("a")) }
    "longish.qualifier.this" in (parse("longish.qualifier.this") shouldBe QualifiedThisReference(new Name(Vector("longish", 
      "qualifier"))))
    "literals" - {
      "1" in (parse("1") shouldBe IntegerLiteral("1"))
      "test.class" in (parse("test.class") shouldBe ClassLiteral(ClassType(Vector, None, new Name("test"), Vector)))
      "\"aa\"" in (parse("\"aa\"") shouldBe StringLiteral("\"aa\""))
    }
    "object creation" - {
      "new Class()" in { parse("new Class()") shouldBe SimpleObjectInstantiation(Vector, ClassType(Vector(), None,
        new Name("Class"),
        Vector),
        Vector()) }
      "new <T> Class()" in {
        parse("new <T> Class()") shouldBe SimpleObjectInstantiation(Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector))), ClassType(Vector(), None, new Name("Class"), Vector), Vector())
      }
      "new <T> @A Class()" in {
        parse("new <T> @A Class()") shouldBe SimpleObjectInstantiation(Vector(ArgumentTemplate(ClassType(Vector, 
          None, new Name("T"), Vector))), ClassType(Vector(MarkerAnnotation(new Name("A"))), None, new Name("Class"), Vector), Vector())
      }
      "new <T> @A Class<U,V>()" in {
        parse("new <T> @A Class<U,V>()") shouldBe SimpleObjectInstantiation(Vector(ArgumentTemplate(ClassType(Vector,
          None, new Name("T"), Vector))), ClassType(Vector(MarkerAnnotation(new Name("A"))), None, new Name("Class"), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("U"), Vector)), ArgumentTemplate(ClassType(Vector, None, new Name("V"), Vector)))), Vector())
      }
      "new <T> @A Class<U,V>(arg1, arg2)" in {
        parse("new <T> @A Class<U,V>(arg1, arg2)") shouldBe SimpleObjectInstantiation(Vector(ArgumentTemplate
          (ClassType(Vector, None, new Name("T"), Vector))), ClassType(Vector(MarkerAnnotation(new Name("A"))), None,
          new Name("Class"), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("U"), Vector)), ArgumentTemplate
            (ClassType(Vector, None, new Name("V"), Vector)))), Vector(Select(new Name("arg1")), Select(new Name("arg2"))))
      }
      "new <T> @A Class<>(arg1, arg2)" in {
        parse("new <T> @A Class<>(arg1, arg2)") shouldBe SimpleObjectInstantiation(Vector(ArgumentTemplate(ClassType
          (Vector, None, new Name("T"), Vector))), ClassType(Vector(MarkerAnnotation(new Name("A"))), None, new Name("Class"), 
          Vector()), Vector(Select(new Name("arg1")), Select(new Name("arg2"))))
      }
      "expr.name. new Class<T>()" in {
        parse("expr.name. new Class<T>()") shouldBe NestedObjectInstantiation(Select(new Name(Vector("expr", "name"))
        ), Vector, ClassType(Vector(), None, new Name("Class"), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector)))), Vector)
      }
      "expr.name.new <T> @A Class<U,V>(arg1, arg2)" in {
        parse("expr.name.new <T> @A Class<U,V>(arg1, arg2)") shouldBe NestedObjectInstantiation(Select(new Name(Vector("expr", "name"))), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector))), ClassType(Vector(MarkerAnnotation(new Name("A"))), None, new Name("Class"), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("U"), Vector)), ArgumentTemplate(ClassType(Vector, None, new Name("V"), Vector)))), Vector(Select(new Name("arg1")), Select(new Name("arg2"))))
      }
      "invoke.new <T> @A Class<U,V>(arg1, arg2)" in {
        parse("invoke.new <T> @A Class<U,V>(arg1, arg2)") shouldBe NestedObjectInstantiation(
          Select(new Name("invoke")),
          Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector))), ClassType(Vector(MarkerAnnotation(new Name("A"))), None, new Name("Class"), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("U"), Vector)), ArgumentTemplate(ClassType(Vector, None, new Name("V"), Vector)))), Vector(Select(new Name("arg1")), Select(new Name("arg2"))))
      }
    }
    "anonymous object creation" - {
      "new Class(){;}" in {
        parse("new Class(){;}") shouldBe AnonymousObjectInstantiation(Vector, ClassType(Vector(), None, new Name("Class"), Vector), Vector(), Vector(EmptyDeclaration))
      }
      "new <T>Class(){;}" in {
        parse("new <T>Class(){;}") shouldBe AnonymousObjectInstantiation(Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector))), ClassType(Vector(), None, new Name("Class"), Vector), Vector(), Vector(EmptyDeclaration))
      }
      "new <T> @A Class(){;}" in {
        parse("new <T> @A Class(){;}") shouldBe AnonymousObjectInstantiation(Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector))), ClassType(Vector(MarkerAnnotation(new Name("A"))), None, new Name("Class"), Vector), Vector(), Vector(EmptyDeclaration))
      }
      "new <T> @A Class<U,V>(){;}" in {
        parse("new <T> @A Class<U,V>(){;}") shouldBe AnonymousObjectInstantiation(Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector))), ClassType(Vector(MarkerAnnotation(new Name("A"))), None, new Name("Class"), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("U"), Vector)), ArgumentTemplate(ClassType(Vector, None, new Name("V"), Vector)))), Vector(), Vector(EmptyDeclaration))
      }
      "new <T> @A Class<U,V>(arg1, arg2){;}" in {
        parse("new <T> @A Class<U,V>(arg1, arg2){;}") shouldBe AnonymousObjectInstantiation(Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector))), ClassType(Vector(MarkerAnnotation(new Name("A"))), None, new Name("Class"), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("U"), Vector)), ArgumentTemplate(ClassType(Vector, None, new Name("V"), Vector)))), Vector(Select(new Name("arg1")), Select(new Name("arg2"))), Vector(EmptyDeclaration))
      }
      "new <T> @A Class<>(arg1, arg2){;}" in {
        parse("new <T> @A Class<>(arg1, arg2){;}") shouldBe
          AnonymousObjectInstantiation(Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector))),
            ClassType(Vector(MarkerAnnotation(new Name("A"))), None, new Name("Class"), Vector()), Vector(Select(new
                Name("arg1")), Select(new Name("arg2"))), Vector(EmptyDeclaration))
      }
      "expr.name. new Class<T>(){;}" in {
        parse("expr.name. new Class<T>(){;}") shouldBe NestedAnonymousObjectInstantiation(Select(new Name(Vector("expr", "name"))), Vector, ClassType(Vector(), None, new Name("Class"), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector)))), Vector, Vector(EmptyDeclaration))
      }
      "expr.name.new <T> @A Class<U,V>(arg1, arg2){;}" in {
        parse("expr.name.new <T> @A Class<U,V>(arg1, arg2){;}") shouldBe NestedAnonymousObjectInstantiation(Select
          (new Name(Vector("expr", "name"))), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector))), 
          ClassType(Vector(MarkerAnnotation(new Name("A"))), None, new Name("Class"), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("U"), Vector)), ArgumentTemplate(ClassType(Vector, None, new Name("V"), Vector)))), Vector(Select(new Name("arg1")), Select(new Name("arg2"))), Vector(EmptyDeclaration))
      }
      "invoke.new <T> @A Class<U,V>(arg1, arg2){;}" in {
        parse("invoke.new <T> @A Class<U,V>(arg1, arg2){;}") shouldBe NestedAnonymousObjectInstantiation(Select(new Name("invoke")), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector))), ClassType(Vector(MarkerAnnotation(new Name("A"))), None, new Name("Class"), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("U"), Vector)), ArgumentTemplate(ClassType(Vector, None, new Name("V"), Vector)))), Vector(Select(new Name("arg1")), Select(new Name("arg2"))), Vector(EmptyDeclaration))
      }
    }
    "field access" - {
      "super.field" in { parse("super.field") shouldBe FieldAccess(ParentReference, new Name("field")) }
      "Class.Name.super.field" in { parse("Class.Name.super.field") shouldBe FieldAccess(QualifiedParentReference(new Name(Vector("Class", "Name"))), new Name("field")) }
      "f().field" in { parse("f().field") shouldBe FieldAccess(MethodInvocation(Vector, new Name("f"), Vector), new Name("field")) }
      "f[0].field" in { parse("f[0].field") shouldBe FieldAccess(Extraction(Select(new Name("f")), IntegerLiteral("0")), 
        new Name("field")) }
      "f.f1.f2" in { parse("f.f1.f2") shouldBe Select(new Name(Vector("f", "f1", "f2"))) }
    }
    "array access" - {
      "f().field[1]" in { parse("f().field[1]") shouldBe Extraction(FieldAccess(MethodInvocation(Vector, new Name("f"), Vector), new Name("field")), IntegerLiteral("1")) }
      "f.field[1]" in { parse("f.field[1]") shouldBe Extraction(Select(new Name(Vector("f", "field"))), IntegerLiteral
        ("1")) }
      "f.f1.f2[1]" in { parse("f.f1.f2[1]") shouldBe Extraction(Select(new Name(Vector("f", "f1", "f2"))), IntegerLiteral("1")) }
    }
    "array creation" - {
      "new int [1]" in { parse("new int [1]") shouldBe EmptyArrayInstantiation(IntegerPrimitive(Vector), Vector(InitializedDimension(Vector, IntegerLiteral("1")))) }
      "new int [1][1]" in { parse("new int [1][1]") shouldBe EmptyArrayInstantiation(IntegerPrimitive(Vector), Vector(InitializedDimension(Vector, IntegerLiteral("1")), InitializedDimension(Vector, IntegerLiteral("1")))) }
      "new int @A [1]@A[1][][]@A[]" in { parse("new int @A [1]@A[1][][]@A[]") shouldBe EmptyArrayInstantiation(IntegerPrimitive(Vector), Vector(InitializedDimension(Vector(MarkerAnnotation(new Name("A"))), IntegerLiteral("1")), InitializedDimension(Vector(MarkerAnnotation(new Name("A"))), IntegerLiteral("1")), AbstractDimension(Vector), AbstractDimension(Vector), AbstractDimension(Vector(MarkerAnnotation(new Name("A")))))) }
      "new Class @A [1]@A[1][][]@A[]" in { parse("new Class @A [1]@A[1][][]@A[]") shouldBe EmptyArrayInstantiation(ClassType(Vector, None, new Name("Class"), Vector), Vector(InitializedDimension(Vector(MarkerAnnotation(new Name("A"))), IntegerLiteral("1")), InitializedDimension(Vector(MarkerAnnotation(new Name("A"))), IntegerLiteral("1")), AbstractDimension(Vector), AbstractDimension(Vector), AbstractDimension(Vector(MarkerAnnotation(new Name("A")))))) }
      "new Class<T> @A [1]@A[1][][]@A[]" in { parse("new Class<T> @A [1]@A[1][][]@A[]") shouldBe EmptyArrayInstantiation(ClassType(Vector, None, new Name("Class"), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector)))), Vector(InitializedDimension(Vector(MarkerAnnotation(new Name("A"))), IntegerLiteral("1")), InitializedDimension(Vector(MarkerAnnotation(new Name("A"))), IntegerLiteral("1")), AbstractDimension(Vector), AbstractDimension(Vector), AbstractDimension(Vector(MarkerAnnotation(new Name("A")))))) }
      "new Class.@A Class @A [1]@A[1][][]@A[]" in { parse("new Class.@A Class @A [1]@A[1][][]@A[]") shouldBe EmptyArrayInstantiation(ClassType(Vector(MarkerAnnotation(new Name("A"))), ClassType(Vector(), None, new Name("Class"), Vector()), new Name("Class"), Vector), Vector(InitializedDimension(Vector(MarkerAnnotation(new Name("A"))), IntegerLiteral("1")), InitializedDimension(Vector(MarkerAnnotation(new Name("A"))), IntegerLiteral("1")), AbstractDimension(Vector), AbstractDimension(Vector), AbstractDimension(Vector(MarkerAnnotation(new Name("A")))))) }

      "new int [][]{{1,2,3}, {1,2,3}}" in { parse("new int [][]{{1,2,3}, {1,2,3}}") shouldBe InitializedArrayInstantiation(ArrayType(IntegerPrimitive(Vector()), Vector(AbstractDimension(Vector), AbstractDimension(Vector))), ArrayInitializer(Vector(ArrayInitializer(Vector(IntegerLiteral("1"), IntegerLiteral("2"), IntegerLiteral("3"))), ArrayInitializer(Vector(IntegerLiteral("1"), IntegerLiteral("2"), IntegerLiteral("3")))))) }
      "new Class[]@A[] {{new Class(), new Class()}}" in { parse("new Class[]@A[] {new Class(), new Class()}") shouldBe InitializedArrayInstantiation(ArrayType(ClassType(Vector(), None, new Name("Class"), Vector()), Vector(AbstractDimension(Vector), AbstractDimension(Vector(MarkerAnnotation(new Name("A")))))), ArrayInitializer(Vector(SimpleObjectInstantiation(Vector, ClassType(Vector(), None, new Name("Class"), Vector), Vector()), SimpleObjectInstantiation(Vector, ClassType(Vector(), None, new Name("Class"), Vector), Vector())))) }
    }
    "method invocation" - {
      "f()" in { parse("f()") shouldBe MethodInvocation(Vector, new Name("f"), Vector) }
      "f(a)" in { parse("f(a)") shouldBe MethodInvocation(Vector, new Name("f"), Vector(Select(new Name("a")))) }
      "f(a1, a2)" in {
        parse("f(a1, a2)") shouldBe MethodInvocation(Vector,
          new Name("f"),
          Vector(Select(new Name(Vector("a1"))), Select(new Name("a2"))))
      }
      "Class.Name.f(a,a)" in { parse("Class.Name.f(a,a)") shouldBe QualifiedMethodInvocation(Select(new Name(Vector("Class", "Name"))), Vector, new Name("f"), Vector(Select(new Name("a")), Select(new Name("a")))) }
      "Class.Name.<T> f(a,a)" in { parse("Class.Name.<T> f(a,a)") shouldBe QualifiedMethodInvocation(Select(new Name(Vector("Class", "Name"))), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector))), new Name("f"), Vector(Select(new Name("a")), Select(new Name("a")))) }
      "Class.Name . <T> f(a,a)" in { parse("Class.Name . <T> f(a,a)") shouldBe QualifiedMethodInvocation(Select(new Name(Vector("Class", "Name"))), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector))), new Name("f"), Vector(Select(new Name("a")), Select(new Name("a")))) }
      "expr.name.f(a,a)" in { parse("expr.name.f(a,a)") shouldBe QualifiedMethodInvocation(Select(new Name(Vector("expr", "name"))), Vector, new Name("f"), Vector(Select(new Name("a")), Select(new Name("a")))) }
      "expr.name.<T> f(a,a)" in { parse("expr.name.<T> f(a,a)") shouldBe QualifiedMethodInvocation(Select(new Name(Vector("expr", "name"))), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector))), new Name("f"), Vector(Select(new Name("a")), Select(new Name("a")))) }
      "a.f()" in { parse("a.f()") shouldBe QualifiedMethodInvocation(Select(new Name("a")), Vector, new Name("f"), Vector) }
      "a[0].f()" in { parse("a[0].f()") shouldBe QualifiedMethodInvocation(Extraction(Select(new Name("a")), IntegerLiteral("0")), Vector, new Name("f"), Vector) }
      "a().f()" in { parse("a().f()") shouldBe QualifiedMethodInvocation(MethodInvocation(Vector, new Name("a"), Vector), Vector, new Name("f"), Vector) }
      "a[0].<T> f()" in { parse("a[0].<T> f()") shouldBe QualifiedMethodInvocation(Extraction(Select(new Name("a")), IntegerLiteral("0")), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector))), new Name("f"), Vector) }
      "a.<T> f()" in { parse("a.<T> f()") shouldBe QualifiedMethodInvocation(Select(new Name("a")), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector))), new Name("f"), Vector) }
      "a().<T> f()" in { parse("a().<T> f()") shouldBe QualifiedMethodInvocation(MethodInvocation(Vector, new Name("a"), Vector), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector))), new Name("f"), Vector) }
      "super.f()" in { parse("super.f()") shouldBe QualifiedMethodInvocation(ParentReference, Vector, new Name("f"), Vector) }
      "Class.super.f()" in { parse("Class.super.f()") shouldBe QualifiedMethodInvocation(QualifiedParentReference(new Name("Class")), Vector, new Name("f"), Vector) }
      "super.<T> f()" in { parse("super.<T> f()") shouldBe QualifiedMethodInvocation(ParentReference, Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector))), new Name("f"), Vector) }
      "Class.super.<T> f()" in { parse("Class.super.<T> f()") shouldBe QualifiedMethodInvocation(QualifiedParentReference(new Name("Class")), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector))), new Name("f"), Vector) }
      "f((a).f())" in { parse("f((a).f())") shouldBe MethodInvocation(Vector, new Name("f"), QualifiedMethodInvocation(Select(new Name("a")), Vector, new Name("f"), Vector)) }
    }
    "method reference" - {
      "expr.name :: f" in { parse("expr.name :: f") shouldBe MethodReference(Select(new Name(Vector("expr", "name"))), Vector, new Name("f")) }
      "Class:: f" in { parse("Class:: f") shouldBe MethodReference(Select(new Name("Class")), Vector, new Name("f")) }
      "f[1] ::f" in { parse("f[1] ::f") shouldBe MethodReference(Extraction(Select(new Name("f")), IntegerLiteral("1")), Vector, new Name("f")) }
      "super::f" in { parse("super::f") shouldBe MethodReference(ParentReference, Vector, new Name("f")) }
      "Class.super ::f" in { parse("Class.super:: f") shouldBe MethodReference(QualifiedParentReference(new Name("Class")), Vector, new Name("f")) }

      "expr.name ::<T> f" in { parse("expr.name ::<T> f") shouldBe MethodReference(Select(new Name(Vector("expr", "name"))), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector))), new Name("f")) }
      "Class:: <T>f" in { parse("Class:: <T>f") shouldBe MethodReference(Select(new Name("Class")), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector))), new Name("f")) }
      "f[1] ::<T>f" in { parse("f[1] ::<T>f") shouldBe MethodReference(Extraction(Select(new Name("f")), IntegerLiteral("1")), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector))), new Name("f")) }
      "super::<T>f" in { parse("super::<T>f") shouldBe MethodReference(ParentReference, Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector))), new Name("f")) }
      "Class.super::<T>f" in { parse("Class.super::<T>f") shouldBe MethodReference(QualifiedParentReference(new Name("Class")), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector))), new Name("f")) }
      "Class::new" in { parse("Class::new") shouldBe ConstructorReference(ClassType(Vector, None, new Name("Class"), Vector), Vector) }
      "Class::<T>new" in { parse("Class::<T>new") shouldBe ConstructorReference(ClassType(Vector, None, new Name("Class"), Vector), Vector(ArgumentTemplate(ClassType(Vector, None, new Name("T"), Vector)))) }
      "int@A[] :: new" in { parse("int@A[] :: new") shouldBe ArrayConstructorReference(ArrayType(IntegerPrimitive(Vector), Vector(AbstractDimension(Vector(MarkerAnnotation(new Name("A"))))))) }
    }
  }
}