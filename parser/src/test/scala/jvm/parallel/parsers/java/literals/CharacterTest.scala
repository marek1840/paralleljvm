package jvm.parallel.parsers.java.literals

import jvm.parallel.ast.parsers.statement.instructions.discardable.literals.{CharLiteral, Literal}
import jvm.parallel.parsers.ParserTest
class CharacterTest extends ParserTest {
  def parse(string: String): Literal = {
    implicit val parser = new Parser(string)
    get(parser.literal.run())
  }

  "Character parser should" - {
    "parse valid characters" - {
      "'a'" in (parse("'a'") shouldBe CharLiteral("'a'"))
      "'%'" in (parse("'%'") shouldBe CharLiteral("'%'"))
      "'\\t'" in (parse("'\\t'") shouldBe CharLiteral("'\\t'"))
      "'\\\\'" in (parse("'\\\\'") shouldBe CharLiteral("'\\\\'"))
      "'\\''" in (parse("'\\''") shouldBe CharLiteral("'\\''"))
      "'\\u03a9'" in (parse("'\\u03a9'") shouldBe CharLiteral("'\\u03a9'"))
      "'\\uFFFF'" in (parse("'\\uFFFF'") shouldBe CharLiteral("'\\uFFFF'"))
      "'\\177'" in (parse("'\\177'") shouldBe CharLiteral("'\\177'"))
    }
    "not parse invalid characters" - {
      "'" in { an[Exception] should be thrownBy parse("'''") }
      "\\" in { an[Exception] should be thrownBy parse("'\\'") }
      "'\n'" in { an[Exception] should be thrownBy parse("'\n'") }
      "'\r'" in { an[Exception] should be thrownBy parse("'\r'") }
    }
  }
}