package jvm.parallel.parsers.java

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.instructions.discardable.literals.IntegerLiteral
import jvm.parallel.ast.parsers.statement.instructions.discardable.reference.Select
import jvm.parallel.ast.parsers.expression.operators.BinaryOperator
import jvm.parallel.ast.parsers.expression.types.annotations._
import jvm.parallel.ast.parsers.expression.{ArrayInitializer, BinaryOperations, TernaryConditional}
import jvm.parallel.parsers.ParserTest

class AnnotationTest extends ParserTest {
  def parse(string: String): Annotation = {
    implicit val parser = new Parser(string)
    get(parser.annotation.run())
  }

  "NormalAnnotation parser should parse" - {
    "normal Annotationss" - {
      "@A(i = i < i ? i : i)" in { parse("@A(i = i < i ? i : i)") shouldBe NormalAnnotation(new Name("A"), NamedAnnotationParameter(new Name("i"), TernaryConditional(BinaryOperations(BinaryOperator.<, Vector(Select(new Name("i")), Select(new Name("i")))), Select(new Name("i")), Select(new Name("i"))))) }
      "@A(i = {i, i, })" in { parse("@A(i = {i, i, })") shouldBe NormalAnnotation(new Name("A"), NamedAnnotationParameter(new Name("i"), ArrayInitializer(Vector(Select(new Name("i")), Select(new Name("i")))))) }
      "@A(i1=@A, i2 = {}, i3 = 3)" in { parse("@A(i1 = @A, i2 = {}, i3 = 3)") shouldBe NormalAnnotation(new Name("A")
        , Vector(NamedAnnotationParameter(new Name("i1"), MarkerAnnotation(new Name("A"))), NamedAnnotationParameter(new Name("i2"), ArrayInitializer(Vector)), NamedAnnotationParameter(new Name("i3"), IntegerLiteral("3"))))
      }
      "@A()" in { parse("@A()") shouldBe NormalAnnotation(new Name("A"), Vector()) }
      "@A( i = 1)" in { parse("@A(i = 1)") shouldBe NormalAnnotation(new Name("A"), NamedAnnotationParameter(new Name("i"), IntegerLiteral("1"))) }
      "@A(i = {})" in { parse("@A(i = {})") shouldBe NormalAnnotation(new Name("A"), NamedAnnotationParameter(new Name("i"), ArrayInitializer(Vector))) }
      "@A(i = @A)" in { parse("@A(i = @A)") shouldBe NormalAnnotation(new Name("A"), NamedAnnotationParameter(new Name("i"), MarkerAnnotation(new Name("A")))) }
    }
    "marker Annotations" - {
      "@A" in { parse("@A") shouldBe MarkerAnnotation(new Name("A")) }
    }
    "single element Annotations" - {
      "@A(5)" in { parse("@A(5)") shouldBe SingleElementAnnotation(new Name("A"), IntegerLiteral("5")) }
      "@A({})" in { parse("@A({})") shouldBe SingleElementAnnotation(new Name("A"), ArrayInitializer(Vector)) }
      "@A({i, i, i})" in { parse("@A({i, i, i})") shouldBe SingleElementAnnotation(new Name("A"), ArrayInitializer(Vector(Select(new Name("i")), Select(new Name("i")), Select(new Name("i"))))) }
      "@A(@A)" in { parse("@A(@A)") shouldBe SingleElementAnnotation(new Name("A"), MarkerAnnotation(new Name("A"))) }
    }
    "@A(@B(i1 = @C, i2 = @D(i)))" in {
      parse("@A(@B(i1 = @C, i2 = @D(i)))") shouldBe SingleElementAnnotation(new Name("A"), NormalAnnotation(
        new Name("B"), Vector(NamedAnnotationParameter(new Name("i1"),
          MarkerAnnotation(new Name("C"))), NamedAnnotationParameter(new Name("i2"),
          SingleElementAnnotation(new Name("D"), Select(new Name("i")))))))
    }
  }

}
