package jvm.parallel.parsers.java.literals

import jvm.parallel.ast.parsers.statement.instructions.discardable.literals.{Literal, StringLiteral}
import jvm.parallel.parsers.ParserTest

class StringTest extends ParserTest {
  def parse(string: String): Literal = {
    implicit val parser = new Parser(string)
    get(parser.literal.run())
  }

  "String parser should" - {
    "parse" - {
      "\"\"" in (parse("\"\"") shouldBe StringLiteral("\"\""))
      """"\""""" in (parse(""""\""""") shouldBe StringLiteral(""""\"""""))
      """"This is a string"""" in (parse(""""This is a string"""") shouldBe StringLiteral(""""This is a string""""))
    }
    "not parse" - {
      "\"\\u000a\"" in (an[Exception] should be thrownBy parse("\"\\u000a\""))
      """"\"""" in (an[Exception] should be thrownBy parse(""""\""""))
      "fu00e9v" in { an[Exception] should be thrownBy parse("f\u00e9v") }
    }
  }

}