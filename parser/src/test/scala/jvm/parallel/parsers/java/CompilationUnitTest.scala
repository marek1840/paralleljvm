package jvm.parallel.parsers.java

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.declaration.CompilationUnitDeclaration
import jvm.parallel.ast.parsers.statement.declaration.imports.{LazyImportDeclaration, SingleImportDeclaration, StaticImportDeclaration, StaticLazyImportDeclaration}
import jvm.parallel.ast.parsers.statement.declaration.packages.{NamedPackageDeclaration, UnnamedPackageDeclaration}
import jvm.parallel.ast.parsers.statement.declaration.types.{ClassDeclaration, EmptyDeclaration, InterfaceDeclaration}
import jvm.parallel.parsers.ParserTest

class CompilationUnitTest extends ParserTest {
  def parse(string: String): CompilationUnitDeclaration = {
    implicit val parser = new Parser(string)
    get(parser.compilationUnit.run())
  }

  val p0 = """package a; import a;"""
  val p1 = """package a; import a.*;"""
  val p2 = """package a; import static a.b;"""
  val p3 = """package a; import static a.*;"""
  val p4 =
    """
      |package a;
      |import a;
      |import static a;
    """.stripMargin
  "Compilation unit parser should parse" - {
    "" in {
      parse("") shouldBe CompilationUnitDeclaration(
        UnnamedPackageDeclaration,
        Vector,
        Vector)
    }
    "package a.b;" in {
      parse("package a.b;") shouldBe CompilationUnitDeclaration(
        new NamedPackageDeclaration(Vector, new Name(Vector("a","b"))),
        Vector,
        Vector)
    }
    p0 in {
      parse(p0) shouldBe CompilationUnitDeclaration(
        new  NamedPackageDeclaration(Vector, new Name("a")),
        new  SingleImportDeclaration(new Name("a")),
        Vector)
    }
    p1 in {
      parse(p1) shouldBe CompilationUnitDeclaration(
        NamedPackageDeclaration(Vector, new Name("a")),
        new  LazyImportDeclaration(new Name("a")),
        Vector)
    }
    p2 in {
      parse(p2) shouldBe CompilationUnitDeclaration(
        NamedPackageDeclaration(Vector, new Name("a")),
        new StaticImportDeclaration(new Name(Vector("a","b"))),
        Vector)
    }
    p3 in {
      parse(p3) shouldBe CompilationUnitDeclaration(
        NamedPackageDeclaration(Vector, new Name("a")),
        new  StaticLazyImportDeclaration(new Name("a")),
        Vector)
    }
    p4 in {
      parse(p4) shouldBe CompilationUnitDeclaration(
        new  NamedPackageDeclaration(Vector, new Name("a")),
        Seq(
          new  SingleImportDeclaration(new Name("a")),
          new  StaticImportDeclaration(new Name("a"))
        ),
        Vector)
    }
    "class A{}" in {
      parse("class A{}") shouldBe CompilationUnitDeclaration(
        UnnamedPackageDeclaration,
        Vector,
        new  ClassDeclaration(
          Vector,
          Vector,
          new Name("A"),
          Vector,
          None,
          Vector,
          Vector))
    }
    "interface A{}" in {
      parse("interface A{}") shouldBe new CompilationUnitDeclaration(
         UnnamedPackageDeclaration,
        Vector,
        new InterfaceDeclaration(
          Vector,
          Vector,
          new Name("A"),
          Vector,
          Vector,
          Vector))
    }
    ";" in {
      parse(";") shouldBe new CompilationUnitDeclaration(
        UnnamedPackageDeclaration,
        Vector,
         EmptyDeclaration)
    }
  }

}