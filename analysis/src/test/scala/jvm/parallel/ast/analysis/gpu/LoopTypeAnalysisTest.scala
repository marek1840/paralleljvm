package jvm.parallel.ast.analysis.gpu

import jvm.parallel.ast.parsers.statement.instructions.loop.For
import jvm.parallel.parsers.java.JavaParser
import org.scalatest.prop.{Checkers, PropertyChecks}
import org.scalatest.{Matchers, FreeSpec, FunSuite}

class LoopTypeAnalysisTest extends FreeSpec with PropertyChecks with Matchers with Checkers {
  def parse(string: String) : For = JavaParser.parseStatement(string).asInstanceOf[For]
  def analyse(string:String) = new LoopTypeAnalysis().reduce(parse(string))

  "Full array scan should be resolved when analyzing" - {
    "simple full scan loop" in {
      analyse("for(int i = 0; i < array.length; ++i) {array[i] = i;}") shouldBe FullArrayScan
    }
  }

  "Other loop should be resolved when analyzing" - {
    "infinite loop" in {
      analyse("for(;;){ }") shouldBe OtherLoopType
    }

    "empty loop" in {
      analyse("for(int i = 0; i < a.length; ++i){ }") shouldBe OtherLoopType
    }
  }
}
