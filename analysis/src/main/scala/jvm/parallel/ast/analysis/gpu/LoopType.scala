package jvm.parallel.ast.analysis.gpu

sealed trait LoopType
case object FullArrayScan extends LoopType
case object PartialArrayScan extends LoopType
case object OtherLoopType extends LoopType