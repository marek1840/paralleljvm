package jvm.parallel.ast.analysis.gpu

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.ASTReducer
import jvm.parallel.ast.parsers.expression.operators.BinaryOperator
import jvm.parallel.ast.parsers.expression.types.primitives.IntegerPrimitive
import jvm.parallel.ast.parsers.expression.{BinaryOperations, Expression}
import jvm.parallel.ast.parsers.statement.declarator.InitializedVariableDeclarator
import jvm.parallel.ast.parsers.statement.instructions.LocalVariableDeclaration
import jvm.parallel.ast.parsers.statement.instructions.discardable.literals.IntegerLiteral
import jvm.parallel.ast.parsers.statement.instructions.discardable.reference.Select
import jvm.parallel.ast.parsers.statement.instructions.loop.For

class LoopTypeAnalysis extends ASTReducer[For, LoopType] {
  private[this] implicit def opToSeq(op: BinaryOperator): Seq[BinaryOperator] = Seq(op)

  override def reduce(loop: For): OUT = {
    val initializedVariable: Option[Name] = {
      loop.initializers match {
        case (v: LocalVariableDeclaration) +: Nil => extractLoopIterator(v)
        case _ => None
      }
    }

    val loopIsBoundByArrayLength = initializedVariable
      .flatMap(name => loop.condition
        .map(expr => isBoundByArrayLength(name, expr)))

    OtherLoopType
  }

  // TODO Work out proper Name unapply
  def isBoundByArrayLength(Name: Name, expr: Expression): Boolean = {
    expr match {
      case
        BinaryOperations(
        Seq(BinaryOperator.<),
        Vector(Select(Name), Select(name)))
        if isArray(name.parent) && isLengthAccess(name.last) =>
        true
      case _ => false
    }
  }

  def isLengthAccess(name: Name): Boolean = name == new Name("length")

  //TODO with dependency to symbol table
  def isArray(arrayName: Option[Name]) = arrayName match {
    case None => false
    case Some(name) => true
  }

  def extractLoopIterator(dec: LocalVariableDeclaration): Option[Name] = {
    dec match {
      case LocalVariableDeclaration(_, _, IntegerPrimitive(_), declarators) =>
        declarators match {
          case InitializedVariableDeclarator(name, IntegerLiteral("0")) +: Nil => Some(name)
          case _ => None
        }

      case _ => None
    }
  }

}
