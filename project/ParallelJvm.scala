import sbt.Keys._
import sbt._


object ParallelJvm extends Build {
  private[this] val log4jVersion: String = "2.3"

  lazy val commonSettings = Seq(
    version := "0.0.1",
    scalaVersion := "2.11.7",
    sourcesInBase := false,
    scalacOptions ++= List("-Ybackend:GenBCode", "-Ydelambdafy:method", "-target:jvm-1.8"),
    libraryDependencies ++= Seq(
      "org.apache.logging.log4j" % "log4j-core" % log4jVersion,
      "org.apache.logging.log4j" % "log4j-api" % log4jVersion,
      "org.slf4j" % "slf4j-log4j12" % "1.7.18",
      "org.scalacheck" %% "scalacheck" % "1.11.4" % "test",
      "org.scalatest" % "scalatest_2.11" % "2.2.0" % "test",
      "org.scala-lang.modules" %% "scala-java8-compat" % "0.5.0",
      "net.openhft" % "compiler" % "2.2.2"
    )
  )

  lazy val vmSettings = Seq(
    resolvers += "Sonatype OSS Snapshots Repository" at "http://oss.sonatype.org/content/groups/public/",
    resolvers += "NativeLibs4Java Repository" at "http://nativelibs4java.sourceforge.net/maven/",
    libraryDependencies ++= Seq(
      "com.nativelibs4java" % "javacl" % "1.0.0-RC4"
    )
  )

  lazy val parserSettings = Seq(
    libraryDependencies += "org.parboiled" %% "parboiled" % "2.1.0"
  )

  lazy val parallelJvm = project("ParallelJvm", ".")
    .settings(commonSettings: _*)

  lazy val ast = project("Ast")
    .dependsOn(dependencyTo(parallelJvm))
    .settings(commonSettings: _*)

  lazy val parser = project("Parser")
    .dependsOn(dependencyTo(ast))
    .settings(commonSettings ++ parserSettings: _*)

  lazy val analysis = project("Analysis")
    .dependsOn(dependencyTo(parser))
    .settings(commonSettings ++ parserSettings: _*)

  lazy val virtualMachine = project("VirtualMachine", "vm")
    .dependsOn(dependencyTo(analysis))
    .settings(commonSettings ++ vmSettings: _*)

  lazy val simpleVM = project("BasedVM", "stack-vm")
    .dependsOn(dependencyTo(virtualMachine))
    .settings(commonSettings ++ vmSettings: _*)


  def dependencyTo(project: Project): ClasspathDependency = {
    project % "test->test;compile->compile"
  }

  def project(name: String): Project = {
    Project(id = "ParallelJvm" + name, base = file(name.toLowerCase()))
  }

  def project(name: String, filename: String): Project = {
    Project(id = "ParallelJvm" + name, base = file(filename))
  }
}

