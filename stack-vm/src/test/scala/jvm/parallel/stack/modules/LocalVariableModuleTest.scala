package jvm.parallel.stack.modules

import jvm.parallel.stack.ops.{Load, Store, Push}

class LocalVariableModuleTest extends ModuleTest {
  private[this] val testValue = 7

  "Store and load" in {
    val instructions = Seq(
      Push(testValue),
      Store(v1),
      Load(v1)
    )

    returnedValue(instructions) shouldBe testValue
  }
}
