package jvm.parallel.stack.modules

import jvm.parallel.stack.ops._

class ComparisonModuleTest extends ModuleTest {
  private[this] val True = 1
  private[this] val False = 0

  "Comparison should work properly for" - {
    "1 == 1" in (returnedValue(Seq(Push(1), Push(1), Equal)) shouldBe True)
    "1 == 2" in (returnedValue(Seq(Push(1), Push(2), Equal)) shouldBe False)
    "2 == 1" in (returnedValue(Seq(Push(2), Push(1), Equal)) shouldBe False)

    "1 != 1" in (returnedValue(Seq(Push(1), Push(1), NotEqual)) shouldBe False)
    "1 != 2" in (returnedValue(Seq(Push(1), Push(2), NotEqual)) shouldBe True)
    "2 != 1" in (returnedValue(Seq(Push(2), Push(1), NotEqual)) shouldBe True)

    "1 < 1" in (returnedValue(Seq(Push(1), Push(1), Lesser)) shouldBe False)
    "1 < 2" in (returnedValue(Seq(Push(1), Push(2), Lesser)) shouldBe True)
    "2 < 1" in (returnedValue(Seq(Push(2), Push(1), Lesser)) shouldBe False)

    "1 > 1" in (returnedValue(Seq(Push(1), Push(1), Greater)) shouldBe False)
    "1 > 2" in (returnedValue(Seq(Push(1), Push(2), Greater)) shouldBe False)
    "2 > 1" in (returnedValue(Seq(Push(2), Push(1), Greater)) shouldBe True)

    "1 <= 1" in (returnedValue(Seq(Push(1), Push(1), LesserOrEqual)) shouldBe True)
    "1 <= 2" in (returnedValue(Seq(Push(1), Push(2), LesserOrEqual)) shouldBe True)
    "2 <= 1" in (returnedValue(Seq(Push(2), Push(1), LesserOrEqual)) shouldBe False)

    "1 >= 1" in (returnedValue(Seq(Push(1), Push(1), GreaterOrEqual)) shouldBe True)
    "1 >= 2" in (returnedValue(Seq(Push(1), Push(2), GreaterOrEqual)) shouldBe False)
    "2 >= 1" in (returnedValue(Seq(Push(2), Push(1), GreaterOrEqual)) shouldBe True)
  }
}
