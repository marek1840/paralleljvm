package jvm.parallel.stack.modules

import jvm.parallel.stack.ops.Push

class StackModuleTest extends ModuleTest {
  private[this] val testValue: Int = 7
  "push" in (returnedValue(Seq(Push(testValue))) shouldBe testValue)
}
