package jvm.parallel.stack.modules

import jvm.parallel.stack.ops._
import java.lang.Float
class ArithmeticModuleTest extends ModuleTest {

  "integer arithmetic" - {
    "Addition" - {
      "0 + 1" in (returnedValue(Seq(Push(0), Push(1), Add)) shouldBe 1)
      "1 + 1" in (returnedValue(Seq(Push(1), Push(1), Add)) shouldBe 2)
    }
    "Multiplication" - {
      "0 * 1" in (returnedValue(Seq(Push(0), Push(1), Multiply)) shouldBe 0)
      "1 * 1" in (returnedValue(Seq(Push(1), Push(1), Multiply)) shouldBe 1)
      "2 * 3" in (returnedValue(Seq(Push(2), Push(3), Multiply)) shouldBe 6)
    }
    "Subtraction" - {
      "1 - 0" in (returnedValue(Seq(Push(1), Push(0), Subtract)) shouldBe 1)
      "0 - 1" in (returnedValue(Seq(Push(0), Push(1), Subtract)) shouldBe -1)
      "3 - 3" in (returnedValue(Seq(Push(3), Push(3), Subtract)) shouldBe 0)
    }
    "Division" - {
//      "1 / 0" in (returnedValue(Seq(Push(1), Push(0), Divide)) shouldBe 1)
      "0 / 7" in (returnedValue(Seq(Push(0), Push(7), Divide)) shouldBe 0)
      "3 / 3" in (returnedValue(Seq(Push(3), Push(3), Divide)) shouldBe 1)
      "3 / 7" in (returnedValue(Seq(Push(3), Push(7), Divide)) shouldBe 0)
      "7 / 3" in (returnedValue(Seq(Push(7), Push(3), Divide)) shouldBe 2)
    }
  }

  "Float arithmetic" - {
    "Addition" - {
      "0.5 + 1.5" in (returnedValue(Seq(PushFloat(0f), PushFloat(1.5f), AddFloat)) shouldBe Float.floatToIntBits(1.5f))
      "1.5 + 1.5" in (returnedValue(Seq(PushFloat(1.5f), PushFloat(1.5f), AddFloat)) shouldBe Float.floatToIntBits(3))
    }
    "Multiplication" - {
      "0 * 1.5" in (returnedValue(Seq(PushFloat(0f), PushFloat(1.5f), MultiplyFloat)) shouldBe Float.floatToIntBits(0))
      "1.5 * 1.5" in (returnedValue(Seq(PushFloat(1.5f), PushFloat(1.5f), MultiplyFloat)) shouldBe Float.floatToIntBits(2.25f))
      "2.5 * 3.5" in (returnedValue(Seq(PushFloat(2.5f), PushFloat(3.5f), MultiplyFloat)) shouldBe Float.floatToIntBits(8.75f))
    }
    "Subtraction" - {
      "1.5 - 0" in (returnedValue(Seq(PushFloat(1.5f), PushFloat(0f), SubtractFloat)) shouldBe Float.floatToIntBits(1.5f))
      "0 - 1.5" in (returnedValue(Seq(PushFloat(0f), PushFloat(1.5f), SubtractFloat)) shouldBe Float.floatToIntBits(-1.5f))
      "3.5 - 3.5" in (returnedValue(Seq(PushFloat(3.5f), PushFloat(3.5f), SubtractFloat)) shouldBe Float.floatToIntBits(0))
    }
    "Division" - {
//      "1.5 / 0" in (returnedValue(Seq(PushFloat(1.5f), PushFloat(0f), SubtractFloat)) shouldBe Float.floatToIntBits(1))
      "0 / 7.5" in (returnedValue(Seq(PushFloat(0f), PushFloat(7.5f), DivideFloat)) shouldBe Float.floatToIntBits(0))
      "3.5 / 3.5" in (returnedValue(Seq(PushFloat(3.5f), PushFloat(3.5f), DivideFloat)) shouldBe Float.floatToIntBits(1))
      "3.5 / 7.5" in (returnedValue(Seq(PushFloat(3.5f), PushFloat(7.5f), DivideFloat)) shouldBe Float.floatToIntBits((3.5f / 7.5f)))
      "7.5 / 3.5" in (returnedValue(Seq(PushFloat(7.5f), PushFloat(3.5f), DivideFloat)) shouldBe Float.floatToIntBits(7.5f / 3.5f))
    }
  }

}
