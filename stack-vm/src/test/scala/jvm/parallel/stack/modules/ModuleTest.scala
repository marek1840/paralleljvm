package jvm.parallel.stack.modules

import jvm.parallel.ast.name.TokenCache
import jvm.parallel.stack._
import jvm.parallel.stack.ops.{Halt, Instruction}
import jvm.parallel.vm2.structs.StructureStorage
import org.scalatest.prop.{Checkers, PropertyChecks}
import org.scalatest.{FreeSpec, Matchers}

trait ModuleTest extends FreeSpec with PropertyChecks with Matchers with Checkers {
  protected[this] val codeStorage = new CodeStorage
  protected[this] val structureStorage = new StructureStorage
  protected[this] val memorySize = 1000

  protected[this] val v1 = TokenCache.fromString("v1")
  protected[this] val v2 = TokenCache.fromString("v2")
  protected[this] val v3 = TokenCache.fromString("v3")
  protected[this] val v4 = TokenCache.fromString("v4")

  private[this] val locals = Seq(v1, v2, v3, v4)

  def run(methodBody: Seq[Instruction]): StackThread with ArrayMemory with FrameStack with MethodDispatch with OperandStack = {
    val thread = newThread(methodBody)
    thread.run()
    thread
  }

  def returnedValue(methodBody: Seq[Instruction]): Int = {
    run(methodBody).pop()
  }

  def newThread(methodBody: Seq[Instruction]): StackThread with ArrayMemory with FrameStack with MethodDispatch with OperandStack = {
    val thread = new StackThread(memorySize, codeStorage, structureStorage) with ArrayMemory with FrameStack with MethodDispatch with
                     OperandStack
    val instructions: Array[Instruction] = methodBody.toArray :+ Halt
    val frame = new Frame(instructions, locals)
    thread.pushFrame(frame)
    thread
  }
}
