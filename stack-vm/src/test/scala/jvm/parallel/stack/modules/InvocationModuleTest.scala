package jvm.parallel.stack.modules

import jvm.parallel.ast.name.{Name, TokenCache}
import jvm.parallel.parsers.java.JavaParser
import jvm.parallel.stack.Method
import jvm.parallel.stack.ops._

class InvocationModuleTest extends ModuleTest {
  private[this] implicit def str2Name(str:String) : Name = TokenCache.fromString(str)
  //TODO
  val ast1 = JavaParser.parseMethod("""int f1(){return f2(1);};""")
  val ast2 = JavaParser.parseMethod("""int f2(int i){return i + 3;};""")

  val instructions1 = Seq(Push(1), Invoke("f2"), Return)
  val instructions2 = Seq(Store("i"), Load("i"), Push(3), Add, Return)

  codeStorage.registerMethod("f1", new Method(instructions1, Some(ast1)))
  codeStorage.registerMethod("f2", new Method(instructions2, Some(ast2)))

  val instructions = Seq(Invoke("f1"))
  "Method Invocation works properly" in {
    returnedValue(instructions) shouldBe 4
  }
}
