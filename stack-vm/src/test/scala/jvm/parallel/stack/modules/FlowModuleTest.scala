package jvm.parallel.stack.modules

import jvm.parallel.stack.ops.{GoIfTrue, Lesser, Push, GoTo}

class FlowModuleTest extends ModuleTest {
  private[this] val testValue = 3

  "Go to" in {
    val ip: Int = 3

    val thread = run(Seq(GoTo(ip), Push(1), Push(2), Push(testValue)))

    thread.pop() shouldBe testValue
    thread.stackIsEmpty shouldBe true
  }

  "Go to if true" in {
    val ip: Int = 6

    val thread = run(Seq(
      Push(1), Push(2), Lesser,
      GoIfTrue(ip),
      Push(1), Push(2), Push(testValue)))

    thread.pop() shouldBe testValue
    thread.stackIsEmpty shouldBe true
  }
}
