package jvm.parallel.stack.modules

import jvm.parallel.stack.ops._

class ArrayModuleTest extends ModuleTest {
  "Create new array" - {
    "of size 0" in (returnedValue(Seq(Push(0), NewArray)) shouldBe 0)
    "of size 7" in (returnedValue(Seq(Push(7), NewArray)) shouldBe 0)
    "after creating other array" in {
      val instructions: Seq[Instruction] = Seq(Push(7), NewArray, Push(9), NewArray)
      returnedValue(instructions) shouldBe 8
    }
  }

  "Load array size" - {
    "of 0-elements array" in (returnedValue(Seq(Push(0), NewArray, ArraySize)) shouldBe 0)
    "of 7-elements array" in (returnedValue(Seq(Push(7), NewArray, ArraySize)) shouldBe 7)
  }

  //TODO
  "Store in 0-element array" // in  (returnedValue(Seq(Push(0), NewArray, Push(5), Push(5), Push(0), ArrayStore)) shouldBe ???)

  "Store and load in array" in {
    val value = 5
    returnedValue(Seq(
      Push(7), NewArray,
      Store(v1),
      Push(value), Push(1), Load(v1), ArrayStore,
      Push(1), Load(v1), ArrayLoad)) shouldBe value
  }

}
