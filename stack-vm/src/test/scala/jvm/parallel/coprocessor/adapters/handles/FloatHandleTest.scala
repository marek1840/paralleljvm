package jvm.parallel.coprocessor.adapters.handles

import java.lang.Float

import jvm.parallel.vm2.TypeSignature

class FloatHandleTest extends FieldHandleTest {
  val structureDefinition = newStructDef("Float")(TypeSignature.Float)

  "set float field" in {
    val memory = newMemory
    val struct = newStruct(structureDefinition)
    val memoryAddress = 5
    val offset = 1
    val handle = new FloatHandle(offset)

    val expectedValue: Float = 3.3F

    memory.writeToMemory(memoryAddress + offset, Float.floatToIntBits(expectedValue))

    handle.writeToStructure(struct)(memoryAddress, memory)

    readField(struct, offset) shouldBe expectedValue
  }

  "read float field" in {
    val memory = newMemory
    val struct = newStruct(structureDefinition)
    val memoryAddress = 5
    val offset = 1
    val handle = new FloatHandle(offset)

    val expectedValue: Float = 3.3F

    writeField[Float](struct, offset, expectedValue, Float.TYPE)

    handle.readFromStructure(struct)(memoryAddress, memory)

    memory.readFromMemory(memoryAddress + offset) shouldBe Float.floatToIntBits(expectedValue)
  }
}
