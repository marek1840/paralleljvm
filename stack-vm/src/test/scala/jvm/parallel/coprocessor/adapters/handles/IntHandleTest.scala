package jvm.parallel.coprocessor.adapters.handles

import jvm.parallel.vm2.TypeSignature

class IntHandleTest extends FieldHandleTest {
  val structureDefinition = newStructDef("Int")(TypeSignature.Integer)

  "set int field" in {
    val memory = newMemory
    val struct = newStruct(structureDefinition)
    val memoryAddress = 5
    val offset = 1
    val handle = new IntHandle(offset)

    val expectedValue = 10

    memory.writeToMemory(memoryAddress + offset, expectedValue)

    handle.writeToStructure(struct)(memoryAddress, memory)

    readField(struct, offset) shouldBe expectedValue
  }

  "read int field" in {
    val memory = newMemory
    val struct = newStruct(structureDefinition)
    val memoryAddress = 5
    val offset = 1
    val handle = new IntHandle(offset)

    val expectedValue = 10

    writeField[Integer](struct, offset, expectedValue, Integer.TYPE)

    handle.readFromStructure(struct)(memoryAddress, memory)

    memory.readFromMemory(memoryAddress + offset) shouldBe expectedValue
  }
}
