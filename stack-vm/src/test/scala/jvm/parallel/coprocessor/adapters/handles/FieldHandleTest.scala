package jvm.parallel.coprocessor.adapters.handles

import jvm.parallel.ast.name.{Name, TokenCache}
import jvm.parallel.coprocessor.adapters.StructureGeneration
import jvm.parallel.stack.ArrayMemory
import jvm.parallel.vm2.TypeSignature
import jvm.parallel.vm2.structs.{ClassName, FieldName, StructureDefinition}
import org.bridj.StructObject
import org.scalatest.prop.{Checkers, PropertyChecks}
import org.scalatest.{FreeSpec, Matchers}

trait FieldHandleTest extends FreeSpec with PropertyChecks with Matchers with Checkers with StructureGeneration {
  private[this] implicit def str2Name(string: String): Name = TokenCache.fromString(string)

  def newStructDef(name: String)(fields: (TypeSignature)*) = {
    val className = new ClassName("pck", name)
    val fieldDeclarations = fields.zipWithIndex.map { case (t, i) => (new FieldName(className, "_" + i), t) }
    val struct = new StructureDefinition(className, fieldDeclarations)
    compileNewClass(struct)
    struct
  }

  def newStruct(structureDefinition: StructureDefinition) = {
    val clazz = compileNewClass(structureDefinition)
    clazz.newInstance()
  }

  def readField(struct: StructObject, offset: Int) = {
    val method = struct.getClass.getMethod(s"get_${offset - 1}")
    method.invoke(struct)
  }

  def writeField[A](struct: StructObject, offset: Int, expectedValue: A, clazz: Class[A]) = {
    val method = struct.getClass.getMethod(s"set_${offset - 1}", clazz)
    method.invoke(struct, expectedValue.asInstanceOf[Object])
  }

  def newMemory = new Memory(100)

  private[handles] class Memory(val memorySize: Int) extends ArrayMemory

}
