package jvm.parallel.coprocessor.adapters.handles

import jvm.parallel.ast.name.{Name, TokenCache}
import jvm.parallel.coprocessor.adapters.StructureGeneration
import jvm.parallel.stack.containers.StructureStorageContainer
import jvm.parallel.vm2.TypeSignature
import jvm.parallel.vm2.structs.{ClassName, FieldName, StructureDefinition, StructureStorage}
import org.scalatest.prop.{Checkers, PropertyChecks}
import org.scalatest.{FreeSpec, Matchers}

class StructProxyFactoryTest extends FreeSpec with PropertyChecks with Matchers with Checkers {
  private[this] implicit def str2Name(string: String): Name = TokenCache.fromString(string)

  "generate proper template for" - {
    "simple structure" in {
      val name = new ClassName("pcg", "struct")
      val structDef = newStructDef(name)(TypeSignature.Integer)
      val factory = newFactory(structDef)
      val template = factory.createTemplate(name)

      template.handles should have size 1
      template.handles.head shouldBe a[IntHandle]
      template.handles.head.offset shouldBe 0
    }

    "structure with primitives" in {
      val name = new ClassName("pcg", "struct")
      val structDef = newStructDef(name)(TypeSignature.Integer, TypeSignature.Float)
      val factory = newFactory(structDef)
      val template = factory.createTemplate(name)

      val handles = template.handles.toSeq
      handles should have size 2
      handles.head shouldBe a[IntHandle]
      handles.head.offset shouldBe 0

      handles(1) shouldBe a[FloatHandle]
      handles(1).offset shouldBe 1
    }

    "nested structure" in {
      val innerName = new ClassName("pcg", "inner")
      val innerStructDef = newStructDef(innerName)(TypeSignature.Integer, TypeSignature.Float)
      val outerName = new ClassName("pcg", "outer")
      val structDef = newStructDef(outerName)(TypeSignature.Type(innerName), TypeSignature.Integer)
      val factory = newFactory(innerStructDef, structDef)
      val template = factory.createTemplate(outerName)

      val outerStructHandles = template.handles.toSeq

      outerStructHandles should have size 2

      outerStructHandles.head shouldBe a[StructHandle[_]]
      outerStructHandles.head.offset shouldBe 0

      outerStructHandles(1) shouldBe a[IntHandle]
      outerStructHandles(1).offset shouldBe 1

      val innerTemplate = outerStructHandles.head.asInstanceOf[StructHandle[_]].template
      val innerStructHandles = innerTemplate.handles.toSeq

      innerStructHandles should have size 2

      innerStructHandles.head shouldBe a[IntHandle]
      outerStructHandles.head.offset shouldBe 0

      innerStructHandles(1) shouldBe a[FloatHandle]
      outerStructHandles(1).offset shouldBe 1
    }
  }

  def newFactory(structs: StructureDefinition*): StructureHandleTemplateFactory = {
    new StructureHandleTemplateFactory with StructureStorageContainer with StructureGeneration {
      override protected[this] val structureArea: StructureStorage = {
        val storage = new StructureStorage
        structs.foreach(s => {
          storage.registerStructure(s.name, s)
        })
        storage
      }
    }
  }

  def newStructDef(name: ClassName)(fields: (TypeSignature)*) = {
    val fieldDeclarations = fields.zipWithIndex.map { case (t, i) => (new FieldName(name, "_" + i), t) }
    val struct = new StructureDefinition(name, fieldDeclarations)
    struct
  }
}
