package jvm.parallel.coprocessor.adapters.handles

import jvm.parallel.vm2.TypeSignature
import jvm.parallel.vm2.structs.StructureDefinition

class StructHandleTemplateTest extends FieldHandleTest {


  "Can read and write using from/to memory" in {
    val sourceMemory = newMemory
    val handle = newStructProxy

    val expectedFloat: Int = java.lang.Float.floatToIntBits(5.5F)

    val structAddress = 5
    sourceMemory.writeToMemory(structAddress + 1, expectedFloat)
    sourceMemory.writeToMemory(structAddress + 2, 1)
    sourceMemory.writeToMemory(structAddress + 3, 2)

    val innerStructAddress = 12
    sourceMemory.writeToMemory(structAddress + 4, innerStructAddress)

    sourceMemory.writeToMemory(innerStructAddress + 1, 3)
    sourceMemory.writeToMemory(innerStructAddress + 2, 4)

    val structure = handle.writeToStructure()(5, sourceMemory)

    val targetMemory = newMemory // clean memory
    targetMemory.writeToMemory(structAddress + 4, innerStructAddress) // set up inner struct address in new memory
    // (since we do not change reference address while writing to memory

    handle.readFromStructure(structure)(structAddress, targetMemory)

    targetMemory.readFromMemory(structAddress + 1) shouldBe expectedFloat
    targetMemory.readFromMemory(structAddress + 2) shouldBe 1
    targetMemory.readFromMemory(structAddress + 3) shouldBe 2

    targetMemory.readFromMemory(innerStructAddress + 1) shouldBe 3
    targetMemory.readFromMemory(innerStructAddress + 2) shouldBe 4
  }

  def newStructProxy = {
    val innerStruct: StructureDefinition = newStructDef("Inner")(TypeSignature.Integer, TypeSignature.Integer)
    val structureDefinition = newStructDef("Outer")(
      TypeSignature.Float, TypeSignature.Integer, TypeSignature.Integer, TypeSignature.Type(innerStruct.name))

    val innerClass = compileNewClass(innerStruct)
    val innerFieldHandle1 = new IntHandle(1)
    val innerFieldHandle2 = new IntHandle(2)
    val innerStructureProxy = new StructureHandleTemplate(innerClass, Seq(innerFieldHandle1, innerFieldHandle2))

    val clazz = compileNewClass(structureDefinition)
    val fieldHandle1 = new FloatHandle(1)
    val fieldHandle2 = new IntHandle(2)
    val fieldHandle3 = new IntHandle(3)
    val fieldHandle4 = new StructHandle(4, innerStructureProxy)

    new StructureHandleTemplate(clazz, Seq(fieldHandle1, fieldHandle2, fieldHandle3, fieldHandle4))
  }
}
