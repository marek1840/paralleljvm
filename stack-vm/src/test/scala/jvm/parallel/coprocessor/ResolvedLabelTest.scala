package jvm.parallel.coprocessor

import jvm.parallel.ast.name.{TokenCache, Name}
import jvm.parallel.ast.parsers.statement.Block
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.MethodInvocation
import jvm.parallel.ast.parsers.statement.instructions.discardable.literals.IntegerLiteral
import jvm.parallel.ast.parsers.statement.instructions.discardable.reference.Select
import jvm.parallel.ast.parsers.statement.instructions.loop.For
import jvm.parallel.coprocessor.v2.labels.ResolvedLabel
import org.scalatest.prop.{Checkers, PropertyChecks}
import org.scalatest.{Matchers, FreeSpec, FunSuite}

class ResolvedLabelTest extends FreeSpec with PropertyChecks with Matchers with Checkers  {
  private[this] implicit def strToName(str:String) : Name = TokenCache.fromString(str)

  "transforming AST should work properly" in {
    val loop : For = For(Nil, None, Nil, Block(Select("i") +: Nil))
    val label = new ResolvedLabel("l", "i",  loop, 100)
    label.transformAST(1).statements shouldBe
      Seq(
        MethodInvocation(Nil, new Name(""), IntegerLiteral("1") +: Nil),
        Select("i")
      )

  }

}
