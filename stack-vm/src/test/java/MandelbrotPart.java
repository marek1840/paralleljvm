import java.io.IOException;

import org.jetbrains.annotations.NotNull;

public class MandelbrotPart {
	private final Point coord;
	private final Point z = new Point();

	public MandelbrotPart(Point coord) {
		this.coord = coord;
		z.x = 0;
		z.y = 0;
	}

	public static void main(String[] args) throws IOException {
		int height = 400;
		int width = 400;
		int pointCount = width * height;

		float dy = 2.2F / (height - 1);
		float dx = 4.0F / (width - 1);

		int iterations = 5000;

		MandelbrotPart[] points = create(height, width, pointCount, dy, dx);

		long start = System.currentTimeMillis();
		iterate(pointCount, iterations, points);
		long end = System.currentTimeMillis();

		System.out.println("Executed in: " + (end - start) / 1000.0 + "s");

//		FileWriter out = new FileWriter(new File("/tmp/out.txt"));
//
//		for (int y = 0; y < height; y++) {
//			for (int x = 0; x < width; x++) {
//				int index = y * width + x;
//				MandelbrotPart mand = points[index];
//				Point z = mand.z;
//
//				if(z.x <= 0 || z.x > 0){
//					System.out.print(index + ",");
//				}
//
////				float abs = z.x * z.x + z.y * z.y;
////				String str = abs < 4 ? "#" : " ";
////				out.write(str);
//			}
////			out.write("\n");
//		}
	}

	private static void iterate(int pointCount, int iterations, MandelbrotPart[] points) {
		for (int i = 0; i < pointCount; i++) {
			MandelbrotPart mand = points[i];
			Point z0 = mand.coord;
			Point z = mand.z;

			for (int j = 0; j < iterations; j++) {
				float x = z.x * z.x - z.y * z.y + z0.x;
				float y = 2 * z.x * z.y + z0.y;

				z.x = x;
				z.y = y;
			}
		}
	}

	@NotNull
	private static MandelbrotPart[] create(int height, int width, int pointCount, float dy, float dx) {
		MandelbrotPart[] points = new MandelbrotPart[pointCount];

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				int index = y * width + x;
				Point point = new Point();
				point.y = dy * y - 1;
				point.x = dx * x - 2;
				points[index] = new MandelbrotPart(point);
			}
		}
		return points;
	}
}

class Point {
	float x;
	float y;

	@Override
	public String toString() {
		return String.format("(%s, %s)", x, y);
	}
}