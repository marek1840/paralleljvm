import org.bridj.BridJ;
import org.bridj.Pointer;
import org.bridj.StructObject;
import org.bridj.ann.Field;

import com.nativelibs4java.opencl.CLBuffer;
import com.nativelibs4java.opencl.CLContext;
import com.nativelibs4java.opencl.CLKernel;
import com.nativelibs4java.opencl.CLMem;
import com.nativelibs4java.opencl.CLProgram;
import com.nativelibs4java.opencl.CLQueue;
import com.nativelibs4java.opencl.JavaCL;

public class StructOpenCLTest {

    private  static final String KERNEL_NAME = "initPoints";
    private  static final String KERNEL_CODE = "typedef struct {\n" +
            "  int x;\n" +
            "  int y;\n" +
            "} Point;\n" +
            "\n" +
            "typedef struct {\n" +
            "   Point p;\n" +
            "   float f;\n" +
            "} R;\n" +
            "\n" +
            "__kernel void initPoints(__global R* rs, int value){\n" +
            "   int i = get_global_id(0);\n" +
            "   rs[i].p.x = value * i;\n" +
            "   rs[i].p.y = value * i >> 2;\n" +
            "\n" +
                "\trs[i].f = rs[i].p.x * rs[i].p.y;\n" +
            "}";

    private  static final CLContext ctx = JavaCL.createBestContext();
    private  static final CLQueue queue = ctx.createDefaultQueue();

    public static void main(String[] args) {
        int size = 1000;

        StructObject[] rs = new StructObject[size];
        for (int i = 0; i < rs.length; i++) {
            rs[i] = new JavaR();
        }

        Pointer<StructObject> ptr = Pointer.pointerToArray(rs);

        CLProgram program = ctx.createProgram(KERNEL_CODE);
        CLKernel kernel = program.createKernel(KERNEL_NAME);

        CLBuffer<StructObject> buffer = ctx.createBuffer(CLMem.Usage.InputOutput, ptr);
        kernel.setArgs(buffer, 5);

        long[] globalSizes = new long[]{rs.length};
        long[] localSizes = new long[]{1};

        kernel.enqueueNDRange(queue, null, globalSizes, localSizes);

        queue.finish();

        Pointer<StructObject> result = buffer.read(queue);
        StructObject[] points2 = result.toArray();

        System.out.println(points2.length);
    }

    ;

    public static class JavaPoint extends StructObject {
        static {
            BridJ.register();
        }

        public JavaPoint() {
            super();
        }

        public JavaPoint(Pointer pointer) {
            super(pointer);
        }

        @Field(0)
        public int _get0() {
            return this.io.getIntField(this, 0);
        }

        @Field(0)
        public JavaPoint _set0(int x) {
            this.io.setIntField(this, 0, x);
            return this;
        }

        @Field(1)
        public int _get1() {
            return this.io.getIntField(this, 1);
        }

        @Field(1)
        public JavaPoint _set1(int y) {
            this.io.setIntField(this, 1, y);
            return this;
        }

        @Override
        public String toString() {
            return String.format("(%s, %s)", _get0(), _get1());
        }
    }

    public static class JavaR extends StructObject{
        static {
            BridJ.register();
        }

        public JavaR() {
        }

        public JavaR(org.bridj.Pointer<? extends org.bridj.StructObject> peer) {
            super(peer);
        }

        @Field(0)
        public JavaPoint _get0(){
            return this.io.getNativeObjectField(this, 0);
        }

        @Field(1)
        public float _get1(){
            return this.io.getFloatField(this, 1);
        }

        @Override
        public String toString() {
            return String.format("f%s = %s", _get0(), _get1());
        }
    }
}
