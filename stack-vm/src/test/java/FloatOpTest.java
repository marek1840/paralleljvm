public class FloatOpTest {
    private final int i;
    private final float f;

    public FloatOpTest(int i, float f) {
        this.i = i;
        this.f = f;
    }

    public float ii(){
        return i + i;
    }

    public float ff(){
        return f + f;
    }
    public float if_(){
        return i + f;
    }
    public float fi(){
        return f + i;
    }
}
