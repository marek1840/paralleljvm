public interface HierarchyTest {
    static void main(String[] args) {
        A a = new B();

        ff(a);
    }

    public static void ff(A a){
        a.f();
    }
}

interface A{
    public int f();
}


class B implements A {
    @Override
    public int f() {
        return 2;
    }
}
class C implements A {
    @Override
    public int f() {
        return 3;
    }
}

