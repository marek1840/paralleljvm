package jvm.parallel.coprocessor.v2.instructions

import com.nativelibs4java.opencl.CLMem
import jvm.parallel.ast.name.Name

case class IntArrayToDevice(arrayName:Name, usage:CLMem.Usage) extends CoprocessorInstruction