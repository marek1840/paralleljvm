package jvm.parallel.coprocessor.v2.labels

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.types.primitives.IntegerPrimitive
import jvm.parallel.ast.parsers.statement.Block
import jvm.parallel.ast.parsers.statement.declarator.VariableDeclarator
import jvm.parallel.ast.parsers.statement.instructions.LocalVariableDeclaration
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.MethodInvocation
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.assignment.Binding
import jvm.parallel.ast.parsers.statement.instructions.discardable.literals.IntegerLiteral
import jvm.parallel.ast.parsers.statement.instructions.discardable.reference.Select
import jvm.parallel.ast.parsers.statement.instructions.loop.For
import jvm.parallel.coprocessor.GPU

class ResolvedLabel(val name: Name,
                    val iterator: Name,
                    val loop: For,
                    val arraySize: Int) {
  def transformAST(index: Int): Block = {
    val iterationDeclaration = LocalVariableDeclaration(Nil, Nil, IntegerPrimitive(Nil), Seq(VariableDeclarator(iterator)))
    val iteratorBinding = Binding(Select(iterator), GPUIndex(index))
    val body = loop.body match {
      case Block(statements) => iterationDeclaration +: iteratorBinding +: statements
      case stmt => Seq(iteratorBinding, stmt)
    }

    Block(body)
  }

  def GPUIndex(index: Int): MethodInvocation = {
    MethodInvocation(Nil, GPU.Function.globalId, IntegerLiteral(index.toString) +: Nil)
  }
}
