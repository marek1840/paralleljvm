package jvm.parallel.coprocessor.v2.instructions

import jvm.parallel.ast.name.Name

case class FloatToDevice(name : Name) extends CoprocessorInstruction