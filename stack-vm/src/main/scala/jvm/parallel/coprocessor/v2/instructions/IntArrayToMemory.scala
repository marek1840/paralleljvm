package jvm.parallel.coprocessor.v2.instructions

import jvm.parallel.ast.name.Name

case class IntArrayToMemory(arrayName:Name) extends CoprocessorInstruction