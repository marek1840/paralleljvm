package jvm.parallel.coprocessor.v2.instructions

import jvm.parallel.ast.name.Name

case class IntToDevice(name : Name) extends CoprocessorInstruction