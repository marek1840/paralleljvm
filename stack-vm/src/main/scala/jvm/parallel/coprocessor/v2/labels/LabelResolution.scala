package jvm.parallel.coprocessor.v2.labels

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.BinaryOperations
import jvm.parallel.ast.parsers.expression.operators.BinaryOperator
import jvm.parallel.ast.parsers.expression.types.primitives.IntegerPrimitive
import jvm.parallel.ast.parsers.statement.declarator.InitializedVariableDeclarator
import jvm.parallel.ast.parsers.statement.instructions.discardable.literals.IntegerLiteral
import jvm.parallel.ast.parsers.statement.instructions.discardable.reference.Select
import jvm.parallel.ast.parsers.statement.instructions.loop.For
import jvm.parallel.ast.parsers.statement.instructions.{LabeledStatement, LocalVariableDeclaration}
import jvm.parallel.ast.parsers.statement.{Block, Statement}
import jvm.parallel.stack.{ArrayMemory, Frame}

import scala.collection.+:

trait LabelResolution {
  this : ArrayMemory =>

  private[this] val length = new Name("length")
  private[this] val LesserThan = Seq(BinaryOperator.<)
  private[this] implicit def toOpt[A](a:A):Option[A] = Some(a)

  def resolveLabel(label: Name, methodBody: Statement, frame : Frame): ResolvedLabel = {
    val loop = resolveLabeledStatement(label, methodBody)
    val iterator = resolveIterator(loop)
    val arraySize = resolveArraySize(iterator, loop, frame)
    new ResolvedLabel(label, iterator, loop, arraySize)
  }

  def resolveLabeledStatement(Label: Name, statement: Statement): For = {
    //FIXME almost identical as in GPUMethod::transform
    def findLabeledStmt(stmt:Statement) : Option[For] = stmt match {
      case LabeledStatement(Label, stmt@For(_, _, _, _)) => stmt

      case LabeledStatement(_, stmt) => findLabeledStmt(stmt)
      case For(_, _, _, body) => findLabeledStmt(body)

      case Block(stmts) => stmts flatMap  findLabeledStmt match {
        case Nil => None
        case hd +: Nil => hd
        case invalidStatements => throw new Exception("Multiple statements with same label: " + invalidStatements)
      }
      case _ => None
    }
    val maybeFor: Option[For] = statement flatMap findLabeledStmt
    maybeFor get
  }

  def resolveIterator(loop: For): Name = loop.initializers match {
    case hd +: Nil => hd match {
      case LocalVariableDeclaration(_, _, IntegerPrimitive(_), dec +: Nil) => dec match {
        case InitializedVariableDeclarator(name, IntegerLiteral("0")) => name
        case _ => throw new Exception(s"Variable not initialized with 0")
      }
      case _ => throw new Exception("Not an integer iterator definition")
    }
    case _ => throw new Exception("Initializing more than one variable")
  }

  def resolveArray(Iterator: Name, loop: For): Name = loop.condition match {
    case None => throw new Exception("Infinite loop")
    case Some(expr) => expr match {
      case BinaryOperations(LesserThan, exprs) => exprs match {
        case Seq(Select(Iterator), Select(name)) if name.last == length => name.parent match {
          case Some(arrayName) => arrayName //FIXME check type
          case _ => throw new Exception("Not an array")
        }
        case _ => throw new Exception("Invalid condition. Expected : iterator < arrayName.length")
      }
      case _ => throw new Exception("Invalid condition. Expected : iterator < arrayName.length")
    }
  }

  def resolveArraySize(Iterator: Name, loop: For, frame:Frame) :Int= {
    loop.condition match {
      case Some(expr) => expr match {
        case BinaryOperations(LesserThan, Seq(Select(Iterator), Select(name))) => frame.get(name)
        case _ => throw new Exception("Invalid condition. Expected: iterator < variable")
      }
      case _ => throw new Exception("Unexpected infinite loop")
    }
  }
}
