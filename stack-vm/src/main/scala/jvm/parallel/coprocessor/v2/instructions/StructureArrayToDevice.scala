package jvm.parallel.coprocessor.v2.instructions

import com.nativelibs4java.opencl.CLMem
import jvm.parallel.ast.name.Name
import jvm.parallel.vm2.TypeSignature

case class StructureArrayToDevice(typeSignature: TypeSignature.Type,
                                  arrayName: Name,
                                  usage: CLMem.Usage) extends CoprocessorInstruction
