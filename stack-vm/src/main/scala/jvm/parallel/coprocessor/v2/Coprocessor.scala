package jvm.parallel.coprocessor.v2

import com.nativelibs4java.opencl._
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.statement.declaration.members.MethodDeclaration
import jvm.parallel.coprocessor.adapters.StructureAdapter
import jvm.parallel.coprocessor.kernel.KernelArguments
import jvm.parallel.coprocessor.kernel.args.StructArrayArg
import jvm.parallel.coprocessor.kernel.generation.KernelGeneration
import jvm.parallel.coprocessor.v2.instructions._
import jvm.parallel.coprocessor.v2.labels.LabelResolution
import jvm.parallel.coprocessor.{GPUCompilationConfig, GPUMethod, InstructionPreparation}
import jvm.parallel.stack.containers.StructureStorageContainer
import jvm.parallel.stack.suits.StatisticsHelper
import jvm.parallel.stack.{ArrayMemory, FrameStack}
import jvm.parallel.vm2.annotations.{DirectiveResolution, VMDirective}
import jvm.parallel.vm2.components.CoprocessorComponent
import org.bridj.StructObject

trait Coprocessor extends CoprocessorComponent with LabelResolution with InstructionPreparation with StatisticsHelper {
  this: ArrayMemory
    with FrameStack
    with LabelResolution
    with StructureStorageContainer
    with StructureAdapter =>

  private[this] val directiveResolution = new DirectiveResolution

  private[this] val ctx: CLContext = JavaCL.createBestContext
  private[this] val queue: CLQueue = ctx.createDefaultQueue()
  //  private[this] val kernelExecution = new KernelExecution(ctx, queue)

  override type GPUExecutionUnit = GPUMethod

  override def compileExecutionUnit(method: MethodDeclaration): GPUMethod = {
    val name = method.declarator.name

    val directives = method.annotations flatMap resolveDirective
    val config = GPUCompilationConfig(directives)
    val labeledStatements = config.labels.map(label => resolveLabel(label, method.body, topFrame())).toSeq

    val instructions = prepareInstructions(method.declarator)

    new GPUMethod(name, instructions, method, labeledStatements)
  }

  override def execute(executionUnit: GPUMethod): Unit = {
    val globalSizes = executionUnit.workingSize
    val kernelArguments = new KernelArguments(globalSizes)
    val usedStructures = executionUnit.usedStructures().map(structureArea.getStructure)

    val ast = executionUnit.transform()

    val frame = topFrame()

    val instructions: Seq[CoprocessorInstruction] = executionUnit.instructions

    instructions foreach {
      case IntToDevice(name) =>
        val value = frame.get(name)
        kernelArguments.addIntegerArg(value)

      case FloatToDevice(name) =>
        val value = frame.get(name)
        kernelArguments.addFloatArg(value)

      case IntArrayToDevice(name, usage) =>
        val address = frame.get(name)
        val size = readFromMemory(address)
        val memoryChunk = readMemoryChunk(address + 1, size).map(new Integer(_))
        kernelArguments.addIntegerArrayArgument(memoryChunk, usage)

      case IntArrayToMemory(name) =>
        val address = frame.get(name)
        val outputArg = kernelArguments.getNextOutputArgument
        val memoryChunk = outputArg.getValue(ctx, queue).asInstanceOf[Array[Integer]].map(_.toInt)
        writeMemoryChunk(address, memoryChunk)

      case StructureArrayToDevice(t, name, usage) =>
        measure("Adapt struct array for device")(s => t => s.addCopyFromHostTime(t)) {
          val address = frame.get(name)
          val adaptedMemoryChunk = adaptMemoryArray(t.name, address)
          kernelArguments.addStructArrayArgument(adaptedMemoryChunk, usage)
        }
      case StructureArrayToMemory(t, name) =>
        measure("Adapt struct array for host")(s => t => s.addCopyToHostTime(t)) {
          val address = frame.get(name)
          val outputArg = kernelArguments.getNextOutputArgument.asInstanceOf[StructArrayArg[StructObject]]
          val deviceOutput = outputArg.getValue(ctx, queue)
          adaptCoprocessorArray(t.name, address, deviceOutput)
        }
      case InvokeOnDevice(name) =>
        measure("Kernel")(s => t => s.addExecutionTime(t)) {

          val kernelSource = new KernelGeneration(usedStructures, ast)
            .generateKernel()

          val kernelName = executionUnit.name.representation

          val program: CLProgram = ctx.createProgram(kernelSource)
          val kernel: CLKernel = program.createKernel(kernelName)

          kernel.setArgs(kernelArguments.getArgs(ctx): _*)
          kernel.enqueueNDRange(queue, null, kernelArguments.getGlobalWorkSizes, kernelArguments.getLocalWorkSizes)

          queue.finish()
        }
      case i => throw new Exception("Unsupported instruction: " + i)
    }


  }

  private[this] def resolveDirective(annotation: Annotation): Option[VMDirective] = directiveResolution.reduce(annotation)
}

