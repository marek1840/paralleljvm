package jvm.parallel.coprocessor.v2.instructions

import jvm.parallel.ast.name.Name
import jvm.parallel.vm2.TypeSignature

case class StructureArrayToMemory(typeSignature: TypeSignature.Type,
                                  arrayName: Name) extends CoprocessorInstruction
