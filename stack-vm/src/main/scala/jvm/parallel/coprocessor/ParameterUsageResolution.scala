package jvm.parallel.coprocessor

import com.nativelibs4java.opencl.CLMem
import jvm.parallel.ast.parsers.ASTReducer
import jvm.parallel.ast.parsers.statement.modifers.{Input, InputOutput, Modifier, Output}

class ParameterUsageResolution extends ASTReducer[Modifier, Option[CLMem.Usage]]{
  private[this] implicit def toOpt[A](a:A) : Option[A] = Some(a)

  override def reduce(node: Modifier): OUT = node match {
    case Input => CLMem.Usage.Input
    case Output => CLMem.Usage.Output
    case InputOutput => CLMem.Usage.InputOutput
    case _ => None
  }
}
