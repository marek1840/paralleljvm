package jvm.parallel.coprocessor.adapters.handles

import jvm.parallel.stack.ArrayMemory
import org.bridj.StructObject

class StructureHandleTemplate[A <: StructObject](val clazz: Class[A],
                                                 val handles: Iterable[FieldHandle[_]]) {
  def createArray(size: Int): Array[A] = {
    java.lang.reflect.Array.newInstance(clazz, size).asInstanceOf[Array[A]]
  }

  def writeToStructure(struct: StructObject = clazz.newInstance())
                      (structAddress: Int, memory: ArrayMemory): StructObject = {
    handles.foreach(_.writeToStructure(struct)(structAddress, memory))
    struct
  }

  def readFromStructure(struct: StructObject = clazz.newInstance())
                       (structAddress: Int, memory: ArrayMemory): StructObject = {
    handles.foreach(_.readFromStructure(struct)(structAddress, memory))
    struct
  }
}
