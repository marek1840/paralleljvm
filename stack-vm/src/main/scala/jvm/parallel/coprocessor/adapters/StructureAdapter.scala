package jvm.parallel.coprocessor.adapters

import jvm.parallel.coprocessor.adapters.handles.StructureHandleTemplateFactory
import jvm.parallel.logging.Logger
import jvm.parallel.stack.ArrayMemory
import jvm.parallel.stack.containers.StructureStorageContainer
import jvm.parallel.vm2.structs.ClassName
import org.bridj.StructObject

trait StructureAdapter extends Logger {
  this: ArrayMemory
    with StructureStorageContainer
    with StructureHandleTemplateFactory =>

  def adaptMemoryArray(className: ClassName, address: Int): Array[StructObject] = {
    val size = readFromMemory(address)
    val memoryChunk = readMemoryChunk(address + 1, size) // skip array length

    val template = createTemplate(className)
    val array = template.createArray(size)

    for {
      i <- 0 until size
      address = memoryChunk(i)
    } array(i) = template.writeToStructure()(address, this)
    array
  }

  def adaptCoprocessorArray(className: ClassName, address: Int, array: Array[StructObject]): Unit = {
    val size = readFromMemory(address)
    val memoryChunk = readMemoryChunk(address + 1, size) // skip array length

    for {
      i <- 0 until size
      address = memoryChunk(i)
      struct = array(i)
    } yield createTemplate(className).readFromStructure(struct)(address, this)
  }
}
