package jvm.parallel.coprocessor.adapters.handles

import jvm.parallel.stack.ArrayMemory
import org.bridj.StructObject


protected[handles] class StructHandle[A <: StructObject](offset: Int,
                                                         private[handles] val template: StructureHandleTemplate[A])
  extends FieldHandle[StructObject](offset, classOf[StructObject]) {

  override def writeToStructure(outerStruct: StructObject)
                               (outerAddress: Int,
                                memory: ArrayMemory): Unit = {
    val structAddress = memory.readFromMemory(outerAddress + offset)
    val innerStruct = getter(outerStruct).invoke(outerStruct).asInstanceOf[StructObject]
    template.writeToStructure(innerStruct)(structAddress, memory)
  }

  override def readFromStructure(outerStruct: StructObject)
                                (outerAddress: Int,
                                 memory: ArrayMemory): Unit = {
    val structAddress = memory.readFromMemory(outerAddress + offset)
    val innerStruct = getter(outerStruct).invoke(outerStruct).asInstanceOf[StructObject]
    template.readFromStructure(innerStruct)(structAddress, memory)
  }
}
