package jvm.parallel.coprocessor.adapters.handles

import jvm.parallel.coprocessor.adapters.StructureGeneration
import jvm.parallel.stack.containers.StructureStorageContainer
import jvm.parallel.vm2.TypeSignature
import jvm.parallel.vm2.TypeSignature._
import jvm.parallel.vm2.structs.{ClassName, StructureDefinitionEntry}
import org.bridj.StructObject

import scala.collection.mutable

trait StructureHandleTemplateFactory {
  this: StructureStorageContainer
    with StructureGeneration =>

  private[this] val compiledClasses = mutable.Map[ClassName, Class[StructObject]]()

  def createTemplate(className: ClassName): StructureHandleTemplate[StructObject] = {
    val structureDef = structureArea.getStructure(className)

    val handles = structureDef.fields.values.map(createHandle)
    val clazz = compiledClasses.getOrElseUpdate(className, compileNewClass(structureDef))

    new StructureHandleTemplate(clazz, handles)
  }

  private[this] def createHandle(fieldEntry: StructureDefinitionEntry): FieldHandle[_] = {
    createHandle(fieldEntry.typeSignature, fieldEntry.offset)
  }

  private[this] def createHandle(signature: TypeSignature, offset: Int): FieldHandle[_] = signature match {
    case Integer => new IntHandle(offset)
    case Float => new FloatHandle(offset)

    case Type(className) =>
      val directProxy = createTemplate(className)
      new StructHandle(offset, directProxy)
  }
}
