package jvm.parallel.coprocessor.adapters.handles

import jvm.parallel.stack.ArrayMemory
import org.bridj.StructObject

protected[handles] class IntHandle(offset: Int) extends FieldHandle[Integer](offset, Integer.TYPE) {
  override def writeToStructure(instance: StructObject)
                               (memoryAddress: Int,
                                memoryComponent: ArrayMemory): Unit = {
    val finalAddress = memoryAddress + offset
    val value = memoryComponent.readFromMemory(finalAddress).asInstanceOf[Object]
    setter(instance).invoke(instance, value)
  }

  override def readFromStructure(instance: StructObject)
                                (memoryAddress: Int,
                                 memoryComponent: ArrayMemory): Unit = {
    val finalAddress = memoryAddress + offset
    val value = getter(instance).invoke(instance).asInstanceOf[Int]

    memoryComponent.writeToMemory(finalAddress, value)
  }
}