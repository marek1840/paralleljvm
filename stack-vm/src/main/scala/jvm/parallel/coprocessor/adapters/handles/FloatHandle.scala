package jvm.parallel.coprocessor.adapters.handles

import java.lang.Float

import jvm.parallel.stack.ArrayMemory
import org.bridj.StructObject

protected[handles] class FloatHandle(offset: Int) extends FieldHandle[Float](offset, Float.TYPE) {
  override def writeToStructure(instance: StructObject)
                               (memoryAddress: Int,
                                memoryComponent: ArrayMemory): Unit = {
    val finalAddress = memoryAddress + offset
    val bits: Int = memoryComponent.readFromMemory(finalAddress)
    val value = Float.intBitsToFloat(bits).asInstanceOf[Object]
    setter(instance).invoke(instance, value)
  }

  override def readFromStructure(instance: StructObject)
                                (memoryAddress: Int,
                                 memoryComponent: ArrayMemory): Unit = {
    val finalAddress = memoryAddress + offset
    val bits = getter(instance).invoke(instance).asInstanceOf[Float]
    val value = Float.floatToIntBits(bits)

    memoryComponent.writeToMemory(finalAddress, value)
  }
}