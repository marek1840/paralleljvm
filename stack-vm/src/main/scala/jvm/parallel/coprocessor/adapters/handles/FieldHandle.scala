package jvm.parallel.coprocessor.adapters.handles

import java.lang.reflect.Method

import jvm.parallel.stack.ArrayMemory
import org.bridj.StructObject

protected[handles] abstract class FieldHandle[A](val offset: Int, val classType: Class[A]) {
  private[this] val index = offset - 1

  def writeToStructure(instance: StructObject)
                      (memoryAddress: Int,
                       memoryComponent: ArrayMemory): Unit

  def readFromStructure(instance: StructObject)
                       (memoryAddress: Int,
                        memoryComponent: ArrayMemory): Unit

  protected def setter(struct: StructObject): Method = struct.getClass.getMethod(s"set_$index", classType)

  protected def getter(struct: StructObject): Method = struct.getClass.getMethod(s"get_$index")
}



