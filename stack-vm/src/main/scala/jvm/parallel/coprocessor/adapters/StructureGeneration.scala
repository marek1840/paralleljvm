package jvm.parallel.coprocessor.adapters

import jvm.parallel.ast.name.{Name, TokenCache}
import jvm.parallel.logging.Logger
import jvm.parallel.vm2.TypeSignature
import jvm.parallel.vm2.TypeSignature._
import jvm.parallel.vm2.structs.{ClassName, StructureDefinition, StructureDefinitionEntry}
import net.openhft.compiler.CompilerUtils
import org.bridj.StructObject

trait StructureGeneration extends Logger {
  private[this] def className(name: Name): Name = TokenCache.fromString(s"Adapted_$name")

  protected[coprocessor] def compileNewClass(struct: StructureDefinition): Class[StructObject] = {
    val name = className(struct.name.name)
    val qualifiedName = s"jvm.parallel.adapters.$name"
    val source = generateSource(name, struct)
    CompilerUtils.CACHED_COMPILER.loadFromJava(qualifiedName, source).asInstanceOf[Class[StructObject]]
  }

  private[this] def generateSource(name: Name, struct: StructureDefinition): String = {
    val methods = adaptFields(struct.fields.values)
    val source = generateSource(name, methods)

//    logger.info(s"Generated structure proxy for [$struct]: \n$source")

    source
  }

  private[this] def adaptFields(fields: Iterable[StructureDefinitionEntry]): String = {
    fields map generateMethod mkString "\n"
  }

  private[this] def generateSource(name: Name, methods: String): String = {
    s"""package jvm.parallel.adapters;
        |
        |import org.bridj.BridJ;
        |import org.bridj.Pointer;
        |import org.bridj.StructObject;
        |import org.bridj.ann.Field;
        |
        |public class $name extends StructObject {
        |
        | public $name() {super();}
        |
        | public $name(Pointer<StructObject> pointer) { super(pointer); }
        |
        |$methods
        |}
      """.stripMargin
  }

  private[this] def generateMethod(field: StructureDefinitionEntry): String = {
    generateMethod(field.typeSignature, field.offset - 1)
  }

  private[this] def generateMethod(fieldType: TypeSignature, offset: Int) = fieldType match {
    case Integer => integerField(offset)
    case Float => floatField(offset)
    case Type(ClassName(_, name)) => structField(className(name), offset)
  }

  private[this] def integerField(i: Int): String = {
    s"""  @Field($i)
        |  public int get_$i(){ return this.io.getIntField(this, $i); }
        |
        |  @Field($i)
        |  public void set_$i(int value){ this.io.setIntField(this, $i, value); }
       """.stripMargin
  }

  private[this] def floatField(i: Int): String = {
    s"""  @Field($i)
        |  public float get_$i(){ return this.io.getFloatField(this, $i); }
        |
        |  @Field($i)
        |  public void set_$i(float value){ this.io.setFloatField(this, $i, value); }
       """.stripMargin
  }

  private[this] def structField(name: Name, i: Int): String = {
    s"""  @Field($i)
        |  public $name get_$i(){ return this.io.getNativeObjectField(this, $i); }
        |
        |  @Field($i)
        |  public void set_$i($name value){ this.io.setNativeObjectField(this, $i, value); }
       """.stripMargin
  }
}
