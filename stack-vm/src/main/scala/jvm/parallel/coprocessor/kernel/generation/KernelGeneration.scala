package jvm.parallel.coprocessor.kernel.generation

import jvm.parallel.ast.parsers.statement.declaration.members.MethodDeclaration
import jvm.parallel.logging.Logger
import jvm.parallel.translator.opencl.OpenCLPrinters
import jvm.parallel.vm2.structs.StructureDefinition

class KernelGeneration(usedStructures: Seq[StructureDefinition],
                       // TODO usedFunctions : Set[MethodDeclaration],
                       kernelAST: MethodDeclaration) extends Logger {

  def generateKernel() : String = {
    val structs : String = generateStructures()
    val kernel : String = OpenCLPrinters.method(0).print(kernelAST)

    val source = s"""$structs
       |
       |$kernel
     """.stripMargin

//    logger.info("Generated kernel:\n " + source)
    source
  }

  def generateStructures(): String = {
    usedStructures
      .map(OpenCLPrinters.structureDefinition(1).print)
      .mkString("\n")
  }
}
