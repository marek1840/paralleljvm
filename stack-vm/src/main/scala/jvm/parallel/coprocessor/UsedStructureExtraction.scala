package jvm.parallel.coprocessor

import jvm.parallel.ast.parsers.ASTReducer
import jvm.parallel.ast.parsers.expression.types.references.ClassType
import jvm.parallel.ast.parsers.statement.declaration.members.MethodDeclaration
import jvm.parallel.ast.parsers.visitor.ASTVisitor
import jvm.parallel.vm2.structs.ClassName

import scala.collection.mutable

class UsedStructureExtraction extends ASTReducer[MethodDeclaration, Set[ClassName]] {
  private[this] val types: mutable.Set[ClassType] = mutable.Set()

  override def reduce(node: MethodDeclaration): OUT = {
    node.accept(new ClassTypeCollector)
    types map resolveClassName toSet
  }

  private[this] def resolveClassName(classType: ClassType): ClassName = classType match {
    case ClassType(_, None, name, _) => ClassName(name.dropLast, name.last)
  }

  private[this] class ClassTypeCollector extends ASTVisitor {
    override def onEnter(t: ClassType) = {
      types.add(t)
    }
  }

}
