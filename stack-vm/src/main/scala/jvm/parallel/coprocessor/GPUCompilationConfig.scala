package jvm.parallel.coprocessor

import jvm.parallel.ast.name.{Name, TokenCache}
import jvm.parallel.vm2.annotations.{MakeParallel, VMDirective}

import scala.collection.mutable

class GPUCompilationConfig private[coprocessor] {
  private[coprocessor] val labels : mutable.Set[Name] = mutable.Set()

  private[coprocessor] def addLabel(label:String) : Unit = labels += TokenCache.fromString(label)
}

object GPUCompilationConfig{
  def apply(directives : Iterable[VMDirective]) : GPUCompilationConfig = {
    val configuration= new GPUCompilationConfig

    directives.foreach {
      case _@MakeParallel(labels) => labels.foreach(configuration.addLabel(_))
      case _ =>
    }

    configuration
  }
}
