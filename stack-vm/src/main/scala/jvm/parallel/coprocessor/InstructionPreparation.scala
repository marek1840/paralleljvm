package jvm.parallel.coprocessor

import com.nativelibs4java.opencl.CLMem
import com.nativelibs4java.opencl.CLMem.Usage
import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.expression.types.primitives.{FloatPrimitive, IntegerPrimitive}
import jvm.parallel.ast.parsers.expression.types.references.ArrayType
import jvm.parallel.ast.parsers.statement.declarator.{FunctionDeclarator, MethodDeclarator}
import jvm.parallel.ast.parsers.statement.parameter.{FormalParameter, Parameter}
import jvm.parallel.coprocessor.v2.instructions._
import jvm.parallel.stack.ArrayMemory
import jvm.parallel.vm2.TypeSignature
import jvm.parallel.vm2.annotations.ModifierResolution

// TODO separate class for each builder
trait InstructionPreparation {
  this: ArrayMemory =>
  private[this] implicit def toOpt[A](a: A): Option[A] = Some(a)

  private[this] type InstructionBuilder = (Name, CLMem.Usage) => CoprocessorInstruction
  private[this] type InstructionOutBuilder = (Name) => CoprocessorInstruction

  private[this] type StructInstructionBuilder = (TypeSignature.Type, Name, CLMem.Usage) => CoprocessorInstruction
  private[this] type StructureOutBuilder = (TypeSignature.Type, Name) => CoprocessorInstruction

  private[this] val usageResolution = new ParameterUsageResolution
  private[this] val modifierResolution = new ModifierResolution

  def prepareInstructions(declarator: FunctionDeclarator): Seq[CoprocessorInstruction] = declarator match {
    case MethodDeclarator(name, params) =>
      val inputParams = deviceInputParams(params)
      val outputParams = deviceOutputParams(params)

      inputParams ++ (InvokeOnDevice(name) +: outputParams)
  }

  def deviceInputParams(params: Seq[Parameter]): Seq[CoprocessorInstruction] = params flatMap inputInstruction

  def deviceOutputParams(params: Seq[Parameter]): Seq[CoprocessorInstruction] = {
    params.filter(isOutputParameter)
      .flatMap(outputInstruction)
  }


  //TODO transform ArrayType -> pointerType
  def inputInstruction(p: Parameter): Option[CoprocessorInstruction] = p match {
    case FormalParameter(as, _, ArrayType(IntegerPrimitive(_), hd +: Nil), name) =>
      buildInstruction(as, name, IntArrayToDevice.apply)

    case FormalParameter(as, _, ArrayType(t, hd +: Nil), name) =>
      buildInstruction(t, as, name, StructureArrayToDevice.apply)

    case FormalParameter(_, _, IntegerPrimitive(_), name) => Some(IntToDevice(name))
    case FormalParameter(_, _, FloatPrimitive(_), name) => Some(FloatToDevice(name))

    case param => throw new Exception("Unexpected parameter: " + param)
  }

  //TODO transform ArrayType -> pointerType
  def outputInstruction(p: Parameter): Option[CoprocessorInstruction] = p match {
    case FormalParameter(as, _, ArrayType(IntegerPrimitive(_), hd +: Nil), name) =>
      buildOutInstruction(as, name, IntArrayToMemory.apply)

    case FormalParameter(as, _, ArrayType(t, hd +: Nil), name) =>
      buildOutInstruction(t, as, name, StructureArrayToMemory.apply)
    case FormalParameter(_, _, IntegerPrimitive(_), name) => None

    case param => throw new Exception("Unexpected parameter: " + param)
  }

  def buildOutInstruction(t: Type, as: Seq[Annotation],
                          name: Name,
                          apply: StructureOutBuilder): Option[CoprocessorInstruction] = {
    usage(as) match {
      case CLMem.Usage.InputOutput | CLMem.Usage.Output => apply(TypeSignature(t).asInstanceOf[TypeSignature.Type], name)
      case _ => None
    }
  }

  def buildOutInstruction(as: Seq[Annotation],
                          name: Name,
                          apply: InstructionOutBuilder): Option[CoprocessorInstruction] = {
    usage(as) match {
      case CLMem.Usage.InputOutput | CLMem.Usage.Output => apply(name)
      case _ => None
    }
  }

  def buildInstruction(t: Type,
                       as: Seq[Annotation],
                       name: Name,
                       apply: StructInstructionBuilder): Option[CoprocessorInstruction] = {
    val signature: TypeSignature.Type = TypeSignature(t).asInstanceOf[TypeSignature.Type]
    apply(signature, name, usage(as))
  }

  def buildInstruction(as: Seq[Annotation], name: Name,
                       f: InstructionBuilder): Option[CoprocessorInstruction] = f(name, usage(as))

  //TODO transform ArrayType -> pointerType
  def isOutputParameter(p: Parameter): Boolean = p match {
    case FormalParameter(as, _, ArrayType(_, _), _) => usage(as) match {
      case CLMem.Usage.Output => true
      case CLMem.Usage.InputOutput => true
      case _ => false
    }
    case _ => false
  }

  private[this] def usage(annotations: Seq[Annotation]): Usage = {
    val modifiers = annotations.flatMap(modifierResolution.reduce)
    val usages = modifiers.flatMap(usageResolution.reduce)
    assert(usages.size == 1)
    usages.head
  }
}
