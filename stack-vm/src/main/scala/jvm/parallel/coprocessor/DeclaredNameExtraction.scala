package jvm.parallel.coprocessor

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.ASTReducer
import jvm.parallel.ast.parsers.statement.declaration.members.MethodDeclaration
import jvm.parallel.ast.parsers.statement.declarator.{InitializedVariableDeclarator, VariableDeclarator}
import jvm.parallel.ast.parsers.statement.parameter.FormalParameter
import jvm.parallel.ast.parsers.visitor.ASTVisitor

import scala.collection.mutable

class DeclaredNameExtraction extends ASTReducer[MethodDeclaration, Seq[Name]] with ASTVisitor {

  private[this] class NameCollector extends ASTVisitor {
    val names = mutable.Buffer[Name]()

    override def onEnter(parameter: FormalParameter): Unit = names += parameter.name
    override def onEnter(parameter: InitializedVariableDeclarator): Unit = names += parameter.name
    override def visit(declarator: VariableDeclarator): Unit = names += declarator.name
  }

  override def reduce(node: MethodDeclaration): OUT = {
    val nameCollector = new NameCollector
    node.accept(nameCollector)

    nameCollector.names
  }
}
