package jvm.parallel.coprocessor

import jvm.parallel.ast.parsers.expression.types.references.ArrayType
import jvm.parallel.ast.parsers.expression.types.{PointerType, Type}
import jvm.parallel.ast.parsers.mapper.ASTMapper

class GPUMapper extends ASTMapper{
  override def mapType(node: Type): Type = node match {
    case ArrayType(t, hd +: Nil) => PointerType(t)
    case _ => super.mapType(node)
  }
}
