package jvm.parallel.coprocessor

import jvm.parallel.ast.name.Name

object GPU {
  object Function{
    val globalId = new Name("get_global_id")
  }
}
