package jvm.parallel.coprocessor

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.types.references.ArrayType
import jvm.parallel.ast.parsers.statement.declaration.members.MethodDeclaration
import jvm.parallel.ast.parsers.statement.declarator.MethodDeclarator
import jvm.parallel.ast.parsers.statement.instructions.LabeledStatement
import jvm.parallel.ast.parsers.statement.instructions.loop.For
import jvm.parallel.ast.parsers.statement.modifers.{Global, Kernel}
import jvm.parallel.ast.parsers.statement.parameter.FormalParameter
import jvm.parallel.ast.parsers.statement.{Block, Statement}
import jvm.parallel.coprocessor.kernel.WorkSizes
import jvm.parallel.coprocessor.v2.instructions.CoprocessorInstruction
import jvm.parallel.coprocessor.v2.labels.ResolvedLabel
import jvm.parallel.vm2.structs.ClassName

class GPUMethod(val name: Name,
                val instructions: Seq[CoprocessorInstruction],
                ast: MethodDeclaration,
                labels: Seq[ResolvedLabel]) {
  assume(labels.size <= 3)

  val workingSize = labels.map(_.arraySize) match {
    case x +: y +: z +: Nil => new WorkSizes(x, y, z)
    case x +: y +: Nil => new WorkSizes(x, y)
    case x +:Nil => new WorkSizes(x)
    case Nil => throw new Exception("No labels")
  }
  val method = transform()

  def usedStructures() : Seq[ClassName] = {
    val structs= new UsedStructureExtraction().reduce(method)
    // TODO sort by dependency
    structs.toSeq
  }

  //TODO map annotations to modifiers
  def transform(): MethodDeclaration = {
    val mappedLabels = labels.zipWithIndex
      .map {
        case (l, index) => (l.name, l.transformAST(index))
      } toMap

    def transform(stmt: Statement): Statement = stmt match {
      case LabeledStatement(l, _) if mappedLabels contains l =>
        val astFromLabel = mappedLabels(l)
        transform(astFromLabel)

      case LabeledStatement(_, innerStatement) => transform(innerStatement)
      case For(inits, cond, upd, body) => {
        val newBody = transform(body)
        For(inits, cond, upd, newBody)
      }

      case Block(stmts) => Block(stmts map transform)
      case s => s
    }

    val newBody = transform(ast.body)

    val parameters = ast.declarator.parameters   .map{
      case p@FormalParameter(_, _, ArrayType(_, _), _) => p.copy(modifiers = Seq(Global))
      case p => p
    }

    val declarator = ast.declarator match {
      case d@MethodDeclarator(_, _) => d.copy(parameters = parameters)
    }

    val methodAST = ast.copy(body = newBody,
      modifiers = Seq(Kernel), declarator = declarator) //TODO copy only body
    val mapper = new GPUMapper

    mapper.mapMethod(methodAST)

  }
}
