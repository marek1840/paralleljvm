package jvm.parallel.stack.containers

import jvm.parallel.stack.CodeStorage
import jvm.parallel.vm2.containers.CodeAreaContainer

trait CodeStorageContainer extends CodeAreaContainer {
  override protected[this] val codeArea : CodeStorage
}
