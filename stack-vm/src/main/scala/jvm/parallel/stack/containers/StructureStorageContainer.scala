package jvm.parallel.stack.containers

import jvm.parallel.vm2.containers.StructAreaContainer
import jvm.parallel.vm2.structs.StructureStorage

trait StructureStorageContainer extends StructAreaContainer {
  override protected[this] val structureArea: StructureStorage
}
