package jvm.parallel.stack

import jvm.parallel.ast.parsers.statement.declaration.members.MethodDeclaration
import jvm.parallel.stack.ops.Instruction

class Method(val instructions : Seq[Instruction], val ast : Option[MethodDeclaration] = None)