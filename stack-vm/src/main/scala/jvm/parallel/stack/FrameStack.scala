package jvm.parallel.stack

import jvm.parallel.stack.{Frame => VMFrame}
import jvm.parallel.vm2.components.CallStackComponent

trait FrameStack extends CallStackComponent {
  type Frame = VMFrame

  private[this] var frames = List[Frame]()

  override def topFrame(): Frame = frames.head

  override def pushFrame(frame: Frame): Unit = {
    frames = frame +: frames
  }

  override def popFrame(): Frame = {
    val head = frames.head
    frames = frames.tail
    head
  }
}
