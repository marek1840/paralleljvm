package jvm.parallel.stack.ops

import jvm.parallel.ast.name.Name
import jvm.parallel.vm2.structs.{ClassName, FieldName}


sealed trait Instruction
case object Halt extends Instruction
case object PrintTop extends Instruction

// Arithmetic

case object Add extends Instruction
case object Subtract extends Instruction
case object Multiply extends Instruction
case object Divide extends Instruction

case object AddFloat extends Instruction
case object SubtractFloat extends Instruction
case object MultiplyFloat extends Instruction
case object DivideFloat extends Instruction

case object IntToFloat extends Instruction

case class Push(value: Int) extends Instruction
case class PushFloat(value: Float) extends Instruction
case object Pop extends Instruction

case class Store(variable: Name) extends Instruction
case class Load(variable: Name) extends Instruction
case object DuplicateTop extends Instruction

case class GoTo(instructionPointer : Int) extends Instruction
case class GoIfTrue(instructionPointer : Int) extends Instruction
case class GoIfFalse(instructionPointer : Int) extends Instruction

case class Invoke(methodName: Name) extends Instruction
case class InvokeOnGPU(methodName : Name) extends Instruction
case object Return extends Instruction

// Arrays

case object ArrayLoad extends Instruction
case object ArrayStore extends Instruction
case object ArraySize extends Instruction
case object NewArray extends Instruction

// objects

case class NewObject(className: ClassName) extends Instruction
case class FieldStore(fieldName: FieldName) extends Instruction
case class FieldLoad(fieldName: FieldName) extends Instruction

// Comparison

case object Equal extends Instruction
case object NotEqual extends Instruction
case object Greater extends Instruction
case object Lesser extends Instruction
case object GreaterOrEqual extends Instruction
case object LesserOrEqual extends Instruction