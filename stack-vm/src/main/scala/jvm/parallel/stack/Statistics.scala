package jvm.parallel.stack

import scala.collection.mutable

class Statistics{
  val copyToDevice = mutable.Map[Int, mutable.Buffer[Long]]()
  val copyFromDevice = mutable.Map[Int, mutable.Buffer[Long]]()
  val executeKernel = mutable.Buffer[Long]()

  def addCopyFromDeviceTime(size: Int, time: Long): Unit = {
    copyFromDevice.getOrElseUpdate(size, mutable.Buffer()) += time
  }

  def addCopyToDeviceTime(size: Int, time: Long): Unit = {
    copyFromDevice.getOrElseUpdate(size, mutable.Buffer()) += time
  }

  def addKernelExecutionTime(time:Long) : Unit = {
    executeKernel += time
  }
}
