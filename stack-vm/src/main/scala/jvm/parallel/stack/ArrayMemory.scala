package jvm.parallel.stack

import jvm.parallel.vm2.components.MemoryComponent

trait ArrayMemory extends MemoryComponent {
  override type MemoryAddress = Int
  override type Operand = Int
  private[this] var firstFreeSlot = 0

  val memorySize : Int
  private[this] val memory: Array[MemoryAddress] = Array.ofDim[MemoryAddress](memorySize)

  override def readFromMemory(address: MemoryAddress): Int = memory(address)
  override def writeToMemory(address: MemoryAddress, operand: Operand): Unit = memory(address) = operand

  override def writeMemoryChunk(address: MemoryAddress, chunk: Array[Operand]): Unit = {
    System.arraycopy(chunk, 0, memory, address, chunk.length)
  }

  override def readMemoryChunk(address: MemoryAddress, amount: MemoryAddress): Array[Operand] = {
    memory.slice(address, address + amount)
  }

  override def allocate(size: Int): MemoryAddress = {
    val address = firstFreeSlot
    firstFreeSlot += size
    address
  }
}
