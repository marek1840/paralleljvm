package jvm.parallel.stack

import java.util

import jvm.parallel.vm2.components.OperandStackComponent

trait OperandStack extends OperandStackComponent {
  override type Operand = Int

  private[this] val stack = new util.LinkedList[Operand]()

  override def push(operand: Operand): Unit = stack.push(operand)
  override def pop(): Operand = stack.poll()
  override def top(): Operand = stack.peek()
  override def stackIsEmpty: Boolean = stack.isEmpty
  override def operand(n:Int): Option[Operand] =
    if(stack.size() <= n) None
    else Some(stack.get(n))
}
