package jvm.parallel.stack

import jvm.parallel.vm2.components.LocalVariableComponent

import scala.collection.mutable

trait Locals extends LocalVariableComponent {
  override type Operand = Int
  private[this] var locals = mutable.Map[Key, Operand]()

  override def get(key: Key): Operand = locals(key)
  override def put(key: Key, value: Operand): Unit = locals += (key -> value)
}
