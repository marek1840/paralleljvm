package jvm.parallel.stack

import jvm.parallel.stack.containers.{CodeStorageContainer, StructureStorageContainer}
import jvm.parallel.stack.modules._
import jvm.parallel.stack.modules.coproessor.CoprocessorModule
import jvm.parallel.stack.ops._
import jvm.parallel.vm2.Thread
import jvm.parallel.vm2.structs.StructureStorage

import scala.annotation.tailrec

abstract class StackThread(val memorySize: Int,
                           val codeArea: CodeStorage,
                           val structureArea: StructureStorage)
  extends Thread with CodeStorageContainer with StructureStorageContainer
          with ArithmeticModule with ArraysModule with ComparisonModule
          with InvocationModule with StructureModule with LocalVariableModule
          with StackModule with FlowModule with CoprocessorModule {

  this: ArrayMemory
    with FrameStack
    with MethodDispatch
    with OperandStack =>

  protected[this] def printTop(): Unit = {
    val top = pop()
//    println("top: " + top)
    push(top)
  }

  override def run(): Unit = {
    val start = System.currentTimeMillis()

    loop()

    val end = System.currentTimeMillis()

    println("Finished in: " + (end - start) / 1000.0 + "s")

    printTop()
  }

  def stack(n:Int) : String = {
    operand(n).map("" + _).getOrElse("")
  }

  @tailrec
  private[this] def loop(): Unit = {
    val instruction: Instruction = topFrame().nextInstruction
//    println(s"$instruction ${stack(0)} ${stack(1)} ${stack(2)}")
    instruction match {
      case Halt => return

      case GoTo(ip) => goto(ip)
      case GoIfTrue(ip) => goIfTrue(ip)
      case GoIfFalse(ip) => goIfFalse(ip)

      case Store(local) => store(local)
      case Load(local) => load(local)

      case NewObject(className) => instantiate(className)
      case FieldStore(fieldName) => storeInField(fieldName)
      case FieldLoad(fieldName) => loadFromField(fieldName)

      case Push(v) => push(v)
      case PushFloat(v) => pushFloat(v)
      case Pop => pop()
      case DuplicateTop => push(top())

      case Add => add()
      case Subtract => sub()
      case Multiply => mul()
      case Divide => div()

      case AddFloat => addFloat()
      case SubtractFloat => subFloat()
      case MultiplyFloat => mulFloat()
      case DivideFloat => divFloat()

      case IntToFloat => push(java.lang.Float.floatToIntBits(pop().toFloat))

      case Equal => equal()
      case NotEqual => notEqual()
      case Greater => greater()
      case Lesser => lesser()
      case GreaterOrEqual => greaterOrEqual()
      case LesserOrEqual => lesserOrEqual()

      case ArraySize => loadArraySize()
      case ArrayLoad => loadFromArray()
      case ArrayStore => storeInArray()
      case NewArray => newArray()

      case Invoke(name) => invoke(name)
      case InvokeOnGPU(name) => invokeOnGPU(name)

      case Return => popFrame()

      case PrintTop => printTop()
    }
    loop()
  }


}
