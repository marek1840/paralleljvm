package jvm.parallel.stack

import jvm.parallel.vm2.CodeAreaComponent

import scala.collection.mutable

class CodeStorage extends CodeAreaComponent {
  override type MethodMetadata = Method

  private[this] val code = mutable.Map[MethodKey, MethodMetadata]()

  override def registerMethod(key: MethodKey, metadata: MethodMetadata): Unit = code += (key -> metadata)
  override def getMethod(key: MethodKey): MethodMetadata = code(key)
}
