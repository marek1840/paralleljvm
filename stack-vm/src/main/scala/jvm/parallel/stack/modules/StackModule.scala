package jvm.parallel.stack.modules

import jvm.parallel.stack.OperandStack

trait StackModule extends ThreadExtensionModule {
  this: OperandStack =>

  protected[this] def pushFloat(v: Float) = {
    val encoded = java.lang.Float.floatToIntBits(v)
    push(encoded)
  }
}
