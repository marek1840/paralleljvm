package jvm.parallel.stack.modules

import jvm.parallel.ast.name.Name
import jvm.parallel.stack.{FrameStack, OperandStack}

trait LocalVariableModule extends ThreadExtensionModule {
  this: FrameStack
    with OperandStack =>

  protected[this] def load(variable: Name): Unit = {
    push(topFrame().get(variable))
  }

  protected[this]  def store(variable: Name): Unit = {
    topFrame().put(variable, pop())
  }
}
