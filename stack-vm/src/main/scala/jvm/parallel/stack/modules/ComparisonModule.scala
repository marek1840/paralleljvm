package jvm.parallel.stack.modules

import jvm.parallel.stack.OperandStack

trait ComparisonModule extends ThreadExtensionModule {
  this: OperandStack =>

  protected[this] val True = 1
  protected[this] val False = 0

  @inline
  private[this] def compare(comparator: (Operand, Operand) => Boolean) = {
    val right: Int = pop()
    val left: Int = pop()
    if (comparator(left, right)) {
      push(True)
    } else {
      push(False)
    }
  }

  protected[this] def equal() = compare(_ == _)
  protected[this] def notEqual() = compare(_ != _)
  protected[this] def lesser() = compare(_ < _)
  protected[this] def lesserOrEqual() = compare(_ <= _)
  protected[this] def greater() = compare(_ > _)
  protected[this] def greaterOrEqual() = compare(_ >= _)

  protected[this]  def topIsTrue(): Boolean = {
    pop() == True
  }
}
