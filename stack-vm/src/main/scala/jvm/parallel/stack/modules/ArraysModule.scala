package jvm.parallel.stack.modules

import jvm.parallel.stack.{ArrayMemory, OperandStack}

trait ArraysModule extends ThreadExtensionModule {
  this: ArrayMemory
    with OperandStack =>

  protected[this] def newArray() = {
    val size = pop()
    val address = allocate(size + 1)
    writeToMemory(address, size)
    push(address)
  }

  protected[this] def loadArraySize() = {
    val address = pop()
    val size = readFromMemory(address)
    push(size)
  }

  protected[this] def loadFromArray() = {
    val address = pop()
    val offset = pop() + 1
    val value = readFromMemory(address + offset)
    push(value)
  }

  protected[this] def storeInArray() = {
    val address = pop()
    val offset = pop() + 1
    val value = pop()
    writeToMemory(address + offset, value)
  }
}
