package jvm.parallel.stack.modules.coproessor

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.declaration.members.MethodDeclaration
import jvm.parallel.coprocessor.adapters.{StructureAdapter, StructureGeneration}
import jvm.parallel.coprocessor.adapters.handles.StructureHandleTemplateFactory
import jvm.parallel.coprocessor.v2.Coprocessor
import jvm.parallel.stack.containers.{CodeStorageContainer, StructureStorageContainer}
import jvm.parallel.stack.modules.InvocationModule
import jvm.parallel.stack.{ArrayMemory, FrameStack, MethodDispatch}

trait CoprocessorModule extends Coprocessor with StructureAdapter
  with StructureHandleTemplateFactory with StructureGeneration {

  this: ArrayMemory
    with MethodDispatch
    with FrameStack
    with InvocationModule
    with StructureStorageContainer
    with CodeStorageContainer =>

  def invokeOnGPU(name: Name): Unit = {
    val method = codeArea.getMethod(name)
    val ast = method.ast

    if (ast.isEmpty) {
      invoke(name)
    } else {
      invokeOnGPU(ast.get)
    }
  }

  private[this] def invokeOnGPU(ast: MethodDeclaration): Unit = {
    val method = compileExecutionUnit(ast)
    execute(method)
  }
}
