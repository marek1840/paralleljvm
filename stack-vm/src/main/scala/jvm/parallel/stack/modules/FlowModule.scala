package jvm.parallel.stack.modules

import jvm.parallel.stack.FrameStack

trait FlowModule extends ThreadExtensionModule {
  this: FrameStack
    with ComparisonModule =>

  protected[this] def goIfTrue(ip: Int): Unit = {
    if (topIsTrue()) goto(ip)
  }

  protected[this] def goIfFalse(ip: Int): Unit = {
    if (!topIsTrue()) goto(ip)
  }

  protected[this] def goto(ip: Int): Unit = {
    topFrame().jumpTo(ip)
  }
}
