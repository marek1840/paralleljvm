package jvm.parallel.stack.modules

import jvm.parallel.ast.name.Name
import jvm.parallel.stack.{FrameStack, MethodDispatch}

trait InvocationModule extends ThreadExtensionModule {
  this: FrameStack
    with MethodDispatch =>

  protected[this] def invoke(name: Name) = {
    val newFrame = resolveMethod(name)
    pushFrame(newFrame)
  }
}
