package jvm.parallel.stack.modules

import jvm.parallel.stack.{ArrayMemory, OperandStack}
import jvm.parallel.vm2.structs.{ClassName, FieldName, StructureStorage}

trait StructureModule extends ThreadExtensionModule {
  this: ArrayMemory
    with OperandStack =>

  val structureArea: StructureStorage

  def instantiate(name: ClassName): Unit = {
    val size = structureArea.getStructure(name).size
    val address = allocate(size)
    push(address)
  }

  def storeInField(name: FieldName): Unit = name match {
    case FieldName(classKey, fieldKey) =>
      val objectAddress  = pop()
      val fieldOffset = structureArea.getStructure(classKey).offset(name)
      val value = pop()
      writeToMemory(objectAddress + fieldOffset, value)
  }

  def loadFromField(name: FieldName): Unit = name match {
    case FieldName(classKey, fieldKey) =>
      val objectAddress  = pop()
      val fieldOffset = structureArea.getStructure(classKey).offset(name)
      val value =  readFromMemory(objectAddress + fieldOffset)
      push(value)
  }
}
