package jvm.parallel.stack.modules

import jvm.parallel.stack.OperandStack

trait ArithmeticModule extends ThreadExtensionModule {
  this: OperandStack =>

  protected[this] def add(): Unit = {
    val res = pop() + pop()
    push(res)
  }

  protected[this] def mul(): Unit = {
    val res = pop() * pop()
    push(res)
  }

  protected[this] def sub() = {
    val right: Int = pop()
    val left: Int = pop()
    val res = left - right
    push(res)
  }

  protected[this] def div() = {
    val right: Int = pop()
    val left: Int = pop()
    val res = left / right
    push(res)
  }


  protected[this] def intToFloat() = {
    val int = pop()
    val float = new java.lang.Float(int)
    val encoded = java.lang.Float.floatToIntBits(float)
    push(encoded)
  }

  protected[this] def addFloat(): Unit = {
    val res = java.lang.Float.intBitsToFloat(pop()) + java.lang.Float.intBitsToFloat(pop())
    push(java.lang.Float.floatToIntBits(res))
  }

  protected[this] def mulFloat(): Unit = {
    val res = java.lang.Float.intBitsToFloat(pop()) * java.lang.Float.intBitsToFloat(pop())
    push(java.lang.Float.floatToIntBits(res))
  }

  protected[this] def subFloat(): Unit = {
    val right = java.lang.Float.intBitsToFloat(pop())
    val left = java.lang.Float.intBitsToFloat(pop())
    val res = java.lang.Float.floatToIntBits(left - right)
    push(res)
  }

  protected[this] def divFloat(): Unit = {
    val right = java.lang.Float.intBitsToFloat(pop())
    val left = java.lang.Float.intBitsToFloat(pop())
    val res = java.lang.Float.floatToIntBits(left / right)
    push(res)
  }
}
