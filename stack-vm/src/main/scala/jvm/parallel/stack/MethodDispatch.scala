package jvm.parallel.stack

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.declaration.members.MethodDeclaration
import jvm.parallel.coprocessor.DeclaredNameExtraction
import jvm.parallel.stack.containers.CodeStorageContainer
import jvm.parallel.stack.ops.Instruction
import jvm.parallel.stack.{Frame => VMFrame}
import jvm.parallel.vm2.components.InvocationDispatchComponent

trait MethodDispatch extends InvocationDispatchComponent {
  this : CodeStorageContainer =>
  override type Frame = VMFrame

  override def resolveMethod(key: MethodKey): Frame = {
    val method = codeArea.getMethod(key)
    val instructions = getInstructions(method)
    val locals = getLocals(method)

    new Frame(instructions, locals)
  }

  private[this] def getInstructions(method: Method): Array[Instruction] = {
    method.instructions.toArray
  }

  private[this] def getLocals(method: Method): Seq[MethodKey] = {
    method.ast
      .map(localsFromAST)
      .getOrElse(Nil)
  }

  private[this] def localsFromAST(ast: MethodDeclaration): Seq[Name] = {
    val nameExtractor = new DeclaredNameExtraction

    nameExtractor.reduce(ast)
  }
}
