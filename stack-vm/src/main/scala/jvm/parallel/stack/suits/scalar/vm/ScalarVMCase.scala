package jvm.parallel.stack.suits.scalar.vm

import jvm.parallel.stack.suits.VMCase
import jvm.parallel.stack.suits.scalar.ScalarStatistics

class ScalarVMCase(size: Int) extends VMCase[ScalarMetadata, ScalarStatistics] {
  override protected[this] def registerMethods(suite: ScalarMetadata): Unit = {
    codeStorage.registerMethod("main", suite.main)
    codeStorage.registerMethod("initialize", suite.init)
  }

  override protected[this] def registerStructures(suite: ScalarMetadata): Unit = {}

  override protected[this] def suiteMetadata: ScalarMetadata = new ScalarMetadata(size)
}
