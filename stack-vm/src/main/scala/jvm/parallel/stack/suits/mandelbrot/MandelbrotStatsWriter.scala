package jvm.parallel.stack.suits.mandelbrot

import java.io.File

import jvm.parallel.stack.suits.StatsWriter

class MandelbrotStatsWriter(parentDir: String) extends StatsWriter[MandelbrotStatistics](parentDir) {

  override protected[this] val files: Seq[File] = Seq(
    new File(parentDir, "comparison.txt"),
    new File(parentDir, "javaCL_summary.txt"),
    new File(parentDir, "vm_summary.txt"),
    new File(parentDir, "execution_time.txt"),
    new File(parentDir, "DTO_wrap.txt"),
    new File(parentDir, "DTO_unwarap.txt")
  )

  protected[this] def headers(): Seq[String] = Seq(
    line("size", "iterations", "vmTotal", "sequential", "javaCL"),
    line("size", "iterations", "wrap", "unwrap", "execution", "total"),
    line("size", "iterations", "wrap", "unwrap", "execution", "total"),
    line("size", "iterations", "vm_execution", "javaCL_execution"),
    line("size", "vm_wrap", "javaCL_wrap"),
    line("size", "vm_unwrap", "javaCL_unwrap")
  )

  protected[this] def buildLines(stats: MandelbrotStatistics): Seq[String] = Seq(
    comparisonLine(stats),
    javaCLSummary(stats),
    vmSummary(stats),
    executionLine(stats),
    toHostLine(stats),
    fromHostLine(stats)
  )

  protected[this] def comparisonLine(stats: MandelbrotStatistics) =
    line(stats.size, stats.iterations,
      stats.vmTotalTime(),
      stats.averageSequential(),
      stats.averageJavaCLExecution())

  protected[this] def javaCLSummary(stats: MandelbrotStatistics): String =
    line(stats.size, stats.iterations,
      stats.averageJavaCLCopyFromHost(),
      stats.averageJavaCLCopyToHost(),
      stats.averageJavaCLExecution(),
      stats.javaCLTotalTime())

  protected[this] def vmSummary(stats: MandelbrotStatistics): String =
    line(stats.size, stats.iterations,
      stats.averageCopyFromHost(),
      stats.averageCopyToHost(),
      stats.averageExecutionTime(),
      stats.vmTotalTime())

  protected[this] def executionLine(stats: MandelbrotStatistics) =
    line(stats.size, stats.iterations,
      stats.averageExecutionTime(),
      stats.averageJavaCLExecution())

  protected[this] def toHostLine(stats: MandelbrotStatistics) =
    line(stats.size,
      stats.averageCopyToHost(),
      stats.averageJavaCLCopyToHost())

  protected[this] def fromHostLine(stats: MandelbrotStatistics) =
    line(stats.size,
      stats.averageCopyFromHost(),
      stats.averageJavaCLCopyFromHost())
}
