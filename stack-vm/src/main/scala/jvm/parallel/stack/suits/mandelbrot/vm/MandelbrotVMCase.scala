package jvm.parallel.stack.suits.mandelbrot.vm

import jvm.parallel.stack.suits.VMCase
import jvm.parallel.stack.suits.mandelbrot.MandelbrotStatistics

class MandelbrotVMCase(height: Int, width: Int, iterations: Int) extends VMCase[MandelbrotMetadata, MandelbrotStatistics] {

  def suiteMetadata: MandelbrotMetadata = {
    new MandelbrotMetadata(height, width, iterations)
  }

  protected[this] def registerMethods(suite: MandelbrotMetadata): Unit = {
    codeStorage.registerMethod("main", suite.main)
    codeStorage.registerMethod("create", suite.create)
    codeStorage.registerMethod("iterate", suite.iterate)
  }

  protected[this] def registerStructures(suite: MandelbrotMetadata): Unit = {
    structStorage.registerStructure(suite.Point.typeName, suite.Point.struct)
    structStorage.registerStructure(suite.Mandelbrot.typeName, suite.Mandelbrot.struct)
  }
}
