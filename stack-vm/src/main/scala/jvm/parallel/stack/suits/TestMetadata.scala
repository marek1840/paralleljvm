package jvm.parallel.stack.suits

import jvm.parallel.ast.name.Name

class TestMetadata {
  protected[this] implicit def strToName(s: String): Name = new Name(s)
}
