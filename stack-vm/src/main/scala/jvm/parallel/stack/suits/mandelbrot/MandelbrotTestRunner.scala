package jvm.parallel.stack.suits.mandelbrot

import jvm.parallel.stack.suits.CurrentStatistics

object MandelbrotTestRunner {

  val sizeStats = new MandelbrotStatsWriter("stats/mandelbrot/size")
  val iterationsStats = new MandelbrotStatsWriter("stats/mandelbrot/iterations")

  def main(args: Array[String]) = {
    val sizeLimit: Long = 4194304
    val iterationsLimit = 1000 * 256

    warmUp()

    val sizes: Seq[Int] = Iterator.iterate(4)(_ * 4) takeWhile (_ <= sizeLimit) toList
    val iterations: Seq[Int] = Iterator.iterate(1000)(_ * 2) takeWhile (_ <= iterationsLimit) toList

    println("Started test suite")
//    test(sizes, Seq(10000), sizeStats)
    println("Finished sizes")
    test(Seq(64 * 64 * 16), iterations, iterationsStats)
    println("Finished iterations")
  }

  def warmUp() = {
    CurrentStatistics.setCurrentCollector(new MandelbrotStatistics(0, 0))
    (1 to 10 ) foreach { _ =>
      new MandelbrotTestSuite(125, 125, 10000, new MandelbrotStatsWriter("/tmp")).run()
    }
    println("finished warmup")
  }

  def test(sizes: Seq[Int], iterations: Seq[Int], writer: MandelbrotStatsWriter): Unit = {

    for {
      size <- sizes.toStream
      iterations <- iterations

      w = Math.sqrt(size).toInt
      h = Math.sqrt(size).toInt

    } {
      val c = new MandelbrotTestSuite(h, w, iterations, writer)
      c.run()
    }
  }


}
