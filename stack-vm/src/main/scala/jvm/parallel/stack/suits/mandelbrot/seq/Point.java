package jvm.parallel.stack.suits.mandelbrot.seq;

public class Point {
	float x;
	float y;

	@Override
	public String toString() {
		return String.format("(%s, %s)", x, y);
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}
}