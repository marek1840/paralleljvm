package jvm.parallel.stack.suits.scalar.seq

import java.util.concurrent.TimeUnit

import jvm.parallel.stack.suits.TestCase
import jvm.parallel.stack.suits.scalar.ScalarStatistics

class ScalarSeqCase(size: Int) extends TestCase[ScalarStatistics] {
  override def run(stats: ScalarStatistics): Unit = {
    if (stats.averageSequential() < TimeUnit.MINUTES.toMillis(1)) {
      repeat(5) {
        new ScalarAlgorithm(size).execute()
      }
    } else {
      println("Skipped seq: " + size)
    }
  }
}