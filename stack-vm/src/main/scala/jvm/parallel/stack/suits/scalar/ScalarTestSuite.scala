package jvm.parallel.stack.suits.scalar

import jvm.parallel.stack.suits.TestSuite
import jvm.parallel.stack.suits.scalar.javacl.ScalarJCLCase
import jvm.parallel.stack.suits.scalar.seq.ScalarSeqCase
import jvm.parallel.stack.suits.scalar.vm.{ScalarMetadata, ScalarVMCase}

class ScalarTestSuite(size: Int, writer: ScalarStatisticsWriter) extends TestSuite[ScalarMetadata, ScalarStatistics](writer) {
  override protected[this] val vmCase = new ScalarVMCase(size)
  override protected[this] val seqCase = new ScalarSeqCase(size)
  override protected[this] val javaCLCase = new ScalarJCLCase(size)

  override protected[this] def newStatisticCollector(): ScalarStatistics = new ScalarStatistics(size)

  override protected[this] def printNewTestCase(): Unit = println(s"($size)")
}
