package jvm.parallel.stack.suits.matrix

import jvm.parallel.stack.suits.CurrentStatistics

object MatrixTestRunner {

  def main(args: Array[String]): Unit = {
    warmUp()

    val sizeLimit = 4096
    val writer = new MatrixStatisticsWriter("stats/matrix")

    for {
      size <- Iterator.iterate(512)(_ + 512) takeWhile (_ <= sizeLimit)
    } {
      val suite = new MatrixTestSuite(size, writer)
      suite.run()
    }
  }

  def warmUp() = {
    CurrentStatistics.setCurrentCollector(new MatrixStatistics(0))
    (1 to 10 ) foreach { _ =>
      new MatrixTestSuite(256, new MatrixStatisticsWriter("/tmp")).run()
    }
    println("finished warmup")
  }
}
