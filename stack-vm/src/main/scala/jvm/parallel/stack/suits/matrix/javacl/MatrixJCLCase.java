package jvm.parallel.stack.suits.matrix.javacl;

import java.util.concurrent.TimeUnit;

import org.bridj.Pointer;

import com.nativelibs4java.opencl.CLBuffer;
import com.nativelibs4java.opencl.CLContext;
import com.nativelibs4java.opencl.CLKernel;
import com.nativelibs4java.opencl.CLMem;
import com.nativelibs4java.opencl.CLProgram;
import com.nativelibs4java.opencl.CLQueue;
import com.nativelibs4java.opencl.JavaCL;

import jvm.parallel.stack.suits.TestCase;
import jvm.parallel.stack.suits.matrix.MatrixStatistics;

public class MatrixJCLCase extends TestCase<MatrixStatistics> {
	private final int size;
	private final CLContext ctx = JavaCL.createBestContext();
	private final CLQueue queue = ctx.createDefaultQueue();

	public MatrixJCLCase(int size) {
		this.size = size;
	}

	@Override
	public void run(MatrixStatistics stats) {
		for (int i = 0; i < 5; i++) {

			int[] A = new int[size * size];
			int[] B = new int[size * size];
			int[] C = new int[size * size];

			for (int j = 0; j < size * size; j++) {
				A[j] = 2;
				B[j] = 2;
			}

			CLBuffer<Integer> bufferA = ctx.createBuffer(CLMem.Usage.Input, Pointer.pointerToInts(A));
			CLBuffer<Integer> bufferB = ctx.createBuffer(CLMem.Usage.Input, Pointer.pointerToInts(B));
			CLBuffer<Integer> bufferC = ctx.createBuffer(CLMem.Usage.InputOutput, Pointer.pointerToInts(C));

			long start = System.nanoTime();

			CLProgram program = ctx.createProgram(MatrixKernel.source());
			CLKernel kernel = program.createKernel("multiply");

			kernel.setArgs(bufferA, bufferB, bufferC, size);

			long[] globalSizes = { size, size };

			kernel.enqueueNDRange(queue, null, globalSizes, null);
			queue.finish();

			long elapsed = System.nanoTime() - start;
			long millis = TimeUnit.NANOSECONDS.toMillis(elapsed);
			System.out.println("javacl in: " + millis / 1000.0 + "s");
			stats.addJavaCLTExecutionime(millis);

			Pointer<Integer> read = bufferC.read(queue);
			Object[] output = read.toArray();
		}
	}
}
