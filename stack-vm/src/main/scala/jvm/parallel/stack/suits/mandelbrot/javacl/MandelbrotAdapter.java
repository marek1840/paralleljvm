package jvm.parallel.stack.suits.mandelbrot.javacl;

import java.util.concurrent.TimeUnit;

import org.bridj.StructObject;

import jvm.parallel.stack.suits.CurrentStatistics;
import jvm.parallel.stack.suits.mandelbrot.seq.MandelbrotPart;
import jvm.parallel.stack.suits.mandelbrot.seq.Point;

public class MandelbrotAdapter {

	public Adapted_Mandelbrot[] adaptForDevice(MandelbrotPart[] mandelbrotParts) {
		Adapted_Mandelbrot[] objects = (Adapted_Mandelbrot[]) java.lang.reflect.Array.newInstance(Adapted_Mandelbrot.class,
				mandelbrotParts.length);

		long start = System.nanoTime();

		for (int i = 0; i < mandelbrotParts.length; i++) {
			MandelbrotPart source = mandelbrotParts[i];
			Adapted_Mandelbrot adapted = new Adapted_Mandelbrot();

			adapted.set_0(adaptPoint(source.getCoord()));
			adapted.set_1(adaptPoint(source.getZ()));

			objects[i] = adapted;
		}

		long end = System.nanoTime();
		long elapsed = end - start;

		CurrentStatistics.getCurrent().addJavaCLCopyFromHostTime(TimeUnit.NANOSECONDS.toMillis(elapsed));

		return objects;
	}

	public MandelbrotPart[] adaptForHost(StructObject[] adaptedObjects) {
		MandelbrotPart[] hostObjects = new MandelbrotPart[adaptedObjects.length];

		long start = System.nanoTime();

		for (int i = 0; i < adaptedObjects.length; i++) {
			Adapted_Mandelbrot adapted = (Adapted_Mandelbrot) adaptedObjects[i];

			MandelbrotPart mandelbrotPart = new MandelbrotPart(adaptPoint(adapted.get_0()), adaptPoint(adapted.get_1()));

			hostObjects[i] = mandelbrotPart;
		}

		long end = System.nanoTime();
		long elapsed = end - start;

		CurrentStatistics.getCurrent().addJavaCLCopyToHostTime(TimeUnit.NANOSECONDS.toMillis(elapsed));

		return hostObjects;
	}

	private Point adaptPoint(Adapted_Point adapted) {
		Point point = new Point();
		point.setX(adapted.get_0());
		point.setY(adapted.get_1());
		return point;
	}

	private Adapted_Point adaptPoint(Point point) {
		Adapted_Point adapted = new Adapted_Point();
		adapted.set_0(point.getX());
		adapted.set_1(point.getY());
		return adapted;
	}

}
