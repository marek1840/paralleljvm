package jvm.parallel.stack.suits


object CurrentStatistics {
  private[this] var currentStatistics: Statistics = _

  def getCurrent() = currentStatistics

  def setCurrentCollector[A <: Statistics](stats: A) = currentStatistics = stats
}

