package jvm.parallel.stack.suits

abstract class TestSuite[VMMetadata <: TestMetadata, Stats <: Statistics](writer: StatsWriter[Stats]) {
  protected[this] val vmCase: VMCase[VMMetadata, Stats]
  protected[this] val seqCase: TestCase[Stats]
  protected[this] val javaCLCase: TestCase[Stats]

  final def run(): Unit = {
    val collector: Stats = newStatisticCollector()
    CurrentStatistics.setCurrentCollector(collector)

    printNewTestCase()

    seqCase.run(collector)
    vmCase.run(collector)
    javaCLCase.run(collector)

    writer.write(collector)
  }

  protected[this] def newStatisticCollector(): Stats

  protected[this] def printNewTestCase(): Unit
}
