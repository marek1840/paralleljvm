package jvm.parallel.stack.suits.mandelbrot.seq;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.jetbrains.annotations.NotNull;

import jvm.parallel.stack.suits.CurrentStatistics;

public class MandelbrotAlgorithm {

	private final int height;
	private final int width;
	private final int iterations;
	private final int pointCount;
	private final float dy;
	private final float dx;

	public MandelbrotAlgorithm(int height, int width, int iterations) {
		this.height = height;
		this.width = width;
		this.iterations = iterations;
		this.pointCount = width * height;
		this.dy = 2.2F / (height - 1);
		this.dx = 4.0F / (width - 1);
	}

	public void execute() throws IOException {
		MandelbrotPart[] points = create();

		long start = System.nanoTime();
		iterate(pointCount, iterations, points);
		long end = System.nanoTime();

		long elapsed = TimeUnit.NANOSECONDS.toMillis(end - start);
		System.out.println("seq in " + elapsed / 1000.0 + "s");
		CurrentStatistics.getCurrent().addsequentialTime(elapsed);
	}

	private void iterate(int pointCount, int iterations, MandelbrotPart[] points) {
		for (int i = 0; i < pointCount; i++) {
			MandelbrotPart mand = points[i];
			Point z0 = mand.coord;
			Point z = mand.z;

			for (int j = 0; j < iterations; j++) {
				float x = z.x * z.x - z.y * z.y + z0.x;
				float y = 2 * z.x * z.y + z0.y;

				z.x = x;
				z.y = y;
			}
		}
	}

	@NotNull
	public MandelbrotPart[] create() {
		MandelbrotPart[] points = new MandelbrotPart[pointCount];

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				int index = y * width + x;
				Point point = new Point();
				point.y = dy * y - 1;
				point.x = dx * x - 2;
				points[index] = new MandelbrotPart(point);
			}
		}
		return points;
	}

}
