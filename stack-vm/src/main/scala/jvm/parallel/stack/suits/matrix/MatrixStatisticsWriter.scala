package jvm.parallel.stack.suits.matrix

import java.io.File

import jvm.parallel.stack.suits.StatsWriter

class MatrixStatisticsWriter(parentDir: String) extends StatsWriter[MatrixStatistics](parentDir) {
  override protected[this] val files: Seq[File] = Seq(
    new File(parentDir, "comparison.txt")
  )

  protected[this] def headers(): Seq[String] = Seq(
    line("size", "vmTotal", "sequential", "javaCL")
  )

  protected[this] def buildLines(stats: MatrixStatistics): Seq[String] = Seq(
    comparisonLine(stats)
  )

  override protected[this] def comparisonLine(stats: MatrixStatistics): String = line(stats.size,
    stats.averageExecutionTime(),
    stats.averageSequential(),
    stats.averageJavaCLExecution()
  )

  override protected[this] def javaCLSummary(stats: MatrixStatistics): String = ""

  override protected[this] def vmSummary(stats: MatrixStatistics): String = ""

  override protected[this] def executionLine(stats: MatrixStatistics): String = ""

  override protected[this] def toHostLine(stats: MatrixStatistics): String = ""

  override protected[this] def fromHostLine(stats: MatrixStatistics): String = ""
}
