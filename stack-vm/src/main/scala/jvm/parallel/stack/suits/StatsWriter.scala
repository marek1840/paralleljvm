package jvm.parallel.stack.suits

import java.io.{File, PrintWriter}

abstract class StatsWriter[A <: Statistics](val parentDir: String) {
  private[this] lazy val writers: Seq[PrintWriter] = {
    val writers = files.map(f => {
      f.getParentFile.mkdirs()
      f.createNewFile()
      new PrintWriter(f)
    })
    writers.zip(headers()).foreach(t => write(t._1, t._2))
    writers
  }

  def write(stats: A): Unit = {
    val lines: Seq[String] = buildLines(stats)
    writers.zip(lines).foreach { t => write(t._1, t._2) }
  }

  def write(out: PrintWriter, line: String): Unit = {
    out.write(line)
    out.write("\n")
    out.flush()
  }

  protected[this] def files: Seq[File]

  protected[this] def buildLines(stats: A): Seq[String]

  protected[this] def headers(): Seq[String]

  protected[this] def comparisonLine(stats: A): String

  protected[this] def javaCLSummary(stats: A): String

  protected[this] def vmSummary(stats: A): String

  protected[this] def executionLine(stats: A): String

  protected[this] def toHostLine(stats: A): String

  protected[this] def fromHostLine(stats: A): String

  protected[this] def line(s: Any*) = s.mkString("\t")
}
