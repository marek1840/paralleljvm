package jvm.parallel.stack.suits.scalar.seq;

import java.util.concurrent.TimeUnit;

import jvm.parallel.stack.suits.CurrentStatistics;

public class ScalarAlgorithm {
	private final int size;

	private final float[] A;
	private final float[] B;

	public ScalarAlgorithm(int size) {
		this.size = size;
		A = new float[size];
		B = new float[size];

		for (int i = 0; i < size; i++) {
			A[i] = 4;
			B[i] = 7;
		}
	}

	public void execute() {
		float[] C = new float[size];

		long start = System.nanoTime();

		float[] result = multiply(A, B, C, size);

		long elapsed = System.nanoTime() - start;
		long millis = TimeUnit.NANOSECONDS.toMillis(elapsed);
		System.out.println("seq in " + millis / 1000.0 + "s");
		CurrentStatistics.getCurrent().addsequentialTime(millis);
	}

	private float[] multiply(float[] a, float[] b, float[] c, int size) {
		for (int i = 0; i < size; i++) {
			c[i] = a[i] * b[i];
		}

		return c;
	}

}
