package jvm.parallel.stack.suits.matrix.vm

import jvm.parallel.parsers.java.JavaParser
import jvm.parallel.stack.Method
import jvm.parallel.stack.ops._
import jvm.parallel.stack.suits.TestMetadata

class MatrixMetadata(size: Int) extends TestMetadata {
  private[this] val multiplyCode =
    """@jvm.parallel.annotations.Parallel(labels = {"outer", "inner"})
      |public void multiply(@jvm.parallel.annotations.In int[] A,
      |                     @jvm.parallel.annotations.In int[] B,
      |                     @jvm.parallel.annotations.Out int[] C,
      |                     @jvm.parallel.annotations.In int size){
      | outer: for(int i = 0; i < size; ++i){
      |   inner: for(int j = 0; j < size; ++j){
      |            int tmp = 0;
      |            for(int k = 0; k < size; ++k){
      |              int a = A[i * size + k];
      |              int b = B[k * size + j];
      |              tmp += a * b;
      |            }
      |            C[i * size + j] = tmp;
      |          }
      |        }
      |}
    """.stripMargin
  private[this] val multiplyAST = JavaParser.parseMethod(multiplyCode)
  private[this] val multiplyInstructions = Seq(
    Store("size"), Store("C"), Store("B"), Store("A"),

    Push(0), Store("i"), // i = 0
    Push(0), Store("j"), // j = 0
    Push(0), Store("tmp"), // tmp = 0
    Push(0), Store("k"), // k = 0

    Load("i"), Load("size"), Multiply,
    Load("k"), Add,
    Load("A"), ArrayLoad, Store("a"), // a = A[i * aCols + k]
    Load("k"), Load("size"), Multiply,
    Load("j"), Add,
    Load("B"), ArrayLoad, Store("b"), // b = B[k * bCols + j]
    Load("a"), Load("b"),
    Multiply, Load("tmp"), Add,
    Store("tmp"), // tmp += a * b

    Load("k"), Push(1), Add, Store("k"), // ++k
    Load("k"), Load("size"), Lesser, // k < size
    GoIfTrue(12), // end of loop through ks

    Load("tmp"),
    Load("i"), Load("size"), Multiply,
    Load("j"), Add,
    Load("C"), ArrayStore, // C[i * size + j] = tmp;

    Load("j"), Push(1), Add, Store("j"), // ++j
    Load("j"), Load("size"), Lesser, // j < size
    GoIfTrue(8), // end of loop through js

    Load("i"), Push(1), Add, Store("i"), // ++i
    Load("i"), Load("size"), Lesser, // i < size
    GoIfTrue(7), // end of loop through is
    Return
  )
  private[this] val mainCode =
    s"""public void main(){
        | int size = $size;
        | int square = size * size;
        |
        | int[] A = new int[size * size];
        | int[] B = new int[size * size];
        | int[] C = new int[size * size];
        |
        | for(int i = 0; i < size; ++i){
        |   A[i] = 2;
        |   B[i] = 2;
        |   C[i] = 0;
        | }
        |
        | multiply(A, B, C, size);
        |}
      """.stripMargin
  private[this] val mainAST = JavaParser.parseMethod(mainCode)
  private[this] val mainInstructions = Seq(
    Push(size), Store("size"), // set matrix size

    Load("size"), Load("size"), Multiply,
    Store("square"), // square = size * size

    Load("square"),
    NewArray, Store("A"), // A = new Int[size * size]

    Load("square"),
    NewArray, Store("B"), // A = new Int[size * size]

    Load("square"),
    NewArray, Store("C"), // A = new Int[size * size]

    Push(0), Store("i"),

    Push(4), Load("i"), Load("A"), ArrayStore, // A[i] = 4
    Push(7), Load("i"), Load("B"), ArrayStore, // B[i] = 7
    Push(0), Load("i"), Load("C"), ArrayStore, // C[i] = 0

    Load("i"), Push(1), Add, Store("i"), // ++i
    Load("i"), Load("square"), Lesser, // i < size
    GoIfTrue(17), // end of loop through is

    Load("A"), Load("B"), Load("C"), Load("size"),
    InvokeOnGPU("multiply"), Return
  )

  def main: Method = new Method(mainInstructions, Some(mainAST))

  def multiply: Method = new Method(multiplyInstructions, Some(multiplyAST))
}
