package jvm.parallel.stack.suits.mandelbrot.seq;

public class MandelbrotPart {
	final Point coord;
	final Point z;

	public MandelbrotPart(Point coord) {
		this.coord = coord;
		this.z = new Point();
		z.x = 0;
		z.y = 0;
	}

	public MandelbrotPart(Point coord, Point z) {
		this.coord = coord;
		this.z = z;
	}

	public Point getCoord() {
		return coord;
	}

	public Point getZ() {
		return z;
	}
}