package jvm.parallel.stack.suits

import java.util.concurrent.TimeUnit

import jvm.parallel.logging.Logger

trait StatisticsHelper extends Logger {
  def measure[A](text: String)(f: Statistics => Long => Unit)(block: => A): A = {
    val start = System.nanoTime()
    val result = block
    val end = System.nanoTime()

    val elapsed = end - start

    val millis = TimeUnit.NANOSECONDS.toMillis(elapsed)
    f.apply(CurrentStatistics.getCurrent()).apply(millis)
    //    logger.info(s"Executed $text in ${elapsed / 1000.0}s")
    result
  }
}
