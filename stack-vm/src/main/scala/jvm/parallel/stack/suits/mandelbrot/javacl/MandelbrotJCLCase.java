package jvm.parallel.stack.suits.mandelbrot.javacl;

import java.util.concurrent.TimeUnit;

import org.bridj.Pointer;
import org.bridj.StructObject;

import com.nativelibs4java.opencl.CLBuffer;
import com.nativelibs4java.opencl.CLContext;
import com.nativelibs4java.opencl.CLKernel;
import com.nativelibs4java.opencl.CLMem;
import com.nativelibs4java.opencl.CLProgram;
import com.nativelibs4java.opencl.CLQueue;
import com.nativelibs4java.opencl.JavaCL;

import jvm.parallel.stack.suits.TestCase;
import jvm.parallel.stack.suits.mandelbrot.MandelbrotStatistics;
import jvm.parallel.stack.suits.mandelbrot.seq.MandelbrotAlgorithm;
import jvm.parallel.stack.suits.mandelbrot.seq.MandelbrotPart;

public class MandelbrotJCLCase extends TestCase<MandelbrotStatistics> {
	private final int height;
	private final int width;
	private final int iterations;

	private final CLContext ctx = JavaCL.createBestContext();
	private final CLQueue queue = ctx.createDefaultQueue();

	public MandelbrotJCLCase(int height, int width, int iterations) {
		this.height = height;
		this.width = width;
		this.iterations = iterations;
	}

	public void run(MandelbrotStatistics stats) {
		for (int i = 0; i < 5; i++) {

			MandelbrotPart[] mandelbrotParts = new MandelbrotAlgorithm(height, width, iterations).create();
			Adapted_Mandelbrot[] inputArray = new MandelbrotAdapter().adaptForDevice(mandelbrotParts);
			Pointer<StructObject> pointerToArray = Pointer.pointerToArray(inputArray);

			CLBuffer<StructObject> buffer = ctx.createBuffer(CLMem.Usage.InputOutput, pointerToArray);

			long start = System.nanoTime();

			CLProgram program = ctx.createProgram(MandelbrotKernel.source());
			CLKernel kernel = program.createKernel("iterate");

			kernel.setArg(0, height * width);
			kernel.setArg(1, iterations);
			kernel.setArg(2, buffer);

			long[] globalSizes = { inputArray.length };
			kernel.enqueueNDRange(queue, null, globalSizes, null);
			queue.finish();

			long elapsed = System.nanoTime() - start;
			long millis = TimeUnit.NANOSECONDS.toMillis(elapsed);
			System.out.println("javacl in: " + millis / 1000.0 + "s");
			stats.addJavaCLTExecutionime(millis);

			StructObject[] output = buffer.read(queue).toArray();
			MandelbrotPart[] outputArray = new MandelbrotAdapter().adaptForHost(output);
		}
	}
}
