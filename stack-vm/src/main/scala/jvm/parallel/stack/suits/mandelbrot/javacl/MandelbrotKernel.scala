package jvm.parallel.stack.suits.mandelbrot.javacl

object MandelbrotKernel {
  val source: String =
    """
      |typedef struct {
      |	float x;
      |	float y;
      |} Point;
      |
      |typedef struct {
      |	Point coord;
      |	Point z;
      |} Mandelbrot;
      |
      |
      |__kernel void iterate(int pointCount, int iterations, __global Mandelbrot* points) {
      |	 {
      |		int i;
      |		i = get_global_id(0);
      |		Mandelbrot mand = points[i];
      |		Point z0 = mand.coord;
      |		for(int j = 0; j < iterations; ++j) {
      |			Point z = points[i].z;
      |			float x = z.x * z.x - z.y * z.y + z0.x;
      |			float y = 2 * z.x * z.y + z0.y;
      |			points[i].z.x = x;
      |			points[i].z.y = y;
      |		}
      |	}
      |}
    """.stripMargin
}
