package jvm.parallel.stack.suits

import jvm.parallel.stack._
import jvm.parallel.stack.ops.{Halt, Instruction, Invoke}
import jvm.parallel.vm2.structs.StructureStorage

trait VMCase[A <: TestMetadata, Stats <: Statistics] extends TestCase[Stats] {
  protected[this] val structStorage = new StructureStorage
  protected[this] val codeStorage = new CodeStorage
  private[this] val memorySize = 600000000

  final def run(stats: Stats): Unit = {
    if (!stats.exceedesTimeConstraints())
      repeat(5) {
        val suite = suiteMetadata

        registerStructures(suite)
        registerMethods(suite)

        val thread: StackThread with FrameStack = newThread(codeStorage, structStorage)
        thread.pushFrame(startFrame)

        thread.run()
      }
  }

  private[this] def newThread(codeStorage: CodeStorage, structStorage: StructureStorage): StackThread with ArrayMemory with FrameStack with MethodDispatch = {
    new StackThread(memorySize, codeStorage, structStorage) with ArrayMemory with FrameStack
      with MethodDispatch with OperandStack
  }

  private[this] def startFrame: Frame = {
    val instructions: Array[Instruction] = Array(Invoke("main"), Halt)
    new Frame(instructions, Nil)
  }

  protected[this] def registerMethods(suite: A): Unit

  protected[this] def registerStructures(suite: A): Unit

  protected[this] def suiteMetadata: A
}
