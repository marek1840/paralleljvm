package jvm.parallel.stack.suits.matrix

import jvm.parallel.stack.suits.TestSuite
import jvm.parallel.stack.suits.matrix.javacl.MatrixJCLCase
import jvm.parallel.stack.suits.matrix.seq.MatrixSeqCase
import jvm.parallel.stack.suits.matrix.vm.{MatrixMetadata, MatrixVMCase}

class MatrixTestSuite(size: Int, writer: MatrixStatisticsWriter) extends TestSuite[MatrixMetadata, MatrixStatistics](writer) {
  override protected[this] val vmCase = new MatrixVMCase(size)
  override protected[this] val seqCase = new MatrixSeqCase(size)
  override protected[this] val javaCLCase = new MatrixJCLCase(size)

  override protected[this] def newStatisticCollector(): MatrixStatistics = new MatrixStatistics(size)

  override protected[this] def printNewTestCase(): Unit = println(s"($size)")
}
