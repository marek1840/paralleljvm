package jvm.parallel.stack.suits.mandelbrot.javacl;

import org.bridj.BridJ;
import org.bridj.Pointer;
import org.bridj.StructObject;
import org.bridj.ann.Field;

public class Adapted_Mandelbrot extends StructObject {
	static {
		BridJ.register();
	}

	public Adapted_Mandelbrot() {
		super();
	}

	public Adapted_Mandelbrot(Pointer<StructObject> pointer) {
		super(pointer);
	}

	@Field(0)
	public Adapted_Point get_0() {
		return this.io.getNativeObjectField(this, 0);
	}

	@Field(0)
	public void set_0(Adapted_Point value) {
		this.io.setNativeObjectField(this, 0, value);
	}

	@Field(1)
	public Adapted_Point get_1() {
		return this.io.getNativeObjectField(this, 1);
	}

	@Field(1)
	public void set_1(Adapted_Point value) {
		this.io.setNativeObjectField(this, 1, value);
	}

}