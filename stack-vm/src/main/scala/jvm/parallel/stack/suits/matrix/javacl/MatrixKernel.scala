package jvm.parallel.stack.suits.matrix.javacl

object MatrixKernel {
  val source =
    """
      |__kernel void multiply(__global int* A, __global int* B, __global int* C, int size) {
      |	 {
      |		int i;
      |		i = get_global_id(1);
      |		 {
      |			int j;
      |			j = get_global_id(0);
      |			int tmp = 0;
      |			for(int k = 0; k < size; ++k) {
      |				int a = A[i * size + k];
      |				int b = B[k * size + j];
      |				tmp += a * b;
      |			}
      |			C[i * size + j] = tmp;
      |		}
      |	}
      |}
    """.stripMargin
}
