package jvm.parallel.stack.suits

import java.util.concurrent.TimeUnit

import jvm.parallel.logging.Logger

import scala.collection.mutable

class Statistics extends Logger {
  private[this] val executionTime = mutable.Buffer[Long]()
  private[this] val copyToHostTime = mutable.Buffer[Long]()
  private[this] val copyFromHostTime = mutable.Buffer[Long]()

  private[this] val sequentialTime = mutable.Buffer[Long]()


  private[this] val javaCLcopyToHostTime = mutable.Buffer[Long]()
  private[this] val javaCLcopyFromHostTime = mutable.Buffer[Long]()
  private[this] val javaCLExecutionTime = mutable.Buffer[Long]()

  def addCopyFromHostTime(time: Long) = copyFromHostTime += time

  def addCopyToHostTime(time: Long) = copyToHostTime += time

  def addExecutionTime(time: Long) = executionTime += time

  def addsequentialTime(time: Long) = sequentialTime += time

  def addJavaCLTExecutionime(time: Long) = javaCLExecutionTime += time

  def addJavaCLCopyToHostTime(time: Long) = javaCLcopyToHostTime += time

  def addJavaCLCopyFromHostTime(time: Long) = javaCLcopyFromHostTime += time

  def averageSequential(): Double =
    if (sequentialTime.isEmpty) 0.0
    else sequentialTime.sum * 1.0 / sequentialTime.size

  def javaCLTotalTime(): Double = averageJavaCLCopyFromHost() + averageJavaCLCopyToHost() + averageJavaCLExecution()

  def averageJavaCLExecution(): Double =
    if (javaCLExecutionTime.isEmpty) 0.0
    else javaCLExecutionTime.sum * 1.0 / javaCLExecutionTime.size

  def averageJavaCLCopyToHost(): Double =
    if (javaCLcopyToHostTime.isEmpty) 0.0
    else javaCLcopyToHostTime.sum * 1.0 / javaCLcopyToHostTime.size

  def averageJavaCLCopyFromHost(): Double =
    if (javaCLcopyFromHostTime.isEmpty) 0.0
    else javaCLcopyFromHostTime.sum * 1.0 / javaCLcopyFromHostTime.size

  def exceedesTimeConstraints(): Boolean = {
    if (executionTime.isEmpty) false
    else {
      val res = averageExecutionTime > TimeUnit.MINUTES.toMillis(1)
      if (res) {
        logger.info(s"Exceeding max execution time: ${vmTotalTime()}ms > ${TimeUnit.MINUTES.toMillis(1)}")
      }
      res
    }
  }

  def vmTotalTime(): Double = averageCopyFromHost() + averageCopyToHost() + averageExecutionTime()

  def averageExecutionTime(): Double =
    if (executionTime.isEmpty) 0.0
    else executionTime.sum * 1.0 / executionTime.size

  def averageCopyToHost(): Double =
    if (copyToHostTime.isEmpty) 0.0
    else copyToHostTime.sum * 1.0 / copyToHostTime.size

  def averageCopyFromHost(): Double =
    if (copyFromHostTime.isEmpty) 0.0
    else copyFromHostTime.sum * 1.0 / copyFromHostTime.size
}
