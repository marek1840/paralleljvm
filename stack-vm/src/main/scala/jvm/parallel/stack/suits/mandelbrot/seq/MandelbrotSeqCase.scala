package jvm.parallel.stack.suits.mandelbrot.seq

import java.util.concurrent.TimeUnit

import jvm.parallel.stack.suits.mandelbrot.MandelbrotStatistics
import jvm.parallel.stack.suits.{CurrentStatistics, TestCase}

class MandelbrotSeqCase(height: Int, width: Int, iterations: Int) extends TestCase[MandelbrotStatistics] {
  override def run(stats: MandelbrotStatistics): Unit = {
    if (stats.averageSequential() < TimeUnit.MINUTES.toMillis(1)) {
      repeat(5) {
        new MandelbrotAlgorithm(height, width, iterations).execute()
      }
    } else {
      CurrentStatistics.getCurrent().addsequentialTime(Long.MaxValue)
    }
  }
}
