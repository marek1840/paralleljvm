package jvm.parallel.stack.suits

import jvm.parallel.ast.name.Name

abstract class TestCase[Stats <: Statistics] {
  def run(stats: Stats): Unit

  protected[this] implicit def strToName(s: String): Name = new Name(s)

  protected[this] def repeat(n: Int)(block: => Unit) = (1 to n).foreach(_ => block)
}
