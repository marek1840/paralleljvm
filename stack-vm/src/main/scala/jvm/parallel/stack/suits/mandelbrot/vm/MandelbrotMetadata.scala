package jvm.parallel.stack.suits.mandelbrot.vm

import jvm.parallel.parsers.java.JavaParser
import jvm.parallel.stack.Method
import jvm.parallel.stack.ops._
import jvm.parallel.stack.suits.TestMetadata
import jvm.parallel.vm2.TypeSignature
import jvm.parallel.vm2.structs.{ClassName, FieldName, StructureDefinition}

class MandelbrotMetadata(width: Int, height: Int, iterations: Int) extends TestMetadata {

  private[this] val mainCode =
    s"""public static void main(String[] args) throws IOException {
        |		int height = $height;
        |		int width = $width;
        |		int pointCount = width * height;
        |
        |		float dy = 2.2F / (height - 1);
        |		float dx = 4.0F / (width - 1);
        |
        |		int iterations = $iterations;
        |
        |   Mandelbrot[] points = new Mandelbrot[pointCount];
        |
        |		create(height, width, pointCount, dy, dx);
        |
        |		iterate(pointCount, iterations, points);
        |	}""".stripMargin
  private[this] val mainAST = JavaParser.parseMethod(mainCode)
  private[this] val mainInstructions = Seq(
    Push(height), Store("height"),
    Push(width), Store("width"),

    Load("width"), Load("height"), Multiply, Store("pointCount"), // pointCount = width * height
    PushFloat(2.2F), Load("height"), Push(1), Subtract, IntToFloat, DivideFloat, Store("dy"), // dy = 2.2F / (height - 1)
    PushFloat(4.0F), Load("width"), Push(1), Subtract, IntToFloat, DivideFloat, Store("dx"), // dy = 2.2F / (height - 1)

    Push(iterations), Store("iterations"),
    Load("pointCount"), NewArray, Store("points"),

    Push(0), Store("i"),
    Load("i"), Load("pointCount"), Lesser, GoIfFalse(49),

    NewObject(Mandelbrot.typeName), Store("tmp"),
    NewObject(Point.typeName), Store("point"),
    Load("point"), Load("tmp"), FieldStore(Mandelbrot.coord),
    Load("tmp"), Load("i"), Load("points"), ArrayStore,

    Load("i"), Push(1), Add, Store("i"), GoTo(29),

    Load("height"), Load("width"), Load("points"), Load("dy"), Load("dx"),
    Invoke("create"),

    Load("pointCount"), Load("iterations"), Load("points"),
    InvokeOnGPU("iterate"),

    Return
  )
  private[this] val createSourceCode =
    """@jvm.parallel.annotations.Parallel(labels = {"outer", "inner"})
      |	private static void create(
      |         @jvm.parallel.annotations.In int height,
      |         @jvm.parallel.annotations.In int width,
      |         @jvm.parallel.annotations.Out Mandelbrot[] points,
      |         @jvm.parallel.annotations.In float dy,
      |         @jvm.parallel.annotations.In float dx) {
      |
      |		outer: for (int x = 0; x < width; x++) {
      |			inner: for (int y = 0; y < height; y++) {
      |				int index = y * width + x;
      |				Point point = points[index].coord;
      |    points[index].coord.x = dy * y - 1;
      |    points[index].coord.y = dx * x - 2;
      |			}
      |		}
      |		return;
      |	}""".stripMargin
  private[this] val createAST = JavaParser.parseMethod(createSourceCode)
  private[this] val createInstructions = Seq(
    Store("dx"), Store("dy"), Store("points"), Store("width"), Store("height"),

    Push(0), Store("x"),
    Load("x"), Load("width"), Lesser, GoIfFalse(54),

    Push(0), Store("y"),
    Load("y"), Load("height"), Lesser, GoIfFalse(49),

    Load("y"), Load("width"), Multiply, Load("x"), Add, Store("index"), // index = y * width + x

    Load("index"), Load("points"), ArrayLoad, FieldLoad(Mandelbrot.coord), Store("point"), // point = points[index.coord
    Load("dy"), Load("y"), IntToFloat, MultiplyFloat, Push(1), SubtractFloat, Load("point"), FieldStore(Point.y), // y = dy * y - 1
    Load("dx"), Load("x"), IntToFloat, MultiplyFloat, Push(2), SubtractFloat, Load("point"), FieldStore(Point.x), // y =
    // dy * y - 1

    Load("y"), Push(1), Add, Store("y"), GoTo(13),

    Load("x"), Push(1), Add, Store("x"), GoTo(7),

    Load("points"), Return
  )
  private[this] val iterateCode =
    """@jvm.parallel.annotations.Parallel(labels = {"loop"})
      |private static void iterate(
      |       @jvm.parallel.annotations.In int pointCount,
      |       @jvm.parallel.annotations.In int iterations,
      |       @jvm.parallel.annotations.InOut Mandelbrot[] points) {
      |		loop: for (int i = 0; i < pointCount; ++i) {
      |			Mandelbrot mand = points[i];
      |			Point z0 = mand.coord;
      |
      |			for (int j = 0; j < iterations; ++j) {
      |       Point z = points[i].z;
      |				float x = z.x * z.x - z.y * z.y + z0.x;
      |				float y = 2 * z.x * z.y + z0.y;
      |
      |				points[i].z.x = x;
      |				points[i].z.y = y;
      |			}
      |		}
      |	}""".stripMargin
  object Point {
    val typeName = new ClassName("", "Point")
    val y: FieldName = FieldName(typeName, "y")
    val x: FieldName = FieldName(typeName, "x")

    val struct = new StructureDefinition(typeName, Seq(
      x -> TypeSignature.Float,
      y -> TypeSignature.Float)
    )
  }
  object Mandelbrot {
    val typeName = new ClassName("", "Mandelbrot")
    val coord: FieldName = FieldName(typeName, "coord")
    val mandelbrot_z: FieldName = FieldName(typeName, "z")

    val struct = new StructureDefinition(typeName, Seq(
      coord -> TypeSignature.Type(Point.typeName),
      mandelbrot_z -> TypeSignature.Type(Point.typeName)
    ))
  }

  def iterateAST = JavaParser.parseMethod(iterateCode)
  def main = new Method(mainInstructions, Some(mainAST))
  def create = new Method(createInstructions, Some(createAST))
  def iterate = new Method(createInstructions, Some(iterateAST))
}
