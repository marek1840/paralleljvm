package jvm.parallel.stack.suits.mandelbrot

import jvm.parallel.stack.suits.TestSuite
import jvm.parallel.stack.suits.mandelbrot.javacl.MandelbrotJCLCase
import jvm.parallel.stack.suits.mandelbrot.seq.MandelbrotSeqCase
import jvm.parallel.stack.suits.mandelbrot.vm.{MandelbrotMetadata, MandelbrotVMCase}

class MandelbrotTestSuite(height: Int, width: Int, iterations: Int, writer: MandelbrotStatsWriter) extends
  TestSuite[MandelbrotMetadata, MandelbrotStatistics](writer) {

  protected[this] val vmCase: MandelbrotVMCase = new MandelbrotVMCase(height, width, iterations)
  protected[this] val seqCase: MandelbrotSeqCase = new MandelbrotSeqCase(height, width, iterations)
  protected[this] val javaCLCase: MandelbrotJCLCase = new MandelbrotJCLCase(height, width, iterations)

  protected[this] def newStatisticCollector(): MandelbrotStatistics = {
    new MandelbrotStatistics(height * width, iterations)
  }

  protected[this] def printNewTestCase(): Unit = println(height * width, height, iterations)


}
