package jvm.parallel.stack.suits.matrix.seq;

import java.util.concurrent.TimeUnit;

import jvm.parallel.stack.suits.CurrentStatistics;

public class MatrixAlgorithm {
	private final int size;

	private final float[] A;
	private final float[] B;

	public MatrixAlgorithm(int size) {
		this.size = size;
		A = new float[size * size];
		B = new float[size * size];

		for (int i = 0; i < size * size; i++) {
			A[i] = 2;
			B[i] = 2;
		}
	}

	public void execute() {
		float[] C = new float[size * size];

		long start = System.nanoTime();

		float[] result = multiply(A, B, C, size);

		long elapsed = System.nanoTime() - start;
		long millis = TimeUnit.NANOSECONDS.toMillis(elapsed);
		System.out.println("seq in " + millis / 1000.0 + "s");
		CurrentStatistics.getCurrent().addsequentialTime(millis);
	}

	public float[] multiply(float[] A, float[] B, float[] C, int size) {
		for (int i = 0; i < size; ++i) {
			for (int j = 0; j < size; ++j) {
				float tmp = 0;

				for (int k = 0; k < size; ++k) {
					float a = A[i * size + k];
					float b = B[k * size + j];
					tmp += a * b;
				}

				C[i * size + j] += tmp;
			}
		}

		return C;
	}
}
