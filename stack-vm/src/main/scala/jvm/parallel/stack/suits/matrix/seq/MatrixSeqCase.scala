package jvm.parallel.stack.suits.matrix.seq

import java.util.concurrent.TimeUnit

import jvm.parallel.stack.suits.TestCase
import jvm.parallel.stack.suits.matrix.MatrixStatistics

class MatrixSeqCase(size: Int) extends TestCase[MatrixStatistics] {
  override def run(stats: MatrixStatistics): Unit = {
    if (stats.averageSequential() < TimeUnit.MINUTES.toMillis(1) && size <= 2048) {
      repeat(5) {
        new MatrixAlgorithm(size).execute()
      }
    } else {
      println("Skipped seq: " + size)
    }
  }
}
