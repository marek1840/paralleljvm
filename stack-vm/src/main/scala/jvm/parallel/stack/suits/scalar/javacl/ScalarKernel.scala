package jvm.parallel.stack.suits.scalar.javacl

object ScalarKernel {
  val source =
    """
      |__kernel void initialize(__global int* xs, __global int* ys, __global int* targets, int size) {
      |	 {
      |		int i;
      |		i = get_global_id(0);
      |		targets[i] = xs[i] * ys[i];
      |	}
      |}
    """.stripMargin
}
