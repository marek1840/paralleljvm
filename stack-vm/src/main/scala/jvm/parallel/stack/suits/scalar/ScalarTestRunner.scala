package jvm.parallel.stack.suits.scalar

import jvm.parallel.stack.suits.CurrentStatistics
import jvm.parallel.stack.suits.matrix.MatrixStatistics

object ScalarTestRunner {
  def main(args: Array[String]): Unit = {
    warmUp()

    val sizeLimit = 67108864
    val writer = new ScalarStatisticsWriter("stats/scalar")

    for {
      size <- Iterator.iterate(512)(_ * 2) takeWhile (_ <= sizeLimit)
    } {
      val suite = new ScalarTestSuite(size, writer)
      suite.run()
    }
  }

  def warmUp() = {
    CurrentStatistics.setCurrentCollector(new MatrixStatistics(0))
    (1 to 10 ) foreach { _ =>
      new ScalarTestSuite(256, new ScalarStatisticsWriter("/tmp")).run()
    }
    println("finished warmup")
  }
}
