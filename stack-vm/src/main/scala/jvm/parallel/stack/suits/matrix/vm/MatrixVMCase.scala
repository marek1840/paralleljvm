package jvm.parallel.stack.suits.matrix.vm

import jvm.parallel.stack.suits.VMCase
import jvm.parallel.stack.suits.matrix.MatrixStatistics

class MatrixVMCase(val size: Int) extends VMCase[MatrixMetadata, MatrixStatistics] {
  override protected[this] def registerMethods(suite: MatrixMetadata): Unit = {
    codeStorage.registerMethod("main", suite.main)
    codeStorage.registerMethod("multiply", suite.multiply)
  }

  override protected[this] def registerStructures(suite: MatrixMetadata): Unit = {}

  override protected[this] def suiteMetadata: MatrixMetadata = new MatrixMetadata(size)
}
