package jvm.parallel.stack.suits.mandelbrot.javacl;

import org.bridj.BridJ;
import org.bridj.Pointer;
import org.bridj.StructObject;
import org.bridj.ann.Field;

public class Adapted_Point extends StructObject {
	static {
		BridJ.register();
	}

	public Adapted_Point() {
		super();
	}

	public Adapted_Point(Pointer<StructObject> pointer) {
		super(pointer);
	}

	@Field(0)
	public float get_0() {
		return this.io.getFloatField(this, 0);
	}

	@Field(0)
	public void set_0(float value) {
		this.io.setFloatField(this, 0, value);
	}

	@Field(1)
	public float get_1() {
		return this.io.getFloatField(this, 1);
	}

	@Field(1)
	public void set_1(float value) {
		this.io.setFloatField(this, 1, value);
	}

}
