package jvm.parallel.stack.suits.scalar

import java.io.File

import jvm.parallel.stack.suits.StatsWriter

class ScalarStatisticsWriter(parentDir: String) extends StatsWriter[ScalarStatistics](parentDir) {
  override protected[this] val files: Seq[File] = Seq(
    new File(parentDir, "comparison.txt")
  )

  protected[this] def headers(): Seq[String] = Seq(
    line("size", "vmTotal", "sequential", "javaCL")
  )

  protected[this] def buildLines(stats: ScalarStatistics): Seq[String] = Seq(
    comparisonLine(stats)
  )

  override protected[this] def comparisonLine(stats: ScalarStatistics): String = line(stats.size,
    stats.averageExecutionTime(),
    stats.averageSequential(),
    stats.averageJavaCLExecution()
  )

  override protected[this] def javaCLSummary(stats: ScalarStatistics): String = ""

  override protected[this] def vmSummary(stats: ScalarStatistics): String = ""

  override protected[this] def executionLine(stats: ScalarStatistics): String = ""

  override protected[this] def toHostLine(stats: ScalarStatistics): String = ""

  override protected[this] def fromHostLine(stats: ScalarStatistics): String = ""
}
