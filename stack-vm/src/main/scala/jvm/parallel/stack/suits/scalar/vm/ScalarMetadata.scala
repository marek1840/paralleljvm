package jvm.parallel.stack.suits.scalar.vm

import jvm.parallel.parsers.java.JavaParser
import jvm.parallel.stack.Method
import jvm.parallel.stack.ops._
import jvm.parallel.stack.suits.TestMetadata

class ScalarMetadata(size: Int) extends TestMetadata {

  private[this] val initCode =
    """@jvm.parallel.annotations.Parallel(labels = {"init"})
      |public void initialize(
      |   @jvm.parallel.annotations.In int[] xs,
      |   @jvm.parallel.annotations.In int[] ys,
      |   @jvm.parallel.annotations.InOut int[] targets,
      |   int size){
      |
      | init: for(int i = 0; i < size; ++i){
      |   targets[i] = xs[i] * ys[i];
      | }
      |}""".stripMargin
  private[this] val initCodeAST = JavaParser.parseMethod(initCode)
  private[this] val mainInstructions = Seq(
    Push(size), Store("size"),
    Load("size"), NewArray, Store("xs"), // points = new Point[size]
    Load("size"), NewArray, Store("ys"), // points = new Point[size]
    Load("size"), NewArray, Store("targets"), // points = new Point[size]

    Push(0), Store("i"), // i = 0

    Load("i"), Load("size"), Lesser, GoIfFalse(30),
    Push(7), Load("i"), Load("xs"), ArrayStore, // xs[i] = 7
    Push(4), Load("i"), Load("ys"), ArrayStore, // ys[i] = 4
    Load("i"), Push(1), Add, Store("i"), //++i
    GoTo(13), // loop

    Load("size"), Load("targets"), Load("ys"), Load("xs"),
    InvokeOnGPU("initialize"),
    Return
  )
  private[this] val initInstructions = Seq()

  val main = new Method(mainInstructions, None)
  val init = new Method(initInstructions, Some(initCodeAST))
}
