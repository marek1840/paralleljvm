package jvm.parallel.stack.suits.mandelbrot

import jvm.parallel.stack.suits.Statistics

class MandelbrotStatistics(val size: Int,
                           val iterations: Int) extends Statistics
