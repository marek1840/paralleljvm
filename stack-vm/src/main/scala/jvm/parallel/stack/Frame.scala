package jvm.parallel.stack

import jvm.parallel.ast.name.Name
import jvm.parallel.stack.ops.Instruction
import jvm.parallel.vm2.CallFrame
import jvm.parallel.vm2.components.LocalVariableComponent

class Frame(private[this] val instructions: Array[Instruction],
            private[this] val locals: Seq[Name]) extends CallFrame with Locals {

  this: LocalVariableComponent =>

  override type Operation = Instruction

  private[this] var ip: Int = 0

  override def jumpTo(ip: Int): Unit = this.ip = ip

  override def nextInstruction: Operation = {
    val i = instructions(ip)
    ip += 1
    i
  }
}
