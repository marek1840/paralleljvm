package jvm.parallel.ast

protected[parallel] trait Visitable[V <: Visitor] {
  def accept(visitor: V): Unit
 }
