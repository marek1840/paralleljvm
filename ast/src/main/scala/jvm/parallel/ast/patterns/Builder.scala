package jvm.parallel.ast.patterns

trait Builder[+A] {
  def build(implicit context: Context): Seq[A]
}

case class ByNameReference[+A](name: String) extends Builder[A] {
  override def build(implicit context: Context): Seq[A] = {
    context valueOf name
  }
}