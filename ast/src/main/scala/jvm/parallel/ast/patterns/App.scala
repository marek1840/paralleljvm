package jvm.parallel.ast.patterns

object App {

  case object One extends Pattern[Int] {
    override def matches[B >: Int](a: B)(implicit context: Context): Boolean = a match {
      case 1 => true
      case _ => false
    }
  }

  case object Zero extends Pattern[Int] {
    override def matches[B >: Int](a: B)(implicit context: Context): Boolean = a match {
      case 0 => true
      case _ => false
    }
  }

  def main(args: Array[String]) {
    implicit val ctx = new Context
    val pattern = "a" := One | ("b"  := Zero)
    val seq = Seq(1, 0)

    val result = pattern process seq
    println(pattern)
    println(result)
    println(ctx.mappings)
  }
}