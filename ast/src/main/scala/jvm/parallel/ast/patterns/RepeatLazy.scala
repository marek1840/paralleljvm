package jvm.parallel.ast.patterns

import scala.annotation.tailrec

case class RepeatLazy[+A](pattern: PatternNode[A],
                          next: PatternNode[A],
                          requiredMatches: Int)
  extends PatternNode[A] {
  override def process[B >: A](matched: Seq[B], seq: Seq[B])(implicit context: Context) = {
    @tailrec
    def loop(acc: Int, matched: Seq[B], values: Seq[B]): PatternNode[B] = next process values match {
      case Success(innerMatch, rest) if acc >= requiredMatches => Success(matched ++: innerMatch, rest)
      case Success(nextMatch, nextRest) => pattern process values match {
        case f@Failure(_, _) => Failure(matched ++: nextMatch, nextRest)
        case Success(innerMatch, rest) => loop(acc + 1, innerMatch, rest)
      }
      case Failure(nextMatch, nextRest) => pattern process values match {
        case f@Failure(_, _) => Failure(matched , values)
        case Success(innerMatch, rest) => loop(acc + 1, matched ++: innerMatch, rest)
      }
    }

    loop(0, Nil, seq) match {
      case Success(innerMatched, innerRest) => Success(matched ++: innerMatched, innerRest)
      case Failure(innerMatched, innerRest) => Failure(matched ++: innerMatched, innerRest)
    }
  }

}
