package jvm.parallel.ast.patterns

trait FinalPatternNode[+A] extends PatternNode[A] {
  override def process[B >: A](matched : Seq[B], seq: Seq[B])(implicit context: Context): PatternNode[B] = this
}

case class Failure[A](matched: Seq[A], rest: Seq[A]) extends FinalPatternNode[A]
case class Success[A](matched: Seq[A], rest: Seq[A]) extends FinalPatternNode[A]

case object EOI extends FinalPatternNode[Nothing]{
  override def process[B >: Nothing](matched : Seq[B], seq: Seq[B])(implicit context: Context): PatternNode[B] = seq match {
    case Nil => Success(matched, Nil)
    case _ => Failure(matched, seq)
  }
}
