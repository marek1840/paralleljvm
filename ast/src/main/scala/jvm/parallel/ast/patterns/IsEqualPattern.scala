package jvm.parallel.ast.patterns

case class IsEqualPattern[+A](pattern: A) extends Pattern[A]{
  override def matches[B >: A](a: B)(implicit context: Context): Boolean = a == pattern
}
