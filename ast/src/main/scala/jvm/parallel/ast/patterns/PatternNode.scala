package jvm.parallel.ast.patterns

trait PatternNode[+A] extends Pattern[Seq[A]]{
  def matches[B >: Seq[A]](a: B)(implicit context: Context): Boolean = a match {
    case seq : Seq[A] =>
      this process seq match {
        case Success(_, _) =>true
        case _ => false
      }
    case _ => false
  }


  def process[B >: A](matched : Seq[B], notYetMatched: Seq[B])(implicit context: Context): PatternNode[B]
  def process[B >: A](notYerMatched: Seq[B])(implicit context: Context = null): PatternNode[B] = process(Nil, notYerMatched)

  def ::[B >: A](pattern: Pattern[B]): PatternNode[B] = AndPattern(SimplePatternNode(pattern), this)
  def ::[B >: A](pattern: PatternNode[B]): PatternNode[B] = AndPattern(pattern, this)

  def |[B >: A](pattern:Pattern[B]) : PatternNode[B] = pattern match {
    case p: PatternNode[A] => OrPattern(this, p)
    case _ => OrPattern(this, SimplePatternNode(pattern))
  }

  def ~[B >: A](pattern: Pattern[B]): PatternNode[B] = pattern match {
    case p: PatternNode[A] => AndPattern(this, p)
    case _ => AndPattern(this, SimplePatternNode(pattern))
  }

  def ~[B >: A](value: B) : PatternNode[B] = AndPattern(this, IsEqualPattern(value))

  def * : PatternNode[A] = new RepeatGreedy(this, 0)
  def + : PatternNode[A] = new RepeatGreedy(this, 1)

  def ~>[B >: A](builders: Seq[Builder[B]]) : Transformation[B] = new Transformation[B](this, builders)
}
