package jvm.parallel.ast.patterns

case object AnyInput extends Pattern[Nothing]{
  override def matches[B >: Nothing](a: B)(implicit context: Context): Boolean = true
}