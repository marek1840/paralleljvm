package jvm.parallel.ast.patterns

case class SimplePatternNode[A](pattern: Pattern[A]) extends PatternNode[A] {
  def process[B >: A](matched : Seq[B], seq: Seq[B])(implicit context: Context) = seq match {
    case (hd :: tl) if pattern matches hd => Success(Seq(hd), tl)
    case _ => Failure(matched, seq)
  }
}
