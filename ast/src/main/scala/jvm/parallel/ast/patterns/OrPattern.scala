package jvm.parallel.ast.patterns

case class OrPattern[+A](pattern: PatternNode[A], next: PatternNode[A]) extends PatternNode[A] {
  def process[B >: A](matched: Seq[B], seq: Seq[B])(implicit context: Context): PatternNode[B] = pattern process(matched, seq) match {
    case s@Success(_, _) => s
    case Failure(innerMatch, tail) => next process(matched, seq)
  }
}