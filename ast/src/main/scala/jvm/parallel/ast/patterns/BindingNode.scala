package jvm.parallel.ast.patterns

case class BindingNode[+A](name:String, pattern: PatternNode[A]) extends PatternNode[A]{
  override def process[B >: A](matched: Seq[B], seq: Seq[B])(implicit context: Context): PatternNode[B] = {
    pattern process (matched, seq) match {
      case s@Success(value, _) =>
        context bind (name, value)
        s
      case res => res
    }
  }
}
