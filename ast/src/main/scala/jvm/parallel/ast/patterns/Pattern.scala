package jvm.parallel.ast.patterns

trait Pattern[+A] {
  def matches[B >: A](a: B)(implicit context: Context): Boolean
}



