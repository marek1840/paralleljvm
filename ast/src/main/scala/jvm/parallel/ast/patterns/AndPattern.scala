package jvm.parallel.ast.patterns

case class AndPattern[+A](pattern: PatternNode[A], next: PatternNode[A]) extends PatternNode[A] {
  def process[B >: A](matched: Seq[B], notYerMatched: Seq[B])(implicit context: Context): PatternNode[B] =
    pattern process notYerMatched match {
      case Success(innerMatch, tail) => next process(Nil, tail) match {
        case Success(nextMatch, nextRest) => Success(innerMatch ++: nextMatch, nextRest)
        case Failure(nextMatch, nextRest) => Failure(innerMatch ++: nextMatch, nextRest)
      }
      case Failure(innerMatch, tail) => Failure(matched ++: innerMatch, tail)
    }
}
