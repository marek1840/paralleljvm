package jvm.parallel.ast.patterns

final class Transformation[A](pattern: PatternNode[A], builders : Seq[Builder[A]]) {
  def transform(values: Seq[A])(implicit context: Context) : Seq[A] = {
    pattern process values match {
      case Success(_, _) => builders flatMap (_ build)
      case _ => Nil
    }
  }
}
