package jvm.parallel.ast

package object patterns {
  implicit def patternToNode[A](pattern: Pattern[A]): PatternNode[A] = SimplePatternNode(pattern)
  implicit def wrapperToNode[A](pattern: LazyPatternWrapper[A]): PatternNode[Any] = pattern ~ EOI
  implicit def stringToReference[A](name:String) : Builder[A] = ByNameReference(name)
  /**
    * eg. '(1 ~ 2).*? - requires 'follow-up' pattern. When to such pattern - above implicit infers EOI
    */
  protected[this] class LazyPatternWrapper[+A](patternNode: PatternNode[A],
                                               requiredMatches: Int) {
    def ~[B >: A](pattern: Pattern[B]): PatternNode[B] = pattern match {
      case p: PatternNode[_] => new RepeatLazy(patternNode, p, requiredMatches)
      case _ => new RepeatLazy(patternNode, pattern, requiredMatches)
    }
  }

  implicit class StringWrapper[A](name: String) {
    def :=(pattern: Pattern[A]): PatternNode[A] = pattern match {
      case p: PatternNode[A] => new BindingNode(name, p)
      case _ => new BindingNode(name, pattern)
    }

//    def :=(value: A): PatternNode[A] = new BindingNode(name, IsEqualPattern(value))
  }

  implicit class NodeWrapper[A](pattern: PatternNode[A]) {
    def *? = new LazyPatternWrapper(pattern, 0)
    def +? = new LazyPatternWrapper(pattern, 1)
  }

  implicit class PatternWrapper[A](pattern: Pattern[A]) {
    def *? = new LazyPatternWrapper(pattern, 0)
    def +? = new LazyPatternWrapper(pattern, 1)
  }

}


