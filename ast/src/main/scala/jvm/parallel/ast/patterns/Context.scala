package jvm.parallel.ast.patterns

import scala.collection.mutable

final class Context {
  val mappings : mutable.Map[String, Any] = mutable.Map()
  def bind(name: String, value: Any) = mappings += (name -> value)
  def valueOf[A](name: String): A = {
    val value = mappings(name)
    if(value != null)
      value.asInstanceOf[A]
    else throw new Exception("No such bounded: " + name)
  }
}