package jvm.parallel.ast

final class TreeTraversalException(msg:String) extends Exception(msg)
