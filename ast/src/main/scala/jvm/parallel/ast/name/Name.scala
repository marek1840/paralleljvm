package jvm.parallel.ast.name

import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.FieldAccess
import jvm.parallel.ast.parsers.statement.instructions.discardable.reference.Select

//TODO consider separate classes representing names of 'package', 'type', 'method'
//TODO remvoe 'case' and work out proper unapply
class Name(val name: Seq[String], delimeter: String = Name.subPackage) {
  val representation: String = name mkString delimeter

  def this(parts: String) = this(parts :: Nil, Name.subPackage)

  def last = new Name(name.last)
  def dropLast = new Name(name.dropRight(1))

  def parent = name match {
    case Nil => None
    case hd +: Nil => None
    case _ => Some(new Name(name.dropRight(1)))
  }
  def withSubPackage(that: Name): Name = {
    if (name.isEmpty) that
    else new Name(this.name ++ that.name, Name.subPackage)
  }

  def withType(that: Name): Name = {
    if (name.isEmpty) that
    else new Name(this.name ++ that.name, Name.subPackage)
  }

  def withMethod(that: Name): Name = {
    if (name.isEmpty) that
    else new Name(this.name ++ that.name, Name.method)
  }

  def withField(that: Name): Name = {
    if (name.isEmpty) that
    else new Name(this.name ++ that.name, Name.field)
  }

  override def toString = representation

  override def hashCode = toString.hashCode

  override def equals(o: Any) = o match {
    case n: Name => representation == n.representation
    case _ => false
  }
}

object Name {
  private[ast] val subPackage = "."
  private[ast] val subType = "$"
  private[ast] val method = "::"
  private[ast] val field = "->"

  def unapply(name:Name) : Option[String] = Some(name.representation)
  def apply(parts : Seq[String]) : Name = new Name(parts)
}