package jvm.parallel.ast.name

final class Token private[name](name: Seq[String]) extends Name(name) {

  override def last: Name = TokenCache.fromName(super.last)
  override def dropLast = TokenCache.fromName(super.dropLast)

  override def withSubPackage(name:Name) :Name = TokenCache.fromString(super.withSubPackage(name).representation)
  override def withType(name:Name) :Name = TokenCache.fromString(super.withSubPackage(name).representation)
  override def withMethod(name:Name) :Name = TokenCache.fromString(super.withMethod(name).representation)
  override def withField(name:Name) :Name = TokenCache.fromString(super.withField(name).representation)

  override def equals(that: Any) = that match {
    case t: Token if t eq this => true
    case n : Name => n.representation == this.representation
    case _ => false
  }
}


