package jvm.parallel.ast.name

import scala.collection.mutable

object TokenCache {
  private[this] val cache: mutable.Map[String, Token] = mutable.Map()

  protected[parallel] def fromString(key:String) = cache.getOrElseUpdate(key, new Token(key +: Nil))
  protected[parallel] def fromName(name:Name) = cache.getOrElseUpdate(name.representation, new Token(name.name))
}
