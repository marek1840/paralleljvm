package jvm.parallel.ast

import scala.collection.mutable

trait AggregatedVisitor[A <: Visitor] extends Visitor{
  protected[this] def visitors: mutable.Seq[A]
}