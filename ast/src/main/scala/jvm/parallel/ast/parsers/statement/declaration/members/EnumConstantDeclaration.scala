package jvm.parallel.ast.parsers.statement.declaration.members

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.statement.declaration.Annotated
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class EnumConstantDeclaration(var annotations: Seq[Annotation],
                                   var name: Name,
                                   var constructorArguments: Seq[Expression]) extends MemberDeclaration with Annotated{
  override def accept(visitor: ASTVisitor) = {
    visitor.onEnter(this)

    annotations.accept(visitor)
    constructorArguments.accept(visitor)

    visitor.onExit(this)
  }
}
