package jvm.parallel.ast.parsers

import jvm.parallel.ast.Visitable
import jvm.parallel.ast.parsers.visitor.ASTVisitor

import scala.annotation.tailrec

trait ASTNode extends Visitable[ASTVisitor] {

  protected implicit class VisitableSeq[A <: ASTNode](seq: Seq[A]) {
    def accept(visitor: ASTVisitor) = seq.foreach(_.accept(visitor))
  }

  protected implicit class VisitableOption[A <: ASTNode](opt: Option[A]) {
    def accept(visitor: ASTVisitor) = opt.foreach(_.accept(visitor))
  }

}

object ASTNode {
  @tailrec
  protected[ast] def equalSeq[A <: ASTNode](ss1: Seq[A], ss2: Seq[A]): Boolean = {
    (ss1.size, ss2.size) match {
      case (0, 0) => true
      case (s1, s2) if s1 != s2 => false
      case _ => (ss1, ss2) match {
        case (null, null) | (Nil, Nil) => true
        case (_, null) | (null, _) => false
        case (h1 +: tl1, h2 +: tl2) => equal(h1, h2) && equalSeq(tl1, tl2)
      }
    }

  }

  protected[ast] def equal[A <: ASTNode](a1: A, a2: A): Boolean = (a1, a2) match {
    case (null, null) => true
    case (_, null) | (null, _) => false
    case _ => a1 == a2
  }
}