package jvm.parallel.ast.parsers.statement.instructions

import jvm.parallel.ast.parsers.statement.Statement

trait InstructionStatement extends Statement