package jvm.parallel.ast.parsers.statement.modifers

import jvm.parallel.ast.parsers.ASTNode
import jvm.parallel.ast.parsers.visitor.ASTVisitor

trait Modifier extends ASTNode {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.visit(this)
  }
}














