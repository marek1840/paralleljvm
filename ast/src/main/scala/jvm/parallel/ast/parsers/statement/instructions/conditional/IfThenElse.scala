package jvm.parallel.ast.parsers.statement.instructions.conditional

import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.statement.Statement
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class IfThenElse(var condition: Expression, var ifTrue: Statement, var ifFalse: Statement) extends ConditionalStatement {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    condition.accept(visitor)
    ifTrue.accept(visitor)
    ifFalse.accept(visitor)

    visitor.onExit(this)
  }
}