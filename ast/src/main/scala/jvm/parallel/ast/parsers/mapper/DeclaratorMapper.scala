package jvm.parallel.ast.parsers.mapper

import jvm.parallel.ast.parsers.statement.declarator._
import jvm.parallel.ast.parsers.statement.instructions.discardable.dimension.{AbstractDimension, InitializedDimension, Dimension}

abstract class DeclaratorMapper extends ExpressionMapper {
  protected[this] def mapDeclarators(nodes: Seq[Declarator]): Seq[Declarator] = nodes.map(mapDeclarator)

  def mapConstructorDeclarator(c: ConstructorDeclarator): ConstructorDeclarator = c match {
    case ConstructorDeclarator(n, ps) => ConstructorDeclarator(mapName(n), mapParameters(ps))
  }

  def mapFunctionDeclarator(c: FunctionDeclarator): FunctionDeclarator = c match {
    case MethodDeclarator(n, ps) => MethodDeclarator(mapName(n), mapParameters(ps))
    case ArrayMethodDeclarator(b, ps, ds) => ArrayMethodDeclarator(mapName(b), mapParameters(ps), mapDim(ds))
  }

  def mapDeclarator(d: Declarator): Declarator = d match {
    case c: ConstructorDeclarator => mapConstructorDeclarator(c)
    case f: FunctionDeclarator => mapFunctionDeclarator(f)

    case ArrayDeclarator(n, ds) => ArrayDeclarator(mapName(n), mapDim(ds))
    case InitializedArrayDeclarator(n, ds, us) => InitializedArrayDeclarator(mapName(n), mapDim(ds), mapExpr(us))
    case VariableDeclarator(n) => VariableDeclarator(mapName(n))
    case InitializedVariableDeclarator(n, e) => InitializedVariableDeclarator(mapName(n), mapExpr(e))
  }
}
