package jvm.parallel.ast.parsers.statement.declarator

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.parameter.Parameter
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class MethodDeclarator(var name: Name, var parameters: Seq[Parameter]) extends FunctionDeclarator {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    parameters.accept(visitor)

    visitor.onExit(this)
  }
}