package jvm.parallel.ast.parsers.expression.types.references

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.expression.types.templates.TemplateArgument
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class ClassType(var annotations: Seq[Annotation],
                     var parent: Option[ClassType],
                     var name: Name,
                     var types: Seq[TemplateArgument]) extends ReferenceType {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    annotations.accept(visitor)
    parent.accept(visitor)
    types.accept(visitor)

    visitor.onExit(this)
  }
}
