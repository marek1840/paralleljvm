package jvm.parallel.ast.parsers.statement.declaration.imports

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class LazyImportDeclaration(var name: Name) extends ImportDeclaration {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.visit(this)
  }
}
