package jvm.parallel.ast.parsers.statement.instructions.discardable.dimension

import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class InitializedDimension(var annotations: Seq[Annotation], var expr: Expression) extends Dimension {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    annotations.accept(visitor)

    expr.accept(visitor)

    visitor.onExit(this)
  }
}

