package jvm.parallel.ast.parsers.statement.instructions.switch

import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.statement.Statement
import jvm.parallel.ast.parsers.statement.instructions.InstructionStatement
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class SwitchStatement(var expression: Expression, var cases: Seq[SwitchRule]) extends InstructionStatement {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    expression.accept(visitor)
    cases.accept(visitor)

    visitor.onExit(this)
  }
}