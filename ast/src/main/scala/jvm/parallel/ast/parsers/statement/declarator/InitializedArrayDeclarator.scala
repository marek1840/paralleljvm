package jvm.parallel.ast.parsers.statement.declarator

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.statement.instructions.discardable.dimension.Dimension
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class InitializedArrayDeclarator(var name: Name,
                                      var dimensions: Seq[Dimension],
                                      var initializer: Expression) extends Declarator {

  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    dimensions.accept(visitor)
    initializer.accept(visitor)
    visitor.onExit(this)
  }
}