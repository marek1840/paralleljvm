package jvm.parallel.ast.parsers.visitor

import jvm.parallel.ast.parsers.expression._
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary._
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.assignment.{AugmentedBinding, Binding}
import jvm.parallel.ast.parsers.statement.instructions.discardable.dimension.{AbstractDimension, InitializedDimension}
import jvm.parallel.ast.parsers.statement.instructions.discardable.instantiation._
import jvm.parallel.ast.parsers.statement.instructions.discardable.literals.ClassLiteral
import jvm.parallel.ast.parsers.statement.instructions.discardable.unary.{PostDecrementation, PostIncrementation, PreDecrementation, PreIncrementation}
import jvm.parallel.ast.parsers.statement.instructions.discardable.{ArrayConstructorReference, ConstructorReference}
import jvm.parallel.ast.parsers.expression.types.PointerType
import jvm.parallel.ast.parsers.expression.types.annotations.{NamedAnnotationParameter, NormalAnnotation, SingleElementAnnotation}
import jvm.parallel.ast.parsers.expression.types.coupled.{ChildOfAll, ChildOfAny}
import jvm.parallel.ast.parsers.expression.types.primitives._
import jvm.parallel.ast.parsers.expression.types.references.{ArrayType, ClassType}
import jvm.parallel.ast.parsers.expression.types.templates.{AnyTemplate, ArgumentTemplate, ParameterTemplate}

trait ExpressionVisitor {
  def onEnter(binding: Binding) = {}
  def onExit(binding: Binding): Unit = {}

  def onEnter(binding: AugmentedBinding) = {}
  def onExit(binding: AugmentedBinding): Unit = {}

  def onEnter(extraction: Extraction) = {}
  def onExit(extraction: Extraction): Unit = {}

  def onEnter(access: FieldAccess) = {}
  def onExit(access: FieldAccess): Unit = {}

  def onEnter(invocation: MethodInvocation) = {}
  def onExit(invocation: MethodInvocation): Unit = {}

  def onEnter(reference: MethodReference) = {}
  def onExit(reference: MethodReference): Unit = {}

  def onEnter(invocation: QualifiedMethodInvocation) = {}
  def onExit(invocation: QualifiedMethodInvocation): Unit = {}

  def onEnter(dimension: AbstractDimension) = {}
  def onExit(dimension: AbstractDimension): Unit = {}

  def onEnter(dimension: InitializedDimension) = {}
  def onExit(dimension: InitializedDimension): Unit = {}

  def onEnter(instantiation: NestedAnonymousObjectInstantiation) = {}
  def onExit(instantiation: NestedAnonymousObjectInstantiation): Unit = {}

  def onEnter(instantiation: InitializedArrayInstantiation) = {}
  def onExit(instantiation: InitializedArrayInstantiation): Unit = {}

  def onEnter(instantiation: AnonymousObjectInstantiation) = {}
  def onExit(instantiation: AnonymousObjectInstantiation): Unit = {}

  def onEnter(instantiation: EmptyArrayInstantiation) = {}
  def onExit(instantiation: EmptyArrayInstantiation): Unit = {}

  def onEnter(instantiation: NestedObjectInstantiation) = {}
  def onExit(instantiation: NestedObjectInstantiation): Unit = {}

  def onEnter(instantiation: SimpleObjectInstantiation) = {}
  def onExit(instantiation: SimpleObjectInstantiation): Unit = {}

  def onEnter(literal: ClassLiteral) = {}
  def onExit(literal: ClassLiteral): Unit = {}

  def onEnter(incrementation: PreDecrementation) = {}
  def onExit(incrementation: PreDecrementation): Unit = {}

  def onEnter(incrementation: PostDecrementation) = {}
  def onExit(incrementation: PostDecrementation): Unit = {}

  def onEnter(incrementation: PostIncrementation) = {}
  def onExit(incrementation: PostIncrementation): Unit = {}

  def onEnter(incrementation: PreIncrementation) = {}
  def onExit(incrementation: PreIncrementation): Unit = {}

  def onEnter(reference: ConstructorReference) = {}
  def onExit(reference: ConstructorReference): Unit = {}

  def onEnter(parameter: NamedAnnotationParameter) = {}
  def onExit(parameter: NamedAnnotationParameter): Unit = {}

  def onEnter(annotation: NormalAnnotation) = {}
  def onExit(annotation: NormalAnnotation): Unit = {}

  def onEnter(annotation: SingleElementAnnotation) = {}
  def onExit(annotation: SingleElementAnnotation): Unit = {}

  def onEnter(any: ChildOfAny) = {}
  def onExit(any: ChildOfAny): Unit = {}

  def onEnter(all: ChildOfAll) = {}
  def onExit(all: ChildOfAll): Unit = {}

  def onEnter(primitive: LongPrimitive) = {}
  def onExit(primitive: LongPrimitive): Unit = {}

  def onEnter(primitive: IntegerPrimitive) = {}
  def onExit(primitive: IntegerPrimitive): Unit = {}

  def onEnter(primitive: FloatPrimitive) = {}
  def onExit(primitive: FloatPrimitive): Unit = {}

  def onEnter(primitive: DoublePrimitive) = {}
  def onExit(primitive: DoublePrimitive): Unit = {}

  def onEnter(primitive: CharPrimitive) = {}
  def onExit(primitive: CharPrimitive): Unit = {}

  def onEnter(primitive: BytePrimitive) = {}
  def onExit(primitive: BytePrimitive): Unit = {}

  def onEnter(primitive: BooleanPrimitive) = {}
  def onExit(primitive: BooleanPrimitive): Unit = {}

  def onEnter(primitive: ShortPrimitive) = {}
  def onExit(primitive: ShortPrimitive): Unit = {}

  def onEnter(primitive: VoidPrimitive) = {}
  def onExit(primitive: VoidPrimitive): Unit = {}

  def onEnter(arrayType: ArrayType) = {}
  def onExit(arrayType: ArrayType): Unit = {}

  def onEnter(classType: ClassType) = {}
  def onExit(classType: ClassType): Unit = {}

  def onEnter(pointerType: PointerType) = {}
  def onExit(pointerType: PointerType): Unit = {}

  def onEnter(reference: ArrayConstructorReference) = {}
  def onExit(reference: ArrayConstructorReference): Unit = {}

  def onEnter(template: AnyTemplate) = {}
  def onExit(template: AnyTemplate): Unit = {}

  def onEnter(template: ArgumentTemplate) = {}
  def onExit(template: ArgumentTemplate): Unit = {}

  def onEnter(template: ParameterTemplate) = {}
  def onExit(template: ParameterTemplate): Unit = {}

  def onEnter(initializer: ArrayInitializer) = {}
  def onExit(initializer: ArrayInitializer): Unit = {}

  def onEnter(operations: BinaryOperations) = {}
  def onExit(operations: BinaryOperations): Unit = {}

  def onEnter(cast: Cast) = {}
  def onExit(cast: Cast): Unit = {}

  def onEnter(lambda: Lambda) = {}
  def onExit(lambda: Lambda): Unit = {}

  def onEnter(conditional: TernaryConditional) = {}
  def onExit(conditional: TernaryConditional): Unit = {}

  def onEnter(operation: UnaryOperations): Unit = {}
  def onExit(operation: UnaryOperations): Unit = {}
}
