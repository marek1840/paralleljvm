package jvm.parallel.ast.parsers.expression

import jvm.parallel.ast.parsers.expression.operators.UnaryOperator
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class UnaryOperations(var operators: Seq[UnaryOperator], var operand: Expression) extends Expression {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    operand.accept(visitor)

    visitor.onExit(this)
  }
}
