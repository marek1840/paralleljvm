package jvm.parallel.ast.parsers.mapper

import jvm.parallel.ast.parsers.statement.{Statement, Block}
import jvm.parallel.ast.parsers.statement.instructions.constructor.{ParentConstructorInvocation, IntermediateConstructorInvocation, IndirectParentConstructorInvocation, AlternateConstructorInvocation}
import jvm.parallel.ast.parsers.statement.instructions.conditional.{If, IfThenElse}
import jvm.parallel.ast.parsers.statement.instructions.flow._
import jvm.parallel.ast.parsers.statement.instructions.interruptable._
import jvm.parallel.ast.parsers.statement.instructions.loop.{While, Iteration, For, DoWhile}
import jvm.parallel.ast.parsers.statement.instructions.switch.{SwitchCases, EmptySwitchCases, SwitchRule, SwitchStatement}
import jvm.parallel.ast.parsers.statement.instructions._
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary._
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.assignment.{AugmentedBinding, Binding}
import jvm.parallel.ast.parsers.statement.instructions.discardable.{ArrayConstructorReference, ConstructorReference, DiscardableExpression}
import jvm.parallel.ast.parsers.statement.instructions.discardable.instantiation._
import jvm.parallel.ast.parsers.statement.instructions.discardable.literals.Literal
import jvm.parallel.ast.parsers.statement.instructions.discardable.reference.{LocalVariableReference, Select}
import jvm.parallel.ast.parsers.statement.instructions.discardable.unary.{PreIncrementation, PostIncrementation, PostDecrementation, PreDecrementation}

abstract class InstructionStatementMapper extends DeclarationMapper{
  protected[this] def mapStatements(nodes: Seq[Statement]): Seq[Statement]
  protected[this] def mapBlock(nodes: Option[Block]): Option[Block]
  protected[this] def mapSwitchRules(nodes: Seq[SwitchRule]): Seq[SwitchRule] = nodes.map(mapSwitchRule)
  protected[this] def mapCatchClauses(nodes: Seq[CatchClause]): Seq[CatchClause] = nodes.map(mapCatchClause)
  protected[this] def mapInstructions(nodes:Seq[InstructionStatement]) : Seq[InstructionStatement] = nodes.map(mapInstruction)

  def mapInstruction (node: InstructionStatement) : InstructionStatement = node match {
    case d: DiscardableExpression => mapDiscardable(d)
    case l: LocalVariableDeclaration => mapLocalDeclaration(l)

    case IfThenElse(c, t, f) => IfThenElse(mapExpr(c), mapStmt(t), mapStmt(f))
    case If(c, t) => If(mapExpr(c), mapStmt(t))
    case DoWhile(b, c) => DoWhile(mapStmt(b), mapExpr(c))
    case For(is, c, us, b) => For(mapStatements(is), mapExpr(c), mapStatements(us), mapStmt(b))
    case Iteration(b, e, s) => Iteration(mapLocalDeclaration(b), mapExpr(e), mapStmt(s))
    case While(c, b) => While(mapExpr(c), mapStmt(b))

    case AlternateConstructorInvocation(ts, es) =>
      AlternateConstructorInvocation(mapTemplateArguments(ts), mapExprs(es))

    case IndirectParentConstructorInvocation(q, ts, es) =>
      IndirectParentConstructorInvocation(mapExpr(q), mapTemplateArguments(ts), mapExprs(es))

    case IntermediateConstructorInvocation(e, ts, es) =>
      IntermediateConstructorInvocation(mapExpr(e), mapTemplateArguments(ts), mapExprs(es))

    case ParentConstructorInvocation(ts, es) =>
      ParentConstructorInvocation(mapTemplateArguments(ts), mapExprs(es))

    case Return(e) => Return(mapExpr(e))
    case EmptyReturn => EmptyReturn
    case ImplicitReturn(e) => ImplicitReturn(mapExpr(e))
    case Throw(e) => Throw(mapExpr(e))
    case Yield(e) => Yield(mapExpr(e))

    case TryCatch(b, cs) => TryCatch(mapBlock(b), mapCatchClauses(cs))
    case TryCatchFinally(b1, cs, b2) => TryCatchFinally(mapBlock(b1), mapCatchClauses(cs), mapBlock(b2))
    case TryFinally(b1, b2) => TryFinally(mapBlock(b1), mapBlock(b2))
    case TryWithResources(ds, b1, cs, b2) => TryWithResources(mapLocalVarDeclarations(ds), mapBlock(b1), mapCatchClauses(cs), mapBlock(b2))

    case SwitchStatement(e, rs) => SwitchStatement(mapExpr(e), mapSwitchRules(rs))
    case Assertion(e1, e2) => Assertion(mapExpr(e1), mapExpr(e2))
    case LabeledStatement(n, s) => LabeledStatement(mapName(n), mapStmt(s))
    case SynchronizedBlock(e, b) => SynchronizedBlock(mapExpr(e), mapBlock(b))
  }

  def mapInstantiation(expr: Instantiation): Instantiation = expr match {
    case NestedAnonymousObjectInstantiation(p, ts, t, as, b) =>
      NestedAnonymousObjectInstantiation(mapExpr(p), mapTemplateArguments(ts), mapType(t), mapExprs(as), mapMemberDeclarations(b))

    case AnonymousObjectInstantiation(cs, t, as, b) =>
      AnonymousObjectInstantiation(mapTemplateArguments(cs), mapType(t), mapExprs(as), mapMemberDeclarations(b))

    case EmptyArrayInstantiation(t, ds) =>
      EmptyArrayInstantiation(mapType(t), mapDim(ds))

    case InitializedArrayInstantiation(t, i) =>
    InitializedArrayInstantiation(mapType(t), mapArrayInitializer(i))

    case NestedObjectInstantiation(p, ts, t, as) =>
      NestedObjectInstantiation(mapExpr(p), mapTemplateArguments(ts), mapType(t), mapExprs(as))

    case SimpleObjectInstantiation(ts, t, as) =>
      SimpleObjectInstantiation(mapTemplateArguments(ts), mapType(t), mapExprs(as))
  }

  def mapDiscardable(expr: DiscardableExpression): DiscardableExpression = expr match {
    case i:Instantiation => mapInstantiation(i)
    case l: Literal => mapLiteral(l)

    case Select(name) => Select(mapName(name))

    case PreDecrementation(e) => PreDecrementation(mapExpr(e))
    case PostDecrementation(e) => PostDecrementation(mapExpr(e))
    case PostIncrementation(e) => PostIncrementation(mapExpr(e))
    case PreIncrementation(e) => PreIncrementation(mapExpr(e))

    case ConstructorReference(t, ts) => ConstructorReference(mapType(t), mapTemplateArguments(ts))
    case ArrayConstructorReference(t) => ArrayConstructorReference(mapArrayType(t))

    case Binding(t, s) => Binding(mapExpr(t), mapExpr(s))
    case AugmentedBinding(t, op, s) => AugmentedBinding(mapExpr(t), mapBin(op), mapExpr(s))

    case Extraction(a, k) => Extraction(mapExpr(a), mapExpr(k))
    case FieldAccess(i, a) => FieldAccess(mapExpr(i), mapName(a))
    case MethodInvocation(ts, n, as) => MethodInvocation(mapTemplateArguments(ts), mapName(n), mapExprs(as))
    case MethodReference(q, ts, n) => MethodReference(mapExpr(q), mapTemplateArguments(ts), mapName(n))
    case QualifiedMethodInvocation(i, ts, n, as) => QualifiedMethodInvocation(mapExpr(i), mapTemplateArguments(ts), mapName(n), mapExprs(as))
    case LocalVariableReference(n, i) => LocalVariableReference(mapName(n), i)
  }

  def mapSwitchRule(r:SwitchRule) : SwitchRule = r match {
    case EmptySwitchCases(es) => EmptySwitchCases(mapExprs(es))
    case SwitchCases(es, ss) => SwitchCases(mapExprs(es), mapStatements(ss))
  }

  def mapCatchClause(c:CatchClause) : CatchClause = c match {
    case CatchClause(l, b) => CatchClause(mapLocalDeclaration(l), mapBlock(b))
  }


}
