package jvm.parallel.ast.parsers.expression.types.templates

import jvm.parallel.ast.name.Name

trait TemplateParameter extends Template{
  def name:Name
}
