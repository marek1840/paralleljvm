package jvm.parallel.ast.parsers.mapper

import jvm.parallel.ast.parsers.expression._
import jvm.parallel.ast.parsers.statement.instructions.InstructionStatement
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary._
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.assignment.{AugmentedBinding, Binding}
import jvm.parallel.ast.parsers.statement.instructions.discardable.instantiation._
import jvm.parallel.ast.parsers.statement.instructions.discardable.literals.Literal
import jvm.parallel.ast.parsers.statement.instructions.discardable.reference.Select
import jvm.parallel.ast.parsers.statement.instructions.discardable.unary.{PostDecrementation, PostIncrementation, PreDecrementation, PreIncrementation}
import jvm.parallel.ast.parsers.statement.instructions.discardable.{ArrayConstructorReference, ConstructorReference, DiscardableExpression}
import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.statement.Statement
import jvm.parallel.ast.parsers.statement.declaration.members.MemberDeclaration

abstract class ExpressionMapper extends TypeMapper {
  protected[this] def mapMemberDeclarations(nodes: Seq[MemberDeclaration]): Seq[MemberDeclaration]
  protected[this] def mapStmt(node: Statement): Statement
  protected[this] def mapInstruction (node: InstructionStatement) : InstructionStatement
  def mapDiscardable(expr: DiscardableExpression): DiscardableExpression

  def mapExpr(node: Expression): Expression = node match {
    case t: Type => mapType(t)
    case a: ArrayInitializer => mapArrayInitializer(a)
    case d: DiscardableExpression => mapDiscardable(d)

    case Cast(t, s) => Cast(mapType(t), mapExpr(s))
    case Lambda(ps, b) => Lambda(mapParameters(ps), mapStmt(b))
    case TernaryConditional(c, t, f) => TernaryConditional(mapExpr(c), mapExpr(t), mapExpr(f))
    case BinaryOperations(os, ops) => BinaryOperations(mapBin(os), mapExprs(ops))
    case UnaryOperations(os, op) => UnaryOperations(mapUn(os), mapExpr(op))

    case _ => unsupported(node)
  }
}
