package jvm.parallel.ast.parsers.statement.instructions

import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.statement.{Block, Statement}
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class SynchronizedBlock(var lock: Option[Expression], var block: Block) extends InstructionStatement {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    lock.accept(visitor)
    block.accept(visitor)

    visitor.onExit(this)
  }
}
