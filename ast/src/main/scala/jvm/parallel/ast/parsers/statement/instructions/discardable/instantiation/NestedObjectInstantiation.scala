package jvm.parallel.ast.parsers.statement.instructions.discardable.instantiation

import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.expression.types.templates.TemplateArgument
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class NestedObjectInstantiation(var parent: Expression,
                                     var constructorTypes: Seq[TemplateArgument],
                                     var `type`: Type,
                                     var constructorArguments: Seq[Expression]) extends ObjectInstantiation {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    parent.accept(visitor)
    constructorTypes.accept(visitor)
    `type`.accept(visitor)
    constructorArguments.accept(visitor)

    visitor.onExit(this)
  }
}