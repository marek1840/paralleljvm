package jvm.parallel.ast.parsers.statement.instructions.discardable.binary.assignment

import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class Binding(var target: Expression, var source: Expression) extends Assignment {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    target.accept(visitor)
    source.accept(visitor)

    visitor.onExit(this)
  }
}
