package jvm.parallel.ast.parsers.statement.declaration.members

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.statement.declaration.Annotated
import jvm.parallel.ast.parsers.statement.modifers.Modifier
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class AnnotationElementDeclaration(var annotations: Seq[Annotation],
                                        var modifier: Seq[Modifier],
                                        var `type`: Type,
                                        var name: Name) extends MemberDeclaration with Annotated{
  override def accept(visitor: ASTVisitor) = {
    visitor.onEnter(this)

    annotations.accept(visitor)
    `type`.accept(visitor)

    visitor.onExit(this)
  }
}
