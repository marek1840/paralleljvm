package jvm.parallel.ast.parsers.expression

import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class ArrayInitializer(var values: Seq[Expression]) extends Expression {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    values.accept(visitor)

    visitor.onExit(this)
  }
}
