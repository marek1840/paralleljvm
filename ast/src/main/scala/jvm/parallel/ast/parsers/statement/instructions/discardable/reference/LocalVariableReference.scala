package jvm.parallel.ast.parsers.statement.instructions.discardable.reference

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.instructions.discardable.DiscardableExpression
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class LocalVariableReference(var name:Name, var Index: Int) extends DiscardableExpression {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.visit(this)
  }
}