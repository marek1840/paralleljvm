package jvm.parallel.ast.parsers.expression

import jvm.parallel.ast.parsers.statement.Statement
import jvm.parallel.ast.parsers.statement.parameter.Parameter
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class Lambda(var parameters: Seq[Parameter], var body: Statement) extends Expression {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    parameters.accept(visitor)
    body.accept(visitor)

    visitor.onExit(this)
  }
}
