package jvm.parallel.ast.parsers.visitor

import jvm.parallel.ast.AggregatedVisitor
import jvm.parallel.ast.parsers.expression._
import jvm.parallel.ast.parsers.statement.instructions._
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary._
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.assignment.{AugmentedBinding, Binding}
import jvm.parallel.ast.parsers.statement.instructions.discardable.dimension.{AbstractDimension, InitializedDimension}
import jvm.parallel.ast.parsers.statement.instructions.discardable.instantiation._
import jvm.parallel.ast.parsers.statement.instructions.discardable.literals._
import jvm.parallel.ast.parsers.statement.instructions.discardable.reference.{ParentReference, Select, ThisReference}
import jvm.parallel.ast.parsers.statement.instructions.discardable.unary.{PostDecrementation, PostIncrementation, PreDecrementation, PreIncrementation}
import jvm.parallel.ast.parsers.statement.instructions.discardable.{ArrayConstructorReference, ConstructorReference}
import jvm.parallel.ast.parsers.expression.operators.BinaryOperator
import jvm.parallel.ast.parsers.expression.types.PointerType
import jvm.parallel.ast.parsers.expression.types.annotations.{MarkerAnnotation, NamedAnnotationParameter, NormalAnnotation, SingleElementAnnotation}
import jvm.parallel.ast.parsers.expression.types.coupled.{ChildOfAll, ChildOfAny}
import jvm.parallel.ast.parsers.expression.types.primitives._
import jvm.parallel.ast.parsers.expression.types.references.{ArrayType, ClassType}
import jvm.parallel.ast.parsers.expression.types.templates._
import jvm.parallel.ast.parsers.statement._
import jvm.parallel.ast.parsers.statement.instructions.conditional.{If, IfThenElse}
import jvm.parallel.ast.parsers.statement.instructions.constructor.{AlternateConstructorInvocation, IndirectParentConstructorInvocation, IntermediateConstructorInvocation, ParentConstructorInvocation}
import jvm.parallel.ast.parsers.statement.declaration.imports.{LazyImportDeclaration, SingleImportDeclaration, StaticImportDeclaration, StaticLazyImportDeclaration}
import jvm.parallel.ast.parsers.statement.declaration.initializers.{InstanceInitializerDeclaration, StaticInitializerDeclaration}
import jvm.parallel.ast.parsers.statement.declaration.members._
import jvm.parallel.ast.parsers.statement.declaration.packages.{NamedPackageDeclaration, UnnamedPackageDeclaration}
import jvm.parallel.ast.parsers.statement.declaration.types._
import jvm.parallel.ast.parsers.statement.declaration.CompilationUnitDeclaration
import jvm.parallel.ast.parsers.statement.declarator._
import jvm.parallel.ast.parsers.statement.instructions.flow._
import jvm.parallel.ast.parsers.statement.instructions.interruptable._
import jvm.parallel.ast.parsers.statement.instructions.loop.{DoWhile, For, Iteration, While}
import jvm.parallel.ast.parsers.statement.modifers.Modifier
import jvm.parallel.ast.parsers.statement.parameter._
import jvm.parallel.ast.parsers.statement.instructions.switch.{DefaultSwitch, EmptySwitchCases, SwitchCases, SwitchStatement}

import scala.collection.mutable

class AggregatedASTVisitor extends ASTVisitor with AggregatedVisitor[ASTVisitor]{
  protected[this] override val visitors : mutable.Buffer[ASTVisitor] = mutable.Buffer()

  override def visit(node: BooleanLiteral): Unit = visitors.foreach(_.visit(node))
  override def visit(node: ByteLiteral): Unit = visitors.foreach(_.visit(node))
  override def visit(node: CharLiteral): Unit = visitors.foreach(_.visit(node))
  override def visit(node: DoubleLiteral): Unit = visitors.foreach(_.visit(node))
  override def visit(node: FloatLiteral): Unit = visitors.foreach(_.visit(node))
  override def visit(node: IntegerLiteral): Unit = visitors.foreach(_.visit(node))
  override def visit(node: LongLiteral): Unit = visitors.foreach(_.visit(node))
  override def visit(node: ShortLiteral): Unit = visitors.foreach(_.visit(node))
  override def visit(node: StringLiteral): Unit = visitors.foreach(_.visit(node))
  override def visit(node: NullLiteral.type): Unit = visitors.foreach(_.visit(node))
  override def visit(node: VoidLiteral): Unit = visitors.foreach(_.visit(node))
  
  override def visit(node: MarkerAnnotation): Unit = visitors.foreach(_.visit(node))
  override def visit(node: Select): Unit = visitors.foreach(_.visit(node))
  
  override def visit(node: ParentReference.type): Unit = visitors.foreach(_.visit(node))
  override def visit(node: ThisReference.type): Unit = visitors.foreach(_.visit(node))
  override def visit(node: QualifiedParentReference): Unit = visitors.foreach(_.visit(node))
  override def visit(node: QualifiedThisReference): Unit = visitors.foreach(_.visit(node))

  override def visit(node: EmptyStatement.type): Unit = visitors.foreach(_.visit(node))
  override def visit(node: DefaultSwitch.type): Unit = visitors.foreach(_.visit(node))
  override def visit(node: InferredParameter): Unit = visitors.foreach(_.visit(node))

  override def visit(node: Break.type): Unit = visitors.foreach(_.visit(node))
  override def visit(node: Continue.type): Unit = visitors.foreach(_.visit(node))
  override def visit(node: EmptyReturn.type): Unit = visitors.foreach(_.visit(node))
  override def visit(node: TargetedBreak): Unit = visitors.foreach(_.visit(node))
  override def visit(node: TargetedContinue): Unit = visitors.foreach(_.visit(node))

  override def visit(node: VariableDeclarator): Unit = visitors.foreach(_.visit(node))
  override def visit(node: EmptyDeclaration.type): Unit = visitors.foreach(_.visit(node))
  override def visit(node: UnnamedPackageDeclaration.type): Unit = visitors.foreach(_.visit(node))

  override def visit(node: LazyImportDeclaration): Unit = visitors.foreach(_.visit(node))
  override def visit(node: SingleImportDeclaration): Unit = visitors.foreach(_.visit(node))
  override def visit(node: StaticImportDeclaration): Unit = visitors.foreach(_.visit(node))
  override def visit(node: StaticLazyImportDeclaration): Unit = visitors.foreach(_.visit(node))

  override def visit(node: BinaryOperator): Unit = visitors.foreach(_.visit(node))
  override def visit(node: Modifier): Unit = visitors.foreach(_.visit(node))

  override def onEnter(node: IfThenElse): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: IfThenElse): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: If): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: If): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: AlternateConstructorInvocation): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: AlternateConstructorInvocation): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: IndirectParentConstructorInvocation): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: IndirectParentConstructorInvocation): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: IntermediateConstructorInvocation): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: IntermediateConstructorInvocation): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: ParentConstructorInvocation): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: ParentConstructorInvocation): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: InstanceInitializerDeclaration): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: InstanceInitializerDeclaration): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: StaticInitializerDeclaration): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: StaticInitializerDeclaration): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: AnnotationDefaultElementDeclaration): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: AnnotationDefaultElementDeclaration): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: AnnotationElementDeclaration): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: AnnotationElementDeclaration): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: AnonymousEnumConstantDeclaration): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: AnonymousEnumConstantDeclaration): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: ConstructorDeclaration): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: ConstructorDeclaration): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: EnumConstantDeclaration): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: EnumConstantDeclaration): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: FieldDeclaration): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: FieldDeclaration): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: MethodDeclaration): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: MethodDeclaration): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: NamedPackageDeclaration): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: NamedPackageDeclaration): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: EnumDeclaration): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: EnumDeclaration): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: InterfaceDeclaration): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: InterfaceDeclaration): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: ClassDeclaration): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: ClassDeclaration): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: AnnotationDeclaration): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: AnnotationDeclaration): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: CompilationUnitDeclaration): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: CompilationUnitDeclaration): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: LocalVariableDeclaration): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: LocalVariableDeclaration): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: ArrayDeclarator): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: ArrayDeclarator): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: ArrayMethodDeclarator): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: ArrayMethodDeclarator): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: ConstructorDeclarator): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: ConstructorDeclarator): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: InitializedArrayDeclarator): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: InitializedArrayDeclarator): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: InitializedVariableDeclarator): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: InitializedVariableDeclarator): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: MethodDeclarator): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: MethodDeclarator): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: ImplicitReturn): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: ImplicitReturn): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: Return): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: Return): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: Throw): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: Throw): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: Yield): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: Yield): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: CatchClause): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: CatchClause): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: TryCatch): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: TryCatch): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: TryCatchFinally): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: TryCatchFinally): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: TryFinally): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: TryFinally): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: TryWithResources): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: TryWithResources): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: DoWhile): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: DoWhile): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: For): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: For): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: Iteration): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: Iteration): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: While): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: While): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: FormalParameter): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: FormalParameter): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: InstanceReceiverParameter): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: InstanceReceiverParameter): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: NestedReceiverParameter): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: NestedReceiverParameter): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: VariableArityParameter): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: VariableArityParameter): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: AnyBaseClassTemplate): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: AnyBaseClassTemplate): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: AnySubClassTemplate): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: AnySubClassTemplate): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: EmptySwitchCases): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: EmptySwitchCases): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: SwitchCases): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: SwitchCases): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: SwitchStatement): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: SwitchStatement): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: Assertion): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: Assertion): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: LabeledStatement): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: LabeledStatement): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: SynchronizedBlock): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: SynchronizedBlock): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: Block): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: Block): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: BoundedParameterTemplate): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: BoundedParameterTemplate): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: Binding) = visitors.foreach(_.onEnter(node))
  override def onExit(node: Binding): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: AugmentedBinding) = visitors.foreach(_.onEnter(node))
  override def onExit(node: AugmentedBinding): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: Extraction) = visitors.foreach(_.onEnter(node))
  override def onExit(node: Extraction): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: FieldAccess) = visitors.foreach(_.onEnter(node))
  override def onExit(node: FieldAccess): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: MethodInvocation) = visitors.foreach(_.onEnter(node))
  override def onExit(node: MethodInvocation): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: MethodReference) = visitors.foreach(_.onEnter(node))
  override def onExit(node: MethodReference): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: QualifiedMethodInvocation) = visitors.foreach(_.onEnter(node))
  override def onExit(node: QualifiedMethodInvocation): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: AbstractDimension) = visitors.foreach(_.onEnter(node))
  override def onExit(node: AbstractDimension): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: InitializedDimension) = visitors.foreach(_.onEnter(node))
  override def onExit(node: InitializedDimension): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: NestedAnonymousObjectInstantiation) = visitors.foreach(_.onEnter(node))
  override def onExit(node: NestedAnonymousObjectInstantiation): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: InitializedArrayInstantiation) = visitors.foreach(_.onEnter(node))
  override def onExit(node: InitializedArrayInstantiation): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: AnonymousObjectInstantiation) = visitors.foreach(_.onEnter(node))
  override def onExit(node: AnonymousObjectInstantiation): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: EmptyArrayInstantiation) = visitors.foreach(_.onEnter(node))
  override def onExit(node: EmptyArrayInstantiation): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: NestedObjectInstantiation) = visitors.foreach(_.onEnter(node))
  override def onExit(node: NestedObjectInstantiation): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: SimpleObjectInstantiation) = visitors.foreach(_.onEnter(node))
  override def onExit(node: SimpleObjectInstantiation): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: ClassLiteral) = visitors.foreach(_.onEnter(node))
  override def onExit(node: ClassLiteral): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: PreDecrementation) = visitors.foreach(_.onEnter(node))
  override def onExit(node: PreDecrementation): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: PostDecrementation) = visitors.foreach(_.onEnter(node))
  override def onExit(node: PostDecrementation): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: PostIncrementation) = visitors.foreach(_.onEnter(node))
  override def onExit(node: PostIncrementation): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: PreIncrementation) = visitors.foreach(_.onEnter(node))
  override def onExit(node: PreIncrementation): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: ConstructorReference) = visitors.foreach(_.onEnter(node))
  override def onExit(node: ConstructorReference): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: NamedAnnotationParameter) = visitors.foreach(_.onEnter(node))
  override def onExit(node: NamedAnnotationParameter): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: NormalAnnotation) = visitors.foreach(_.onEnter(node))
  override def onExit(node: NormalAnnotation): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: SingleElementAnnotation) = visitors.foreach(_.onEnter(node))
  override def onExit(node: SingleElementAnnotation): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: ChildOfAny) = visitors.foreach(_.onEnter(node))
  override def onExit(node: ChildOfAny): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: ChildOfAll) = visitors.foreach(_.onEnter(node))
  override def onExit(node: ChildOfAll): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: LongPrimitive) = visitors.foreach(_.onEnter(node))
  override def onExit(node: LongPrimitive): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: IntegerPrimitive) = visitors.foreach(_.onEnter(node))
  override def onExit(node: IntegerPrimitive): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: FloatPrimitive) = visitors.foreach(_.onEnter(node))
  override def onExit(node: FloatPrimitive): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: DoublePrimitive) = visitors.foreach(_.onEnter(node))
  override def onExit(node: DoublePrimitive): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: CharPrimitive) = visitors.foreach(_.onEnter(node))
  override def onExit(node: CharPrimitive): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: BytePrimitive) = visitors.foreach(_.onEnter(node))
  override def onExit(node: BytePrimitive): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: BooleanPrimitive) = visitors.foreach(_.onEnter(node))
  override def onExit(node: BooleanPrimitive): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: ShortPrimitive) = visitors.foreach(_.onEnter(node))
  override def onExit(node: ShortPrimitive): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: VoidPrimitive) = visitors.foreach(_.onEnter(node))
  override def onExit(node: VoidPrimitive): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: ArrayType) = visitors.foreach(_.onEnter(node))
  override def onExit(node: ArrayType): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: ClassType) = visitors.foreach(_.onEnter(node))
  override def onExit(node: ClassType): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: PointerType) = visitors.foreach(_.onEnter(node))
  override def onExit(node: PointerType): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: ArrayConstructorReference) = visitors.foreach(_.onEnter(node))
  override def onExit(node: ArrayConstructorReference): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: AnyTemplate) = visitors.foreach(_.onEnter(node))
  override def onExit(node: AnyTemplate): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: ArgumentTemplate) = visitors.foreach(_.onEnter(node))
  override def onExit(node: ArgumentTemplate): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: ParameterTemplate) = visitors.foreach(_.onEnter(node))
  override def onExit(node: ParameterTemplate): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: ArrayInitializer) = visitors.foreach(_.onEnter(node))
  override def onExit(node: ArrayInitializer): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: BinaryOperations) = visitors.foreach(_.onEnter(node))
  override def onExit(node: BinaryOperations): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: Cast) = visitors.foreach(_.onEnter(node))
  override def onExit(node: Cast): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: Lambda) = visitors.foreach(_.onEnter(node))
  override def onExit(node: Lambda): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: TernaryConditional) = visitors.foreach(_.onEnter(node))
  override def onExit(node: TernaryConditional): Unit = visitors.foreach(_.onExit(node))

  override def onEnter(node: UnaryOperations): Unit = visitors.foreach(_.onEnter(node))
  override def onExit(node: UnaryOperations): Unit = visitors.foreach(_.onExit(node))
}