package jvm.parallel.ast.parsers.statement.declarator

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class InitializedVariableDeclarator(var name: Name, var initializer: Expression) extends Declarator {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    initializer.accept(visitor)

    visitor.onExit(this)
  }
}