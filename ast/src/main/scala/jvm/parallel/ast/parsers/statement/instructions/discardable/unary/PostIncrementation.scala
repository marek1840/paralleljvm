package jvm.parallel.ast.parsers.statement.instructions.discardable.unary

import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.statement.instructions.discardable.DiscardableExpression
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class PostIncrementation(var expression: Expression) extends DiscardableExpression {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    expression.accept(visitor)

    visitor.onExit(this)
  }
}
