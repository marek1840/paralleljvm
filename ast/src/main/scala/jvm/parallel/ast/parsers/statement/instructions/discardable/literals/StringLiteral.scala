package jvm.parallel.ast.parsers.statement.instructions.discardable.literals

import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class StringLiteral(var value: String) extends Literal {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.visit(this)
  }

  def withoutQuotes : String = value.drop(1).dropRight(1)
}