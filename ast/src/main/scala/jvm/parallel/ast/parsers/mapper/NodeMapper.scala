package jvm.parallel.ast.parsers.mapper

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.operators.{BinaryOperator, UnaryOperator}
import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.statement.declarator.VariableDeclarator
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.{QualifiedParentReference, QualifiedThisReference}
import jvm.parallel.ast.parsers.statement.instructions.discardable.dimension.{InitializedDimension, Dimension}
import jvm.parallel.ast.parsers.statement.instructions.discardable.literals._
import jvm.parallel.ast.parsers.statement.instructions.flow._
import jvm.parallel.ast.parsers.statement.modifers.Modifier
import jvm.parallel.ast.parsers.statement.parameter.InferredParameter

abstract class NodeMapper {
  protected[this] def mapType(t: Type): Type
  protected[this] def unsupported[A](o: Any): A = throw new Exception("unsupported node" + o)

  protected[this] def map(node: QualifiedParentReference): QualifiedParentReference =
    QualifiedParentReference(mapName(node.name))

  protected[this] def map(node: QualifiedThisReference): QualifiedThisReference =
    QualifiedThisReference(mapName(node.name))

  protected[this] def map(node: InferredParameter): InferredParameter =
    InferredParameter(mapName(node.name))

  protected[this] def map(node: TargetedBreak): TargetedBreak =
    TargetedBreak(mapName(node.name))

  protected[this] def map(node: TargetedContinue): TargetedContinue =
    TargetedContinue(mapName(node.name))

  protected[this] def map(node: VariableDeclarator): VariableDeclarator =
    VariableDeclarator(mapName(node.name))

  protected[this] def mapName(name: Name): Name = name

  protected[this] def mapLiteral(node: Literal): Literal = node match {
    case ClassLiteral(t) => ClassLiteral(mapType(t))
    case _ => node
  }

  protected[this] def mapBin(operator: BinaryOperator): BinaryOperator = operator
  protected[this] def mapUn(operator: UnaryOperator): UnaryOperator = operator
  protected[this] def mapMod(modifier: Modifier): Modifier = modifier
  protected[this] def mapModifiers(nodes: Seq[Modifier]): Seq[Modifier] = nodes.map(mapMod)
  protected[this] def mapBin(nodes: Seq[BinaryOperator]): Seq[BinaryOperator] = nodes.map(mapBin)
  protected[this] def mapUn(nodes: Seq[UnaryOperator]): Seq[UnaryOperator] = nodes.map(mapUn)
  protected[this] def mapLit(nodes: Seq[Literal]): Seq[Literal] = nodes.map(mapLiteral)

}
