package jvm.parallel.ast.parsers

import jvm.parallel.ast.{TreeTraversalException, Visitable}
import org.apache.logging.log4j.LogManager

abstract class ASTReducer[A <: Visitable[_], B] {
  val logger = LogManager.getLogger(getClass.getSimpleName)
  protected[this] type OUT = B

  def reduce(node: A): OUT = unsupportedNode(node)

  protected[this] final implicit class SeqMapper[C <: Visitable[_]](seq: Seq[C]) {
    def reduceAST(f: () => ASTReducer[C, B]): Seq[OUT] = {
      seq.map(node => f().reduce(node))
    }
  }

  protected[this] final def unsupportedNode(v: Visitable[_]): B = {
    throw new TreeTraversalException(s"Unsupported node of type: ${v.getClass.getSimpleName}")
  }
}
