package jvm.parallel.ast.parsers.statement.parameter

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class InferredParameter(var name: Name) extends Parameter {
  override def annotations: Seq[Annotation] = Nil

  override def accept(visitor: ASTVisitor): Unit = {
    visitor.visit(this)
  }
}
