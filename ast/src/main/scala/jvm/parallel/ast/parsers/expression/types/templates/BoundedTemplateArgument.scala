package jvm.parallel.ast.parsers.expression.types.templates

import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.expression.types.references.ReferenceType

trait BoundedTemplateArgument extends TemplateArgument {
  def annotations: Seq[Annotation]

  def bound: ReferenceType


}
