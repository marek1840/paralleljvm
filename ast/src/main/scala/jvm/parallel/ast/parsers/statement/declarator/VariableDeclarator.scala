package jvm.parallel.ast.parsers.statement.declarator

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class VariableDeclarator(var name: Name) extends Declarator {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.visit(this)
  }
}