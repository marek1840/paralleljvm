package jvm.parallel.ast.parsers.expression.types.references

import jvm.parallel.ast.parsers.expression.types.Type

trait ReferenceType extends Type