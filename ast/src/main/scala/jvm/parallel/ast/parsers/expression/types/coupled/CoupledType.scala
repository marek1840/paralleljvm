package jvm.parallel.ast.parsers.expression.types.coupled

import jvm.parallel.ast.parsers.expression.types.Type

trait CoupledType extends Type {
  def types: Seq[Type]

}
