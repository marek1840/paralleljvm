package jvm.parallel.ast.parsers.statement.declaration.types

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.statement.declaration.members.MemberDeclaration
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case object EmptyDeclaration extends TypeDeclaration {
  override var members: Seq[MemberDeclaration] = Vector()

  override def accept(visitor: ASTVisitor): Unit = {
    visitor.visit(this)
  }

  override def name: Name = {
    throw new UnsupportedOperationException("Empty Declaration does not have a name")
  }

  override def annotations: Seq[Annotation] =
    throw new UnsupportedOperationException("Empty Declaration does not have annotations")
}
