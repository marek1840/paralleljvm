package jvm.parallel.ast.parsers.statement.instructions.interruptable

import jvm.parallel.ast.parsers.ASTNode
import jvm.parallel.ast.parsers.statement.Block
import jvm.parallel.ast.parsers.statement.instructions.LocalVariableDeclaration
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class CatchClause(var declaration: LocalVariableDeclaration, var block: Block) extends ASTNode {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    declaration.accept(visitor)
    block.accept(visitor)

    visitor.onExit(this)
  }
}