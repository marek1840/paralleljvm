package jvm.parallel.ast.parsers.statement.instructions.flow

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class TargetedBreak(var name: Name) extends FlowStatement {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.visit(this)
  }
}
