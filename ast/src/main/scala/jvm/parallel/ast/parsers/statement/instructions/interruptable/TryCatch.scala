package jvm.parallel.ast.parsers.statement.instructions.interruptable

import jvm.parallel.ast.parsers.statement.Block
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class TryCatch(var tryBlock: Block, var catches: Seq[CatchClause]) extends TryStatement {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    tryBlock.accept(visitor)
    catches.accept(visitor)

    visitor.onExit(this)
  }
}