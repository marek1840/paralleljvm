package jvm.parallel.ast.parsers.statement.declaration.members

import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.statement.declaration.Annotated
import jvm.parallel.ast.parsers.statement.declarator.Declarator
import jvm.parallel.ast.parsers.statement.modifers.Modifier
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class FieldDeclaration(var annotations: Seq[Annotation],
                            var modifiers: Seq[Modifier],
                            var `type`: Type,
                            var declarators: Seq[Declarator]) extends MemberDeclaration with Annotated{
  override def accept(visitor: ASTVisitor) = {
    visitor.onEnter(this)

    annotations.accept(visitor)
    `type`.accept(visitor)
    declarators.accept(visitor)

    visitor.onExit(this)
  }
}
