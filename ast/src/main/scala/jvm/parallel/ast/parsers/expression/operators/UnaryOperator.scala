package jvm.parallel.ast.parsers.expression.operators

sealed trait UnaryOperator

object UnaryOperator {
  case object + extends UnaryOperator
  case object - extends UnaryOperator
  case object ~ extends UnaryOperator
  case object not extends UnaryOperator

}