package jvm.parallel.ast.parsers.expression.operators

import jvm.parallel.ast.parsers.ASTNode
import jvm.parallel.ast.parsers.visitor.ASTVisitor

sealed trait BinaryOperator extends ASTNode {
  override def accept(visitor: ASTVisitor) = {
    visitor.visit(this)
  }
}

object BinaryOperator {

  case object or extends BinaryOperator
  case object and extends BinaryOperator
  case object instanceof extends BinaryOperator
  case object + extends BinaryOperator
  case object - extends BinaryOperator
  case object * extends BinaryOperator
  case object / extends BinaryOperator
  case object ** extends BinaryOperator
  //power
  case object ^ extends BinaryOperator
  //xor
  case object & extends BinaryOperator
  case object | extends BinaryOperator
  case object << extends BinaryOperator
  case object <<< extends BinaryOperator
  case object >> extends BinaryOperator
  case object >>> extends BinaryOperator
  case object mod extends BinaryOperator
  case object div extends BinaryOperator
  case object < extends BinaryOperator
  case object <= extends BinaryOperator
  case object > extends BinaryOperator
  case object >= extends BinaryOperator
  case object == extends BinaryOperator
  case object != extends BinaryOperator
  case object in extends BinaryOperator
  case object notIn extends BinaryOperator
  case object is extends BinaryOperator
  case object isNot extends BinaryOperator

}

