package jvm.parallel.ast.parsers.statement.instructions.discardable.literals

import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class ClassLiteral(var typ: Type) extends Literal {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    typ.accept(visitor)

    visitor.onExit(this)
  }
}
