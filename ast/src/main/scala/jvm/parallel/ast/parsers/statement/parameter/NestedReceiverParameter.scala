package jvm.parallel.ast.parsers.statement.parameter

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class NestedReceiverParameter(var annotations: Seq[Annotation],
                                   var `type`: Type,
                                   var name: Name) extends Parameter {

  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    annotations.accept(visitor)
    `type`.accept(visitor)

    visitor.onExit(this)
  }
}