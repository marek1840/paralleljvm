package jvm.parallel.ast.parsers.mapper

import jvm.parallel.ast.parsers.statement._
import jvm.parallel.ast.parsers.statement.declaration.Declaration
import jvm.parallel.ast.parsers.statement.instructions._

abstract class StatementMapper extends InstructionStatementMapper {
  protected[this] def mapBlock(b: Option[Block]): Option[Block] = b.map(mapBlock)
  protected[this] def mapStatements(nodes: Seq[Statement]): Seq[Statement] = nodes.map(mapStmt)

  def mapStmt(stmt: Statement): Statement = stmt match {
    case i: InstructionStatement => mapInstruction(i)
    case d: Declaration => mapDeclaration(d)

    case b:Block => mapBlock(b)
  }

  def mapBlock(b:Block) : Block = b match {
    case Block(ss) => Block(mapStatements(ss))
  }
}
