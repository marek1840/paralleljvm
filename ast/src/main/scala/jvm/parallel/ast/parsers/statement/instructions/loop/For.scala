package jvm.parallel.ast.parsers.statement.instructions.loop

import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.statement.Statement
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class For(var initializers: Seq[Statement],
               var condition: Option[Expression],
               var update: Seq[Statement],
               var body: Statement) extends Loop {

  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    initializers.accept(visitor)

    condition.accept(visitor)
    update.accept(visitor)
    body.accept(visitor)

    visitor.onExit(this)
  }
}