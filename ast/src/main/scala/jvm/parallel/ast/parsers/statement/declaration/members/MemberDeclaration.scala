package jvm.parallel.ast.parsers.statement.declaration.members

import jvm.parallel.ast.parsers.statement.declaration.Declaration

trait MemberDeclaration extends Declaration
