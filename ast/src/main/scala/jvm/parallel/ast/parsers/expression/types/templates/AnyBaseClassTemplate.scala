package jvm.parallel.ast.parsers.expression.types.templates

import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.expression.types.references.ReferenceType
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class AnyBaseClassTemplate(var annotations: Seq[Annotation], var bound: ReferenceType) extends BoundedTemplateArgument {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    annotations.accept(visitor)
    bound.accept(visitor)

    visitor.onExit(this)
  }
}
