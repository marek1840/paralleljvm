package jvm.parallel.ast.parsers.expression.types

import jvm.parallel.ast.parsers.expression.Expression

trait Type extends Expression