package jvm.parallel.ast.parsers.statement.instructions

import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class Assertion(var condition: Expression, var detail: Option[Expression]) extends InstructionStatement {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    condition.accept(visitor)
    detail.accept(visitor)

    visitor.onExit(this)
  }
}
