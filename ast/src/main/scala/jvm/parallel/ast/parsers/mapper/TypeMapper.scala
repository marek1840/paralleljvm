package jvm.parallel.ast.parsers.mapper

import jvm.parallel.ast.parsers.expression.types.annotations._
import jvm.parallel.ast.parsers.expression.types.coupled.{ChildOfAll, ChildOfAny}
import jvm.parallel.ast.parsers.expression.types.primitives._
import jvm.parallel.ast.parsers.expression.types.references.{ArrayType, ClassType, ReferenceType}
import jvm.parallel.ast.parsers.expression.types.templates._
import jvm.parallel.ast.parsers.expression.types.{PointerType, Type}
import jvm.parallel.ast.parsers.expression.{ArrayInitializer, Expression}
import jvm.parallel.ast.parsers.statement.instructions.discardable.dimension.{AbstractDimension, InitializedDimension, Dimension}
import jvm.parallel.ast.parsers.statement.parameter._

abstract class TypeMapper extends NodeMapper {
  protected[this] def mapExpr(node: Expression): Expression
  protected[this] def mapExprs(nodes: Seq[Expression]): Seq[Expression] = nodes.map(mapExpr)
  protected[this] def mapExpr(nodes: Option[Expression]): Option[Expression] = nodes.map(mapExpr)
  protected[this] def mapTypes(nodes: Seq[Type]): Seq[Type] = nodes.map(mapType)
  protected[this] def mapTemplateArguments(nodes: Seq[TemplateArgument]): Seq[TemplateArgument] = nodes.map(mapTemplateArgument)
  protected[this] def mapTemplateParameters(nodes: Seq[TemplateParameter]): Seq[TemplateParameter] = nodes.map(mapTemplateParameter)
  protected[this] def mapAnnotations(nodes: Seq[Annotation]): Seq[Annotation] = nodes.map(mapAnnotation)
  protected[this] def mapParameters(nodes: Seq[Parameter]): Seq[Parameter] = nodes.map(mapParameter)
  protected[this] def mapClassTypes(nodes: Seq[ClassType]): Seq[ClassType] = nodes.map(mapClassType)
  protected[this] def mapClassType(nodes: Option[ClassType]): Option[ClassType] = nodes.map(mapClassType)
  protected[this] def mapAps(nodes: Seq[NamedAnnotationParameter]): Seq[NamedAnnotationParameter] = nodes.map(mapAnnotationParam)
  protected[this] def mapDim(nodes: Seq[Dimension]): Seq[Dimension] = nodes.map(mapDim)

  protected[this] def mapDim(dim: Dimension): Dimension = dim match {
    case InitializedDimension(as, expr) => InitializedDimension(mapAnnotations(as), mapExpr(expr))
    case AbstractDimension(as) => AbstractDimension(mapAnnotations(as))
  }

  def mapArrayType(node: ArrayType): ArrayType = node match {
    case ArrayType(t, ds) => ArrayType(mapType(t), mapDim(ds))
  }

  def mapClassType(node: ClassType): ClassType = node match {
    case ClassType(as, p, n, ts) => ClassType(mapAnnotations(as), mapClassType(p), mapName(n), mapTemplateArguments(ts))
  }

  def mapArrayInitializer(node: ArrayInitializer): ArrayInitializer = node match {
    case ArrayInitializer(vs) => ArrayInitializer(mapExprs(vs))
  }

  def mapAnnotationParam(node: NamedAnnotationParameter): NamedAnnotationParameter = node match {
    case NamedAnnotationParameter(n, v) => NamedAnnotationParameter(mapName(n), mapExpr(v))
  }

  def mapTemplateParameter(node: TemplateParameter): TemplateParameter = node match {
    case ParameterTemplate(as, n) => ParameterTemplate(mapAnnotations(as), mapName(n))
    case BoundedParameterTemplate(as, n, t) => BoundedParameterTemplate(mapAnnotations(as), mapName(n), mapType(t))
  }

  def mapTemplateArgument(node: TemplateArgument): TemplateArgument = node match {
    case AnyTemplate(as) => AnyTemplate(mapAnnotations(as))
    case ArgumentTemplate(t) => ArgumentTemplate(mapType(t))
    case AnyBaseClassTemplate(as, b) => AnyBaseClassTemplate(mapAnnotations(as), mapReferenceType(b))
    case AnySubClassTemplate(as, b) => AnySubClassTemplate(mapAnnotations(as), mapReferenceType(b))
  }

  def mapParameter(node: Parameter): Parameter = node match {
    case NestedReceiverParameter(as, t, n) => NestedReceiverParameter(mapAnnotations(as), mapType(t), mapName(n))
    case FormalParameter(as, ms, t, n) => FormalParameter(mapAnnotations(as), mapModifiers(ms), mapType(t), mapName(n))
    case VariableArityParameter(as, ms, t, n) => VariableArityParameter(mapAnnotations(as), mapModifiers(ms), mapType(t), mapName(n))
    case InstanceReceiverParameter(as, t) => InstanceReceiverParameter(mapAnnotations(as), mapType(t))
    case InferredParameter(n) => InferredParameter(mapName(n))

  }

  def mapAnnotation(node: Annotation): Annotation = node match {
    case MarkerAnnotation(n) => MarkerAnnotation(mapName(n))
    case NormalAnnotation(n, es) => NormalAnnotation(mapName(n), mapAps(es))
    case SingleElementAnnotation(n, v) => SingleElementAnnotation(mapName(n), mapExpr(v))
  }

  def mapReferenceType(node: ReferenceType): ReferenceType = node match {
    case a: ArrayType => mapArrayType(a)
    case c: ClassType => mapClassType(c)
  }

  def mapType(node: Type): Type = node match {
    case a: Annotation => mapAnnotation(a)
    case r: ReferenceType => mapReferenceType(r)
    case tp: TemplateParameter => mapTemplateParameter(tp)

    case PointerType(t) => PointerType(mapType(t))

    case LongPrimitive(as) => LongPrimitive(mapAnnotations(as))
    case IntegerPrimitive(as) => IntegerPrimitive(mapAnnotations(as))
    case FloatPrimitive(as) => FloatPrimitive(mapAnnotations(as))
    case DoublePrimitive(as) => DoublePrimitive(mapAnnotations(as))
    case CharPrimitive(as) => CharPrimitive(mapAnnotations(as))
    case BytePrimitive(as) => BytePrimitive(mapAnnotations(as))
    case BooleanPrimitive(as) => BooleanPrimitive(mapAnnotations(as))
    case ShortPrimitive(as) => ShortPrimitive(mapAnnotations(as))
    case VoidPrimitive(as) => VoidPrimitive(mapAnnotations(as))

    case ChildOfAny(ts) => ChildOfAny(mapTypes(ts))
    case ChildOfAll(ts) => ChildOfAll(mapTypes(ts))
  }
}
