package jvm.parallel.ast.parsers.statement

import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class Block(var statements: Seq[Statement]) extends Statement {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    statements.accept(visitor)

    visitor.onExit(this)
  }
}
