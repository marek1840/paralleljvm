package jvm.parallel.ast.parsers.expression.types.annotations

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.ASTNode
import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class NamedAnnotationParameter(var name: Name, var value: Expression) extends ASTNode {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    value.accept(visitor)

    visitor.onExit(this)
  }
}