package jvm.parallel.ast.parsers.expression.types.templates

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class ParameterTemplate(var annotations: Seq[Annotation], var name: Name) extends TemplateParameter {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    annotations.accept(visitor)

    visitor.onExit(this)
  }
}
