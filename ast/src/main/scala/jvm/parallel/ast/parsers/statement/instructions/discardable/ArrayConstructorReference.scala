package jvm.parallel.ast.parsers.statement.instructions.discardable

import jvm.parallel.ast.parsers.expression.types.references.ArrayType
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class ArrayConstructorReference(var `type`: ArrayType) extends DiscardableExpression {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    `type`.accept(visitor)

    visitor.onExit(this)
  }
}

