package jvm.parallel.ast.parsers.statement.instructions.discardable.dimension

import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class AbstractDimension(var annotations: Seq[Annotation]) extends Dimension {

  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    annotations.accept(visitor)

    visitor.onExit(this)
  }
}

