package jvm.parallel.ast.parsers.statement.instructions.switch

import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class EmptySwitchCases(var cases: Seq[Expression]) extends SwitchRule {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    cases.accept(visitor)

    visitor.onExit(this)
  }
}