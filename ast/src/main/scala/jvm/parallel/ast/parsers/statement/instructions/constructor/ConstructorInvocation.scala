package jvm.parallel.ast.parsers.statement.instructions.constructor

import jvm.parallel.ast.parsers.statement.instructions.InstructionStatement

trait ConstructorInvocation extends InstructionStatement