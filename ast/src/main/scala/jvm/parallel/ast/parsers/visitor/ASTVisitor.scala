package jvm.parallel.ast.parsers.visitor

import jvm.parallel.ast.Visitor
import jvm.parallel.ast.parsers.ASTNode
import jvm.parallel.ast.parsers.statement.instructions.discardable.reference.LocalVariableReference
import jvm.parallel.ast.parsers.statement.instructions.flow.Halt

trait ASTVisitor extends Visitor with NodeVisitor with StatementVisitor with ExpressionVisitor{

  protected[this] final def unsupported(a:Any) = throw new Exception("Unsopported node of type: " + a.getClass.getSimpleName)
}