package jvm.parallel.ast.parsers.expression.types.annotations

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class SingleElementAnnotation(var name: Name, var value: Expression) extends Annotation {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    value.accept(visitor)

    visitor.onExit(this)
  }
}
