package jvm.parallel.ast.parsers.expression

import jvm.parallel.ast.parsers.expression.operators.BinaryOperator
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class BinaryOperations(var operators: Seq[BinaryOperator], var operands: Seq[Expression]) extends Expression {
  require(operators.size == operands.size - 1)
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    operands.accept(visitor)

    visitor.onExit(this)
  }
}
