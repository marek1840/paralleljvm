package jvm.parallel.ast.parsers.statement.instructions

import jvm.parallel.ast.parsers.visitor.ASTVisitor

case object EmptyStatement extends InstructionStatement {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.visit(this)
  }
}
