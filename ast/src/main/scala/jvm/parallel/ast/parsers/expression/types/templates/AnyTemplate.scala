package jvm.parallel.ast.parsers.expression.types.templates

import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class AnyTemplate(var annotations: Seq[Annotation]) extends TemplateArgument {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    annotations.accept(visitor)

    visitor.onExit(this)
  }
}
