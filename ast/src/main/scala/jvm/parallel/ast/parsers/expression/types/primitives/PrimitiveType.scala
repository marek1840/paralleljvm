package jvm.parallel.ast.parsers.expression.types.primitives

import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation

trait PrimitiveType extends Type {
  def annotations: Seq[Annotation]
}

object PrimitiveType{
  val Integer = IntegerPrimitive(Nil)
  val Float = FloatPrimitive(Nil)
}