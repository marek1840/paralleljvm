package jvm.parallel.ast.parsers.statement.instructions.switch

import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case object DefaultSwitch extends Expression {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.visit(this)
  }
}