package jvm.parallel.ast.parsers.statement.instructions.constructor

import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.expression.types.templates.TemplateArgument
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class IndirectParentConstructorInvocation(var qualifier: Expression,
                                               var types: Seq[TemplateArgument],
                                               var arguments: Seq[Expression]) extends ConstructorInvocation {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    qualifier.accept(visitor)
    types.accept(visitor)
    arguments.accept(visitor)

    visitor.onExit(this)
  }
}

