package jvm.parallel.ast.parsers.statement.declaration.initializers

import jvm.parallel.ast.parsers.statement.Block
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class InstanceInitializerDeclaration(var block: Block) extends InitializerDeclaration {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    block.accept(visitor)

    visitor.onExit(this)
  }
}
