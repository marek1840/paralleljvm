package jvm.parallel.ast.parsers.statement.instructions.interruptable

import jvm.parallel.ast.parsers.statement.Statement
import jvm.parallel.ast.parsers.statement.instructions.InstructionStatement

trait TryStatement extends InstructionStatement