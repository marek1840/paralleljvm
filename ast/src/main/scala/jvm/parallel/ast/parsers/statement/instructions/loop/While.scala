package jvm.parallel.ast.parsers.statement.instructions.loop

import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.statement.Statement
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class While(var condition: Expression, var body: Statement) extends Loop {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    condition.accept(visitor)
    body.accept(visitor)

    visitor.onExit(this)
  }
}