package jvm.parallel.ast.parsers.expression.types.annotations

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class NormalAnnotation(var name: Name, var elements: Seq[NamedAnnotationParameter]) extends Annotation {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    elements.accept(visitor)

    visitor.onExit(this)
  }
}
