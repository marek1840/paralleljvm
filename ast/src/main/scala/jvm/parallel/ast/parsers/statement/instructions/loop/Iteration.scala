package jvm.parallel.ast.parsers.statement.instructions.loop

import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.statement.Statement
import jvm.parallel.ast.parsers.statement.instructions.LocalVariableDeclaration
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class Iteration(var binding: LocalVariableDeclaration,
                     var source: Expression,
                     var body: Statement) extends Loop {

  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    binding.accept(visitor)
    source.accept(visitor)
    body.accept(visitor)
    visitor.onExit(this)
  }
}