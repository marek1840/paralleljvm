package jvm.parallel.ast.parsers.statement.declaration.members

import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.expression.types.references.ClassType
import jvm.parallel.ast.parsers.expression.types.templates.TemplateParameter
import jvm.parallel.ast.parsers.statement.Block
import jvm.parallel.ast.parsers.statement.declaration.Annotated
import jvm.parallel.ast.parsers.statement.declarator.ConstructorDeclarator
import jvm.parallel.ast.parsers.statement.modifers.Modifier
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class ConstructorDeclaration(var annotations: Seq[Annotation],
                                  var modifiers: Seq[Modifier],
                                  var types: Seq[TemplateParameter],
                                  var declarator: ConstructorDeclarator,
                                  var thrown: Seq[ClassType],
                                  var body: Block) extends MemberDeclaration with Annotated{
  override def accept(visitor: ASTVisitor) = {
    visitor.onEnter(this)

    annotations.accept(visitor)
    types.accept(visitor)
    declarator.accept(visitor)
    thrown.accept(visitor)
    body.accept(visitor)

    visitor.onExit(this)
  }
}
