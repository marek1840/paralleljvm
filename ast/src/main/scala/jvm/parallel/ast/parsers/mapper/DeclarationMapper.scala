package jvm.parallel.ast.parsers.mapper

import jvm.parallel.ast.parsers.statement.Block
import jvm.parallel.ast.parsers.statement.declaration.imports._
import jvm.parallel.ast.parsers.statement.declaration.initializers.{InitializerDeclaration, InstanceInitializerDeclaration, StaticInitializerDeclaration}
import jvm.parallel.ast.parsers.statement.declaration.members._
import jvm.parallel.ast.parsers.statement.declaration.packages.{NamedPackageDeclaration, PackageDeclaration}
import jvm.parallel.ast.parsers.statement.declaration.types._
import jvm.parallel.ast.parsers.statement.declaration.{CompilationUnitDeclaration, Declaration}
import jvm.parallel.ast.parsers.statement.instructions.LocalVariableDeclaration

abstract class DeclarationMapper extends DeclaratorMapper {
  def mapBlock(node: Block): Block
  def mapMemberDeclarations(nodes: Seq[MemberDeclaration]): Seq[MemberDeclaration] = nodes.map(mapMemberDeclaration)
  def MapDeclarations(nodes: Seq[Declaration]): Seq[Declaration] = nodes.map(mapDeclaration)
  def mapTypeDeclarations(nodes: Seq[TypeDeclaration]): Seq[TypeDeclaration] = nodes.map(mapTypeDeclaration)
  def mapImports(nodes: Seq[ImportDeclaration]): Seq[ImportDeclaration] = nodes.map(mapImport)
  def mapLocalVarDeclarations(nodes: Seq[LocalVariableDeclaration]): Seq[LocalVariableDeclaration] = nodes.map(mapLocalDeclaration)

  def mapMethod(m:MethodDeclaration) : MethodDeclaration = m match {
    case MethodDeclaration(as, ms, ts, t, d, ths, b) =>
      MethodDeclaration(mapAnnotations(as), mapModifiers(ms), mapTemplateParameters(ts), mapType(t),
        mapFunctionDeclarator(d), mapClassTypes(ths), mapStmt(b))
  }

  def mapLocalDeclaration(l: LocalVariableDeclaration): LocalVariableDeclaration = l match {
    case LocalVariableDeclaration(as, ms, t, ds) =>
      LocalVariableDeclaration(mapAnnotations(as), mapModifiers(ms), mapType(t), mapDeclarators(ds))
  }

  def mapPackage(p: PackageDeclaration): PackageDeclaration = p match {
    case NamedPackageDeclaration(as, n) => NamedPackageDeclaration(mapAnnotations(as), mapName(n))
    case _ => p
  }

  def mapInitializer(i: InitializerDeclaration): InitializerDeclaration = i match {
    case InstanceInitializerDeclaration(b) => InstanceInitializerDeclaration(mapBlock(b))
    case StaticInitializerDeclaration(b) => StaticInitializerDeclaration(mapBlock(b))
  }

  def mapImport(i: ImportDeclaration): ImportDeclaration = i match {
    case LazyImportDeclaration(name) => LazyImportDeclaration(mapName(name))
    case SingleImportDeclaration(name) => SingleImportDeclaration(mapName(name))
    case StaticImportDeclaration(name) => StaticImportDeclaration(mapName(name))
    case StaticLazyImportDeclaration(name) => StaticLazyImportDeclaration(mapName(name))
  }

  def mapTypeDeclaration(td: TypeDeclaration): TypeDeclaration = td match {
    case EnumDeclaration(as, ms, n, is, mbs) =>
      EnumDeclaration(mapAnnotations(as), mapModifiers(ms), mapName(n), mapClassTypes(is), mapMemberDeclarations(mbs))

    case InterfaceDeclaration(as, ms, n, ts, is, mbs) =>
      InterfaceDeclaration(mapAnnotations(as), mapModifiers(ms), mapName(n),
        mapTemplateParameters(ts), mapClassTypes(is), mapMemberDeclarations(mbs))

    case ClassDeclaration(as, ms, n, ts, cs, is, mbs) =>
      ClassDeclaration(mapAnnotations(as), mapModifiers(ms), mapName(n), mapTemplateParameters(ts),
        mapClassType(cs), mapClassTypes(is), mapMemberDeclarations(mbs))

    case AnnotationDeclaration(as, ms, n, mbs) =>
      AnnotationDeclaration(mapAnnotations(as), mapModifiers(ms), mapName(n), mapMemberDeclarations(mbs))

    case _ => td
  }

  def mapMemberDeclaration(node: MemberDeclaration): MemberDeclaration = node match {
    case t: TypeDeclaration => mapTypeDeclaration(t)
    case i: InitializerDeclaration => mapInitializer(i)
    case m:MethodDeclaration => mapMethod(m)

    case AnnotationDefaultElementDeclaration(as, ms, t, n, e) =>
      AnnotationDefaultElementDeclaration(mapAnnotations(as), mapModifiers(ms), mapType(t), mapName(n), mapExpr(e))

    case AnnotationElementDeclaration(as, ms, t, n) =>
      AnnotationElementDeclaration(mapAnnotations(as), mapModifiers(ms), mapType(t), mapName(n))

    case AnonymousEnumConstantDeclaration(as, n, cs, ms) =>
      AnonymousEnumConstantDeclaration(mapAnnotations(as), mapName(n), mapExprs(cs), mapMemberDeclarations(ms))

    case ConstructorDeclaration(as, ms, ts, d, ths, b) =>
      ConstructorDeclaration(mapAnnotations(as), mapModifiers(ms), mapTemplateParameters(ts),
        mapConstructorDeclarator(d), mapClassTypes(ths), mapBlock(b))

    case EnumConstantDeclaration(as, n, args) =>
      EnumConstantDeclaration(mapAnnotations(as), mapName(n), mapExprs(args))

    case FieldDeclaration(as, ms, t, ds) =>
      FieldDeclaration(mapAnnotations(as), mapModifiers(ms), mapType(t), mapDeclarators(ds))
  }

   def mapDeclaration(d: Declaration): Declaration = d match {
    case m: MemberDeclaration => mapMemberDeclaration(m)
    case i: ImportDeclaration => mapImport(i)
    case p: PackageDeclaration => mapPackage(p)

    case l: LocalVariableDeclaration => mapLocalDeclaration(l)
    case c: CompilationUnitDeclaration => mapCompilationUnit(c)
  }

  def mapCompilationUnit(c: CompilationUnitDeclaration): CompilationUnitDeclaration = c match {
    case CompilationUnitDeclaration(om, is, ts) =>
      CompilationUnitDeclaration(mapPackage(om), mapImports(is), mapTypeDeclarations(ts))
  }
}
