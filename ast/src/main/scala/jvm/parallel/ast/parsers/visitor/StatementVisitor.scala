package jvm.parallel.ast.parsers.visitor

import jvm.parallel.ast.parsers.expression.types.templates.{AnyBaseClassTemplate, AnySubClassTemplate, BoundedParameterTemplate}
import jvm.parallel.ast.parsers.statement.Block
import jvm.parallel.ast.parsers.statement.instructions.conditional.{If, IfThenElse}
import jvm.parallel.ast.parsers.statement.instructions.constructor.{AlternateConstructorInvocation, IndirectParentConstructorInvocation, IntermediateConstructorInvocation, ParentConstructorInvocation}
import jvm.parallel.ast.parsers.statement.declaration.initializers.{InstanceInitializerDeclaration, StaticInitializerDeclaration}
import jvm.parallel.ast.parsers.statement.declaration.members._
import jvm.parallel.ast.parsers.statement.declaration.packages.NamedPackageDeclaration
import jvm.parallel.ast.parsers.statement.declaration.types.{AnnotationDeclaration, ClassDeclaration, EnumDeclaration, InterfaceDeclaration}
import jvm.parallel.ast.parsers.statement.declaration.CompilationUnitDeclaration
import jvm.parallel.ast.parsers.statement.declarator._
import jvm.parallel.ast.parsers.statement.instructions.flow.{ImplicitReturn, Return, Throw, Yield}
import jvm.parallel.ast.parsers.statement.instructions._
import jvm.parallel.ast.parsers.statement.instructions.interruptable._
import jvm.parallel.ast.parsers.statement.instructions.loop.{DoWhile, For, Iteration, While}
import jvm.parallel.ast.parsers.statement.parameter.{FormalParameter, InstanceReceiverParameter, NestedReceiverParameter, VariableArityParameter}
import jvm.parallel.ast.parsers.statement.instructions.switch.{EmptySwitchCases, SwitchCases, SwitchStatement}

trait StatementVisitor {
  def onEnter(thenElse: IfThenElse): Unit = {}
  def onExit(thenElse: IfThenElse): Unit = {}

  def onEnter(value: If): Unit = {}
  def onExit(value: If): Unit = {}

  def onEnter(invocation: AlternateConstructorInvocation): Unit = {}
  def onExit(invocation: AlternateConstructorInvocation): Unit = {}

  def onEnter(invocation: IndirectParentConstructorInvocation): Unit = {}
  def onExit(invocation: IndirectParentConstructorInvocation): Unit = {}

  def onEnter(invocation: IntermediateConstructorInvocation): Unit = {}
  def onExit(invocation: IntermediateConstructorInvocation): Unit = {}

  def onEnter(invocation: ParentConstructorInvocation): Unit = {}
  def onExit(invocation: ParentConstructorInvocation): Unit = {}

  def onEnter(declaration: InstanceInitializerDeclaration): Unit = {}
  def onExit(declaration: InstanceInitializerDeclaration): Unit = {}

  def onEnter(declaration: StaticInitializerDeclaration): Unit = {}
  def onExit(declaration: StaticInitializerDeclaration): Unit = {}

  def onEnter(declaration: AnnotationDefaultElementDeclaration): Unit = {}
  def onExit(declaration: AnnotationDefaultElementDeclaration): Unit = {}

  def onEnter(declaration: AnnotationElementDeclaration): Unit = {}
  def onExit(declaration: AnnotationElementDeclaration): Unit = {}

  def onEnter(declaration: AnonymousEnumConstantDeclaration): Unit = {}
  def onExit(declaration: AnonymousEnumConstantDeclaration): Unit = {}

  def onEnter(declaration: ConstructorDeclaration): Unit = {}
  def onExit(declaration: ConstructorDeclaration): Unit = {}

  def onEnter(declaration: EnumConstantDeclaration): Unit = {}
  def onExit(declaration: EnumConstantDeclaration): Unit = {}

  def onEnter(declaration: FieldDeclaration): Unit = {}
  def onExit(declaration: FieldDeclaration): Unit = {}

  def onEnter(declaration: MethodDeclaration): Unit = {}
  def onExit(declaration: MethodDeclaration): Unit = {}

  def onEnter(declaration: NamedPackageDeclaration): Unit = {}
  def onExit(declaration: NamedPackageDeclaration): Unit = {}

  def onEnter(declaration: EnumDeclaration): Unit = {}
  def onExit(declaration: EnumDeclaration): Unit = {}

  def onEnter(declaration: InterfaceDeclaration): Unit = {}
  def onExit(declaration: InterfaceDeclaration): Unit = {}

  def onEnter(declaration: ClassDeclaration): Unit = {}
  def onExit(declaration: ClassDeclaration): Unit = {}

  def onEnter(declaration: AnnotationDeclaration): Unit = {}
  def onExit(declaration: AnnotationDeclaration): Unit = {}

  def onEnter(declaration: CompilationUnitDeclaration): Unit = {}
  def onExit(declaration: CompilationUnitDeclaration): Unit = {}

  def onEnter(declaration: LocalVariableDeclaration): Unit = {}
  def onExit(declaration: LocalVariableDeclaration): Unit = {}

  def onEnter(declarator: ArrayDeclarator): Unit = {}
  def onExit(declarator: ArrayDeclarator): Unit = {}

  def onEnter(declarator: ArrayMethodDeclarator): Unit = {}
  def onExit(declarator: ArrayMethodDeclarator): Unit = {}

  def onEnter(declarator: ConstructorDeclarator): Unit = {}
  def onExit(declarator: ConstructorDeclarator): Unit = {}

  def onEnter(declarator: InitializedArrayDeclarator): Unit = {}
  def onExit(declarator: InitializedArrayDeclarator): Unit = {}

  def onEnter(declarator: InitializedVariableDeclarator): Unit = {}
  def onExit(declarator: InitializedVariableDeclarator): Unit = {}

  def onEnter(declarator: MethodDeclarator): Unit = {}
  def onExit(declarator: MethodDeclarator): Unit = {}

  def onEnter(implicitReturn: ImplicitReturn): Unit = {}
  def onExit(implicitReturn: ImplicitReturn): Unit = {}

  def onEnter(value: Return): Unit = {}
  def onExit(value: Return): Unit = {}

  def onEnter(value: Throw): Unit = {}
  def onExit(value: Throw): Unit = {}

  def onEnter(value: Yield): Unit = {}
  def onExit(value: Yield): Unit = {}

  def onEnter(clause: CatchClause): Unit = {}
  def onExit(clause: CatchClause): Unit = {}

  def onEnter(tryCatch: TryCatch): Unit = {}
  def onExit(tryCatch: TryCatch): Unit = {}

  def onEnter(catchFinally: TryCatchFinally): Unit = {}
  def onExit(catchFinally: TryCatchFinally): Unit = {}

  def onEnter(tryFinally: TryFinally): Unit = {}
  def onExit(tryFinally: TryFinally): Unit = {}

  def onEnter(resources: TryWithResources): Unit = {}
  def onExit(resources: TryWithResources): Unit = {}

  def onEnter(doWhile: DoWhile): Unit = {}
  def onExit(doWhile: DoWhile): Unit = {}

  def onEnter(value: For): Unit = {}
  def onExit(value: For): Unit = {}

  def onEnter(iteration: Iteration): Unit = {}
  def onExit(iteration: Iteration): Unit = {}

  def onEnter(value: While): Unit = {}
  def onExit(value: While): Unit = {}

  def onEnter(parameter: FormalParameter): Unit = {}
  def onExit(parameter: FormalParameter): Unit = {}

  def onEnter(parameter: InstanceReceiverParameter): Unit = {}
  def onExit(parameter: InstanceReceiverParameter): Unit = {}

  def onEnter(parameter: NestedReceiverParameter): Unit = {}
  def onExit(parameter: NestedReceiverParameter): Unit = {}

  def onEnter(parameter: VariableArityParameter): Unit = {}
  def onExit(parameter: VariableArityParameter): Unit = {}

  def onEnter(template: AnyBaseClassTemplate): Unit = {}
  def onExit(template: AnyBaseClassTemplate): Unit = {}

  def onEnter(template: AnySubClassTemplate): Unit = {}
  def onExit(template: AnySubClassTemplate): Unit = {}

  def onEnter(cases: EmptySwitchCases): Unit = {}
  def onExit(cases: EmptySwitchCases): Unit = {}

  def onEnter(cases: SwitchCases): Unit = {}
  def onExit(cases: SwitchCases): Unit = {}

  def onEnter(statement: SwitchStatement): Unit = {}
  def onExit(statement: SwitchStatement): Unit = {}

  def onEnter(assertion: Assertion): Unit = {}
  def onExit(assertion: Assertion): Unit = {}

  def onEnter(statement: LabeledStatement): Unit = {}
  def onExit(statement: LabeledStatement): Unit = {}

  def onEnter(block: SynchronizedBlock): Unit = {}
  def onExit(block: SynchronizedBlock): Unit = {}

  def onEnter(block: Block): Unit = {}
  def onExit(block: Block): Unit = {}

  def onEnter(template: BoundedParameterTemplate): Unit = {}
  def onExit(template: BoundedParameterTemplate): Unit = {}

}
