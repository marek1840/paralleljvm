package jvm.parallel.ast.parsers.statement.declarator

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.instructions.discardable.dimension.Dimension
import jvm.parallel.ast.parsers.statement.parameter.Parameter
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class ArrayMethodDeclarator(var name: Name,
                                 var parameters: Seq[Parameter],
                                 var dimensions: Seq[Dimension]) extends FunctionDeclarator {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    parameters.accept(visitor)
    dimensions.accept(visitor)

    visitor.onExit(this)
  }
}