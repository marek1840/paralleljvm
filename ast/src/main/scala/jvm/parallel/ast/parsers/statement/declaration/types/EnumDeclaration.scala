package jvm.parallel.ast.parsers.statement.declaration.types

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.expression.types.references.ClassType
import jvm.parallel.ast.parsers.statement.declaration.members.MemberDeclaration
import jvm.parallel.ast.parsers.statement.modifers.Modifier
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class EnumDeclaration(var annotations: Seq[Annotation],
                           var modifiers: Seq[Modifier],
                           var name: Name,
                           var interfaces: Seq[ClassType],
                           var members: Seq[MemberDeclaration]) extends TypeDeclaration {

  override def accept(visitor: ASTVisitor) = {
    visitor.onEnter(this)

    annotations.accept(visitor)
    interfaces.accept(visitor)
    members.accept(visitor)

    visitor.onExit(this)
  }
}
