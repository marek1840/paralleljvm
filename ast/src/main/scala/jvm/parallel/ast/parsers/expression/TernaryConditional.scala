package jvm.parallel.ast.parsers.expression

import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class TernaryConditional(var condition: Expression,
                              var onTrue: Expression,
                              var onFalse: Expression) extends Expression {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    condition.accept(visitor)
    onTrue.accept(visitor)
    onFalse.accept(visitor)

    visitor.onExit(this)
  }
}
