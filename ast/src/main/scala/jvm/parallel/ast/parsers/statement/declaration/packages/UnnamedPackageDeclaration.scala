package jvm.parallel.ast.parsers.statement.declaration.packages

import jvm.parallel.ast.parsers.visitor.ASTVisitor

case object UnnamedPackageDeclaration extends PackageDeclaration {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.visit(this)
  }
}
