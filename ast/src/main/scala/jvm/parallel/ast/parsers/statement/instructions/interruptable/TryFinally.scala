package jvm.parallel.ast.parsers.statement.instructions.interruptable

import jvm.parallel.ast.parsers.statement.Block
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class TryFinally(var tryBlock: Block, var finallyBlock: Block) extends TryStatement {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    tryBlock.accept(visitor)
    finallyBlock.accept(visitor)

    visitor.onExit(this)
  }
}