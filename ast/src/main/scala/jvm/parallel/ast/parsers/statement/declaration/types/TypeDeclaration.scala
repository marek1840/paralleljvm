package jvm.parallel.ast.parsers.statement.declaration.types

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.declaration.Annotated
import jvm.parallel.ast.parsers.statement.declaration.members.MemberDeclaration

trait TypeDeclaration extends MemberDeclaration with Annotated{
  var members: Seq[MemberDeclaration]
  def name:Name
}