package jvm.parallel.ast.parsers.statement.instructions

import jvm.parallel.ast.parsers.ASTNode
import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.statement.declaration.{Annotated, Declaration}
import jvm.parallel.ast.parsers.statement.declarator.Declarator
import jvm.parallel.ast.parsers.statement.modifers.Modifier
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class LocalVariableDeclaration(var annotations: Seq[Annotation],
                                    var modifiers: Seq[Modifier],
                                    var typ: Type,
                                    var declarators: Seq[Declarator]) extends Declaration with Annotated with InstructionStatement{
  override def accept(visitor: ASTVisitor) = {
    visitor.onEnter(this)

    annotations.accept(visitor)
    typ.accept(visitor)
    declarators.accept(visitor)

    visitor.onExit(this)
  }

  override def equals(o: scala.Any): Boolean = {
    import ASTNode._
    o match {
      case LocalVariableDeclaration(as, mo, t, ds) =>
        equalSeq(annotations, as) && equalSeq(modifiers, mo) &&
          equal(typ, t) && equalSeq(declarators, ds)

      case _ => false
    }
  }
}