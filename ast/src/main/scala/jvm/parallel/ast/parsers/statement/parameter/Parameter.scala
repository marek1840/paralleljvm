package jvm.parallel.ast.parsers.statement.parameter

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.ASTNode
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation

trait Parameter extends ASTNode{
  def annotations: Seq[Annotation]
  def name:Name
}