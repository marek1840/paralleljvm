package jvm.parallel.ast.parsers.statement.instructions.discardable.binary

import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.statement.instructions.discardable.DiscardableExpression
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class Extraction(var array: Expression, var key: Expression) extends DiscardableExpression {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    array.accept(visitor)
    key.accept(visitor)

    visitor.onExit(this)
  }
}

