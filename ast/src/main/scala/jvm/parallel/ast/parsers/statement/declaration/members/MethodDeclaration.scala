package jvm.parallel.ast.parsers.statement.declaration.members

import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.expression.types.references.ClassType
import jvm.parallel.ast.parsers.expression.types.templates.TemplateParameter
import jvm.parallel.ast.parsers.statement.Statement
import jvm.parallel.ast.parsers.statement.declaration.Annotated
import jvm.parallel.ast.parsers.statement.declarator.FunctionDeclarator
import jvm.parallel.ast.parsers.statement.modifers.Modifier
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class MethodDeclaration(var annotations: Seq[Annotation],
                             var modifiers: Seq[Modifier],
                             var types: Seq[TemplateParameter],
                             var resultType: Type,
                             var declarator: FunctionDeclarator,
                             var thrown: Seq[ClassType],
                             var body: Statement) extends MemberDeclaration with Annotated{
  override def accept(visitor: ASTVisitor) = {
    visitor.onEnter(this)

    annotations.accept(visitor)
    types.accept(visitor)
    resultType.accept(visitor)
    declarator.accept(visitor)
    thrown.accept(visitor)
    body.accept(visitor)

    visitor.onExit(this)
  }
}
