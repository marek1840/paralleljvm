package jvm.parallel.ast.parsers.statement.instructions.discardable.binary.assignment

import jvm.parallel.ast.parsers.statement.instructions.discardable.DiscardableExpression

trait Assignment extends DiscardableExpression
