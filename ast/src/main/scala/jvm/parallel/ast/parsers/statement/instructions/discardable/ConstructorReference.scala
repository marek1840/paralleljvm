package jvm.parallel.ast.parsers.statement.instructions.discardable

import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.expression.types.templates.TemplateArgument
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class ConstructorReference(var `type`: Type, var types: Seq[TemplateArgument]) extends DiscardableExpression {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    `type`.accept(visitor)
    types.accept(visitor)

    visitor.onExit(this)
  }
}
