package jvm.parallel.ast.parsers.statement.declaration.initializers

import jvm.parallel.ast.parsers.statement.Block
import jvm.parallel.ast.parsers.statement.declaration.members.MemberDeclaration
import jvm.parallel.ast.parsers.statement.instructions.InstructionStatement

trait InitializerDeclaration extends MemberDeclaration{
  def block: Block
}


