package jvm.parallel.ast.parsers.statement.instructions.flow

import jvm.parallel.ast.parsers.visitor.ASTVisitor

case object Continue extends FlowStatement {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.visit(this)
  }
}
