package jvm.parallel.ast.parsers.expression.types.annotations

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class MarkerAnnotation(var name: Name) extends Annotation {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.visit(this)
  }
}
