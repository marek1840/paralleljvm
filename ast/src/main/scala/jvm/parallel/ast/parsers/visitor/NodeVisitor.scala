package jvm.parallel.ast.parsers.visitor

import jvm.parallel.ast.parsers.statement.instructions.EmptyStatement
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.{QualifiedParentReference, QualifiedThisReference}
import jvm.parallel.ast.parsers.statement.instructions.discardable.literals._
import jvm.parallel.ast.parsers.statement.instructions.discardable.reference.{LocalVariableReference, ParentReference, Select, ThisReference}
import jvm.parallel.ast.parsers.expression.operators.BinaryOperator
import jvm.parallel.ast.parsers.expression.types.annotations.MarkerAnnotation
import jvm.parallel.ast.parsers.statement.declaration.imports.{LazyImportDeclaration, SingleImportDeclaration, StaticImportDeclaration, StaticLazyImportDeclaration}
import jvm.parallel.ast.parsers.statement.declaration.packages.UnnamedPackageDeclaration
import jvm.parallel.ast.parsers.statement.declaration.types.EmptyDeclaration
import jvm.parallel.ast.parsers.statement.declarator.VariableDeclarator
import jvm.parallel.ast.parsers.statement.instructions.flow._
import jvm.parallel.ast.parsers.statement.modifers.Modifier
import jvm.parallel.ast.parsers.statement.parameter.InferredParameter
import jvm.parallel.ast.parsers.statement.instructions.switch.DefaultSwitch


trait NodeVisitor {
  def visit(halt: Halt.type): Unit = {}

  def visit(literal: BooleanLiteral): Unit = {}
  def visit(literal: ByteLiteral): Unit = {}
  def visit(literal: CharLiteral): Unit = {}
  def visit(literal: DoubleLiteral): Unit = {}
  def visit(literal: FloatLiteral): Unit = {}
  def visit(literal: IntegerLiteral): Unit = {}
  def visit(literal: LongLiteral): Unit = {}
  def visit(literal: ShortLiteral): Unit = {}
  def visit(literal: StringLiteral): Unit = {}
  def visit(literal: NullLiteral.type): Unit = {}

  def visit(literal: VoidLiteral): Unit = {}
  def visit(annotation: MarkerAnnotation): Unit = {}
  def visit(select: Select): Unit = {}
  def visit(reference: ParentReference.type): Unit = {}
  def visit(reference: ThisReference.type): Unit = {}

  def visit(reference: QualifiedParentReference): Unit = {}
  def visit(reference: QualifiedThisReference): Unit = {}

  def visit(statement: EmptyStatement.type): Unit = {}
  def visit(switch: DefaultSwitch.type): Unit = {}
  def visit(parameter: InferredParameter): Unit = {}

  def visit(break: Break.type): Unit = {}
  def visit(continue: Continue.type): Unit = {}
  def visit(emptyReturn: EmptyReturn.type): Unit = {}
  def visit(break: TargetedBreak): Unit = {}
  def visit(continue: TargetedContinue): Unit = {}

  def visit(declarator: VariableDeclarator): Unit = {}
  def visit(declaration: EmptyDeclaration.type): Unit = {}
  def visit(declaration: UnnamedPackageDeclaration.type): Unit = {}

  def visit(declaration: LazyImportDeclaration): Unit = {}
  def visit(declaration: SingleImportDeclaration): Unit = {}
  def visit(declaration: StaticImportDeclaration): Unit = {}
  def visit(declaration: StaticLazyImportDeclaration): Unit = {}

  def visit(operator: BinaryOperator): Unit = {}
  def visit(modifier: Modifier): Unit = {}

  def visit(reference: LocalVariableReference): Unit = {}
}
