package jvm.parallel.ast.parsers.statement.instructions.discardable.instantiation

import jvm.parallel.ast.parsers.expression.ArrayInitializer
import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class InitializedArrayInstantiation(var `type`: Type, var initializer: ArrayInitializer) extends ArrayInstantiation {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    `type`.accept(visitor)
    initializer.accept(visitor)

    visitor.onExit(this)
  }
}
