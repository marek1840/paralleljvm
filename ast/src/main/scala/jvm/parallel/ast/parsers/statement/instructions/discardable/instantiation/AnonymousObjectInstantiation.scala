package jvm.parallel.ast.parsers.statement.instructions.discardable.instantiation

import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.expression.types.templates.TemplateArgument
import jvm.parallel.ast.parsers.statement.declaration.members.MemberDeclaration
import jvm.parallel.ast.parsers.visitor.ASTVisitor


case class AnonymousObjectInstantiation(var constructorTypes: Seq[TemplateArgument],
                                        var `type`: Type,
                                        var constructorArguments: Seq[Expression],
                                        var body: Seq[MemberDeclaration]) extends ObjectInstantiation {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    constructorTypes.accept(visitor)
    `type`.accept(visitor)
    constructorArguments.accept(visitor)
    body.accept(visitor)

    visitor.onExit(this)
  }
}
