package jvm.parallel.ast.parsers.statement.declaration.packages

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class NamedPackageDeclaration(var annotations: Seq[Annotation], var name: Name) extends PackageDeclaration {
  override def accept(visitor: ASTVisitor) = {
    visitor.onEnter(this)

    annotations.accept(visitor)

    visitor.onExit(this)
  }
}
