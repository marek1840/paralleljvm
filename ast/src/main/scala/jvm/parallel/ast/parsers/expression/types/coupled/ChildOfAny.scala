package jvm.parallel.ast.parsers.expression.types.coupled

import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class ChildOfAny(var types: Seq[Type]) extends CoupledType {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    types.accept(visitor)

    visitor.onExit(this)
  }
}