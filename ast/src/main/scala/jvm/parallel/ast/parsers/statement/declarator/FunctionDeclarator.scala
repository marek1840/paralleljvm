package jvm.parallel.ast.parsers.statement.declarator

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.parameter.Parameter

trait FunctionDeclarator extends Declarator{
  def name: Name
  def parameters: Seq[Parameter]
}