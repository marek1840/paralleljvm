package jvm.parallel.ast.parsers.statement.instructions.discardable.literals

import jvm.parallel.ast.parsers.statement.instructions.discardable.DiscardableExpression

trait Literal extends DiscardableExpression