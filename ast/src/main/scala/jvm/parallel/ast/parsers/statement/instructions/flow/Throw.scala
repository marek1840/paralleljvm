package jvm.parallel.ast.parsers.statement.instructions.flow

import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class Throw(var exception: Expression) extends FlowStatement {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    exception.accept(visitor)

    visitor.onExit(this)
  }
}