package jvm.parallel.ast.parsers.expression.types.primitives

import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class CharPrimitive(var annotations: Seq[Annotation]) extends PrimitiveType {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    annotations.accept(visitor)

    visitor.onExit(this)
  }
}

