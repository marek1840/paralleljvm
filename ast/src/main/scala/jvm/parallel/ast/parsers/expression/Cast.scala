package jvm.parallel.ast.parsers.expression

import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class Cast(var target: Type, var source: Expression) extends Expression {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    target.accept(visitor)
    source.accept(visitor)

    visitor.onExit(this)
  }
}
