package jvm.parallel.ast.parsers.statement.instructions.interruptable

import jvm.parallel.ast.parsers.statement.Block
import jvm.parallel.ast.parsers.statement.instructions.LocalVariableDeclaration
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class TryWithResources(var resources: Seq[LocalVariableDeclaration],
                            var tryBlock: Block,
                            var catches: Seq[CatchClause],
                            var finallyBlock: Option[Block]) extends TryStatement {

  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    resources.accept(visitor)
    tryBlock.accept(visitor)
    catches.accept(visitor)
    finallyBlock.accept(visitor)

    visitor.onExit(this)
  }
}