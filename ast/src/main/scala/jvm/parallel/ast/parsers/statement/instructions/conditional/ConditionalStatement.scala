package jvm.parallel.ast.parsers.statement.instructions.conditional

import jvm.parallel.ast.parsers.statement.Statement
import jvm.parallel.ast.parsers.statement.instructions.InstructionStatement

trait ConditionalStatement extends InstructionStatement