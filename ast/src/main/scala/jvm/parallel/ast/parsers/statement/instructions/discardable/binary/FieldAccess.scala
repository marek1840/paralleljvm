package jvm.parallel.ast.parsers.statement.instructions.discardable.binary

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.statement.instructions.discardable.DiscardableExpression
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class FieldAccess(var instance: Expression, var accesor: Name) extends DiscardableExpression {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    instance.accept(visitor)

    visitor.onExit(this)
  }
}

