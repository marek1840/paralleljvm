package jvm.parallel.ast.parsers.statement.instructions.discardable.binary

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.statement.instructions.discardable.DiscardableExpression
import jvm.parallel.ast.parsers.expression.types.templates.TemplateArgument
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class MethodReference(var qualifier: Expression,
                           var types: Seq[TemplateArgument],
                           var name: Name) extends DiscardableExpression {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    qualifier.accept(visitor)
    types.accept(visitor)

    visitor.onExit(this)
  }
}
