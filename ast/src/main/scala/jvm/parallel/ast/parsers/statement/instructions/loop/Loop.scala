package jvm.parallel.ast.parsers.statement.instructions.loop

import jvm.parallel.ast.parsers.statement.Statement
import jvm.parallel.ast.parsers.statement.instructions.InstructionStatement

trait Loop extends InstructionStatement