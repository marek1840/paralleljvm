package jvm.parallel.ast.parsers.statement.declaration

import jvm.parallel.ast.parsers.statement.declaration.imports.ImportDeclaration
import jvm.parallel.ast.parsers.statement.declaration.packages.PackageDeclaration
import jvm.parallel.ast.parsers.statement.declaration.types.TypeDeclaration
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class CompilationUnitDeclaration(var `package`: PackageDeclaration,
                                      var imports: Seq[ImportDeclaration],
                                      var declarations: Seq[TypeDeclaration]) extends Declaration {
  override def accept(visitor: ASTVisitor) = {
    visitor.onEnter(this)

    `package`.accept(visitor)
    imports.accept(visitor)
    declarations.accept(visitor)

    visitor.onExit(this)
  }
}