package jvm.parallel.ast.parsers.statement.instructions.discardable.dimension

import jvm.parallel.ast.parsers.ASTNode

trait Dimension extends ASTNode
