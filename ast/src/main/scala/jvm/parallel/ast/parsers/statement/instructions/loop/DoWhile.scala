package jvm.parallel.ast.parsers.statement.instructions.loop

import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.statement.Statement
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class DoWhile(var body: Statement, var condition: Expression) extends Loop {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    body.accept(visitor)
    condition.accept(visitor)

    visitor.onExit(this)
  }
}