package jvm.parallel.ast.parsers.statement.declaration

import jvm.parallel.ast.parsers.ASTNode
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation

trait Annotated extends ASTNode{
  def annotations: Seq[Annotation]
}