package jvm.parallel.ast.parsers.statement.instructions.switch

import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.statement.Statement
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class SwitchCases(var cases: Seq[Expression], var statements: Seq[Statement]) extends SwitchRule {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    cases.accept(visitor)
    statements.accept(visitor)

    visitor.onExit(this)
  }
}