package jvm.parallel.ast.parsers.expression.types

import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class PointerType(var typ: Type) extends Type {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    typ.accept(visitor)

    visitor.onExit(this)
  }
}
