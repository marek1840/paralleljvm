package jvm.parallel.ast.parsers.statement.instructions.discardable.instantiation

import jvm.parallel.ast.parsers.statement.instructions.discardable.dimension.Dimension
import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class EmptyArrayInstantiation(var `type`: Type, var dimensions: Seq[Dimension]) extends ArrayInstantiation {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    `type`.accept(visitor)
    dimensions.accept(visitor)

    visitor.onExit(this)
  }
}
