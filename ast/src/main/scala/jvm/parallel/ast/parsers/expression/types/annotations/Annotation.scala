package jvm.parallel.ast.parsers.expression.types.annotations

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.types.Type

trait Annotation extends Type{
  def name:Name
}