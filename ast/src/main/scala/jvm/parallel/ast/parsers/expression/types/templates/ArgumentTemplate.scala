package jvm.parallel.ast.parsers.expression.types.templates

import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class ArgumentTemplate(var `type`: Type) extends TemplateArgument {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    `type`.accept(visitor)

    visitor.onExit(this)
  }
}
