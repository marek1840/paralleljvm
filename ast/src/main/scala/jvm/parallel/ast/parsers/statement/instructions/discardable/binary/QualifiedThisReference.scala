package jvm.parallel.ast.parsers.statement.instructions.discardable.binary

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.instructions.discardable.DiscardableExpression
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class QualifiedThisReference(var name: Name) extends DiscardableExpression {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.visit(this)
  }
}
