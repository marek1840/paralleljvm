package jvm.parallel.ast.parsers.statement.instructions

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.Statement
import jvm.parallel.ast.parsers.visitor.ASTVisitor

case class LabeledStatement(var name: Name, var statement: Statement) extends InstructionStatement {
  override def accept(visitor: ASTVisitor): Unit = {
    visitor.onEnter(this)

    statement.accept(visitor)

    visitor.onExit(this)
  }
}
