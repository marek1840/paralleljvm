package jvm.parallel.ast.helpers

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.declaration.CompilationUnitDeclaration
import jvm.parallel.ast.parsers.statement.declaration.types.{AnnotationDeclaration, ClassDeclaration, EnumDeclaration, InterfaceDeclaration}

object CompilationUnitHelper {
  def interfaceDef(node: CompilationUnitDeclaration)(Name: Name): InterfaceDeclaration = {
    node.declarations.collectFirst { case i@InterfaceDeclaration(_, _, Name, _, _, _) => i }.get
  }

  def classDef(node: CompilationUnitDeclaration)(Name: Name): ClassDeclaration = {
    node.declarations.collectFirst { case c@ClassDeclaration(_, _, Name, _, _, _, _) => c }.get
  }

  def enumDef(node: CompilationUnitDeclaration)(Name: Name): EnumDeclaration = {
    node.declarations.collectFirst { case e@EnumDeclaration(_, _, Name, _, _) => e }.get
  }

  def annDef(node: CompilationUnitDeclaration)(Name: Name): AnnotationDeclaration = {
    node.declarations.collectFirst { case e@AnnotationDeclaration(_, _, Name, _) => e }.get
  }
}
