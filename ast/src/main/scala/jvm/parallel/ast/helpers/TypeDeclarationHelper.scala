package jvm.parallel.ast.helpers

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.declaration.members.{FieldDeclaration, MethodDeclaration}
import jvm.parallel.ast.parsers.statement.declaration.types.TypeDeclaration
import jvm.parallel.ast.parsers.statement.declarator.{MethodDeclarator, VariableDeclarator}

object TypeDeclarationHelper {
  def field(node: TypeDeclaration)(Name: Name): FieldDeclaration = {
    node.members.collectFirst { case f@FieldDeclaration(_, _, _, Vector(VariableDeclarator(Name))) => f }.get
  }

  def method(node: TypeDeclaration)(Name: Name): MethodDeclaration = {
    node.members.collectFirst { case m@MethodDeclaration(_, _, _, _, MethodDeclarator(Name, Nil), _, _) => m }.get
  }
}
