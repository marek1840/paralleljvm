package jvm.parallel.ast.parsers.statements

import jvm.parallel.ast.parsers.VisitorTest
import jvm.parallel.ast.parsers.statement.declarator._

class DeclaratorVisitorTest extends VisitorTest {
  "Visitor should be properly dispatched in" - {
    "VariableDeclarator" in {
      val v = new VariableDeclarator("").visit()
      v.check(0, 1)
    }
    "InitializedArrayDeclarator" in {
      val v = new InitializedArrayDeclarator("", dim, expr).visit()
      v.check(2, 1)
    }
    "MethodDeclarator" in {
      val v = new MethodDeclarator("", argParam).visit()
      v.check(1, 1)
    }
    "ArrayDeclarator" in {
      val v = new  ArrayDeclarator("", dim).visit()
      v.check(2, 0)
    }
    "InitializedVariableDeclarator" in {
      val v = new InitializedVariableDeclarator("", expr).visit()
      v.check(1, 1)
    }
    "ArrayMethodDeclarator" in {
      val v = new ArrayMethodDeclarator("", argParam, dim).visit()
      v.check(2, 1)
    }
    "ConstructorDeclarator" in {
      val v = new ConstructorDeclarator("", argParam).visit()
      v.check(1, 1)
    }
  }
}
