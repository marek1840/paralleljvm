package jvm.parallel.ast.parsers

import jvm.parallel.ast.parsers.expression._
import jvm.parallel.ast.parsers.statement.instructions._
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary._
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.assignment.{AugmentedBinding, Binding}
import jvm.parallel.ast.parsers.statement.instructions.discardable.dimension.{AbstractDimension, InitializedDimension}
import jvm.parallel.ast.parsers.statement.instructions.discardable.instantiation._
import jvm.parallel.ast.parsers.statement.instructions.discardable.literals._
import jvm.parallel.ast.parsers.statement.instructions.discardable.reference.{ParentReference, Select, ThisReference}
import jvm.parallel.ast.parsers.statement.instructions.discardable.unary.{PostDecrementation, PostIncrementation, PreDecrementation, PreIncrementation}
import jvm.parallel.ast.parsers.statement.instructions.discardable.{ArrayConstructorReference, ConstructorReference}
import jvm.parallel.ast.parsers.expression.types.PointerType
import jvm.parallel.ast.parsers.expression.types.annotations.{MarkerAnnotation, NamedAnnotationParameter, NormalAnnotation, SingleElementAnnotation}
import jvm.parallel.ast.parsers.expression.types.coupled.{ChildOfAll, ChildOfAny}
import jvm.parallel.ast.parsers.expression.types.primitives._
import jvm.parallel.ast.parsers.expression.types.references.{ArrayType, ClassType}
import jvm.parallel.ast.parsers.expression.types.templates._
import jvm.parallel.ast.parsers.statement._
import jvm.parallel.ast.parsers.statement.instructions.conditional.{If, IfThenElse}
import jvm.parallel.ast.parsers.statement.instructions.constructor.{AlternateConstructorInvocation, IndirectParentConstructorInvocation, IntermediateConstructorInvocation, ParentConstructorInvocation}
import jvm.parallel.ast.parsers.statement.declaration.imports.{LazyImportDeclaration, SingleImportDeclaration, StaticImportDeclaration, StaticLazyImportDeclaration}
import jvm.parallel.ast.parsers.statement.declaration.initializers.{InstanceInitializerDeclaration, StaticInitializerDeclaration}
import jvm.parallel.ast.parsers.statement.declaration.members._
import jvm.parallel.ast.parsers.statement.declaration.packages.{NamedPackageDeclaration, UnnamedPackageDeclaration}
import jvm.parallel.ast.parsers.statement.declaration.types._
import jvm.parallel.ast.parsers.statement.declaration.CompilationUnitDeclaration
import jvm.parallel.ast.parsers.statement.declarator._
import jvm.parallel.ast.parsers.statement.instructions.flow._
import jvm.parallel.ast.parsers.statement.instructions.interruptable._
import jvm.parallel.ast.parsers.statement.instructions.loop.{DoWhile, For, Iteration, While}
import jvm.parallel.ast.parsers.statement.parameter._
import jvm.parallel.ast.parsers.statement.instructions.switch.{DefaultSwitch, EmptySwitchCases, SwitchCases, SwitchStatement}
import jvm.parallel.ast.parsers.visitor.ASTVisitor

class TestASTVisitor extends ASTVisitor{
  var enteredNodes = 0
  var exitedNodes = 0
  var leafsVisited = 0

  override def onEnter(thenElse: IfThenElse): Unit = enteredNodes += 1
  override def onExit(thenElse: IfThenElse): Unit = exitedNodes += 1

  override def onEnter(value: If): Unit = enteredNodes += 1
  override def onExit(value: If): Unit = exitedNodes += 1

  override def onEnter(invocation: AlternateConstructorInvocation): Unit = enteredNodes += 1
  override def onExit(invocation: AlternateConstructorInvocation): Unit = exitedNodes += 1

  override def onEnter(invocation: IndirectParentConstructorInvocation): Unit = enteredNodes += 1
  override def onExit(invocation: IndirectParentConstructorInvocation): Unit = exitedNodes += 1

  override def onEnter(invocation: IntermediateConstructorInvocation): Unit = enteredNodes += 1
  override def onExit(invocation: IntermediateConstructorInvocation): Unit = exitedNodes += 1

  override def onEnter(invocation: ParentConstructorInvocation): Unit = enteredNodes += 1
  override def onExit(invocation: ParentConstructorInvocation): Unit = exitedNodes += 1

  override def onEnter(declaration: InstanceInitializerDeclaration): Unit = enteredNodes += 1
  override def onExit(declaration: InstanceInitializerDeclaration): Unit = exitedNodes += 1

  override def onEnter(declaration: StaticInitializerDeclaration): Unit = enteredNodes += 1
  override def onExit(declaration: StaticInitializerDeclaration): Unit = exitedNodes += 1

  override def onEnter(declaration: AnnotationDefaultElementDeclaration): Unit = enteredNodes += 1
  override def onExit(declaration: AnnotationDefaultElementDeclaration): Unit = exitedNodes += 1

  override def onEnter(declaration: AnnotationElementDeclaration): Unit = enteredNodes += 1
  override def onExit(declaration: AnnotationElementDeclaration): Unit = exitedNodes += 1

  override def onEnter(declaration: AnonymousEnumConstantDeclaration): Unit = enteredNodes += 1
  override def onExit(declaration: AnonymousEnumConstantDeclaration): Unit = exitedNodes += 1

  override def onEnter(declaration: ConstructorDeclaration): Unit = enteredNodes += 1
  override def onExit(declaration: ConstructorDeclaration): Unit = exitedNodes += 1

  override def onEnter(declaration: EnumConstantDeclaration): Unit = enteredNodes += 1
  override def onExit(declaration: EnumConstantDeclaration): Unit = exitedNodes += 1

  override def onEnter(declaration: FieldDeclaration): Unit = enteredNodes += 1
  override def onExit(declaration: FieldDeclaration): Unit = exitedNodes += 1

  override def onEnter(declaration: MethodDeclaration): Unit = enteredNodes += 1
  override def onExit(declaration: MethodDeclaration): Unit = exitedNodes += 1

  override def onEnter(declaration: NamedPackageDeclaration): Unit = enteredNodes += 1
  override def onExit(declaration: NamedPackageDeclaration): Unit = exitedNodes += 1

  override def onEnter(declaration: EnumDeclaration): Unit = enteredNodes += 1
  override def onExit(declaration: EnumDeclaration): Unit = exitedNodes += 1

  override def onEnter(declaration: InterfaceDeclaration): Unit = enteredNodes += 1
  override def onExit(declaration: InterfaceDeclaration): Unit = exitedNodes += 1

  override def onEnter(declaration: ClassDeclaration): Unit = enteredNodes += 1
  override def onExit(declaration: ClassDeclaration): Unit = exitedNodes += 1

  override def onEnter(declaration: AnnotationDeclaration): Unit = enteredNodes += 1
  override def onExit(declaration: AnnotationDeclaration): Unit = exitedNodes += 1

  override def onEnter(declaration: CompilationUnitDeclaration): Unit = enteredNodes += 1
  override def onExit(declaration: CompilationUnitDeclaration): Unit = exitedNodes += 1

  override def onEnter(declaration: LocalVariableDeclaration): Unit = enteredNodes += 1
  override def onExit(declaration: LocalVariableDeclaration): Unit = exitedNodes += 1

  override def onEnter(declarator: ArrayDeclarator): Unit = enteredNodes += 1
  override def onExit(declarator: ArrayDeclarator): Unit = exitedNodes += 1

  override def onEnter(declarator: ArrayMethodDeclarator): Unit = enteredNodes += 1
  override def onExit(declarator: ArrayMethodDeclarator): Unit = exitedNodes += 1

  override def onEnter(declarator: ConstructorDeclarator): Unit = enteredNodes += 1
  override def onExit(declarator: ConstructorDeclarator): Unit = exitedNodes += 1

  override def onEnter(declarator: InitializedArrayDeclarator): Unit = enteredNodes += 1
  override def onExit(declarator: InitializedArrayDeclarator): Unit = exitedNodes += 1

  override def onEnter(declarator: InitializedVariableDeclarator): Unit = enteredNodes += 1
  override def onExit(declarator: InitializedVariableDeclarator): Unit = exitedNodes += 1

  override def onEnter(declarator: MethodDeclarator): Unit = enteredNodes += 1
  override def onExit(declarator: MethodDeclarator): Unit = exitedNodes += 1

  override def onEnter(implicitReturn: ImplicitReturn): Unit = enteredNodes += 1
  override def onExit(implicitReturn: ImplicitReturn): Unit = exitedNodes += 1

  override def onEnter(value: Return): Unit = enteredNodes += 1
  override def onExit(value: Return): Unit = exitedNodes += 1

  override def onEnter(value: Throw): Unit = enteredNodes += 1
  override def onExit(value: Throw): Unit = exitedNodes += 1

  override def onEnter(value: Yield): Unit = enteredNodes += 1
  override def onExit(value: Yield): Unit = exitedNodes += 1

  override def onEnter(clause: CatchClause): Unit = enteredNodes += 1
  override def onExit(clause: CatchClause): Unit = exitedNodes += 1

  override def onEnter(tryCatch: TryCatch): Unit = enteredNodes += 1
  override def onExit(tryCatch: TryCatch): Unit = exitedNodes += 1

  override def onEnter(catchFinally: TryCatchFinally): Unit = enteredNodes += 1
  override def onExit(catchFinally: TryCatchFinally): Unit = exitedNodes += 1

  override def onEnter(tryFinally: TryFinally): Unit = enteredNodes += 1
  override def onExit(tryFinally: TryFinally): Unit = exitedNodes += 1

  override def onEnter(resources: TryWithResources): Unit = enteredNodes += 1
  override def onExit(resources: TryWithResources): Unit = exitedNodes += 1

  override def onEnter(doWhile: DoWhile): Unit = enteredNodes += 1
  override def onExit(doWhile: DoWhile): Unit = exitedNodes += 1

  override def onEnter(value: For): Unit = enteredNodes += 1
  override def onExit(value: For): Unit = exitedNodes += 1

  override def onEnter(iteration: Iteration): Unit = enteredNodes += 1
  override def onExit(iteration: Iteration): Unit = exitedNodes += 1

  override def onEnter(value: While): Unit = enteredNodes += 1
  override def onExit(value: While): Unit = exitedNodes += 1

  override def onEnter(parameter: FormalParameter): Unit = enteredNodes += 1
  override def onExit(parameter: FormalParameter): Unit = exitedNodes += 1

  override def onEnter(parameter: InstanceReceiverParameter): Unit = enteredNodes += 1
  override def onExit(parameter: InstanceReceiverParameter): Unit = exitedNodes += 1

  override def onEnter(parameter: NestedReceiverParameter): Unit = enteredNodes += 1
  override def onExit(parameter: NestedReceiverParameter): Unit = exitedNodes += 1

  override def onEnter(parameter: VariableArityParameter): Unit = enteredNodes += 1
  override def onExit(parameter: VariableArityParameter): Unit = exitedNodes += 1

  override def onEnter(cases: EmptySwitchCases): Unit = enteredNodes += 1
  override def onExit(cases: EmptySwitchCases): Unit = exitedNodes += 1

  override def onEnter(cases: SwitchCases): Unit = enteredNodes += 1
  override def onExit(cases: SwitchCases): Unit = exitedNodes += 1

  override def onEnter(statement: SwitchStatement): Unit = enteredNodes += 1
  override def onExit(statement: SwitchStatement): Unit = exitedNodes += 1

  override def onEnter(assertion: Assertion): Unit = enteredNodes += 1
  override def onExit(assertion: Assertion): Unit = exitedNodes += 1

  override def onEnter(statement: LabeledStatement): Unit = enteredNodes += 1
  override def onExit(statement: LabeledStatement): Unit = exitedNodes += 1

  override def onEnter(block: SynchronizedBlock): Unit = enteredNodes += 1
  override def onExit(block: SynchronizedBlock): Unit = exitedNodes += 1

  override def onEnter(block: Block): Unit = enteredNodes += 1
  override def onExit(block: Block): Unit = exitedNodes += 1

  override def onEnter(template: BoundedParameterTemplate): Unit = enteredNodes += 1
  override def onExit(template: BoundedParameterTemplate): Unit = exitedNodes += 1

  override def onEnter(binding: Binding): Unit = enteredNodes += 1
  override def onExit(binding: Binding): Unit = exitedNodes += 1

  override def onEnter(binding: AugmentedBinding): Unit = enteredNodes += 1
  override def onExit(binding: AugmentedBinding): Unit = exitedNodes += 1

  override def onEnter(extraction: Extraction): Unit = enteredNodes += 1
  override def onExit(extraction: Extraction): Unit = exitedNodes += 1

  override def onEnter(access: FieldAccess): Unit = enteredNodes += 1
  override def onExit(access: FieldAccess): Unit = exitedNodes += 1

  override def onEnter(invocation: MethodInvocation): Unit = enteredNodes += 1
  override def onExit(invocation: MethodInvocation): Unit = exitedNodes += 1

  override def onEnter(reference: MethodReference): Unit = enteredNodes += 1
  override def onExit(reference: MethodReference): Unit = exitedNodes += 1

  override def onEnter(invocation: QualifiedMethodInvocation): Unit = enteredNodes += 1
  override def onExit(invocation: QualifiedMethodInvocation): Unit = exitedNodes += 1

  override def onEnter(dimension: AbstractDimension): Unit = enteredNodes += 1
  override def onExit(dimension: AbstractDimension): Unit = exitedNodes += 1

  override def onEnter(dimension: InitializedDimension): Unit = enteredNodes += 1
  override def onExit(dimension: InitializedDimension): Unit = exitedNodes += 1

  override def onEnter(instantiation: NestedAnonymousObjectInstantiation): Unit = enteredNodes += 1
  override def onExit(instantiation: NestedAnonymousObjectInstantiation): Unit = exitedNodes += 1

  override def onEnter(instantiation: InitializedArrayInstantiation): Unit = enteredNodes += 1
  override def onExit(instantiation: InitializedArrayInstantiation): Unit = exitedNodes += 1

  override def onEnter(instantiation: AnonymousObjectInstantiation): Unit = enteredNodes += 1
  override def onExit(instantiation: AnonymousObjectInstantiation): Unit = exitedNodes += 1

  override def onEnter(instantiation: EmptyArrayInstantiation): Unit = enteredNodes += 1
  override def onExit(instantiation: EmptyArrayInstantiation): Unit = exitedNodes += 1

  override def onEnter(instantiation: NestedObjectInstantiation): Unit = enteredNodes += 1
  override def onExit(instantiation: NestedObjectInstantiation): Unit = exitedNodes += 1

  override def onEnter(instantiation: SimpleObjectInstantiation): Unit = enteredNodes += 1
  override def onExit(instantiation: SimpleObjectInstantiation): Unit = exitedNodes += 1

  override def onEnter(literal: ClassLiteral): Unit = enteredNodes += 1
  override def onExit(literal: ClassLiteral): Unit = exitedNodes += 1

  override def onEnter(incrementation: PreDecrementation): Unit = enteredNodes += 1
  override def onExit(incrementation: PreDecrementation): Unit = exitedNodes += 1

  override def onEnter(incrementation: PostDecrementation): Unit = enteredNodes += 1
  override def onExit(incrementation: PostDecrementation): Unit = exitedNodes += 1

  override def onEnter(incrementation: PostIncrementation): Unit = enteredNodes += 1
  override def onExit(incrementation: PostIncrementation): Unit = exitedNodes += 1

  override def onEnter(incrementation: PreIncrementation): Unit = enteredNodes += 1
  override def onExit(incrementation: PreIncrementation): Unit = exitedNodes += 1

  override def onEnter(reference: ConstructorReference): Unit = enteredNodes += 1
  override def onExit(reference: ConstructorReference): Unit = exitedNodes += 1

  override def onEnter(parameter: NamedAnnotationParameter): Unit = enteredNodes += 1
  override def onExit(parameter: NamedAnnotationParameter): Unit = exitedNodes += 1

  override def onEnter(annotation: NormalAnnotation): Unit = enteredNodes += 1
  override def onExit(annotation: NormalAnnotation): Unit = exitedNodes += 1

  override def onEnter(annotation: SingleElementAnnotation): Unit = enteredNodes += 1
  override def onExit(annotation: SingleElementAnnotation): Unit = exitedNodes += 1

  override def onEnter(any: ChildOfAny): Unit = enteredNodes += 1
  override def onExit(any: ChildOfAny): Unit = exitedNodes += 1

  override def onEnter(all: ChildOfAll): Unit = enteredNodes += 1
  override def onExit(all: ChildOfAll): Unit = exitedNodes += 1

  override def onEnter(primitive: LongPrimitive): Unit = enteredNodes += 1
  override def onExit(primitive: LongPrimitive): Unit = exitedNodes += 1

  override def onEnter(primitive: IntegerPrimitive): Unit = enteredNodes += 1
  override def onExit(primitive: IntegerPrimitive): Unit = exitedNodes += 1

  override def onEnter(primitive: FloatPrimitive): Unit = enteredNodes += 1
  override def onExit(primitive: FloatPrimitive): Unit = exitedNodes += 1

  override def onEnter(primitive: DoublePrimitive): Unit = enteredNodes += 1
  override def onExit(primitive: DoublePrimitive): Unit = exitedNodes += 1

  override def onEnter(primitive: CharPrimitive): Unit = enteredNodes += 1
  override def onExit(primitive: CharPrimitive): Unit = exitedNodes += 1

  override def onEnter(primitive: BytePrimitive): Unit = enteredNodes += 1
  override def onExit(primitive: BytePrimitive): Unit = exitedNodes += 1

  override def onEnter(primitive: BooleanPrimitive): Unit = enteredNodes += 1
  override def onExit(primitive: BooleanPrimitive): Unit = exitedNodes += 1

  override def onEnter(primitive: ShortPrimitive): Unit = enteredNodes += 1
  override def onExit(primitive: ShortPrimitive): Unit = exitedNodes += 1

  override def onEnter(primitive: VoidPrimitive): Unit = enteredNodes += 1
  override def onExit(primitive: VoidPrimitive): Unit = exitedNodes += 1

  override def onEnter(arrayType: ArrayType): Unit = enteredNodes += 1
  override def onExit(arrayType: ArrayType): Unit = exitedNodes += 1

  override def onEnter(classType: ClassType): Unit = enteredNodes += 1
  override def onExit(classType: ClassType): Unit = exitedNodes += 1

  override def onExit(pointerType: PointerType): Unit = enteredNodes += 1
  override def onEnter(pointerType: PointerType) = exitedNodes += 1

  override def onEnter(reference: ArrayConstructorReference): Unit = enteredNodes += 1
  override def onExit(reference: ArrayConstructorReference): Unit = exitedNodes += 1

  override def onEnter(template: AnyTemplate): Unit = enteredNodes += 1
  override def onExit(template: AnyTemplate): Unit = exitedNodes += 1

  override def onEnter(template: ArgumentTemplate): Unit = enteredNodes += 1
  override def onExit(template: ArgumentTemplate): Unit = exitedNodes += 1

  override def onEnter(template: AnyBaseClassTemplate) = enteredNodes += 1
  override def onExit(template: AnyBaseClassTemplate): Unit = exitedNodes += 1

  override def onEnter(template: AnySubClassTemplate) = enteredNodes += 1
  override def onExit(template: AnySubClassTemplate): Unit = exitedNodes += 1

  override def onEnter(template: ParameterTemplate): Unit = enteredNodes += 1
  override def onExit(template: ParameterTemplate): Unit = exitedNodes += 1

  override def onEnter(initializer: ArrayInitializer): Unit = enteredNodes += 1
  override def onExit(initializer: ArrayInitializer): Unit = exitedNodes += 1

  override def onEnter(operations: BinaryOperations): Unit = enteredNodes += 1
  override def onExit(operations: BinaryOperations): Unit = exitedNodes += 1

  override def onEnter(cast: Cast): Unit = enteredNodes += 1
  override def onExit(cast: Cast): Unit = exitedNodes += 1

  override def onEnter(lambda: Lambda): Unit = enteredNodes += 1
  override def onExit(lambda: Lambda): Unit = exitedNodes += 1

  override def onEnter(conditional: TernaryConditional): Unit = enteredNodes += 1
  override def onExit(conditional: TernaryConditional): Unit = exitedNodes += 1

  override def onEnter(operation: UnaryOperations): Unit = enteredNodes += 1
  override def onExit(operation: UnaryOperations): Unit = exitedNodes += 1

  override def visit(literal: BooleanLiteral): Unit = leafsVisited += 1
  override def visit(literal: ByteLiteral): Unit = leafsVisited += 1
  override def visit(literal: CharLiteral): Unit = leafsVisited += 1
  override def visit(literal: DoubleLiteral): Unit = leafsVisited += 1
  override def visit(literal: FloatLiteral): Unit = leafsVisited += 1
  override def visit(literal: IntegerLiteral): Unit = leafsVisited += 1
  override def visit(literal: LongLiteral): Unit = leafsVisited += 1
  override def visit(literal: ShortLiteral): Unit = leafsVisited += 1
  override def visit(literal: StringLiteral): Unit = leafsVisited += 1
  override def visit(literal: NullLiteral.type): Unit = leafsVisited += 1
  override def visit(literal: VoidLiteral): Unit = leafsVisited += 1
  override def visit(annotation: MarkerAnnotation): Unit = leafsVisited += 1
  override def visit(select: Select): Unit = leafsVisited += 1
  override def visit(reference: ParentReference.type): Unit = leafsVisited += 1
  override def visit(reference: ThisReference.type): Unit = leafsVisited += 1
  override def visit(reference: QualifiedParentReference): Unit = leafsVisited += 1
  override def visit(reference: QualifiedThisReference): Unit = leafsVisited += 1
  override def visit(statement: EmptyStatement.type): Unit = leafsVisited += 1
  override def visit(switch: DefaultSwitch.type): Unit = leafsVisited += 1
  override def visit(parameter: InferredParameter): Unit = leafsVisited += 1
  override def visit(break: Break.type): Unit = leafsVisited += 1
  override def visit(continue: Continue.type): Unit = leafsVisited += 1
  override def visit(emptyReturn: EmptyReturn.type): Unit = leafsVisited += 1
  override def visit(break: TargetedBreak): Unit = leafsVisited += 1
  override def visit(continue: TargetedContinue): Unit = leafsVisited += 1
  override def visit(declarator: VariableDeclarator): Unit = leafsVisited += 1
  override def visit(declaration: EmptyDeclaration.type): Unit = leafsVisited += 1
  override def visit(declaration: UnnamedPackageDeclaration.type): Unit = leafsVisited += 1
  override def visit(declaration: LazyImportDeclaration): Unit = leafsVisited += 1
  override def visit(declaration: SingleImportDeclaration): Unit = leafsVisited += 1
  override def visit(declaration: StaticImportDeclaration): Unit = leafsVisited += 1
  override def visit(declaration: StaticLazyImportDeclaration): Unit = leafsVisited += 1

}
