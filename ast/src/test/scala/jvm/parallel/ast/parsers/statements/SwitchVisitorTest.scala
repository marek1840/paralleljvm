package jvm.parallel.ast.parsers.statements

import jvm.parallel.ast.parsers.VisitorTest
import jvm.parallel.ast.parsers.statement.instructions.switch.{DefaultSwitch, EmptySwitchCases, SwitchCases, SwitchStatement}

class SwitchVisitorTest extends VisitorTest {
  "Visitor should be properly dispatched in" - {
    "DefaultSwitch" in {
      val v = DefaultSwitch.visit()
      v.check(0, 1)
    }
    "EmptySwitchCases" in {
      val v =  new EmptySwitchCases(expr).visit()
      v.check(1, 1)
    }
    "SwitchCases" in {
      val v = new SwitchCases(expr, stmt).visit()
      v.check(1, 2)
    }
    "SwitchStatement" in {
      val v =  new SwitchStatement(expr, new EmptySwitchCases(expr)).visit()
      v.check(2, 2)
    }
  }
}
