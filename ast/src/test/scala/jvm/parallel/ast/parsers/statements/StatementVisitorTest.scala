package jvm.parallel.ast.parsers.statements

import jvm.parallel.ast.parsers.VisitorTest
import jvm.parallel.ast.parsers.statement._
import jvm.parallel.ast.parsers.statement.instructions.conditional.{If, IfThenElse}
import jvm.parallel.ast.parsers.statement.instructions.constructor.{AlternateConstructorInvocation, IndirectParentConstructorInvocation, IntermediateConstructorInvocation, ParentConstructorInvocation}
import jvm.parallel.ast.parsers.statement.instructions._

class StatementVisitorTest extends VisitorTest {

  "Visitor should be properly dispatched in" - {
    "Block" in {
      val v = new Block(Nil).visit()
      v.check(1, 0)
    }

    "EmptyStatement" in {
      val v = EmptyStatement.visit()
      v.check(0, 1)
    }

    "LabeledStatement" in {
      val v = new LabeledStatement("", stmt).visit()
      v.check(1, 1)
    }

    "SynchronizedBlock" in {
      val v = new SynchronizedBlock(expr, block).visit()
      v.check(2, 1)
    }

    "Assertion" in {
      val v = new Assertion(expr, expr).visit()
      v.check(1, 2)
    }

    "conditional" - {
      "If" in {
        val v = new If(expr, stmt).visit()
        v.check(1, 2)
      }
      "IfThenElse" in {
        val v = new IfThenElse(expr, stmt, stmt).visit()
        v.check(1, 3)
      }
    }
    "constructor invocations" - {
      "AlternateConstructorInvocation" in {
        val v = new AlternateConstructorInvocation(arg, expr).visit()
        v.check(2, 1)
      }
      "IndirectParentConstructorInvocation" in {
        val v = new IndirectParentConstructorInvocation(expr, arg, expr).visit()
        v.check(2, 2)
      }
      "IntermidiateConstructorInvocation" in {
        val v = new IntermediateConstructorInvocation(expr, arg, expr).visit()
        v.check(2, 2)
      }
      "ParentConstructorInvocation" in {
        val v = new ParentConstructorInvocation(arg, expr).visit()
        v.check(2, 1)
      }
    }


  }
}
