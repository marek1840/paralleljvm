package jvm.parallel.ast.parsers.statements

import jvm.parallel.ast.parsers.VisitorTest
import jvm.parallel.ast.parsers.statement.parameter._

class ParameterVisitorTest extends VisitorTest {
  "Visitor should be properly dispatched in" - {
    "FormalParameter" in {
      val v = new FormalParameter(ann, Nil, typ, "").visit()
      v.check(2, 1)
    }
    "InferredParameter" in {
      val v = new InferredParameter("").visit()
      v.check(0, 1)
    }
    "InstanceReceiverParameter" in {
      val v = new InstanceReceiverParameter(ann, typ).visit()
      v.check(2, 1)
    }
    "NestedReceiverParameter" in {
      val v = new NestedReceiverParameter(ann, typ, "").visit()
      v.check(2, 1)
    }
    "VariableArityParameter" in {
      val v = new VariableArityParameter(ann, Nil, typ, "").visit()
      v.check(2, 1)
    }
  }
}
