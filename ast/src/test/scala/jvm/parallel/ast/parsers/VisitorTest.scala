package jvm.parallel.ast.parsers

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.Block
import jvm.parallel.ast.parsers.statement.instructions.{LocalVariableDeclaration, EmptyStatement}
import jvm.parallel.ast.parsers.statement.instructions.discardable.dimension.AbstractDimension
import jvm.parallel.ast.parsers.statement.instructions.discardable.literals.BooleanLiteral
import jvm.parallel.ast.parsers.expression.types.annotations.MarkerAnnotation
import jvm.parallel.ast.parsers.expression.types.references.ClassType
import jvm.parallel.ast.parsers.expression.types.templates.{AnyTemplate, ParameterTemplate}
import jvm.parallel.ast.parsers.statement.declaration.types.EmptyDeclaration
import jvm.parallel.ast.parsers.statement.declarator.VariableDeclarator
import jvm.parallel.ast.parsers.statement.parameter.InferredParameter
import org.scalatest.matchers.{HavePropertyMatchResult, HavePropertyMatcher}
import org.scalatest.prop.{Checkers, PropertyChecks}
import org.scalatest.{BeforeAndAfterEach, FreeSpec, Matchers}

abstract class VisitorTest extends FreeSpec with PropertyChecks with Matchers with Checkers with BeforeAndAfterEach {

  protected[VisitorTest] implicit def str2Name(str: String): Name = new Name(str)
  protected[VisitorTest] implicit def strs2Name(strs: Seq[String]): Name = new Name(strs)

  val dim = new AbstractDimension(Nil)
  val declarator = new VariableDeclarator("")
  val ann = new MarkerAnnotation("")
  val block = new Block(Nil)
  val expr = new BooleanLiteral("1")
  val stmt = EmptyStatement
  val decl = EmptyDeclaration
  val typ = new ClassType(Nil, None, "A", Nil)
  val arg = new AnyTemplate(Nil)
  val argParam = new InferredParameter("")
  val templParam = new ParameterTemplate(Nil, "")
  val local = new LocalVariableDeclaration(ann, Nil, typ, declarator)

  implicit class VisitableToVisitor(node: ASTNode) {
    private[this] val visitor = new TestASTVisitor

    def visit(): TestASTVisitor = {
      node.accept(visitor)
      this.visitor
    }
  }

  implicit class VisitorStateCheck(v: TestASTVisitor) {
    def check(expectedNodesVisited: Int, expectedLeafsVisited: Int) = {
      v should have(
        balance(),
        visitedNodes(expectedNodesVisited),
        visitedLeafs(expectedLeafsVisited)
      )
    }
  }

  def visitedLeafs(expectedValue: Int) =
    new HavePropertyMatcher[TestASTVisitor, Int] {
      def apply(visitor: TestASTVisitor) =
        HavePropertyMatchResult(
          visitor.leafsVisited == expectedValue,
          "visitedLeafs",
          expectedValue,
          visitor.leafsVisited
        )
    }

  def visitedNodes(nodes: Int) =
    new HavePropertyMatcher[TestASTVisitor, Int] {
      def apply(visitor: TestASTVisitor) =
        HavePropertyMatchResult(
          visitor.enteredNodes == nodes,
          "visitedNodes",
          nodes,
          visitor.enteredNodes
        )
    }

  def balance() =
    new HavePropertyMatcher[TestASTVisitor, Int] {
      def apply(visitor: TestASTVisitor) =
        HavePropertyMatchResult(
          visitor.enteredNodes == visitor.exitedNodes,
          "balance",
          0,
          visitor.enteredNodes - visitor.exitedNodes
        )
    }

  protected implicit def toSeq[A](a: A): Seq[A] = Seq(a)

  protected implicit def toOpt[A](a: A): Option[A] = Some(a)

}
