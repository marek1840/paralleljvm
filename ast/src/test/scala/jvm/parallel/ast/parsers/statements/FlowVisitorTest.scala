package jvm.parallel.ast.parsers.statements

import jvm.parallel.ast.parsers.VisitorTest
import jvm.parallel.ast.parsers.statement.instructions.flow._

class FlowVisitorTest extends VisitorTest {
  "Visitor should be properly dispatched in" - {
    "ImplicitReturn" in {
      val v = new  ImplicitReturn(expr).visit()
      v.check(1, 1)
    }
    "Return" in {
      val v = new Return(expr).visit()
      v.check(1, 1)
    }
    "Throw" in {
      val v = new Throw(expr).visit()
      v.check(1, 1)
    }
    "Yield" in {
      val v = new Yield(expr).visit()
      v.check(1, 1)
    }

    "Break" in {
      val v =  Break.visit()
      v.check(0, 1)
    }
    "Continue" in {
      val v =   Continue.visit()
      v.check(0, 1)
    }
    "EmptyReturn" in {
      val v =  EmptyReturn.visit()
      v.check(0, 1)
    }
    "TargetedBreak" in {
      val v = new TargetedBreak("").visit()
      v.check(0, 1)
    }
    "TargetedContinue" in {
      val v = new  TargetedContinue("").visit()
      v.check(0, 1)
    }
  }
}
