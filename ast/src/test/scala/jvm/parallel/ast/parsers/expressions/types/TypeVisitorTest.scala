package jvm.parallel.ast.parsers.expressions.types

import jvm.parallel.ast.parsers.VisitorTest
import jvm.parallel.ast.parsers.expression.types.annotations.{MarkerAnnotation, NamedAnnotationParameter, NormalAnnotation, SingleElementAnnotation}
import jvm.parallel.ast.parsers.expression.types.coupled.{ChildOfAll, ChildOfAny}
import jvm.parallel.ast.parsers.expression.types.primitives._
import jvm.parallel.ast.parsers.expression.types.references.{ArrayType, ClassType}

class TypeVisitorTest extends VisitorTest {

  "Visitor should be properly dispatched in" - {
    "ArrayType" in {
      val v = new ArrayType(typ, dim).visit()
      v.check(3, 0)
    }
    "ClassType" in {
     val v = new  ClassType(ann, typ, "", arg).visit()
      v.check(3, 1)
    }

    "ChildOfAll" in {
      val v = new ChildOfAll(typ).visit()
      v.check(2, 0)
    }
    "ChildOfAny" in {
      val v = new ChildOfAny(typ).visit()
      v.check(2, 0)
    }

    "annotations" - {
      "MarkerAnnotation" in {
       val v = new  MarkerAnnotation("").visit()
        v.check(0, 1)
      }
      "NormalAnnotation" in {
        val v = new NormalAnnotation("", new NamedAnnotationParameter("", expr)).visit()
        v.check(2, 1)
      }
      "SingleElementAnnotation" in {
        val v = new SingleElementAnnotation("", expr).visit()
        v.check(1, 1)
      }
    }

    "primitives" - {
      "Boolean" in {
       val v = new  BooleanPrimitive(ann).visit()
        v.check(1, 1)
      }
      "Byte" in {
        val v = new BytePrimitive(ann).visit()
        v.check(1, 1)
      }
      "Char" in {
       val v = new  CharPrimitive(ann).visit()
        v.check(1, 1)
      }
      "Double" in {
       val v = new  DoublePrimitive(ann).visit()
        v.check(1, 1)
      }
      "Float" in {
        val v = new FloatPrimitive(ann).visit()
        v.check(1, 1)
      }
      "Integer" in {
        val v = new IntegerPrimitive(ann).visit()
        v.check(1, 1)
      }
      "Long" in {
        val v = new LongPrimitive(ann).visit()
        v.check(1, 1)
      }
      "Short" in {
       val v = new  ShortPrimitive(ann).visit()
        v.check(1, 1)
      }
      "Void" in {
       val v = new  VoidPrimitive(ann).visit()
        v.check(1, 1)
      }
    }

  }
}
