package jvm.parallel.ast.parsers.statements

import jvm.parallel.ast.parsers.VisitorTest
import jvm.parallel.ast.parsers.statement.instructions.interruptable._

class InterruptibleVisitorTest extends VisitorTest {
  "Visitor should be properly dispatched in" - {
    "CatchClause" in {
      val v = new CatchClause(local, block).visit()
      v.check(4, 2)
    }
    "TryCatch" in {
      val v = new TryCatch(block, new CatchClause(local, block)).visit()
      v.check(6, 2)
    }
    "TryCatchFinally" in {
      val v = new TryCatchFinally(block,new  CatchClause(local, block), block).visit()
      v.check(7, 2)
    }
    "TryFinally" in {
      val v = new  TryFinally(block, block).visit()
      v.check(3, 0)
    }
    "TryWithResources" in {
      val v = new  TryWithResources(local, block,new CatchClause(local, block), block).visit()
      v.check(9, 4)
    }
  }
}
