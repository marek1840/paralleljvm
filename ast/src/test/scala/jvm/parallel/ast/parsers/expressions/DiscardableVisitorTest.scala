package jvm.parallel.ast.parsers.expressions

import jvm.parallel.ast.parsers.VisitorTest
import jvm.parallel.ast.parsers.expression.ArrayInitializer
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary._
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.assignment.{AugmentedBinding, Binding}
import jvm.parallel.ast.parsers.statement.instructions.discardable.dimension.{AbstractDimension, InitializedDimension}
import jvm.parallel.ast.parsers.statement.instructions.discardable.instantiation._
import jvm.parallel.ast.parsers.statement.instructions.discardable.literals._
import jvm.parallel.ast.parsers.statement.instructions.discardable.reference.{ParentReference, Select, ThisReference}
import jvm.parallel.ast.parsers.statement.instructions.discardable.unary.{PostDecrementation, PostIncrementation, PreDecrementation, PreIncrementation}
import jvm.parallel.ast.parsers.statement.instructions.discardable.{ArrayConstructorReference, ConstructorReference}
import jvm.parallel.ast.parsers.expression.types.references.ArrayType

class DiscardableVisitorTest extends VisitorTest {

  "Visitor should be properly dispatched in" - {
    "ArrayConstructorReference" in {
      val v  = new  ArrayConstructorReference( new ArrayType(typ, dim)).visit()
      v.check(4, 0)
    }
    "ConstructorReference" in {
      val v  = new ConstructorReference(typ, arg).visit()
      v.check(3, 0)
    }
    "ParentReference$" in {
      val v =ParentReference.visit()
      v.check(0, 1)
    }
    "Select" in {
      val v = new  Select("").visit()
      v.check(0, 1)
    }
    "ThisReference$" in {
      val v =ThisReference.visit()
      v.check(0, 1)
    }
    "PostDecrementation" in {
      val v = new PostDecrementation(expr).visit()
      v.check(1, 1)
    }
    "PostIncrementation" in {
      val v = new PostIncrementation(expr).visit()
     v.check(1, 1)
    }
    "PreDecrementation" in {
      val v = new PreDecrementation(expr).visit()
      v.check(1, 1)
    }
    "PreIncrementation" in {
      val v  = new  PreIncrementation(expr).visit()
      v.check(1, 1)
    }
    "literals" - {
      "StringLiteral" in {
        val v = new  StringLiteral("").visit()
        v.check(0, 1)
      }
      "ShortLiteral" in {
        val v = new   ShortLiteral("").visit()
        v.check(0, 1)
      }
      "BooleanLiteral" in {
        val v = new  BooleanLiteral("").visit()
        v.check(0, 1)
      }
      "ByteLiteral" in {
        val v = new   ByteLiteral("").visit()
        v.check(0, 1)
      }
      "FloatLiteral" in {
        val v = new  FloatLiteral("").visit()
        v.check(0, 1)
      }
      "ClassLiteral" in {
        val v = new  ClassLiteral(typ).visit()
        v.check(2, 0)
      }
      "VoidLiteral" in {
        val v = new   VoidLiteral("").visit()
        v.check(0, 1)
      }
      "LongLiteral" in {
        val v = new   LongLiteral("").visit()
        v.check(0, 1)
      }
      "DoubleLiteral" in {
        val v = new   DoubleLiteral("").visit()
        v.check(0, 1)
      }
      "IntegerLiteral" in {
        val v = new  IntegerLiteral("").visit()
        v.check(0, 1)
      }
      "CharLiteral" in {
        val v = new CharLiteral("").visit()
        v.check(0, 1)
      }
    }
    "ClassLiteral" in {
      val v = new ClassLiteral(typ).visit()
      v.check(2, 0)
    }
    "AnonymousObjectInstantiation" in {
      val v = new AnonymousObjectInstantiation(arg, typ, expr, decl).visit()
      v.check(3, 2)
    }
    "EmptyArrayInstantiation" in {
      val v = new  EmptyArrayInstantiation(typ, dim).visit()
      v.check(3, 0)
    }
    "InitializedArrayInstantiation" in {
      val v = new  InitializedArrayInstantiation(typ,  new ArrayInitializer(expr)).visit()
      v.check(3, 1)
    }
    "NestedAnonymousObjectInstantiation" in {
      val v = new  NestedAnonymousObjectInstantiation(expr, arg, typ, expr, decl).visit()
      v.check(3, 3)
    }
    "NestedObjectInstantiation" in {
      val v = new NestedObjectInstantiation(expr, arg, typ, expr).visit()
      v.check(3, 2)
    }
    "ObjecInstantiationt" in {
      val v = new  SimpleObjectInstantiation(arg, typ, expr).visit()
      v.check(3, 1)
    }
    "AbstractDimension" in {
      val v = new AbstractDimension(ann).visit()
      v.check(1,1)
    }
    "InitializedDimension" in {
      val v = new InitializedDimension(ann, expr).visit()
      v.check(1,2)
    }
    "unary" - {
      "assignment" - {
        "AugmentedBinding" in {
          val v = new  AugmentedBinding(expr, null, expr).visit()
          v.check(1,2)
        }
        "Binding" in {
          val v = new  Binding(expr, expr).visit()
          v.check(1,2)
        }
      }
      "Extraction" in {
        val v = new Extraction(expr, expr).visit()
        v.check(1,2)
      }
      "FieldAccess" in {
        val v = new   FieldAccess(expr, "").visit()
        v.check(1,1)
      }
      "MethodInvocation" in {
        val v = new   MethodInvocation(arg, "", expr).visit()
        v.check(2,1)
      }
      "MethodReference" in {
        val v = new  MethodReference(expr, arg, "").visit()
        v.check(2,1)
      }
      "QualifiedMethodInvocation" in {
        val v = new    QualifiedMethodInvocation(expr, arg, "", expr).visit()
        v.check(2,2)
      }
      "QualifiedParentReference" in {
        val v = new  QualifiedParentReference("").visit()
        v.check(0, 1)
      }
      "QualifiedThisReference" in {
        val v = new  QualifiedThisReference("").visit()
        v.check(0, 1)
      }
    }
  }

}
