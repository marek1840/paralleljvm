package jvm.parallel.ast.parsers.expressions

import jvm.parallel.ast.parsers.VisitorTest
import jvm.parallel.ast.parsers.expression._

class ExpressionVisitorTest extends VisitorTest {

  "Visitor should be properly dispatched in" - {
    "BinaryOperations" in {
      val v = new BinaryOperations(Vector(), expr).visit()
      v.check(1, 1)
    }
    "ArrayInitializer" in {
      val v = new ArrayInitializer(expr).visit()
      v.check(1, 1)
    }
    "Lambda" in {
      val v = new Lambda(argParam, stmt).visit()
      v.check(1,2)
    }
    "Cast" in {
      val v = new Cast(typ, expr).visit()
      v.check(2,1)
    }
    "TernaryConditional" in {
      val v = new TernaryConditional(expr, expr, expr).visit()
      v.check(1,3)
    }
    "UnaryOperations" in {
      val v = new UnaryOperations(Vector(), expr).visit()
      v.check(1, 1)
    }
  }
}
