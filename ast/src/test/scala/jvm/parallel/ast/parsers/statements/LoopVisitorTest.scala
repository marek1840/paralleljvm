package jvm.parallel.ast.parsers.statements

import jvm.parallel.ast.parsers.VisitorTest
import jvm.parallel.ast.parsers.statement.instructions.loop.{DoWhile, For, Iteration, While}

class LoopVisitorTest extends VisitorTest {
  "Visitor should be properly dispatched in" - {
    "Iteration" in {
      val v = new Iteration(local, expr, stmt).visit()
      v.check(3, 4)
    }
    "DoWhile" in {
      val v = new DoWhile(stmt, expr).visit()
      v.check(1, 2)
    }
    "While" in {
      val v = new While(expr, stmt).visit()
      v.check(1, 2)
    }
    "For" in {
      val v = new For(stmt, expr, stmt, stmt).visit()
      v.check(1, 4)
    }
  }
}
