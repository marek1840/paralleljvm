package jvm.parallel.ast.parsers.statements

import jvm.parallel.ast.parsers.VisitorTest
import jvm.parallel.ast.parsers.statement.declaration.imports.{SingleImportDeclaration, StaticLazyImportDeclaration}
import jvm.parallel.ast.parsers.statement.declaration.initializers.{InstanceInitializerDeclaration, StaticInitializerDeclaration}
import jvm.parallel.ast.parsers.statement.declaration.members._
import jvm.parallel.ast.parsers.statement.declaration.packages.{NamedPackageDeclaration, UnnamedPackageDeclaration}
import jvm.parallel.ast.parsers.statement.declaration.types.{AnnotationDeclaration, ClassDeclaration, EmptyDeclaration, EnumDeclaration}
import jvm.parallel.ast.parsers.statement.declaration.CompilationUnitDeclaration
import jvm.parallel.ast.parsers.statement.declarator.{ConstructorDeclarator, MethodDeclarator, VariableDeclarator}
import jvm.parallel.ast.parsers.statement.instructions.LocalVariableDeclaration

class DeclarationVisitorTest extends VisitorTest {
  "Visitor should be properly dispatched in" - {
    "CompilationUnitDeclaration" in {
      val v = new CompilationUnitDeclaration(UnnamedPackageDeclaration, new SingleImportDeclaration(""), decl).visit()
      v.check(1, 3)
    }
    "LocalVariableDeclaration" in {
      val v = new LocalVariableDeclaration(ann, Nil, typ,new  VariableDeclarator("")).visit()
      v.check(2, 2)
    }
    "declarations" - {
      "import" - {
        "LazyImport" in {
          val v = new  StaticLazyImportDeclaration("").visit()
          v.check(0, 1)
        }
        "SingleImport" in {
          val v = new StaticLazyImportDeclaration("").visit()
          v.check(0, 1)
        }
        "StaticImport" in {
          val v = new StaticLazyImportDeclaration("").visit()
         v.check(0, 1)
        }
        "StaticLazyImport" in {
          val v = new  StaticLazyImportDeclaration("").visit()
          v.check(0, 1)
        }
      }
      "initializers" - {
        "InstanceInitializerDeclaration" in {
          val v = new InstanceInitializerDeclaration(block).visit()
          v.check(2, 0)
        }
        "StaticInitializerDeclaration" in {
          val v = new  StaticInitializerDeclaration(block).visit()
          v.check(2, 0)
        }
      }
      "members" - {
        "AnnotationDefaultElementDeclaration" in {
          val v = new AnnotationDefaultElementDeclaration(ann, Nil, typ, "", expr).visit()
          v.check(2, 2)
        }
        "AnnotationElementDeclaration" in {
          val v = new AnnotationElementDeclaration(ann, Nil, typ, "").visit()
          v.check(2, 1)
        }
        "AnonymousEnumConstantDeclaration" in {
          val v = new  AnonymousEnumConstantDeclaration(ann, "", expr, decl).visit()
         v.check(1, 3)
        }
        "ConstructorDeclaration" in {
          val v = new  ConstructorDeclaration(ann, Nil, templParam, new ConstructorDeclarator("", argParam), typ, block).visit()
          v.check(5, 2)
        }
        "EnumConstantDeclaration" in {
          val v = new EnumConstantDeclaration(ann, "", expr).visit()
          v.check(1,2)
        }
        "FieldDeclaration" in {
          val v = new FieldDeclaration(ann, Nil, typ, new VariableDeclarator("")).visit()
          v.check(2, 2)
        }
        "MethodDeclaration" in {
          val v = new MethodDeclaration(ann, Nil, templParam, typ, new MethodDeclarator("", argParam), typ, stmt).visit()
          v.check(5, 3)
        }
      }
      "packages" - {
        "NamedPackageDeclaration" in {
          val v = new  NamedPackageDeclaration(ann, "").visit()
          v.check(1, 1)
        }
        "UnnamedPackageDeclaration$" in {
          val v= UnnamedPackageDeclaration.visit()
          v.check(0, 1)
        }
      }
      "types" - {
        "AnnotationDeclaration" in {
          val v = new AnnotationDeclaration(ann, Nil, "", decl).visit()
          v.check(1, 2)
        }
        "ClassDeclaration" in {
          val v = new ClassDeclaration(ann, Nil, "", templParam, typ, typ, decl).visit()
          v.check(4, 2)
        }
        "EmptyDeclaration" in {
         val v = EmptyDeclaration.visit()
          v.check(0, 1)
        }
        "EnumDeclaration" in {
          val v = new EnumDeclaration(ann, Nil, "", typ, decl).visit()
          v.check(2, 2)
        }
      }
    }
  }
}
