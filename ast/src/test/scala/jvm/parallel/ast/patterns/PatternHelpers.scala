package jvm.parallel.ast.patterns

private[patterns] trait PatternHelpers {
  implicit def toPatternNode(int: Int): PatternNode[Int] =
    if (int == 0) SimplePatternNode(Zero)
    else if (int == 1) SimplePatternNode(One)
    else throw new Exception("Invalid number: " + int)

  implicit def toBuilder(int: Int): Builder[Int] =
    if (int == 0) IntBuilder(0)
    else if (int == 1) IntBuilder(1)
    else throw new Exception("Invalid number: " + int)

  case object One extends Pattern[Int] {
    override def matches[B >: Int](a: B)(implicit context: Context): Boolean = a match {
      case 1 => true
      case _ => false
    }
  }

  case object Zero extends Pattern[Int] {
    override def matches[B >: Int](a: B)(implicit context: Context): Boolean = a match {
      case 0 => true
      case _ => false
    }
  }

  case class IntBuilder(value: Int) extends Builder[Int] {
    override def build(implicit context: Context): Seq[Int] = Seq(value)
  }

}
