package jvm.parallel.ast.patterns

import org.scalatest.prop.{Checkers, PropertyChecks}
import org.scalatest.{FreeSpec, Matchers}

class TransformationTest extends FreeSpec with PropertyChecks with Matchers with Checkers with PatternHelpers {
  implicit val context: Context = new Context

  "transformation should" - {
    "return empty list when pattern does not match" in {
      0 ~ 1 ~> Seq[Builder[Int]](0, 0) transform Seq(0) shouldBe Nil
    }
    "extract singular values from context by name" in {
      val zero = "zero" := Zero
      val one = "one" := One
      val pattern = zero ~ one
      val transformation = pattern ~> Seq[Builder[Int]]("one", "zero")
      transformation transform Seq(0, 1) shouldBe Seq(1, 0)
    }
    "extract multiple values from context by name" in {
      val zeros = "zeros" := Zero.+
      val ones = "ones" := One.+
      val pattern = zeros ~ ones
      val transformation = pattern ~> Seq[Builder[Int]]("ones", "zeros")
      transformation transform Seq(0, 0, 0, 1, 1, 1) shouldBe Seq(1, 1, 1, 0, 0, 0)
    }
    "perform simple transformation" in {
      val transformation = 0 ~ 1 ~> Seq[Builder[Int]](1, 0)
      transformation transform Seq(0, 1) shouldBe Seq(1, 0)
    }
  }
}
