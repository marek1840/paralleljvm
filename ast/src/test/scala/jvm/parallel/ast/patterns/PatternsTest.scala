package jvm.parallel.ast.patterns

import org.scalatest.prop.{Checkers, PropertyChecks}
import org.scalatest.{FreeSpec, Matchers}

class PatternsTest extends FreeSpec with PropertyChecks with Matchers with Checkers with PatternHelpers {
  implicit val context: Context = new Context
  "pattern recognition should work properly for" - {
    "singular patterns of" - {
      "correct input" in (0 process Seq(0) shouldBe Success(Seq(0), Nil))
      "wrong input" in (0 process Seq(1) shouldBe Failure(Nil, Seq(1)))
      "longer input" in (0 process Seq(0, 1) shouldBe Success(Seq(0), Seq(1)))
    }
    "and patterns" - {
      "correct input" in (1 ~ 0 process Seq(1, 0) shouldBe Success(Seq(1, 0), Nil))
      "wrong input" in (1 ~ 0 process Seq(0, 1) shouldBe Failure(Nil, Seq(0, 1)))
      "partially wrong input" in (1 ~ 0 process Seq(1, 1) shouldBe Failure(Seq(1), Seq(1)))
      "longer input" in (1 ~ 0 process Seq(1, 0, 0) shouldBe Success(Seq(1, 0), Seq(0)))
      "shorter input" in (1 ~ 0 process Seq(1) shouldBe Failure(Seq(1), Nil))
    }
    "or patterns" - {
      "correct input" in (1 | 0 process Seq(1) shouldBe Success(Seq(1), Nil))
      "wrong input" in (1 | 0 process Seq(0) shouldBe Failure(Nil, Seq(0)))
      "longer input" in (1 | 0 process Seq(1, 1, 1) shouldBe Success(Seq(1), Seq(1, 1)))
    }

    "greedy pattern" - {
      "correct input" in ((1 ~ 0).* process Seq(1, 0, 1, 0, 1, 0) shouldBe Success(Seq(1, 0, 1, 0, 1, 0), Nil))
      "wrong input" in ((1 ~ 0).* process Seq(0, 1, 0, 1, 0, 1, 0) shouldBe Success(Nil, Seq(0, 1, 0, 1, 0, 1, 0)))
      "partially correct input" in ((1 ~ 0).* process Seq(1, 0, 1, 0, 1, 1) shouldBe Success(Seq(1, 0, 1, 0, 1), Seq(1)))
      "empty input" in ((1 ~ 0).* process Nil shouldBe Success(Nil, Nil))
    }
    "lazy pattern" - {
      "correct input 1" in ((1 ~ 0).*? ~ 1 process Seq(1) shouldBe Success(Seq(1), Nil))
      "correct input 2" in ((1 ~ 0).+? ~ 1 process Seq(1, 0, 1) shouldBe Success(Seq(1, 0, 1), Nil))
      "correct input 3" in ((1 ~ 0).*? ~ 1 process Seq(1, 0, 1) shouldBe Success(Seq(1), Seq(0, 1)))
      "empty input 1" in ((1 ~ 0).+? ~ 1 process Nil shouldBe Failure(Nil, Nil))
      "empty input 2" in ((1 ~ 0).*? ~ 1 process Nil shouldBe Failure(Nil, Nil))
      "wrong input" in ((1 ~ 0).+? ~ 1 process Seq(1, 0) shouldBe Failure(Seq(1, 0), Nil))
      "shorter input" in ((1 ~ 0).*? ~ 1 process Seq(1, 0) shouldBe Success(Seq(1), Seq(0)))
    }

    "'and' & 'or' patterns composed" in {
      val pattern: PatternNode[Int] = (One | Zero) ~ One
      pattern process Seq(1, 1) shouldBe Success(Seq(1, 1), Nil)
      pattern process Seq(0, 1) shouldBe Success(Seq(0, 1), Nil)

      (One | Zero).* process Seq(1, 0, 0, 1) shouldBe Success(Seq(1, 0, 0, 1), Nil)
      (One | Zero).+ process Seq(1, 0, 0, 1) shouldBe Success(Seq(1, 0, 0, 1), Nil)
      (One | Zero).*? ~ (One ~ One) process Seq(1, 0, 0, 1, 1) shouldBe Success(Seq(1, 0, 0, 1, 1), Nil)
      (One | Zero).+? ~ (One ~ One) process Seq(1, 0, 0, 1, 1) shouldBe Success(Seq(1, 0, 0, 1, 1), Nil)
    }
  }
}
