package test;

import jvm.parallel.annotations.In;
import jvm.parallel.annotations.Out;
import jvm.parallel.annotations.Parallel;

public class MatrixMultiplication {

	@Parallel
	void multiply(
			@In float[] A,
			@In float[] B,
			@Out float[] C,
			int aRows, int aCols, int bCols) {
		for (int i = 0; i < aRows; ++i) {
			for (int j = 0; j < bCols; ++j) {
				float tmp = 0;

				for (int k = 0; k < aCols; ++k) {
					float a = A[i * aCols + k];
					float b = B[k * bCols + j];
					tmp += a * b;
				}

				C[i * bCols + j] += tmp;
			}
		}
	}
}