package jvm.parallel.engine.parsers

import jvm.parallel.engine.properties.{Empty, StackProperties, StackProperty, Top}
import org.parboiled2.Rule1

trait StackClauseParser extends TestPropertyParser {
  protected [this] def stackClause: Rule1[StackProperties] = rule {
    `@` ~ "Stack " ~ `(` ~ stackProperty.+(comma) ~ `)` ~> StackProperties
  }

  private[this] def stackProperty: Rule1[StackProperty] = rule {
    `@` ~ "Empty " ~ push(Empty)  |
      "top " ~ `=` ~ decimalNumber ~> ((value: Int) => Top(value))
  }
}
