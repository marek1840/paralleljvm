package jvm.parallel.engine.parsers

import org.parboiled2._
import shapeless.HNil

trait TestPropertyParser extends Parser {
  protected[this] def int2int(int: Int): Integer = int

  protected[this] def whitespace: Rule0 = rule {
    zeroOrMore(spacing)
  }

  private[this] def spacing: Rule0 = rule {
    anyOf(" \t\u000C") | newLine
  }

  private[this] def newLine: Rule0 = rule {
    "\r\n" | '\n'
  }

  protected[this] implicit def str2Rule(s: String): Rule0 =
    if (s.endsWith("  "))
      rule {
        str(s.dropRight(2)) ~ oneOrMore(spacing)
      }
    else if (s.endsWith(" "))
      rule {
        str(s.dropRight(1)) ~ whitespace
      }
    else rule {
      str(s)
    }


  protected[this] def `@`: Rule0 = rule("@ ")
  protected[this] def `(`: Rule0 = rule("( ")
  protected[this] def `)`: Rule0 = rule(") ")
  protected[this] def `[`: Rule0 = rule("[ ")
  protected[this] def `]`: Rule0 = rule("] ")
  protected[this] def `=`: Rule0 = rule("= ")
  protected[this] def `"`: Rule0 = rule("\" ")
  protected[this] def `"""`: Rule0 = rule("\"\"\" ")
  protected[this] def comma: Rule0 = rule(", ")

  protected[this] def decimalNumber: Rule1[Int] = rule {
    capture('0' | CharPredicate('1' to '9') ~ zeroOrMore(CharPredicate.Digit)) ~ whitespace ~> ((s: String) => Integer
      .parseInt
      (s))
  }

  protected[this] def text: Rule1[String] = rule( `"` ~ capture((!(comma | `)` | `"`) ~ ANY).*) ~ `"`)

  protected[this] abstract class Builder[A] {
    protected[Builder] implicit def option[B](b: B): Option[B] = Option(b)
    protected[Builder] def validate[B](b: B): Unit = if(b != null) throw new Exception("Duplicate specification")
    def build() : A
  }

  protected[this] def build[A] = (builder: Builder[A]) => builder.build()
}