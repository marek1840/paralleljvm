package jvm.parallel.engine.parsers

import jvm.parallel.engine.properties.ExecutionProperties
import org.parboiled2.Rule1

trait ExecutionClauseParser extends TestPropertyParser with StepClauseParser {
  protected[this] def executionClause: Rule1[ExecutionProperties] = rule {
    `@` ~ "Execution " ~ `(` ~ stepClause.*(comma) ~ `)` ~> ExecutionProperties
  }
}
