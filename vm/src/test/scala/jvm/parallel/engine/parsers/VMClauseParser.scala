package jvm.parallel.engine.parsers

import jvm.parallel.engine.properties.VMProperties
import org.parboiled2._

trait VMClauseParser extends TestPropertyParser {
  protected[this] def vmClause: Rule1[VMProperties] = rule {
    `@` ~ "VM " ~ `(` ~ {
      "memory " ~ `=` ~ decimalNumber ~> VMProperties
    } ~ `)`
  }
}