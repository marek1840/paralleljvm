package jvm.parallel.engine.parsers

import jvm.parallel.engine.properties._
import org.parboiled2.Rule1

trait MemoryClauseParser extends TestPropertyParser {
  protected[this] def memoryClause: Rule1[MemoryProperties] = rule {
    `@` ~ "Memory " ~ `(` ~ memoryProperty.*(comma) ~ `)` ~> MemoryProperties
  }

  private[this] def memoryProperty = rule {
    `@` ~ {
      "All " ~ `(` ~ decimalNumber ~ `)` ~> ((value: Int) => AllCells(value)) |
        "At " ~ `(` ~ decimalNumber ~ `comma` ~ decimalNumber ~ `)` ~> ((index: Int, value: Int) => At(index, value)) |
        "Range " ~ `(` ~
          "offset " ~ `=` ~ decimalNumber ~ comma ~
          "length " ~ `=` ~ decimalNumber ~ comma ~
          "values " ~ `=` ~ `[` ~ decimalNumber.*(comma) ~ `]` ~
      `)` ~> ((from:Int, to:Int, values:Seq[Int]) => Range(from, to, values.map(int2int)))
    }
  }


}
