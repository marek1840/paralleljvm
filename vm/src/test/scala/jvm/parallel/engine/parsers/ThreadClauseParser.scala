package jvm.parallel.engine.parsers

import jvm.parallel.engine.properties.{ExecutionProperties, ThreadProperties}
import org.parboiled2.{Rule, Rule1}
import shapeless.HNil

trait ThreadClauseParser extends TestPropertyParser with ExecutionClauseParser {
  private[this] type BuilderReduction = () => Rule[shapeless.::[ThreadBuilder, HNil], shapeless.::[ThreadBuilder, HNil]]

  protected[this] def threadClause: Rule1[ThreadProperties] = rule {
    `@` ~ "Thread " ~ `(` ~ push(new ThreadBuilder) ~ threadProperties ~ `)` ~> ((b:ThreadBuilder) => b.build())
  }

  private[this] def threadProperties = rule {
    threadProperty(name, exec).*(comma)
  }

  private[this] def threadProperty = (name: BuilderReduction, exec: BuilderReduction) => rule{
    name() | exec()
  }

  private[this] def name = () => rule {
    "name " ~ `=` ~ text ~> ((builder: ThreadBuilder, name: String) => builder.setName(name))
  }

  private[this] def exec = () => rule {
    executionClause ~> ((builder: ThreadBuilder, exec: ExecutionProperties) => builder.setExec(exec))
  }

  private[this] class ThreadBuilder extends Builder[ThreadProperties] {
    var name: String = _
    var exec: ExecutionProperties = _

    def setName(n: String) = {
      validate(name)
      name = n
      this
    }

    def setExec(e: ExecutionProperties) = {
      validate(exec)
      exec = e
      this
    }

    override def build(): ThreadProperties = new ThreadProperties(name, exec)
  }

}
