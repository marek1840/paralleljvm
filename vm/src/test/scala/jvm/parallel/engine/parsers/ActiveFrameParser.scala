package jvm.parallel.engine.parsers

import jvm.parallel.engine.properties.{ActiveFrameProperties, StackProperties}
import org.parboiled2.{Rule, Rule1}
import shapeless.HNil

trait ActiveFrameParser extends TestPropertyParser with StackClauseParser {
  private[this] type BuilderReduction = () => Rule[shapeless.::[ActiveFrameBuilder, HNil], shapeless.::[ActiveFrameBuilder, HNil]]

  def activeFrameClause: Rule1[ActiveFrameProperties] = rule {
    `@` ~ "ActiveFrame " ~ `(` ~ push(new ActiveFrameBuilder) ~ frameProperties ~ `)` ~> { (b: ActiveFrameBuilder) =>
      b.build()
    }
  }

  private[this] def frameProperties = rule {
    frameProperty(clazz, ip, stack).*(comma)
  }

  private[this] def frameProperty = (clazz: BuilderReduction, ip: BuilderReduction, stack: BuilderReduction) => rule {
    clazz() | ip() | stack()
  }

  private[this] def clazz: BuilderReduction = () => rule {
    "currentClass " ~ `=` ~ text ~> ((builder: ActiveFrameBuilder, clazz: String) => builder.setClass(clazz))
  }

  private[this] def ip: BuilderReduction = () => rule {
    "instructionPointer " ~ `=` ~ decimalNumber ~> ((builder: ActiveFrameBuilder, ip: Int) => builder.setIP(ip))
  }

  private[this] def stack: BuilderReduction = () => rule {
    stackClause ~> ((builder: ActiveFrameBuilder, stack: StackProperties) => builder.setStackProps(stack))
  }

  private[this] class ActiveFrameBuilder extends Builder[ActiveFrameProperties] {
    var currentClass: String = _
    var instructionPointer: Int = _
    var stackProps: StackProperties = _

    def setClass(str: String) = {
      validate(currentClass)
      currentClass = str
      this
    }

    def setIP(ip: Int) = {
      validate(instructionPointer)
      instructionPointer = ip
      this
    }

    def setStackProps(stack: StackProperties) = {
      validate(stackProps)
      stackProps = stack
      this
    }

    override def build(): ActiveFrameProperties =
      ActiveFrameProperties(currentClass, instructionPointer, stackProps)

  }

}


