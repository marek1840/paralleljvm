package jvm.parallel.engine.parsers

import jvm.parallel.engine.properties.TestCase
import org.parboiled2._

class TestCaseParser(override val input: ParserInput) extends TestPropertyParser with VMClauseParser with
                                                              SourceClauseParser with ThreadClauseParser {
  def testCaseClause: Rule1[TestCase] = rule {
    vmClause ~ sourceClause ~ {
      threadClause.+(comma) |
        push(Vector())
    } ~ whitespace ~ EOI ~> TestCase
  }
}