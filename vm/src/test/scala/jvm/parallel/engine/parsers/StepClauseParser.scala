package jvm.parallel.engine.parsers

import jvm.parallel.engine.properties.{ActiveFrameProperties, MemoryProperties, StepProperties}
import org.parboiled2.{Rule, Rule1}
import shapeless.HNil

trait StepClauseParser extends TestPropertyParser with MemoryClauseParser with ActiveFrameParser {
  private type BuilderReduction = () => Rule[shapeless.::[StepBuilder, HNil], shapeless.::[StepBuilder, HNil]]

  protected[this] def stepClause: Rule1[StepProperties] = rule {
    `@` ~ "Step " ~ `(` ~ push(new StepBuilder) ~ stepProperties ~ `)` ~> ((b: StepBuilder) => b.build())
  }

  private[this] def stepProperties = rule {
    stepProperty(number, memory, frame).*(comma)
  }

  private[this] def stepProperty = (number: BuilderReduction,
                                    memory: BuilderReduction,
                                    frame: BuilderReduction) => rule {
    number() | memory() | frame()
  }

  private[this] val number: BuilderReduction = () => rule {
    "number " ~ `=` ~ decimalNumber ~> ((builder: StepBuilder, value: Int) => builder.setNumber(value))
  }

  private[this] val memory: BuilderReduction = () => rule {
    memoryClause ~> ((builder: StepBuilder, props: MemoryProperties) => builder.setMemory(props))
  }

  private[this] val frame: BuilderReduction = () => rule {
    activeFrameClause ~> ((builder: StepBuilder, props: ActiveFrameProperties) => builder.setFrame(props))
  }

  private[this] class StepBuilder extends Builder[StepProperties] {
    var number: Integer = _
    var memory: MemoryProperties = _
    var activeFrame: ActiveFrameProperties = _

    def setNumber(i: Int): StepBuilder = {
      validate(number)
      number = i
      this
    }

    def setMemory(m: MemoryProperties): StepBuilder = {
      validate(memory)
      memory = m
      this
    }

    def setFrame(f: ActiveFrameProperties): StepBuilder = {
      validate(activeFrame)
      activeFrame = f
      this
    }

    override def build() = {
      StepProperties(number, memory, activeFrame)
    }
  }

}