package jvm.parallel.engine.parsers

import jvm.parallel.engine.properties._
import org.parboiled2._

trait SourceClauseParser extends TestPropertyParser {
  protected[this] def sourceClause: Rule1[SourceProperties] = rule {
    `@` ~ "Source " ~ `(` ~ "mainClass " ~ `=` ~ text ~ comma ~ sourceProperties ~ `)` ~> SourceProperties
  }

  private[this] def sourceProperties = rule {
    sourceProperty.*(comma)
  }

  private[this] def sourceProperty = rule {
    path | content | dir
  }

  private[this] def path = rule {
    `@` ~ "File " ~ `(` ~ text ~ `)` ~> File
  }

  private[this] def content = rule {
    `@` ~ "Content " ~ `(` ~ `"""` ~ capture((!`"""` ~ ANY).*) ~ `"""` ~ `)` ~> Content
  }

  private[this] def dir = rule {
    `@` ~ "Dir " ~ `(` ~ "path " ~ `=` ~ text ~ `)` ~> Directory
  }


}