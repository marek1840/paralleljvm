package jvm.parallel.engine.properties

import jvm.parallel.ast.name.Name

case class SourceProperties(mainClass:String, props: Seq[SourceProperty])

sealed trait SourceProperty
case class Directory(path:String) extends SourceProperty
case class File(path:String) extends SourceProperty
case class Content(content: String) extends SourceProperty