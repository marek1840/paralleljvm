package jvm.parallel.engine.properties

case class StackProperties(props: Seq[StackProperty])

sealed trait StackProperty
case object Empty extends StackProperty
case class Top(value:Integer) extends StackProperty