package jvm.parallel.engine.properties

case class ExecutionProperties(stepsProps: Seq[StepProperties])