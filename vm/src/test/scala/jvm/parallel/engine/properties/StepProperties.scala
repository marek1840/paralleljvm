package jvm.parallel.engine.properties

case class StepProperties(stepNumber: Int,
                          memoryProps: Option[MemoryProperties],
                          activeFrameProps: Option[ActiveFrameProperties])