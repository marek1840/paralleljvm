package jvm.parallel.engine.properties

case class ActiveFrameProperties(currentClassName: Option[String],
                                 instructionPointer: Option[Int],
                                 stackProps: Option[StackProperties])