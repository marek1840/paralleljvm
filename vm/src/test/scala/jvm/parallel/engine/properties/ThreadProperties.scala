package jvm.parallel.engine.properties

case class ThreadProperties(name: String,
                            executionProps: ExecutionProperties)