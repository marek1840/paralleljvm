package jvm.parallel.engine.properties

case class TestCase(configuration: VMProperties,
                    sourceConfiguration: SourceProperties,
                    threadProps: Seq[ThreadProperties])