package jvm.parallel.engine.properties

case class MemoryProperties(props: Iterable[MemoryProperty])

sealed trait MemoryProperty
case class AllCells(value: Integer) extends MemoryProperty
case class At(index:Int, value: Integer) extends MemoryProperty
case class Range(offset: Int, length:Int, values : Seq[Integer]) extends MemoryProperty
