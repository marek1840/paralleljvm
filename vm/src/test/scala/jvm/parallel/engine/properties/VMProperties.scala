package jvm.parallel.engine.properties

case class VMProperties(memorySize: Int)