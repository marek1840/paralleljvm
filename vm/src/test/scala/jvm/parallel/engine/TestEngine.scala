package jvm.parallel.engine

import java.io.InputStream

import jvm.parallel.ast.name.{Name, TokenCache}
import jvm.parallel.compiler.JavaCompiler
import jvm.parallel.compiler.metadata.TypeMetadata
import jvm.parallel.engine.parsers.TestCaseParser
import jvm.parallel.engine.properties._
import jvm.parallel.logging.Logger
import org.parboiled2.ParseError
import org.scalatest.prop.{Checkers, PropertyChecks}
import org.scalatest.{FreeSpec, Matchers}

import scala.io.Source
import scala.util.{Failure, Try}



object TestEngine extends Logger {


  val testCases = {
    val location: String = "/integration"
    val files = Source.fromInputStream(getClass.getResourceAsStream(location)).getLines().toStream
    val resolvedFiles = files.map(name => TestCaseHolder(location + "/" + name))
    val inputStreams = resolvedFiles.map(h => TestCaseHolder(h.name, stream = getClass.getResourceAsStream(h.name)))

    val contents = inputStreams.map(h => TestCaseHolder(h.name, content = Source.fromInputStream(h.stream).mkString))
    val parsers = contents.map(h => TestCaseHolder(h.name, parser = new TestCaseParser(h.content)))
    val parseResults = parsers.map(h => TestCaseHolder(h.name, parser = h.parser, result = h.parser.testCaseClause.run()))
    val (testCases, invalidTestCases) = parseResults.partition(_.result.isSuccess)

    reportFailedTests(invalidTestCases)

    testCases.map(h => (h.name, h.result.get))
  }

  def reportFailedTests(holders: Stream[TestCaseHolder]) = {
    holders.foreach { case TestCaseHolder(name, _, _, parser, Failure(error: ParseError)) => {
      val msg = parser.formatError(error)
      logger.error(s"[$name] - $msg")
    }
    case res => logger.error(s"Unexpected test case preprocessing result: $res")
    }
  }

  private[this] case class TestCaseHolder(name: String,
                                          stream: InputStream = null,
                                          content: String = null,
                                          parser: TestCaseParser = null,
                                          result: Try[TestCase] = null)

}
