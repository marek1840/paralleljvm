package jvm.parallel.compiler.phases.preprocessing

import jvm.parallel.ast.parsers.statement.Block
import jvm.parallel.parsers.ParserImplicits
import jvm.parallel.parsers.java.JavaParser
import org.scalatest.prop.{Checkers, PropertyChecks}
import org.scalatest.{FreeSpec, Matchers}

class VariableDefinitionSplitterTest extends FreeSpec with PropertyChecks with Matchers with Checkers with ParserImplicits {
  val mapper = new VariableDefinitionSplitter

  def parse(str: String) = JavaParser.parseStatement(str)

  "Variable definition splitter" - {
    "should not change 'int a;'" in {
      val stmt = parse("{int a;}")
      mapper.mapStmt(stmt) shouldBe stmt
    }

    "should expand 'int a = 3;' to 'int a; a = 3'" in {
      val stmt = parse("{int a = 3;}")
      mapper.mapStmt(stmt) match {
        case Block(s1 +: s2 +: Nil) =>
          s1 shouldBe parse("int a;")
          s2 shouldBe parse("a = 3;")
        case Block(ss) => fail("Expected block with two statements, got " + ss.size)
        case s => fail("Not even Block: " + s)
      }
    }

    "should expand nested blocks" in {
      val stmt = parse("{{int a = 3;}}")

      mapper.mapStmt(stmt) match {
        case Block(Block(s1 +: s2 +: Nil) +: Nil) =>
          s1 shouldBe parse("int a;")
          s2 shouldBe parse("a = 3;")
        case Block(ss) => fail("Expected block with inner block, got " + ss.getClass.getSimpleName)
        case s => fail("Not even Block: " + s)
      }
    }
  }
}
