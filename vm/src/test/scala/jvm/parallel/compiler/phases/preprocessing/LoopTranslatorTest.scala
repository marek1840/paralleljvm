package jvm.parallel.compiler.phases.preprocessing

import jvm.parallel.parsers.ParserImplicits
import jvm.parallel.parsers.java.JavaParser
import org.scalatest.prop.{Checkers, PropertyChecks}
import org.scalatest.{Matchers, FreeSpec, FunSuite}

class LoopTranslatorTest extends FreeSpec with PropertyChecks with Matchers with Checkers with ParserImplicits  {
  val mapper = new LoopTranslator
  def parse(str:String) = JavaParser.parseStatement(str)

  "Loop translator" - {
    "should not change 'while loop'" in {
      val stmt = parse("while(i < 3){}")
      mapper.mapStmt(stmt) shouldBe stmt
    }

    "should translate 'for loop''" in {
      val stmt = parse("for(i = 0 , j = 0; i < 100; ++i){j += i * 3;}")
      mapper.mapStmt(stmt) shouldBe parse(
        """{
          | i = 0;
          | j = 0;
          | while(i < 100){
          |   j += i * 3;
          |   ++i;
          | }
          |}
          |
        """.stripMargin)
    }

    // TODO nested
  }
}
