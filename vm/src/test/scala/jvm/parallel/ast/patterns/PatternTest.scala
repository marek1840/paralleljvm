package jvm.parallel.ast.patterns

import jvm.parallel.ast.parsers.expression.operators.BinaryOperator
import jvm.parallel.ast.parsers.statement.instructions.InstructionStatement
import jvm.parallel.parsers.ParserImplicits
import jvm.parallel.parsers.java.JavaParser
import org.scalatest.prop.{Checkers, PropertyChecks}
import org.scalatest.{FreeSpec, Matchers}

class PatternTest extends FreeSpec with PropertyChecks with Matchers with Checkers with ParserImplicits {
  implicit val context: Context = new Context

  def parse(str: String) = JavaParser.parseStatement(str)
  private def testCase[A <: InstructionStatement](statement: String, pattern: Pattern[A]) =
    statement in (pattern matches parse(statement) shouldBe true)

  "statements should match patterns " - {
//    testCase("a = b;", binding(select("a"), select("b")))
//    testCase("a += b;", augmentedBinding(select("a"), BinaryOperator.+, select("b")))
  }
}
