package jvm.parallel.compiler.phases.preprocessing

import jvm.parallel.ast.parsers.mapper.ASTMapper
import jvm.parallel.ast.parsers.statement.Block

class BlockFlattener extends ASTMapper {
  override def mapBlock(block: Block) = block match {
    case Block((b: Block) +: Nil) => b
    case b => b
  }
}
