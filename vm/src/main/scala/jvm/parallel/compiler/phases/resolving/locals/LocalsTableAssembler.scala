package jvm.parallel.compiler.phases.resolving.locals

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.declaration.members.MethodDeclaration
import jvm.parallel.ast.parsers.statement.declarator.{InitializedVariableDeclarator, VariableDeclarator}
import jvm.parallel.ast.parsers.statement.instructions.LabeledStatement
import jvm.parallel.ast.parsers.statement.parameter.FormalParameter
import jvm.parallel.ast.parsers.visitor.ASTVisitor
import jvm.parallel.compiler.context.Context

import scala.collection.mutable

class LocalsTableAssembler(implicit ctx: Context) extends ASTVisitor {
  private[this] var methods: Seq[MethodDeclaration] = Nil
  private[this] var locals: mutable.Map[Name, Int] = _

  private[this] def top = methods.head

  private[this] def push(m: MethodDeclaration) = {
    methods = m +: methods
    locals = ctx.locals(top)
  }

  private[this] def pop() = {
    methods = methods.tail
    if (methods.nonEmpty) locals = ctx.locals(top)
  }


  override def onEnter(m : MethodDeclaration): Unit = push(m)
  override def onExit(m : MethodDeclaration): Unit = pop()

  override def onEnter(p: FormalParameter) = p match {
    case FormalParameter(_, _, _, name) => addLocal(name)
  }

  override def visit(d: VariableDeclarator) = d match {
    case VariableDeclarator(name) => addLocal(name)
  }

  override def onEnter(d: InitializedVariableDeclarator) = d match {
    case InitializedVariableDeclarator(name, _) => addLocal(name)
  }

  private[this] def addLocal(name: Name): Unit = {
    if (!(locals contains name)) locals += (name -> locals.size)
  }
}
