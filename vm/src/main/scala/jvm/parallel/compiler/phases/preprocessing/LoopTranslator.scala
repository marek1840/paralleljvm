package jvm.parallel.compiler.phases.preprocessing

import jvm.parallel.ast.parsers.mapper.ASTMapper
import jvm.parallel.ast.parsers.statement.instructions.discardable.literals.BooleanLiteral
import jvm.parallel.ast.parsers.statement.instructions.loop.{For, While}
import jvm.parallel.ast.parsers.statement.{Block, Statement}

class LoopTranslator extends ASTMapper {
  override def mapStmt(stmt: Statement): Statement = stmt match {
    case For(inits, condition, after, body) =>
      val cond = condition.getOrElse(BooleanLiteral("true"))
      val newBody = body match {
        case Block(ss) => Block(ss ++ after)
        case s => Block(s +: after)
      }
      val loop = While(cond, mapBlock(newBody))

      Block(inits :+ loop)

    case _ => super.mapStmt(stmt)
  }
}
