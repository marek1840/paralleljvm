package jvm.parallel.compiler.phases.preprocessing

import jdk.nashorn.internal.ir.LiteralNode.PrimitiveLiteralNode
import jvm.parallel.ast.parsers.expression.types.primitives.VoidPrimitive
import jvm.parallel.ast.parsers.statement.Block
import jvm.parallel.ast.parsers.statement.declaration.members.MethodDeclaration
import jvm.parallel.ast.parsers.statement.instructions.discardable.literals.VoidLiteral
import jvm.parallel.ast.parsers.statement.instructions.flow.EmptyReturn
import jvm.parallel.ast.parsers.visitor.ASTVisitor

class ProcedureReturnAppender extends ASTVisitor{
  override def onEnter(m: MethodDeclaration) : Unit = (m.resultType, m.body) match {
    case (_:VoidPrimitive, Block(stmts)) if stmts.last != EmptyReturn =>
      m.body = Block(stmts :+ EmptyReturn)
  }
}
