package jvm.parallel.compiler.phases

import jvm.parallel.ast.parsers.mapper.ASTMapper
import jvm.parallel.ast.parsers.statement.declaration.types._
import jvm.parallel.ast.parsers.statement.declaration.{CompilationUnitDeclaration, Declaration}
import jvm.parallel.ast.parsers.visitor.ASTVisitor
import jvm.parallel.compiler.context.Context
import jvm.parallel.logging.Logger

import scala.collection.mutable

object FlattenTypes extends Phase with Logger {
  def process(implicit ctx: Context): Unit = {

    ctx map TypeFlattener

//    logger.debug(s"Flattened types.")

  }

  private object TypeFlattener extends ASTMapper with Logger{
    override def mapCompilationUnit(c: CompilationUnitDeclaration): CompilationUnitDeclaration = {
      val typeExtractor = new TypeExtractor
      c.accept(typeExtractor)

      val types: mutable.Buffer[TypeDeclaration] = typeExtractor.types

//      logger.info(s"Extracted ${types.size} types" )
      CompilationUnitDeclaration(c.`package`, c.imports, c.declarations ++ types)
    }
  }


  /**
   * Builds internal buffer containing all types declared in processed ast
   */
  private class TypeExtractor extends ASTVisitor {
    val types = mutable.Buffer[TypeDeclaration]()

    override def onEnter(declaration: EnumDeclaration): Unit = types += declaration
    override def onEnter(declaration: ClassDeclaration): Unit = types += declaration
    override def onEnter(declaration: InterfaceDeclaration): Unit = types += declaration
    override def onEnter(declaration: AnnotationDeclaration): Unit = types += declaration

    override def onExit(declaration: EnumDeclaration): Unit =
      declaration.members = removeTypes(declaration.members)

    override def onExit(declaration: ClassDeclaration): Unit =
      declaration.members = removeTypes(declaration.members)

    override def onExit(declaration: InterfaceDeclaration): Unit =
      declaration.members = removeTypes(declaration.members)

    override def onExit(declaration: AnnotationDeclaration): Unit =
      declaration.members = removeTypes(declaration.members)

    override def onExit(cu: CompilationUnitDeclaration): Unit =
      cu.declarations = removeTypes(cu.declarations)

    private[this] def removeTypes[A <: Declaration](ds: Seq[A]): Seq[A] = {
      ds filterNot (_.isInstanceOf[TypeDeclaration])
    }
  }

}
