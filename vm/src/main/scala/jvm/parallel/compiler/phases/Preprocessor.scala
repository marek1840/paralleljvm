package jvm.parallel.compiler.phases

import jvm.parallel.compiler.context.Context
import jvm.parallel.compiler.phases.preprocessing._
import jvm.parallel.logging.Logger

object Preprocessor extends Phase with Logger {
  private[this] val emtyStatementRemover = new EmptyStatementsRemover
  private[this] val emptyDeclarationRemover = new EmptyDeclarationRemover

  private[this] val variableDefinitionSplitter = new VariableDefinitionSplitter
  private[this] val blockFlattener = new BlockFlattener
  private[this] val augmentationRemover = new AugmentationRemover
  private[this] val procedureReturnAppender = new ProcedureReturnAppender
  private[this] val loopTranslator = new LoopTranslator

  def process(implicit ctx: Context): Unit = {
    ctx.accept(
      emptyDeclarationRemover,
      emtyStatementRemover,
      procedureReturnAppender
    )

    ctx.map(
      variableDefinitionSplitter,
      loopTranslator,
      augmentationRemover,
      blockFlattener
    )

//    logger.info("Finished preprocessing")
  }
}
