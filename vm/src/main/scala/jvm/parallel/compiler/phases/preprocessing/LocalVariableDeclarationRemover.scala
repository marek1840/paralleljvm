package jvm.parallel.compiler.phases.preprocessing

import jvm.parallel.ast.parsers.statement.Block
import jvm.parallel.ast.parsers.statement.instructions.LocalVariableDeclaration
import jvm.parallel.ast.parsers.visitor.ASTVisitor

class LocalVariableDeclarationRemover extends ASTVisitor {
  override def onEnter(block: Block) = {
    block.statements = block.statements.filter {
      case _: LocalVariableDeclaration => false
      case _ => true
    }
  }
}
