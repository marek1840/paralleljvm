package jvm.parallel.compiler.phases.preprocessing

import jvm.parallel.ast.parsers.mapper.ASTMapper
import jvm.parallel.ast.parsers.statement.declarator.{InitializedVariableDeclarator, VariableDeclarator}
import jvm.parallel.ast.parsers.statement.instructions.loop.For
import jvm.parallel.ast.parsers.statement.instructions.{InstructionStatement, LocalVariableDeclaration}
import jvm.parallel.ast.parsers.statement.instructions.discardable.DiscardableExpression
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.assignment.Binding
import jvm.parallel.ast.parsers.statement.instructions.discardable.reference.Select
import jvm.parallel.ast.parsers.statement.{Block, Statement}

class VariableDefinitionSplitter extends ASTMapper {
  override def mapBlock(block: Block) = {
    val stmts = block.statements.flatMap(splitMultipleDeclarations)
    Block(mapStatements(stmts))
  }

    override def mapInstruction(node: InstructionStatement) : InstructionStatement = node match {
    case For(is, c, us, b) =>
      val inits = is.flatMap(splitMultipleDeclarations)
      For(mapStatements(inits), mapExpr(c), mapStatements(us), mapStmt(b))
    case _ => super.mapInstruction(node)
  }

  private[this] def splitMultipleDeclarations(stmt: Statement): Seq[Statement] = stmt match {
    case LocalVariableDeclaration(as, ms, t, ds) =>
      ds.map(d => LocalVariableDeclaration(as, ms, t, d :: Nil))
        .flatMap(splitDeclarationAndDefinition)
    case _ => stmt :: Nil
  }

  private[this] def splitDeclarationAndDefinition(stmt: Statement): Seq[Statement] = stmt match {
    case LocalVariableDeclaration(as, ms, t, InitializedVariableDeclarator(name, expr) :: _) =>
      LocalVariableDeclaration(as, ms, t, VariableDeclarator(name) :: Nil) ::
        Binding(Select(name), expr) :: Nil
    case _ => stmt :: Nil
  }

}
