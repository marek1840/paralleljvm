package jvm.parallel.compiler.phases

import jvm.parallel.compiler.context.Context

trait Phase {
  def process(implicit ctx: Context): Unit
}
