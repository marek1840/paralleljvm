package jvm.parallel.compiler.phases.preprocessing

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.declarator.MethodDeclarator
import jvm.parallel.ast.parsers.visitor.ASTVisitor

class MemberNameQualifier(val name: Name) extends ASTVisitor {
  override def onEnter(declarator: MethodDeclarator) = {
    val methodName: Name = declarator.name
    declarator.name = name withMethod methodName
  }

}
