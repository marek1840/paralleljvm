package jvm.parallel.compiler.phases.preprocessing

import jvm.parallel.ast.parsers.statement.declaration.types._
import jvm.parallel.ast.parsers.statement.declaration.{CompilationUnitDeclaration, Declaration}
import jvm.parallel.ast.parsers.visitor.ASTVisitor

class EmptyDeclarationRemover extends ASTVisitor {
  private[this] def removeEmptyDeclarations[A <: Declaration](seq: Seq[A]): Seq[A] = seq.filter {
    case EmptyDeclaration => false
    case _ => true
  }

  override def onEnter(declaration: EnumDeclaration): Unit = {
    declaration.members = removeEmptyDeclarations(declaration.members)
  }

  override def onEnter(declaration: InterfaceDeclaration): Unit = {
    declaration.members = removeEmptyDeclarations(declaration.members)
  }

  override def onEnter(declaration: ClassDeclaration): Unit = {
    declaration.members = removeEmptyDeclarations(declaration.members)
  }

  override def onEnter(declaration: AnnotationDeclaration): Unit = {
    declaration.members = removeEmptyDeclarations(declaration.members)
  }

  override def onEnter(declaration: CompilationUnitDeclaration): Unit = {
    declaration.declarations = removeEmptyDeclarations(declaration.declarations)
  }
}
