package jvm.parallel.compiler.phases

import jvm.parallel.compiler.context.Context
import jvm.parallel.compiler.phases.preprocessing.LocalVariableDeclarationRemover
import jvm.parallel.compiler.phases.resolving.locals.{LocalVariableMapper, LocalsTableAssembler}
import jvm.parallel.logging.Logger

object SymbolResolver extends Phase with Logger {
  private[this] val localVariableDeclarationRemover = new LocalVariableDeclarationRemover

  def process(implicit ctx: Context): Unit = {
    //TODO Rewrite using symbol tables
    //TODO make symbol table return A <: DiscardableExpression (maybe symbol reference?)
    val localsTablePopulator = new LocalsTableAssembler
    val variableMapper = new LocalVariableMapper

    ctx.accept(localsTablePopulator)
    ctx.map(variableMapper)
//    ctx.accept(localVariableDeclarationRemover)
  }
}
