package jvm.parallel.compiler.phases.resolving.names

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.declaration.types._
import jvm.parallel.ast.parsers.statement.declarator.MethodDeclarator
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.MethodInvocation
import jvm.parallel.ast.parsers.visitor.ASTVisitor

class NameQualifier(n: Name) extends ASTVisitor {
  private[this] var nameStack: Seq[Name] = n :: Nil

  private[this] def name = nameStack.head

  private[this] def push(cl: TypeDeclaration): Unit = {
    nameStack = cl.name +: nameStack
  }

  private[this] def pop(): Unit = nameStack = {
    nameStack.tail
  }

  override def onEnter(cl: ClassDeclaration): Unit = {
    cl.name = name withType cl.name
    push(cl)
  }

  override def onEnter(cl: InterfaceDeclaration): Unit = {
    cl.name = name withType cl.name
    push(cl)
  }

  override def onEnter(cl: AnnotationDeclaration): Unit = {
    cl.name = name withType cl.name
    push(cl)
  }

  override def onEnter(cl: EnumDeclaration): Unit = {
    cl.name = name withType cl.name
    push(cl)
  }

  override def onExit(cl: ClassDeclaration): Unit = pop()
  override def onExit(cl: InterfaceDeclaration): Unit = pop()
  override def onExit(cl: AnnotationDeclaration): Unit = pop()
  override def onExit(cl: EnumDeclaration): Unit = pop()

  override def onEnter(dec: MethodDeclarator): Unit = {
    dec.name = name withMethod dec.name
  }

  //TODO fix when supporting more than one type
  override def onEnter(inv: MethodInvocation): Unit = {
    inv.name = name withMethod inv.name
  }
}
