package jvm.parallel.compiler.phases.resolving.names

import jvm.parallel.ast.name.{Name, Token, TokenCache}
import jvm.parallel.ast.parsers.mapper.ASTMapper

class Tokenizer extends ASTMapper {
  private[this] def tokenize(name: Name): Token = TokenCache.fromName(name)

  override def mapName(n: Name) = tokenize(n)
}
