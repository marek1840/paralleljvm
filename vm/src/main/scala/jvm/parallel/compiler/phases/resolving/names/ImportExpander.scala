package jvm.parallel.compiler.phases.resolving.names

import jvm.parallel.ast.parsers.expression.types.annotations.{MarkerAnnotation, NormalAnnotation, SingleElementAnnotation}
import jvm.parallel.ast.parsers.statement.declaration.types.ClassDeclaration
import jvm.parallel.ast.parsers.visitor.ASTVisitor

class ImportExpander(val ctx: ImportContext) extends ASTVisitor {

  override def visit(annotation: MarkerAnnotation): Unit = {
    ctx.imports.get(annotation.name).foreach(annotation.name = _)
  }

  override def onEnter(annotation: SingleElementAnnotation): Unit = {
    ctx.imports.get(annotation.name).foreach(annotation.name = _)
  }

  override def onEnter(annotation: NormalAnnotation): Unit = {
    ctx.imports.get(annotation.name).foreach(annotation.name = _)
  }

  override def onEnter(cl: ClassDeclaration): Unit = {
    ctx.imports.get(cl.name).foreach(cl.name = _)
  }
}
