package jvm.parallel.compiler.phases.resolving.locals

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.mapper.ASTMapper
import jvm.parallel.ast.parsers.statement.declaration.members.MethodDeclaration
import jvm.parallel.ast.parsers.statement.instructions.discardable.DiscardableExpression
import jvm.parallel.ast.parsers.statement.instructions.discardable.reference.{LocalVariableReference, Select}
import jvm.parallel.ast.parsers.visitor.ASTVisitor
import jvm.parallel.compiler.context.Context

import scala.collection.mutable

class LocalVariableMapper(implicit ctx: Context) extends ASTMapper {
  private[this] var localsTable : mutable.Map[Name, Int] = _
  private[this] def local(name:Name) : LocalVariableReference = {
    val i: Int = localsTable(name)
    LocalVariableReference(name, i)
  }
  override def mapMethod(m : MethodDeclaration): MethodDeclaration = {
    localsTable = ctx.locals(m)
    super.mapMethod(m)
  }

  override def mapDiscardable(s: DiscardableExpression): DiscardableExpression = s match {
    case Select(name) if localsTable contains name => local(name)
    case Select(_) => throw new Exception(s"Unresolved selection: $s")

    case _ => super.mapDiscardable(s)
  }
}
