package jvm.parallel.compiler.phases.preprocessing

import jvm.parallel.ast.parsers.expression.BinaryOperations
import jvm.parallel.ast.parsers.mapper.ASTMapper
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.assignment.{AugmentedBinding, Binding}
import jvm.parallel.ast.parsers.statement.{Block, Statement}

class AugmentationRemover extends ASTMapper {

  override def mapBlock(b: Block): Block = {
    val stmts = b.statements.map(removeAugmentation)
    Block(mapStatements(stmts))
  }

  def removeAugmentation(stmt: Statement): Statement = stmt match {
    case AugmentedBinding(target, operator, source) =>
      Binding(target, BinaryOperations(Seq(operator), Seq(target, source)))
    case _ => stmt
  }
}
