package jvm.parallel.compiler.phases.preprocessing

import jvm.parallel.ast.parsers.statement.Block
import jvm.parallel.ast.parsers.statement.instructions.EmptyStatement
import jvm.parallel.ast.parsers.visitor.ASTVisitor

class EmptyStatementsRemover extends ASTVisitor {
  override def onEnter(block: Block): Unit = {
    block.statements = block.statements.filter {
      case EmptyStatement => false
      case _ => true
    }
  }

}
