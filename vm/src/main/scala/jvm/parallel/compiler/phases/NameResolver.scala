package jvm.parallel.compiler.phases

import jvm.parallel.ast.parsers.visitor.AggregatedASTVisitor
import jvm.parallel.compiler.context.Context
import jvm.parallel.compiler.phases.resolving.names._
import jvm.parallel.logging.Logger

object NameResolver extends Phase with Logger {
  private[this] val tokenizer = new Tokenizer
  private[this] val importExpansion = new ImportExpansion

  def process(implicit ctx: Context): Unit = {
    val packageName = PackageNameResolver.reduce(ctx.ast)

    ctx accept(
      new NameQualifier(packageName),
      importExpansion
      )

    ctx.map(tokenizer)

//    logger.debug("Resolved names")

  }


  class ImportExpansion extends AggregatedASTVisitor {
    val importContext = new ImportContext

    visitors += new ImportResolver(importContext)
    visitors += new ImportExpander(importContext)
  }

}
