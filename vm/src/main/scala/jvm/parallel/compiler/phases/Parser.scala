package jvm.parallel.compiler.phases

import jvm.parallel.compiler.context.Context
import jvm.parallel.logging.Logger
import jvm.parallel.parsers.java.JavaParser

object Parser extends Logger {
  def apply(value: String): Context = {
    val result = JavaParser.parseCompilationUnit(value)

//    logger.debug("Parsed " + result)

    new Context(result)
  }
}
