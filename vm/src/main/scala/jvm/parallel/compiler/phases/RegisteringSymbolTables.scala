package jvm.parallel.compiler.phases

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.statement.declaration.CompilationUnitDeclaration
import jvm.parallel.ast.parsers.statement.declaration.members.MethodDeclaration
import jvm.parallel.ast.parsers.statement.declaration.types.{AnnotationDeclaration, ClassDeclaration, EnumDeclaration, InterfaceDeclaration}
import jvm.parallel.ast.parsers.statement.declarator.{InitializedVariableDeclarator, VariableDeclarator}
import jvm.parallel.ast.parsers.statement.instructions.LocalVariableDeclaration
import jvm.parallel.ast.parsers.statement.modifers.Modifier
import jvm.parallel.ast.parsers.statement.parameter.FormalParameter
import jvm.parallel.ast.parsers.statement.{Block, Statement}
import jvm.parallel.ast.parsers.visitor.ASTVisitor
import jvm.parallel.compiler.context.{Context, NameSpace, SymbolTable}
import jvm.parallel.logging.Logger
import jvm.parallel.vm2.TypeSignature

object RegisteringSymbolTables extends Phase with Logger {
  override def process(implicit ctx: Context): Unit = {
    ctx accept (
      new SymbolTableCreator,
      new SymbolTableFiller
      )
  }

  private class SymbolTableCreator(implicit ctx: Context) extends ASTVisitor {
    private[this] var parents = Seq[Statement]()

    private[this] def top = parents.headOption.orNull

    private[this] def pop() = parents = parents.tail

    private[this] def push(stmt: Statement): Unit = {
      ctx.addSymbolTable(stmt, top)
      parents = stmt +: parents
    }

    override def onEnter(declaration: EnumDeclaration): Unit = push(declaration)
    override def onEnter(declaration: InterfaceDeclaration): Unit = push(declaration)
    override def onEnter(declaration: ClassDeclaration): Unit = push(declaration)
    override def onEnter(declaration: AnnotationDeclaration): Unit = push(declaration)
    override def onEnter(declaration: CompilationUnitDeclaration): Unit = push(declaration)
    override def onEnter(method: MethodDeclaration): Unit = push(method)
    override def onEnter(block: Block): Unit = push(block)


    override def onExit(declaration: CompilationUnitDeclaration): Unit = pop()
    override def onExit(declaration: EnumDeclaration): Unit = pop()
    override def onExit(declaration: InterfaceDeclaration): Unit = pop()
    override def onExit(declaration: ClassDeclaration): Unit = pop()
    override def onExit(declaration: AnnotationDeclaration): Unit = pop()
    override def onExit(method: MethodDeclaration): Unit = pop()
    override def onExit(block: Block): Unit = pop()
  }

  class SymbolTableFiller(implicit val ctx: Context) extends ASTVisitor {
    private[this] var tables: Seq[SymbolTable] = Nil

    private[this] def push(stmt: Statement): Unit = {
      tables = ctx.getSymbolTable(stmt) +: tables
    }

    private[this] def pop(): Unit = tables = tables.tail
    private[this] def top = tables.head

    private[this] def addVariable(name: Name,
                          signature: TypeSignature,
                          modifiers: Seq[Modifier] = Nil,
                          annotations: Seq[Annotation] = Nil) = {
      top.add(name, signature, modifiers, annotations)(NameSpace.Variable)
    }

    private[this] def addMethod(name: Name,
                                signature: TypeSignature,
                                modifiers: Seq[Modifier] = Nil,
                                annotations: Seq[Annotation] = Nil) = {
      top.add(name, signature, modifiers, annotations)(NameSpace.Method)
    }

    override def onEnter(block: Block): Unit = push(block)
    override def onEnter(declaration: ClassDeclaration): Unit = push(declaration)
    override def onEnter(declaration: InterfaceDeclaration): Unit = push(declaration)
    override def onEnter(declaration: EnumDeclaration): Unit = push(declaration)
    override def onEnter(declaration: AnnotationDeclaration): Unit = push(declaration)
    override def onEnter(declaration: CompilationUnitDeclaration): Unit = push(declaration)
    override def onEnter(method: MethodDeclaration): Unit = {
      val sig = SignatureResolver reduce method
      val name = method.declarator.name
      addMethod(name, sig, method.modifiers, method.annotations)
      push(method)
    }

    override def onExit(block: Block): Unit = pop()
    override def onExit(declaration: ClassDeclaration): Unit = pop()
    override def onExit(declaration: InterfaceDeclaration): Unit = pop()
    override def onExit(declaration: EnumDeclaration): Unit = pop()
    override def onExit(declaration: AnnotationDeclaration): Unit = pop()
    override def onExit(declaration: CompilationUnitDeclaration): Unit = pop()
    override def onExit(declaration: MethodDeclaration): Unit = pop()

    override def onEnter(node: LocalVariableDeclaration) = node match {
      case LocalVariableDeclaration(as, ms, t, ds) =>
        val sig = TypeSignature(t)
        ds foreach {
          case VariableDeclarator(name) => addVariable(name, sig, ms, as)
          case InitializedVariableDeclarator(name, _) => addVariable(name, sig, ms, as)
        }
      case _ => unsupported(node)
    }

    override def onEnter(param : FormalParameter) = param match {
      case FormalParameter(as, ms, t, name) => addVariable(name, TypeSignature(t), ms, as)
    }
  }
}
