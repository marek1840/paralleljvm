package jvm.parallel.compiler.phases.resolving.names

import jvm.parallel.ast.name.Name

import scala.collection.mutable

class ImportContext {
  // simpleName -> [package].[simpleName]
  val imports = mutable.Map[Name, Name]()

  def addImport(qualifier: Name): Unit = {
    val simpleName = qualifier.last

    imports get simpleName match {
      case Some(qName) if qName equals qualifier =>
      case Some(qName) =>
      case None => imports += (simpleName -> qualifier)
    }

  }
}
