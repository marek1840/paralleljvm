package jvm.parallel.compiler.phases.resolving.names

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.ASTReducer
import jvm.parallel.ast.parsers.statement.declaration.CompilationUnitDeclaration
import jvm.parallel.ast.parsers.statement.declaration.packages.{NamedPackageDeclaration, PackageDeclaration, UnnamedPackageDeclaration}

object PackageNameResolver extends ASTReducer[CompilationUnitDeclaration, Name] {
  override def reduce(node: CompilationUnitDeclaration): OUT = node.`package` match {
    case _: UnnamedPackageDeclaration.type => new Name(Nil)
    case p: NamedPackageDeclaration => p.name
  }
}
