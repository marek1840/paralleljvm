package jvm.parallel.compiler.phases

import jvm.parallel.ast.TreeTraversalException
import jvm.parallel.ast.parsers.ASTReducer
import jvm.parallel.ast.parsers.statement.declaration.members.MethodDeclaration
import jvm.parallel.ast.parsers.statement.parameter.{FormalParameter, Parameter}
import jvm.parallel.vm2.TypeSignature

object SignatureResolver extends ASTReducer[MethodDeclaration, TypeSignature.Method] {
  override def reduce(node: MethodDeclaration): OUT = {
    val parameters = node.declarator.parameters.map(parameterToType)
    val resultType: TypeSignature = TypeSignature(node.resultType)
    TypeSignature.Method(parameters, resultType)
  }

  private[this] def parameterToType(param: Parameter): TypeSignature = param match {
    case FormalParameter(_, _, t, _) => TypeSignature(t)
    case _ => throw new TreeTraversalException(s"Unsupported node of type: ${param.getClass.getSimpleName}")
  }
}
