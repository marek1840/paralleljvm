package jvm.parallel.compiler.phases.resolving.names

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.declaration.imports.{SingleImportDeclaration, StaticImportDeclaration}
import jvm.parallel.ast.parsers.visitor.ASTVisitor


class ImportResolver(val ctx: ImportContext) extends ASTVisitor {
  def register(node: {def name: Name}) = ctx.addImport(node.name)

  override def visit(i: SingleImportDeclaration): Unit = register(i)

  override def visit(i: StaticImportDeclaration): Unit = register(i)
}
