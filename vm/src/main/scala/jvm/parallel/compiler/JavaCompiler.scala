package jvm.parallel.compiler

import jvm.parallel.ast.parsers.ASTReducer
import jvm.parallel.ast.parsers.statement.declaration.members.MethodDeclaration
import jvm.parallel.ast.parsers.statement.declaration.types.{ClassDeclaration, TypeDeclaration}
import jvm.parallel.compiler.metadata.{MethodMetadata, TypeMetadata}
import jvm.parallel.compiler.phases._

class JavaCompiler extends MethodCompiler {
  def compile(source: String): Iterable[TypeMetadata] = {
    implicit val ctx = Parser(source)
    Preprocessor.process
    NameResolver.process
    FlattenTypes.process
    RegisteringSymbolTables.process
    SymbolResolver.process

    ctx.ast.declarations.map(TypeMetadataExtractor reduce)
  }

  object TypeMetadataExtractor extends ASTReducer[TypeDeclaration, TypeMetadata] {
    override def reduce(node: TypeDeclaration): OUT = node match {
      case ClassDeclaration(_, _, name, _, _, _, members) =>
        val methodsMetadata = members
          .filter(_.isInstanceOf[MethodDeclaration])
          .map(_.asInstanceOf[MethodDeclaration])
          .map(MethodMetadataExtractor reduce)

        TypeMetadata(name, methodsMetadata)
      case _ => unsupportedNode(node)
    }
  }

  object MethodMetadataExtractor extends ASTReducer[MethodDeclaration, MethodMetadata] {
    override def reduce(node: MethodDeclaration): OUT = {
      val sig = SignatureResolver reduce node

      MethodMetadata(sig, node, 1000, 1000)
    }
  }

}
