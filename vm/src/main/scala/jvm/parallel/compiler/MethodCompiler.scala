package jvm.parallel.compiler

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.declaration.members.MethodDeclaration
import jvm.parallel.compiler.context.Context
import jvm.parallel.compiler.metadata.MethodMetadata

class MethodCompiler {
  def compileMethod(m: MethodDeclaration, className: Name)(implicit ctx:Context): MethodMetadata = {
//    val method = LocalVariableResolver(m)
//    compile(method, className)
    ???
  }

  private[this] def compile(method: MethodDeclaration, className: Name)(implicit ctx:Context): MethodMetadata = {
//    val signature = SignatureResolver(method)

    //TODO it can be extracted earlier and stored in context
    val stackSize = 100
    val localsAmount = 100
    val parameterAmount = method.declarator.parameters.size

//    MethodMetadata(signature, method, stackSize, localsAmount, parameterAmount)
    ???
  }
}
