package jvm.parallel.compiler.metadata

import jvm.parallel.ast.name.Name

case class TypeMetadata(name: Name,
                        methods: Seq[MethodMetadata])