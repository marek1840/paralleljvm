package jvm.parallel.compiler.metadata

import jvm.parallel.ast.parsers.statement.declaration.members.MethodDeclaration
import jvm.parallel.vm2.TypeSignature

case class MethodMetadata(signature: TypeSignature.Method,
                          ast: MethodDeclaration,
                          stackSize: Int,
                          localsAmount: Int)