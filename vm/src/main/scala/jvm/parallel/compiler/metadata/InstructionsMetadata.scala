package jvm.parallel.compiler.metadata

protected[compiler] case class InstructionsMetadata(stackSize: Int, localsAmount: Int)