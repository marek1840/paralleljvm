package jvm.parallel.compiler.context

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.statement.modifers.Modifier
import jvm.parallel.vm2.TypeSignature

final case class SymbolEntry(name: Name,
                             signature: TypeSignature,
                             modifiers: Seq[Modifier] = Nil,
                             annotations:Seq[Annotation] = Nil) {
  override def equals(o: scala.Any): Boolean = o match {
    case SymbolEntry(n, s, _, _) => name == n && s == signature
    case _ => false
  }

  override def hashCode(): Int = 3 * {
    name.hashCode + 7 * {
      signature.hashCode()
    }
  }
}