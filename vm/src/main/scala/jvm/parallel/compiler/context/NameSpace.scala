package jvm.parallel.compiler.context

trait NameSpace

object NameSpace {
  case object Variable extends NameSpace
  case object Method extends NameSpace

}