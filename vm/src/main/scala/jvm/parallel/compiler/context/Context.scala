package jvm.parallel.compiler.context

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.expression.types.primitives.{FloatPrimitive, IntegerPrimitive, VoidPrimitive}
import jvm.parallel.ast.parsers.expression.types.references.{ArrayType, ClassType}
import jvm.parallel.ast.parsers.mapper.ASTMapper
import jvm.parallel.ast.parsers.statement.Statement
import jvm.parallel.ast.parsers.statement.declaration.CompilationUnitDeclaration
import jvm.parallel.ast.parsers.statement.declaration.members.MethodDeclaration
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.MethodInvocation
import jvm.parallel.ast.parsers.statement.instructions.discardable.reference.Select
import jvm.parallel.ast.parsers.visitor.ASTVisitor
import jvm.parallel.vm2.TypeSignature

import scala.collection.mutable

class Context(var ast: CompilationUnitDeclaration) {
  private[this] val symbolTables = mutable.Map[Statement, SymbolTable]()
  private[this] val localVariables = mutable.Map[MethodDeclaration, mutable.Map[Name, Int]]()

  def addSymbolTable(stmt: Statement, parent: Statement): SymbolTable = {
    val parentTable = if (parent == null) null else symbolTables(parent)
    val newSymbolTable = new SymbolTable(parentTable, stmt)
    symbolTables.getOrElseUpdate(stmt, newSymbolTable)
  }

  def getSymbolTable(stmt: Statement): SymbolTable = symbolTables.getOrElse(stmt, throw new Exception("No Table for: " + stmt))

  def locals(m: MethodDeclaration): mutable.Map[Name, Int] = localVariables.getOrElseUpdate(m, mutable.Map())


  final def accept(visitors: ASTVisitor*): Unit = visitors foreach (ast accept)
  final def map(mappers: ASTMapper*): Unit = mappers foreach (mapper => ast = mapper mapCompilationUnit ast)

  object symbols {
    final def resolveVariable(name: Name)(implicit stmt: Statement): Option[SymbolEntry] = {
      resolve(name, NameSpace.Variable, stmt)
    }

    final def resolveMethod(name: Name)(implicit stmt: Statement): Option[SymbolEntry] = {
      resolve(name, NameSpace.Method, stmt)
    }

    private[this] def resolve(name: Name, nameSpace: NameSpace, stmt: Statement): Option[SymbolEntry] = {
      var table = symbolTables(stmt)
      var entry: Option[SymbolEntry] = None
      while (entry.isEmpty && table != null) {
        entry = table.resolve(name)(nameSpace)
        table = table.parent
      }
      entry
    }
  }

}

