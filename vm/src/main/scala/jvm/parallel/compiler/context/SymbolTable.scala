package jvm.parallel.compiler.context

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.statement.Statement
import jvm.parallel.ast.parsers.statement.modifers.Modifier
import jvm.parallel.vm2.TypeSignature

import scala.collection.mutable

class SymbolTable(val parent: SymbolTable = null, stmt: Statement) extends Symbols {

  private[this] type Key = (Name, NameSpace)
  private[this] type NameMap = mutable.Map[Key, SymbolEntry]

  val lookup: NameMap = mutable.Map()

  def add(name: Name,
          signature: TypeSignature,
          modifiers: Seq[Modifier],
          annotations: Seq[Annotation])(nameSpace: NameSpace): Unit = {
    val key = (name.last, nameSpace)

    if (lookup contains key) {
      throw new Exception("SymbolEntry already in table: " + key)
    }
    lookup += (key -> SymbolEntry(name, signature, modifiers, annotations))
  }

  def resolve(name: Name)(nameSpace: NameSpace): Option[SymbolEntry] = {
    if (name.name.size > 1) None
    else {
      val key= (name, nameSpace)
      if (lookup contains key)
        Some(lookup(key))
      else None
    }
  }
}


