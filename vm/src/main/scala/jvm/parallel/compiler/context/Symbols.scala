package jvm.parallel.compiler.context

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.statement.modifers.Modifier
import jvm.parallel.vm2.TypeSignature

trait Symbols {
  def add(name: Name,
          signature: TypeSignature,
          modifiers: Seq[Modifier],
          annotations: Seq[Annotation])(nameSpace: NameSpace) : Unit

  def resolve(name: Name)(nameSpace: NameSpace): Option[SymbolEntry]
}
