package jvm.parallel.translator

import jvm.parallel.ast.Visitable
import jvm.parallel.ast.parsers.ASTReducer

abstract class ASTPrinter[A <: Visitable[_]](val tabulators: Int) extends ASTReducer[A, String] with Printer[A] {
  override def print(a: A): String = reduce(a)

  protected[this] final implicit def toLazyEval[B](printer: Int => B): () => B = () => printer(tabulators)

  protected[this] final implicit class toLazyEval[B <: Visitable[_]](printer: Int => ASTReducer[B, String]) {
    def reduce(node: B): OUT = printer(tabulators).reduce(node)

    def reduce(nodes: Seq[B]): Seq[OUT] = nodes.reduceAST(printer)

    def reduce(nodes: Option[B]): Option[OUT] = nodes.map(printer.reduce)
  }

}