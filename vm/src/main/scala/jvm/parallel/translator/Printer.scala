package jvm.parallel.translator

trait Printer[A] {
  val tabulators: Int
  val builder: StringBuilder = new StringBuilder()

  def append(str: String) = builder.append(str)

  def print(a:A) :String

  protected[this] final def tabs(t:Int = tabulators) = "\t" * t
  protected[this] final def toLine(str: String): String = s"${"\t" * tabulators}$str"
}
