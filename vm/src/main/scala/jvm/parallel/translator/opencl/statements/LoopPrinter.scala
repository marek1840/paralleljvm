package jvm.parallel.translator.opencl.statements

import jvm.parallel.ast.parsers.statement.instructions.loop.{While, For, Loop}
import jvm.parallel.translator.ASTPrinter
import jvm.parallel.translator.opencl.OpenCLPrinters

class LoopPrinter(tabs: Int) extends ASTPrinter[Loop](tabs) {

  override def reduce(node: Loop): OUT = node match {
    case f: For => printForLoop(f)
    case w : While => printWhileLoop(w)
    case _ => super.reduce(node)
  }

  def printForLoop(value: For): OUT = {
    val inits = OpenCLPrinters.statements.reduce(value.initializers).mkString(", ")
    val condition = OpenCLPrinters.expressions.reduce(value.condition)
    val updates = OpenCLPrinters.statements.reduce(value.update).mkString(", ")
    val body = OpenCLPrinters.statements.reduce(value.body)

    if (condition.isDefined) s"for($inits; ${condition.get}; $updates)$body"
    else s"for($inits;; $updates)$body"
  }

  def printWhileLoop(value: While): OUT = {
    val condition = OpenCLPrinters.expressions.reduce(value.condition)
    val body = OpenCLPrinters.statements.reduce(value.body)

    s"while($condition)$body"
  }
}
