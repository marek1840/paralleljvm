package jvm.parallel.translator.opencl.statements

import jvm.parallel.ast.parsers.statement.Block
import jvm.parallel.translator.ASTPrinter
import jvm.parallel.translator.opencl.OpenCLPrinters

class BlockPrinter(tabulations: Int) extends ASTPrinter[Block](tabulations) {
  override def reduce(node: Block): OUT = {
    val statements = OpenCLPrinters.statements.reduce(node.statements).map {
      case s if s.endsWith("}") || s.endsWith(";") ||
        s.endsWith("\t") || s.isEmpty => toLine(s)
      case s => toLine(s + ";")
    }.mkString("\n")

    val begin = " {"
    val end = tabs(tabulations - 1) + "}"

    val stmt = s"$begin\n$statements\n$end"
    val r = stmt

    r
  }
}
