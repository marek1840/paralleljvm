package jvm.parallel.translator.opencl.statements

import jvm.parallel.ast.parsers.statement.instructions.flow.EmptyReturn
import jvm.parallel.ast.parsers.statement.instructions.{LocalVariableDeclaration, EmptyStatement}
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.MethodInvocation
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.assignment.{AugmentedBinding, Binding}
import jvm.parallel.ast.parsers.statement.instructions.discardable.unary.PreIncrementation
import jvm.parallel.ast.parsers.statement.instructions.loop.Loop
import jvm.parallel.ast.parsers.statement.{Block, Statement}
import jvm.parallel.translator.ASTPrinter
import jvm.parallel.translator.opencl.OpenCLPrinters

class StatementPrinter(tabs: Int) extends ASTPrinter[Statement](tabs) {

  override def reduce(node: Statement): OUT = node match {
    case b: Block => OpenCLPrinters.blocks(tabs + 1).reduce(b)
    case l: LocalVariableDeclaration => printLocalVarDeclaration(l)
    case l: Loop => OpenCLPrinters.loops.reduce(l)
    case b: Binding => printBinding(b)
    case b: AugmentedBinding => printAugmentedBinding(b)
    case i: PreIncrementation => printPreIncrementation(i)
    case i : MethodInvocation => OpenCLPrinters.methodInvocation.reduce(i)
    case EmptyStatement => toLine("")
    case EmptyReturn => "return"
    case _ => super.reduce(node)
  }

  def printPreIncrementation(inc: PreIncrementation): OUT = {
    val expr = OpenCLPrinters.expressions.reduce(inc.expression)

    s"++$expr"
  }

  def printAugmentedBinding(binding: AugmentedBinding): OUT = {
    val source = OpenCLPrinters.expressions.reduce(binding.source)
    val op = OpenCLPrinters.operators.reduce(binding.operator)
    val target = OpenCLPrinters.expressions.reduce(binding.target)

    s"$target $op= $source"
  }

  def printBinding(binding: Binding): OUT = {
    val source = OpenCLPrinters.expressions.reduce(binding.source)
    val target = OpenCLPrinters.expressions.reduce(binding.target)

    s"$target = $source"
  }

  def printLocalVarDeclaration(declaration: LocalVariableDeclaration): OUT = {
    val modifiers = OpenCLPrinters.modifiers.reduce(declaration.modifiers).mkString(" ")
    val typ = OpenCLPrinters.types.reduce(declaration.typ)
    val declarator = OpenCLPrinters.declarators.reduce(declaration.declarators).mkString(", ")

    if (modifiers.nonEmpty) s"$modifiers $typ $declarator"
    else s"$typ $declarator"

  }
}
