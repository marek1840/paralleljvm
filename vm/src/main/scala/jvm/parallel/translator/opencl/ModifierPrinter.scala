package jvm.parallel.translator.opencl

import jvm.parallel.ast.parsers.statement.modifers._
import jvm.parallel.translator.ASTPrinter

protected[opencl] class ModifierPrinter(tabs: Int) extends ASTPrinter[Modifier](tabs) {
  override def reduce(node: Modifier): OUT = node match {
    case Kernel => "__kernel"
    case Local => "__local"
    case Global => "__global"
    case Private => "__private"
    case Final => "const"

    case Parallel | Input |
         InputOutput | Output => ""

    case Public | Private | Protected => ""
    case _ => super.reduce(node)
  }
}
