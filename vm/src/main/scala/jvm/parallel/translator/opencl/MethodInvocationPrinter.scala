package jvm.parallel.translator.opencl

import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.MethodInvocation
import jvm.parallel.translator.ASTPrinter

protected[opencl] class MethodInvocationPrinter(tabs: Int) extends ASTPrinter[MethodInvocation](tabs) {
  override def reduce(invocation: MethodInvocation) = {
    val name = invocation.name
    val args = OpenCLPrinters.expressions.reduce(invocation.args).mkString(", ")

    s"$name($args)"
  }
}
