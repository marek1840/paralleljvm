package jvm.parallel.translator.opencl.expressions

import jvm.parallel.ast.parsers.expression.operators.BinaryOperator
import jvm.parallel.translator.ASTPrinter

class OperatorPrinter(tabs:Int) extends ASTPrinter[BinaryOperator](tabs){
  override def reduce(node: BinaryOperator): OUT = node match {
    case BinaryOperator.or => "or"
    case BinaryOperator.and => "and"
    case BinaryOperator.instanceof => "instanceof"
    case BinaryOperator.+ => "+"
    case BinaryOperator.- => "-"
    case BinaryOperator.* => "*"
    case BinaryOperator./ => "/"
    case BinaryOperator.** => "**"
    case BinaryOperator.^ => "^"
    case BinaryOperator.& => "&"
    case BinaryOperator.| => "|"
    case BinaryOperator.<< => "<<"
    case BinaryOperator.<<< => "<<<"
    case BinaryOperator.>> => ">>"
    case BinaryOperator.>>> => ">>>"
    case BinaryOperator.mod => "mod"
    case BinaryOperator.div => "div"
    case BinaryOperator.< => "<"
    case BinaryOperator.<= => "<="
    case BinaryOperator.> => ">"
    case BinaryOperator.>= => ">="
    case BinaryOperator.== => "=="
    case BinaryOperator.!= => "!="
    case BinaryOperator.in => "in"
    case BinaryOperator.notIn => "notIn"
    case BinaryOperator.is => "is"
    case BinaryOperator.isNot => "isNot"

    case _ => super.reduce(node)
  }
}
