package jvm.parallel.translator.opencl

import jvm.parallel.ast.parsers.expression.types.primitives.PrimitiveType
import jvm.parallel.ast.parsers.expression.types.references.ClassType
import jvm.parallel.ast.parsers.expression.types.{PointerType, Type}
import jvm.parallel.translator.ASTPrinter

protected[opencl] class TypePrinter(tabs: Int) extends ASTPrinter[Type](tabs){

  override def reduce(node: Type): OUT = node match{
    case t: PrimitiveType => OpenCLPrinters.primitiveTypes.reduce(t)
    case t: PointerType => printPointerType(t)
    case t : ClassType => t.name.representation

    case _ => super.reduce(node)
  }

  def printPointerType(pointerType: PointerType): OUT = {
    val typ = reduce(pointerType.typ)

    s"$typ*"
  }
}
