package jvm.parallel.translator.opencl

import jvm.parallel.translator.opencl.annotations.AnnotationPrinter
import jvm.parallel.translator.opencl.expressions._
import jvm.parallel.translator.opencl.statements._
import jvm.parallel.translator.opencl.types.PrimitiveTypePrinter

object OpenCLPrinters {
  def methodInvocation = (tabs: Int) => new MethodInvocationPrinter(tabs)
  def declarators= (tabs: Int) => new DeclaratorPrinter(tabs)
  def parameters  = (tabs: Int) => new ParametersPrinter(tabs)
  def types = (tabs: Int) => new TypePrinter(tabs)
  def primitiveTypes = (tabs: Int) => new PrimitiveTypePrinter(tabs)
  def modifiers = (tabs: Int) => new ModifierPrinter(tabs)
  def method= (tabs: Int) => new MethodPrinter(tabs)
  def annotations= (tabs: Int) => new AnnotationPrinter(tabs)
  def statements = (tabs:Int) => new StatementPrinter(tabs)
  def blocks = (tabs:Int) => new BlockPrinter(tabs)
  def expressions = (tabs:Int) => new ExpressionPrinter(tabs)
  def literals= (tabs:Int) => new LiteralPrinter(tabs)
  def loops= (tabs:Int) => new LoopPrinter(tabs)
  def operators= (tabs:Int) => new OperatorPrinter(tabs)
  def structureDefinition = (tabs : Int) => new StructDefinitionPrinter(tabs)
}
