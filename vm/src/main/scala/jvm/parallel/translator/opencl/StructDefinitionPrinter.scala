package jvm.parallel.translator.opencl

import jvm.parallel.ast.name.Name
import jvm.parallel.translator.Printer
import jvm.parallel.vm2.TypeSignature
import jvm.parallel.vm2.structs.{FieldName, StructureDefinition}

protected[opencl] class StructDefinitionPrinter(val tabulators: Int) extends Printer[StructureDefinition] {
  override def print(struct: StructureDefinition): String = {
    val fields = struct.types.map(printField).mkString("\n")

    s"""typedef struct {
        |$fields
        |} ${struct.name.name};
        |""".stripMargin
  }

  private[this] def printField(field: (FieldName, TypeSignature)) = {
    val typeSignature = printTypeSignature(field._2)
    val str = s"$typeSignature ${field._1.name};"
    toLine(str)
  }

  private[this] def printTypeSignature(typeSignature: TypeSignature): String = typeSignature match {
    case TypeSignature.Integer => "int"
    case TypeSignature.Float => "float"
    case TypeSignature.Type(name) => name.name.representation
  }
}
