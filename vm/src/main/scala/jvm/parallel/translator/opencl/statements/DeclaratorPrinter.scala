package jvm.parallel.translator.opencl.statements

import jvm.parallel.ast.parsers.statement.declarator.{Declarator, InitializedVariableDeclarator, MethodDeclarator, VariableDeclarator}
import jvm.parallel.translator.ASTPrinter
import jvm.parallel.translator.opencl.OpenCLPrinters

class DeclaratorPrinter(tabs: Int) extends ASTPrinter[Declarator](tabs) {

  protected[this] def printMethodDeclarator(declarator: MethodDeclarator): OUT = {
    val name = declarator.name.last
    val parameters = OpenCLPrinters.parameters.reduce(declarator.parameters).mkString(", ")

    s"$name($parameters)"
  }

  protected[this] def printVariableDeclarator(declarator: VariableDeclarator): OUT = {
    declarator.name.last.representation
  }

  protected[this] def printInitializedVariableDeclarator(declarator: InitializedVariableDeclarator): OUT = {
    val name= declarator.name
    val value = OpenCLPrinters.expressions.reduce(declarator.initializer)

    s"$name = $value"
  }

  override def reduce(node: Declarator): OUT = node match {
    case d: MethodDeclarator => printMethodDeclarator(d)
    case d: VariableDeclarator => printVariableDeclarator(d)
    case d: InitializedVariableDeclarator => printInitializedVariableDeclarator(d)

    case _ => super.reduce(node)
  }
}
