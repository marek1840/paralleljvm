package jvm.parallel.translator.opencl.annotations

import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.translator.ASTPrinter

class AnnotationPrinter(tabs: Int) extends ASTPrinter[Annotation](tabs){
}
