package jvm.parallel.translator.opencl.statements

import jvm.parallel.ast.parsers.statement.parameter.{FormalParameter, Parameter}
import jvm.parallel.translator.ASTPrinter
import jvm.parallel.translator.opencl.OpenCLPrinters

class ParametersPrinter(tabs:Int) extends ASTPrinter[Parameter](tabs) {
  override def reduce(node: Parameter): OUT = node match {
    case p: FormalParameter => printFormalParameter(p)

    case _ => super.reduce(node)
  }

  def printFormalParameter(param: FormalParameter): OUT = {
    val modifiers = OpenCLPrinters.modifiers.reduce(param.modifiers).mkString(" ")
    val typ = OpenCLPrinters.types.reduce(param.`type`)
    val name = param.name

    if (modifiers.isEmpty) s"$typ $name"
    else s"$modifiers $typ $name"
  }
}
