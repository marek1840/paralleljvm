package jvm.parallel.translator.opencl.expressions

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.{Extraction, FieldAccess, MethodInvocation}
import jvm.parallel.ast.parsers.statement.instructions.discardable.literals.Literal
import jvm.parallel.ast.parsers.statement.instructions.discardable.reference.{LocalVariableReference, Select}
import jvm.parallel.ast.parsers.expression.{BinaryOperations, Expression}
import jvm.parallel.ast.parsers.statement.instructions.discardable.instantiation.SimpleObjectInstantiation
import jvm.parallel.translator.ASTPrinter
import jvm.parallel.translator.opencl.OpenCLPrinters

class ExpressionPrinter(tabs: Int) extends ASTPrinter[Expression](tabs) {

  def createStruct(t: Type): OUT = s"${OpenCLPrinters.types(0).reduce(t)}()"

  override def reduce(node: Expression): OUT = node match {
    case l: Literal => OpenCLPrinters.literals.reduce(l)
    case i: MethodInvocation => OpenCLPrinters.methodInvocation.reduce(i)
    case Select(name) => name.representation
    case LocalVariableReference(name, i) => name.representation
    case bin: BinaryOperations => printBinaryOperations(bin)
    case e: Extraction => printExtraction(e)

    case SimpleObjectInstantiation(_, t, args)  if args.isEmpty => createStruct(t)

    case FieldAccess(parent, field) => printFieldAccess(parent, field)
    case _ => super.reduce(node)
  }

  def printFieldAccess(parentExpr: Expression, field: Name): OUT = {
    val parent= reduce(parentExpr)
    s"$parent.$field"
  }

  def printExtraction(extraction: Extraction): OUT = {
    val array = OpenCLPrinters.expressions.reduce(extraction.array)
    val key = OpenCLPrinters.expressions.reduce(extraction.key)

    s"$array[$key]"
  }

  def printBinaryOperations(binOp: BinaryOperations): OUT = {
    var operators = OpenCLPrinters.operators.reduce(binOp.operators)
    var operands = OpenCLPrinters.expressions.reduce(binOp.operands)

    builder.append(operands.head)

    while (operators.nonEmpty) {
      val op = operators.head
      val expr = operands.tail.head

      val s: String = s" $op $expr"
      builder.append(s)

      operators = operators.tail
      operands = operands.tail
    }

    val res = builder.toString()
    builder.toString()
  }

}
