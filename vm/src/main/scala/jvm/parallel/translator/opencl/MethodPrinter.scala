package jvm.parallel.translator.opencl

import jvm.parallel.ast.parsers.statement.declaration.members.MethodDeclaration
import jvm.parallel.translator.ASTPrinter

protected[opencl] class MethodPrinter(tabs: Int) extends ASTPrinter[MethodDeclaration](tabs) {

  override def reduce(node: MethodDeclaration): OUT = {
    val modifiers = OpenCLPrinters.modifiers.reduce(node.modifiers).mkString(" ")
    val resultType = OpenCLPrinters.types.reduce(node.resultType)
    val declarator = OpenCLPrinters.declarators.reduce(node.declarator)
    val body = OpenCLPrinters.statements.reduce(node.body)

    val res = s"$modifiers $resultType $declarator$body"
    res
  }

}
