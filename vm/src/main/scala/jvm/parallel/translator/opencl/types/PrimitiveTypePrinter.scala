package jvm.parallel.translator.opencl.types

import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.expression.types.primitives._
import jvm.parallel.translator.ASTPrinter
import jvm.parallel.translator.opencl.OpenCLPrinters

class PrimitiveTypePrinter(tabs: Int) extends ASTPrinter[PrimitiveType](tabs) {

  private[this] def primitiveType(annotations: Seq[Annotation], s: String): OUT = {
    if (annotations.isEmpty) s
    else {
      val as = annotations.reduceAST(OpenCLPrinters.annotations)
      s"$as $s"
    }
  }

  override def reduce(node: PrimitiveType): OUT = node match {
    case p: DoublePrimitive => primitiveType(p.annotations, "double")
    case p: BytePrimitive => primitiveType(p.annotations, "byte")
    case p: LongPrimitive => primitiveType(p.annotations, "long")
    case p: FloatPrimitive => primitiveType(p.annotations, "float")
    case p: CharPrimitive => primitiveType(p.annotations, "char")
    case p: ShortPrimitive => primitiveType(p.annotations, "short")
    case p: IntegerPrimitive => primitiveType(p.annotations, "int")
    case p: VoidPrimitive => primitiveType(p.annotations, "void")
    case p: BooleanPrimitive => primitiveType(p.annotations, "boolean")
  }
}
