package jvm.parallel.translator.opencl.expressions

import jvm.parallel.ast.parsers.statement.instructions.discardable.literals._
import jvm.parallel.translator.ASTPrinter

class LiteralPrinter(tabs: Int) extends ASTPrinter[Literal](tabs) {

  override def reduce(node: Literal): OUT = node match {
    case NullLiteral => "null"
    case l: ByteLiteral => l.value
    case l: LongLiteral => l.value
    case l: FloatLiteral => l.value
    case l: ShortLiteral => l.value
    case l: CharLiteral => l.value
    case l: DoubleLiteral => l.value
    case l: IntegerLiteral => l.value
    case l: BooleanLiteral => l.value
    case l: VoidLiteral => l.value
    case l: StringLiteral => "\"" + l.value + "\""

    case _ => super.reduce(node)
  }

}
