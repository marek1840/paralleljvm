package jvm.parallel.exceptions

import jvm.parallel.ast.parsers.ASTNode

class UnsupportedNodeException(value: ASTNode) extends RuntimeException(value.getClass.getSimpleName)