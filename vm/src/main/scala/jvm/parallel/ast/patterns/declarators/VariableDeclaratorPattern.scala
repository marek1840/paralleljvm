package jvm.parallel.ast.patterns.declarators

import jvm.parallel.ast.parsers.statement.declarator.VariableDeclarator
import jvm.parallel.ast.patterns.{Context, Pattern, NamePattern}

class VariableDeclaratorPattern(namePattern: NamePattern) extends Pattern[VariableDeclarator]{
  override def matches[B >: VariableDeclarator](value: B)(implicit context: Context): Boolean = value match {
    case VariableDeclarator(name) => namePattern matches name
    case _ => false
  }
}
