package jvm.parallel.ast

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.expression.operators.BinaryOperator
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.assignment.{AugmentedBinding, Binding}
import jvm.parallel.ast.parsers.statement.instructions.discardable.reference.Select

package object patterns {
  implicit def stringToPattern(str:String) : Pattern[Name] = new NamePattern(str)
  implicit def binOpToPattern(op:BinaryOperator) : Pattern[BinaryOperator] = new BinaryOperatorPattern(op)

  def any = AnyInput
  def select(namePattern: Pattern[Name]) = new SelectPattern(namePattern)
  def binding(expr1:Pattern[Expression], expr2:Pattern[Expression]) = new BindingPattern(expr1, expr2)
  def augmentedBinding(e1:Pattern[Expression],  op : Pattern[BinaryOperator], e2:Pattern[Expression]) =
    new AugmentedBindingPattern(e1, op, e2)


  def select: Pattern[Select] = select(any)
  def binding: Pattern[Binding] = binding(any, any)
  def augmentedBinding: Pattern[AugmentedBinding] = augmentedBinding(any, any, any)
}
