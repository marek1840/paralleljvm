package jvm.parallel.ast.patterns.declarators

import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.statement.declarator.{InitializedVariableDeclarator, VariableDeclarator}
import jvm.parallel.ast.patterns.{Context, Pattern, NamePattern}

class InitializedVariableDeclaratorPattern(namePattern: NamePattern,
                                           expressionPattern: Pattern[Expression])
  extends Pattern[InitializedVariableDeclaratorPattern] {

  override def matches[B >: InitializedVariableDeclaratorPattern](value: B)(implicit context: Context): Boolean =
    value match {
      case InitializedVariableDeclarator(name, expr) =>
        (namePattern matches name) && (expressionPattern matches expr)
      case _ => false
    }
}