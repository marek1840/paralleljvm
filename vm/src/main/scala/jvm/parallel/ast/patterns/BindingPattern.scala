package jvm.parallel.ast.patterns

import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.assignment.Binding

class BindingPattern(sourcePattern: Pattern[Expression], targetPattern: Pattern[Expression]) extends Pattern[Binding] {
  override def matches[A >: Binding](value: A)(implicit context: Context): Boolean = value match {
    case Binding(source, target) =>
      (sourcePattern matches source) && (targetPattern matches target)
    case _ => false
  }

}
