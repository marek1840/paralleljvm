package jvm.parallel.ast.patterns

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.statement.instructions.discardable.reference.Select

class SelectPattern(namePattern: Pattern[Name]) extends Pattern[Select] {
  override def matches[A >: Select](value: A)(implicit context: Context): Boolean = value match {
    case Select(name) => namePattern matches name
    case _ => false
  }
}
