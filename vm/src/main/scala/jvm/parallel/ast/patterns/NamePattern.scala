package jvm.parallel.ast.patterns

import jvm.parallel.ast.name.Name

class NamePattern(name:String) extends Pattern[Name] {
  override def matches[A >: Name](value: A)(implicit context: Context): Boolean = value match {
    case n : Name => n.representation == name
    case _ => false
  }
}
