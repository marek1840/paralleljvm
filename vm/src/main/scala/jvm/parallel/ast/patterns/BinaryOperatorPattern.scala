package jvm.parallel.ast.patterns

import jvm.parallel.ast.parsers.expression.operators.BinaryOperator


class BinaryOperatorPattern(op : BinaryOperator) extends Pattern[BinaryOperator]{
  override def matches[B >: BinaryOperator](value: B)(implicit context: Context): Boolean = value == op
}
