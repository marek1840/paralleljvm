package jvm.parallel.ast.patterns

import jvm.parallel.ast.parsers.expression.types.Type
import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.statement.declarator.Declarator
import jvm.parallel.ast.parsers.statement.instructions.LocalVariableDeclaration
import jvm.parallel.ast.parsers.statement.modifers.Modifier

class VariableDeclarationPattern(annotationsPattern: Pattern[Seq[Annotation]],
                                 modifiersPattern: Pattern[Seq[Modifier]],
                                 typePattern: Pattern[Type],
                                 declaratorsPattern: Pattern[Seq[Declarator]]) extends Pattern[LocalVariableDeclaration] {
  override def matches[B >: LocalVariableDeclaration](value: B)(implicit context: Context): Boolean = value match {
    case LocalVariableDeclaration(as, ms, t, ds) =>
      (annotationsPattern matches as) &&
        (modifiersPattern matches ms) &&
        (typePattern matches t) &&
        (declaratorsPattern matches ds)
    case _ => false
  }
}
