package jvm.parallel.ast.patterns

import jvm.parallel.ast.parsers.expression.Expression
import jvm.parallel.ast.parsers.expression.operators.BinaryOperator
import jvm.parallel.ast.parsers.statement.instructions.discardable.binary.assignment.{AugmentedBinding, Binding}

class AugmentedBindingPattern(sourcePattern: Pattern[Expression],
                              operatorPattern: Pattern[BinaryOperator],
                              targetPattern: Pattern[Expression]) extends Pattern[AugmentedBinding] {
  override def matches[A >: AugmentedBinding](value: A)(implicit context: Context): Boolean = value match {
    case AugmentedBinding(source, op, target) =>
      (sourcePattern matches source) &&
        (operatorPattern matches op) &&
        (targetPattern matches target)
    case _ => false
  }
}
