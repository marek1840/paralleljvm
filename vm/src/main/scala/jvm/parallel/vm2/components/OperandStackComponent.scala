package jvm.parallel.vm2.components

trait OperandStackComponent extends ComponentWithOperands {
  def push(operand: Operand): Unit
  def pop(): Operand
  def top(): Operand
  def stackIsEmpty: Boolean
  def operand(n:Int): Option[Operand]
}
