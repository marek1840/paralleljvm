package jvm.parallel.vm2.components

trait MemoryComponent extends ComponentWithOperands {
  type MemoryAddress
  type Operand

  def readFromMemory(address: MemoryAddress) : Operand
  def writeToMemory(address: MemoryAddress, operand: Operand) : Unit

  def readMemoryChunk(address: MemoryAddress, offset : MemoryAddress) : Array[Operand]
  def writeMemoryChunk(address: MemoryAddress, operands: Array[Operand]) : Unit

  def allocate(size: Int) : MemoryAddress
}
