package jvm.parallel.vm2.components

import jvm.parallel.ast.name.Name

trait LocalVariableComponent extends ComponentWithOperands{
  final type Key = Name

  def put(key: Key, value: Operand) : Unit
  def get(key: Key) : Operand
}
