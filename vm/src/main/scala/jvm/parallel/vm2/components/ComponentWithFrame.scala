package jvm.parallel.vm2.components

import jvm.parallel.vm2.CallFrame

private[parallel] trait ComponentWithFrame {
  type Frame <: CallFrame
}
