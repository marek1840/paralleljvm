package jvm.parallel.vm2.components

trait CallStackComponent extends ComponentWithFrame {
  def topFrame() : Frame
  def popFrame() : Frame
  def pushFrame(frame: Frame) : Unit
}
