package jvm.parallel.vm2.components

private[parallel] trait ComponentWithOperands {
  type Operand
}
