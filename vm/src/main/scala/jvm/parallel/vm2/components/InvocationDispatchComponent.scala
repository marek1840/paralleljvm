package jvm.parallel.vm2.components

import jvm.parallel.vm2.containers.CodeAreaContainer

trait InvocationDispatchComponent extends ComponentWithFrame {
  this: CodeAreaContainer =>

  type MethodKey = codeArea.MethodKey

  def resolveMethod(key: MethodKey): Frame
}
