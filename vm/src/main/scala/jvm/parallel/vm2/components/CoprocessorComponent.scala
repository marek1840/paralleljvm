package jvm.parallel.vm2.components

import jvm.parallel.ast.parsers.statement.declaration.members.MethodDeclaration

trait CoprocessorComponent extends ComponentWithFrame {
  type GPUExecutionUnit

  def compileExecutionUnit(ast: MethodDeclaration) : GPUExecutionUnit
  def execute(executionUnit: GPUExecutionUnit) : Unit
}
