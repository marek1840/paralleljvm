package jvm.parallel.vm2

trait CallFrame {
  type Operation
  //TODO def get(name:Name) : MemoryAddress
  def nextInstruction: Operation
  def jumpTo(ip: Int): Unit
}
