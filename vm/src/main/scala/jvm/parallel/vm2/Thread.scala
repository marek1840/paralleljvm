package jvm.parallel.vm2

import jvm.parallel.vm2.components.{CallStackComponent, InvocationDispatchComponent, MemoryComponent}
import jvm.parallel.vm2.containers.{CodeAreaContainer, StructAreaContainer}

trait Thread {
  this: MemoryComponent
    with CallStackComponent
    with InvocationDispatchComponent
    with StructAreaContainer
    with CodeAreaContainer =>

  type Operation = Frame#Operation

  def run(): Unit
}
