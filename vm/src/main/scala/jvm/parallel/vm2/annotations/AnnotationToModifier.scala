package jvm.parallel.vm2.annotations

import jvm.parallel.ast.parsers.expression.types.annotations.Annotation
import jvm.parallel.ast.parsers.statement.modifers._

object AnnotationToModifier {

  private[this] def modifier: PartialFunction[String, Modifier] = {
    case "jvm.parallel.annotations.Parallel" => Parallel
    case "jvm.parallel.annotations.In" => Input
    case "jvm.parallel.annotations.Out" => Output
    case "jvm.parallel.annotations.InOut" => InputOutput
    case "jvm.parallel.annotations.Local" => Local
    case "jvm.parallel.annotations.Global" => Global
  }

  def apply(as: Seq[Annotation]): Seq[Modifier] = {
    as.map(_.name.representation)
      .map(modifier)
      .distinct
  }
}
