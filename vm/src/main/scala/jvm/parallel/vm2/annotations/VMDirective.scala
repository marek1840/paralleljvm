package jvm.parallel.vm2.annotations

sealed trait VMDirective

case class MakeParallel(labels : Set[String])extends VMDirective
