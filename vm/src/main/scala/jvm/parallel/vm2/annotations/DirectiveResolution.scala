package jvm.parallel.vm2.annotations

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.ASTReducer
import jvm.parallel.ast.parsers.expression.types.annotations.{Annotation, NamedAnnotationParameter, NormalAnnotation}
import jvm.parallel.ast.parsers.expression.{ArrayInitializer, Expression}
import jvm.parallel.ast.parsers.statement.instructions.discardable.literals.StringLiteral

class DirectiveResolution extends ASTReducer[Annotation, Option[VMDirective]] {
  private[this] implicit def toOpt[A](a: A): Option[A] = Some(a)

  private[this] val Parallel = new Name("jvm.parallel.annotations.Parallel")
  private[this] val Labels = new Name("labels")

  override def reduce(node: Annotation): OUT = node match {
    case NormalAnnotation(Parallel, labels) =>
      val args = extractArgs(labels)
      MakeParallel(args(Labels) map extractLabel toSet)

    case _ => None
  }

  private[this] def extractLabel(expr: Expression): String = {
    expr match {
      case str:StringLiteral => str.withoutQuotes
      case _ => throw new RuntimeException("Expected String Literal. Got: " + expr)
    }
  }

  private[this] def extractArgs(params: Seq[NamedAnnotationParameter]) : Map[Name, Seq[Expression]] = {
    params map {
      case NamedAnnotationParameter(name, ArrayInitializer(exprs)) => (name, exprs)
      case NamedAnnotationParameter(name, expr) => (name, Seq(expr))
    } toMap
  }
}
