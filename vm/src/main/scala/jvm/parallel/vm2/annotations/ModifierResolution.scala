package jvm.parallel.vm2.annotations

import jvm.parallel.ast.name.Name
import jvm.parallel.ast.parsers.ASTReducer
import jvm.parallel.ast.parsers.expression.types.annotations.{Annotation, MarkerAnnotation}
import jvm.parallel.ast.parsers.statement.modifers._

class ModifierResolution extends ASTReducer[Annotation, Option[Modifier]] {
  val InName = new Name(Seq("jvm", "parallel", "annotations", "In"))
  val OutName = new Name(Seq("jvm", "parallel", "annotations", "Out"))
  val InOutName = new Name(Seq("jvm", "parallel", "annotations", "InOut"))
  val ParallelName = new Name(Seq("jvm", "parallel", "annotations", "Parallel"))
  val LocalName = new Name(Seq("jvm", "parallel", "annotations", "Local"))
  val GlobalName = new Name(Seq("jvm", "parallel", "annotations", "Global"))

  override def reduce(node: Annotation): OUT = node match {
    case MarkerAnnotation(ParallelName) => Some(Parallel)
    case MarkerAnnotation(InName) => Some(Input)
    case MarkerAnnotation(OutName) => Some(Output)
    case MarkerAnnotation(InOutName) => Some(InputOutput)
    case MarkerAnnotation(LocalName) => Some(Local)
    case MarkerAnnotation(GlobalName) => Some(Global)
    case _ => None
  }
}
