package jvm.parallel.vm2.containers

import jvm.parallel.vm2.StructureAreaComponent

trait StructAreaContainer {
  protected[this] val structureArea : StructureAreaComponent
}
