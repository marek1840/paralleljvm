package jvm.parallel.vm2.containers

import jvm.parallel.vm2.CodeAreaComponent

trait CodeAreaContainer {
  protected[this] val codeArea : CodeAreaComponent
}
