package jvm.parallel.vm2

trait StructureAreaComponent {
  type StructKey
  type StructDef

  def registerStructure(key : StructKey, definition: StructDef) : Unit
  def getStructure(key: StructKey) : StructDef
}
