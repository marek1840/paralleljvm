package jvm.parallel.vm2

import jvm.parallel.ast.name.TokenCache
import jvm.parallel.ast.parsers.expression.types.primitives.{FloatPrimitive, IntegerPrimitive, VoidPrimitive}
import jvm.parallel.ast.parsers.expression.types.references.{ArrayType, ClassType}
import jvm.parallel.ast.parsers.expression.types.{Type => ParsedType}
import jvm.parallel.vm2.structs.ClassName

trait TypeSignature

object TypeSignature {
  // TODO trait MetaType ex. pointer to className

  case object Integer extends TypeSignature
  case object Float extends TypeSignature
  case object Void extends TypeSignature
  case class Array(elementsType: TypeSignature, dimension: Int) extends TypeSignature
  case class Type(name: ClassName) extends TypeSignature

  case class Method(parameters: Seq[TypeSignature], resultType: TypeSignature) extends TypeSignature

  def apply(typ: ParsedType): TypeSignature = typ match {
    case IntegerPrimitive(_) => Integer
    case FloatPrimitive(_) => Float
    case VoidPrimitive(_) => Void

    case ArrayType(t, dims) => Array(apply(t), dims.size)
    case ClassType(_, _, name, _) => Type(ClassName(TokenCache.fromString(""), name)) //FIXME qualified name

    case _ => throw new Exception(s"Unsupported type: $typ")
  }

}