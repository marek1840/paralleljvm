package jvm.parallel.vm2.structs

import jvm.parallel.vm2.StructureAreaComponent

import scala.collection.mutable

class StructureStorage extends StructureAreaComponent {
  override type StructKey = ClassName
  override type StructDef = StructureDefinition

  private[this] val structs = mutable.Map[StructKey, StructDef]()

  override def getStructure(key: StructKey): StructDef = structs(key)

  override def registerStructure(key: StructKey, definition: StructDef): Unit = structs += (key -> definition)
}
