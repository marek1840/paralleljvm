package jvm.parallel.vm2.structs

import jvm.parallel.vm2.TypeSignature

//TODO only fields, drop offsets / types
class StructureDefinition(val name: ClassName, fieldSeq: Seq[(FieldName, TypeSignature)]) {
  val size: Int = fieldSeq.size + 1
  val types: Map[FieldName, TypeSignature] = fieldSeq.toMap
  /**
    * Point(x, y) -> Map(x -> 0, y -> 1)
    */
  val offsets: Map[FieldName, Int] = fieldSeq.zipWithIndex.map(tuple => (tuple._1._1, tuple._2 + 1)).toMap

  val fields: Map[FieldName, StructureDefinitionEntry] = types.map { case (n, t) =>
    n -> StructureDefinitionEntry(t, offsets(n))
  }

  def offset(field: FieldName): Int = fields(field).offset
  def typeOf(field: FieldName): TypeSignature = fields(field).typeSignature

  override def toString: String = {
    val fieldString = fields.map{case (fieldName, StructureDefinitionEntry(fieldType, _)) =>
        s"\t$fieldName: $fieldType"
      } mkString "\n"

    s"$name{\n$fieldString\n}"
  }
}
