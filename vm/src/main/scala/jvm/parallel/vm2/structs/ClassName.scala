package jvm.parallel.vm2.structs

import jvm.parallel.ast.name.Name

case class ClassName(packageName : Name, name : Name)
