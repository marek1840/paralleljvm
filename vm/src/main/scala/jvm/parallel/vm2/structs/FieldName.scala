package jvm.parallel.vm2.structs

import jvm.parallel.ast.name.Name

case class FieldName(className: ClassName, name: Name)
