package jvm.parallel.vm2.structs

import jvm.parallel.vm2.TypeSignature

case class StructureDefinitionEntry(typeSignature: TypeSignature, offset: Int)