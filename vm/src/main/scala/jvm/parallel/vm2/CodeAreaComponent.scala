package jvm.parallel.vm2

import jvm.parallel.ast.name.Name

trait CodeAreaComponent {
  type MethodKey = Name
  type MethodMetadata

  def registerMethod(key: MethodKey, metadata: MethodMetadata) : Unit
  def getMethod(key: MethodKey) : MethodMetadata
}
