package jvm.parallel;

import java.util.Arrays;

public class IntStack {
    private final int[] stack;
    private int top = -1;

    public IntStack(int size) {
        stack = new int[size];
    }

    public int pop() {
        return stack[top--];
    }

    public int top() {
        return stack[top];
    }

    public void push(int object) {
        stack[++top] = object;
    }

    public boolean isEmpty() {
        return top == -1;
    }

    public boolean nonEmpty(){return top >= 0;}

    @Override
    public String toString() {
        return Arrays.toString(Arrays.copyOfRange(stack, 0, top + 1));
    }
}
