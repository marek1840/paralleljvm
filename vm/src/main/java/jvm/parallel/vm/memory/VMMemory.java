package jvm.parallel.vm.memory;

public interface VMMemory {
    Integer allocate(Integer size);

    Integer getValueAt(Integer address);

    void setValueAt(Integer address, Integer value);

    Integer[] getChunk(int offset, int length);

    void setChunk(int offset, Integer[] chunk);

}
