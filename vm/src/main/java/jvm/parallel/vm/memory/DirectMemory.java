package jvm.parallel.vm.memory;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class DirectMemory implements VMMemory{
    private final IntBuffer memory;
    private int firstFreeSlot = 0;

    public DirectMemory(int size) {
        this.memory = ByteBuffer.allocateDirect(size * 4).asIntBuffer();
    }
    @Override
    public Integer allocate(Integer size) {
        int address = firstFreeSlot;
        firstFreeSlot += size;

        return address;
    }
    @Override

    public Integer getValueAt(Integer address) {
        return memory.get(address);
    }
    @Override
    public void setValueAt(Integer address, Integer value) {
        memory.put(address, value);
    }
    @Override
    public Integer[] getChunk(int offset, int length) {
        memory.position(offset);
        int[] chunk = new int[length];
        memory.get(chunk);


        return IntStream.of(chunk).boxed().toArray(Integer[]::new);
    }
    @Override
    public void setChunk(int offset, Integer[] chunk) {
        memory.position(offset);
        int[] memChunk = Stream.of(chunk).mapToInt(Integer::intValue).toArray();
        memory.put(memChunk);
    }

    @Override
    public String toString() {
        return Arrays.toString(getChunk(0, firstFreeSlot));
    }
}
