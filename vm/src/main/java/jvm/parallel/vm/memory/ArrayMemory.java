package jvm.parallel.vm.memory;

import java.util.Arrays;

public class ArrayMemory implements VMMemory {
    private final Integer[] memory;
    private int firstFreeSlot = 0;

    public ArrayMemory(int size) {
        this.memory = new Integer[size];
    }
    @Override
    public Integer allocate(Integer size) {
        if (memory.length - firstFreeSlot < size) {
            throw new RuntimeException("Out of memory");
        }

        int address = firstFreeSlot;
        firstFreeSlot += size;

        return address;
    }
    @Override
    public Integer getValueAt(Integer address) {
        return memory[address];
    }
    @Override
    public void setValueAt(Integer address, Integer value) {
        memory[address] = value;
    }
    @Override
    public Integer[] getChunk(int offset, int length) {
        Integer[] chunk = new Integer[length];
        System.arraycopy(memory, offset, chunk, 0, length);

        return chunk;
    }
    @Override
    public void setChunk(int offset, Integer[] chunk) {
        System.arraycopy(chunk, 0, memory, offset, chunk.length);
    }

    @Override

    public String toString() {
        return Arrays.toString(getChunk(0, firstFreeSlot));
    }
}
