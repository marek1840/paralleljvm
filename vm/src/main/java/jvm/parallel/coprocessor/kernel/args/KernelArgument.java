package jvm.parallel.coprocessor.kernel.args;

import com.nativelibs4java.opencl.CLContext;
import com.nativelibs4java.opencl.CLMem;

public abstract class KernelArgument<OUT> {
    protected final CLMem.Usage memoryUsage;

    public KernelArgument(CLMem.Usage memoryUsage) {
        this.memoryUsage = memoryUsage;
    }

    public KernelArgument() {
        this.memoryUsage = CLMem.Usage.Input;
    }

    public abstract OUT get(CLContext ctx);
}
