package jvm.parallel.coprocessor.kernel.args;

import com.nativelibs4java.opencl.CLMem;
import org.bridj.Pointer;
import org.bridj.StructObject;

public class StructArrayArg<T extends StructObject> extends ArrayKernelArgument<T>{
    public StructArrayArg(T[] array, CLMem.Usage usage) {
        super(usage, array);
    }

    @Override
    protected Pointer<T> createPointer(T[] objects) {
        return Pointer.<T>pointerToArray(objects);
    }
}
