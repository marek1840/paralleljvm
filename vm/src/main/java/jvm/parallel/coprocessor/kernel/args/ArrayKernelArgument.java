package jvm.parallel.coprocessor.kernel.args;

import com.nativelibs4java.opencl.CLBuffer;
import com.nativelibs4java.opencl.CLContext;
import com.nativelibs4java.opencl.CLMem;
import com.nativelibs4java.opencl.CLQueue;
import org.bridj.Pointer;

public abstract class ArrayKernelArgument<T> extends KernelArgument<CLBuffer<T>>{
    protected final Pointer<T> pointer;
    protected CLBuffer<T> buffer;

    public ArrayKernelArgument(CLMem.Usage memoryUsage, T[] array) {
        super(memoryUsage);
        pointer = createPointer(array);
    }

    protected abstract Pointer<T> createPointer(T[] objects);

    public final T[] getValue(CLContext ctx, CLQueue queue){
        Pointer<T> pointer = get(ctx).read(queue);
        return pointer.toArray();
    }

    @Override
    public final CLBuffer<T> get(CLContext ctx) {
        if(buffer == null)
            buffer =  ctx.createBuffer(memoryUsage, pointer);

        return buffer;
    }
}
