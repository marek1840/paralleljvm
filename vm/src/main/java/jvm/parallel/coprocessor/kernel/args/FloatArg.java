package jvm.parallel.coprocessor.kernel.args;

import com.nativelibs4java.opencl.CLContext;

public class FloatArg extends KernelArgument<Float> {
	private final Float arg;

	public FloatArg(Integer arg) {
		this.arg = Float.intBitsToFloat(arg);
	}

	@Override
	public Float get(CLContext ctx) {
		return arg;
	}

	@Override
	public String toString() {
		return "" + arg;
	}
}
