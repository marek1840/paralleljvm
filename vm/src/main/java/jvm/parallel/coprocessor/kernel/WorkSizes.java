package jvm.parallel.coprocessor.kernel;

public class WorkSizes {
    private final long[] sizes;

    public WorkSizes(long x, long y, long z) {
        sizes = new long[]{x, y, z};
    }

    public WorkSizes(long x, long y){
        sizes = new long[]{x, y};
    }

    public WorkSizes(long x){
        sizes = new long[]{x};
    }

    public long[] getSizes() {
        return sizes;
    }
}
