package jvm.parallel.coprocessor.kernel;

import java.util.ArrayList;
import java.util.List;

import org.bridj.StructObject;

import com.nativelibs4java.opencl.CLContext;
import com.nativelibs4java.opencl.CLMem;

import jvm.parallel.coprocessor.kernel.args.ArrayKernelArgument;
import jvm.parallel.coprocessor.kernel.args.FloatArg;
import jvm.parallel.coprocessor.kernel.args.IntegerArg;
import jvm.parallel.coprocessor.kernel.args.IntegerArrayArg;
import jvm.parallel.coprocessor.kernel.args.KernelArgument;
import jvm.parallel.coprocessor.kernel.args.StructArrayArg;

public class KernelArguments {
    private final List<KernelArgument<?>> args = new ArrayList<>();

    private final List<Integer> outputArgIndices = new ArrayList<>();
    private final WorkSizes globalWorkSizes;
    private int outputArgPointer = 0;
    private ArrayKernelArgument[] outputArgs;

    public KernelArguments(WorkSizes globalWorkSizes) {
        this.globalWorkSizes = globalWorkSizes;
    }

    public Object[] getArgs(CLContext ctx) {
        Object[] as = new Object[args.size()];

        for (int i = 0; i < args.size(); i++) {
            as[i] = args.get(i).get(ctx);
        }

        return as;
    }

    public ArrayKernelArgument[] getOutputArguments() {
        if (outputArgs != null) {
            return outputArgs;
        }

        ArrayKernelArgument[] as = new ArrayKernelArgument[outputArgIndices.size()];

        for (int i = 0; i < outputArgIndices.size(); ++i) {
            int argIndex = outputArgIndices.get(i);
            as[i] = (ArrayKernelArgument) args.get(argIndex);
        }

        return as;
    }

    public void addIntegerArg(Integer i) {
        IntegerArg arg = new IntegerArg(i);
        add(arg);
    }

    public void addFloatArg(Integer i) {
        FloatArg arg = new FloatArg(i);
        add(arg);
    }

    public void addIntegerArrayArgument(Integer[] array, CLMem.Usage usage) {
        IntegerArrayArg arg = new IntegerArrayArg(array, usage);

        addArray(usage, arg);
    }

    public <T extends StructObject> void addStructArrayArgument(T[] array, CLMem.Usage usage) {
        StructArrayArg<T> arg = new StructArrayArg<>(array, usage);

        addArray(usage, arg);
    }

    private void add(KernelArgument<?> arg) {
        args.add(arg);
    }

    private void addArray(CLMem.Usage usage, ArrayKernelArgument arg) {
        args.add(arg);

        if (usage != CLMem.Usage.Input) {
            outputArgIndices.add(args.size() - 1);
        }
    }

    public long[] getGlobalWorkSizes() {
        return globalWorkSizes.getSizes();
    }

    public long[] getLocalWorkSizes() {
        return null; // allows JavaCL to infer this value
    }

    public ArrayKernelArgument<?> getNextOutputArgument() {
        if (outputArgs == null) {
            outputArgs = getOutputArguments();
        }

        return outputArgs[outputArgPointer++];
    }
}
