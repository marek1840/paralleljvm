package jvm.parallel.coprocessor.kernel.args;

import com.nativelibs4java.opencl.CLMem;
import org.bridj.Pointer;

public class IntegerArrayArg extends ArrayKernelArgument<Integer> {
    public IntegerArrayArg(Integer[] array, CLMem.Usage usage) {
        super(usage, array);
    }

    @Override
    protected Pointer<Integer> createPointer(Integer[] array) {
        int[] ints = new int[array.length];
        for (int i = 0; i < array.length; ++ i)
            ints[i] = array[i];

        return Pointer.pointerToInts(ints);
    }
}
