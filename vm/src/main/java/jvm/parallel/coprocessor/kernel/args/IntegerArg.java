package jvm.parallel.coprocessor.kernel.args;

import com.nativelibs4java.opencl.CLContext;

public class IntegerArg extends KernelArgument<Integer> {
    private final Integer arg;

    public IntegerArg(Integer arg) {
        this.arg = arg;
    }

    @Override
    public Integer get(CLContext ctx) {
        return arg;
    }

	@Override
	public String toString() {
		return "" + arg;
	}
}
