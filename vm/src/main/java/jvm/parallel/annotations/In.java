package jvm.parallel.annotations;

public @interface In {
    String[] labels() default {};

}
