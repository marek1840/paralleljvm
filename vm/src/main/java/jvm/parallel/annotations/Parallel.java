package jvm.parallel.annotations;

public @interface Parallel {
    String[] labels() default {};

}
