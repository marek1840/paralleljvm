package jvm.parallel.annotations;

public @interface Out {
    String[] labels() default {};

}
