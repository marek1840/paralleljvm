package jvm.parallel;

import java.util.Arrays;
import java.util.Iterator;

public class Stack<T> implements Iterator<T> {
    private final Object[] stack;
    private int top = -1;

    public Stack(int size) {
        stack = new Object[size];
    }

    @SuppressWarnings(value = "unchecked")
    public T pop() {
        return (T) stack[top--];
    }

    @SuppressWarnings(value = "unchecked")
    public T top() {
        return (T) stack[top];
    }

    public void push(T object) {
        stack[++top] = object;
    }

    public boolean isEmpty() {
        return top == -1;
    }

    public boolean nonEmpty(){return top >= 0;}

    @Override
    public String toString() {
        return Arrays.toString(Arrays.copyOfRange(stack, 0, top + 1));
    }

    @Override
    public boolean hasNext() {
        return nonEmpty();
    }

    @Override
    public T next() {
        return pop();
    }
}
